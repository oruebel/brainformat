"""
Module containing a number of general helper classes for specification of
HDF5-based data formats. This module alos contains specific file format
implementations, e.g, the brainformat.
"""
__all__ = ['brainformat', 'base', 'spec', 'annotations']


def get_derived_formats():
    """
    Get the list of data format modules derived from the base module.
    This allows the base class to discover all format classes.
    """
    return ["brain.dataformat.brainformat", "brain.dataformat.annotations.collection"]
