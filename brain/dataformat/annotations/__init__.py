"""
Package specifying in-memory and in-file management of annotations
"""
__author__ = 'oruebel'
__all__ = ['annotation', 'collection', 'selection']
