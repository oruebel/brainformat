"""
Module with classes to specify a singles data annotations.
"""
from brain.dataformat.annotations.selection import DataSelection

class AnnotationProperties(dict):
    """
    Dictionary data structure for storing annotation properties
    """
    def __init__(self, *args, **kwargs):
        super(AnnotationProperties, self).__init__(*args, **kwargs)

    def __setitem__(self, key, value):
        """
        Overwrite the settings of keys and value to ensure that all keys are strings
        and that all values are built-in data types
        """
        if not(isinstance(key, str) or isinstance(key, unicode)):
            raise KeyError('Invalid key given. The key is not a string')
        if not(isinstance(value, str) or
               isinstance(value, unicode) or
               isinstance(value, int) or
               isinstance(value, float) or
               isinstance(value, long) or
               isinstance(value, complex) or
               isinstance(value, bool) or
               value is None):
            raise ValueError('Invalid property given')
        dict.__setitem__(self, key, value)


class Annotation(object):
    """
    Annotate a particular dataset or subset of data.

    :ivar annotation_type: The type of annotation, e.g., anatomy, event, or general feature.
                           See AnnotationCollection.annotation_type_indexes. Other user-defined types are
                           permitted but it is recommended to chose from the predefined
                           annotation_type_indexes if possible.
    :type annotation_type: str or unicode. Typically one of Annotation.annotation_type_indexes.
    :ivar description: The description of the annotation
    :type description: str or unicode
    :ivar data_selection: The data subset the annotation refers to.
    :type data_selection: DataSelection
    :ivar properties: Dictionary with additional properties of the annotation
    :type properties: AnnotationProperties. Alternatively a dict may be used where keys must be strings and values
        must be built-in types. In the later case, an AnnotationProperties object will be constructed and the
        dict will be copied.

    """
    def __init__(self, annotation_type, description, data_selection, properties=None):
        self.annotation_type = annotation_type
        self.description = description
        self.data_selection = data_selection
        if properties is not None and not isinstance(properties, AnnotationProperties):
            if isinstance(properties, dict):
                anno_probs = AnnotationProperties()
                for k, v in properties.iteritems():
                    anno_probs[k] = v
            else:
                raise ValueError("Annotation properties must be defined via the AnnotationProperties type")
        self.properties = properties if properties is not None else AnnotationProperties()

    def description_logical_operation(self, left_description, right_description, operation):
        """
        Apply a logical operation to the description of an annotation

        The default implementation returns the left_type if both types match. Otherwise the strings
        are merged with (left_description operation right_description) syntax. Overwrite in child
        classes to define custom behavior for how logical operations affect annotation descriptions.

        :param left_description: Left-hand side of the logical operation.
        :param right_description: Right-hand of the logical operation. Ignored when operation=="NOT".
        :param operation: One of "OR", "AND', "XOR", "NOT"
        :return: The new description
        """
        if left_description == right_description:
            return left_description
        else:
            return u"(" + unicode(left_description) + u" " + operation + u" " + right_description + u")"

    def type_logical_operation(self, left_type, right_type, operation):
        """
        Apply a logical operation to the description of an annotation

        The default implementation returns the left_type if both types match. Otherwise the strings
        are merged with (left_type operation right_type) syntax.  Overwrite in child classes to define custom
        behavior for how logical operations affect annotation types

        :param left_type: Left-hand side of the logical operation.
        :param right_type: Right-hand of the logical operation. Ignored when operation=="NOT".
        :param operation: One of "OR", "AND', "XOR", "NOT"
        :return: The new type
        """
        if left_type == right_type:
            return left_type
        else:
            return u"(" + unicode(left_type) + u" " + operation + u" " + right_type + u")"

    def properties_logical_operations(self, left_properties, right_properties, operation):
        """
        Apply a logical operation to the dictionary of additional properties of an annotation

        The default implementation:
         -- NOT: Set all properties ot NONE
         -- OR: Preserves all common properties, i.e., properties where both the key and value match. \
                Properties that are shared (i.e., same key but different values) are set to None. \
                Properties that appear only in one of the two dicts are preserved.
        -- AND: Only properties that appear in both annotations are preserved. Properties that are \
                shared (but with different values) are set to None. Properties that appear only in \
                one of the two dicts are set to None.
        Overwrite in child classes to define custom behavior for how logical operations affect annotation properties

        :param left_properties: Left-hand side of the logical operation. Dict with annotation properties.
        :param right_properties: Right-hand of the logical operation. Dict with annotation properties.
            Ignored when operation=="NOT"
        :param operation: One of "OR", "AND', "XOR", "NOT"
        :return: New properties dictionary.
        """
        unique_keys = set(left_properties.keys() + right_properties.keys()) # List of available keys
        out_dict = AnnotationProperties({key: None for key in unique_keys})
        if operation != "NOT":
            for key in unique_keys:
                in_left = key in left_properties
                in_right = key in right_properties
                # Common property available in both dicts
                if in_left and in_right:
                    out_dict = None if (left_properties[key] != right_properties[key]) else left_properties
                # Single-sided property
                elif operation == 'OR':
                    out_dict[key] = left_properties[key] if in_left else right_properties
                # And operation
                else:
                    out_dict[key] = None
        return out_dict

    def __logical_operations__(self, other, operation):
        """
        Internal helper function used to implement OR, AND, and XOR operations.

        See also __or__, ___and__, and __xor__.

        :param other: The second operand in the logical operation (the first operand is self).
        :param operation: String. One of 'OR', 'AND' or 'XOR'

        :returns: New Annotation object with the merged logical result
        """
        if operation not in ["OR", "AND", "XOR"]:
            raise ValueError("Invalid logical operation specified. " + operation)
        if isinstance(other, Annotation):
            new_description = self.description_logical_operation(left_description=self.description,
                                                                 right_description=other.description,
                                                                 operation=operation)
            new_annotation_type = self.type_logical_operation(left_type=self.annotation_type,
                                                              right_type=other.annotation_type,
                                                              operation=operation)
            new_properties = self.properties_logical_operations(left_properties=self.properties,
                                                                right_properties=self.properties,
                                                                operation=operation)
            if operation == "OR":
                new_selection = self.data_selection | other.data_selection
            elif operation == "AND":
                new_selection = self.data_selection & other.data_selection
            elif operation == "XOR":
                new_selection = self.data_selection ^ other.data_selection
            else:
                raise ValueError("Invalid logical operation specified. " + operation)
            return Annotation(annotation_type=new_annotation_type,
                              description=new_description,
                              data_selection=new_selection,
                              properties=new_properties)
        else:
            raise ValueError(str(operation) + "operations not supported for objects other than Annotations")

    def __or__(self, other):
        """
        Implements the A | B logical operation.

        :returns: New Annotation object with the merged logical result
        """
        return self.__logical_operations__(other=other,
                                           operation='OR')

    def __and__(self, other):
        """
        Implements the A & B logical operation.

        :returns: New Annotation object with the merged logical result
        """
        return self.__logical_operations__(other=other,
                                           operation='AND')

    def __xor__(self, other):
        """
        Implements the A ^ B logical operation.

        :returns: New Annotation object with the merged logical result
        """
        return self.__logical_operations__(other=other,
                                           operation='XOR')

    def __invert__(self):
        return Annotation(annotation_type=self.annotation_type,
                          description='NOT '+self.description,
                          data_selection=~self.data_selection)

    def __rshift__(self, other):
        """
        Check whether the data selection of the annotation
        follows the selection of the other annotation. Same
        as self.data_selection >> other.data_selection.
        See :func:`DataSelection.__rshift__` for details.
        """
        if isinstance(other, DataSelection):
            return self.data_selection >> other
        elif isinstance(other, Annotation):
            return self.data_selection >> other.data_selection
        else:
            raise ValueError('>> operation not supported for objects other than Annotation and DataAnnotation')

    def __lshift__(self, other):
        """
        Check whether the data selection of the annotation
        precedes the selection of the other annotation. Same
        as self.data_selection >> other.data_selection.
        See :func:`DataSelection.__lshift__` for details.
        """
        if isinstance(other, DataSelection):
            return self.data_selection << other
        elif isinstance(other, Annotation):
            return self.data_selection << other.data_selection
        else:
            raise ValueError('<< operation not supported for objects other than Annotation and DataAnnotation')

    def __lt__(self, other):
        """
        Less than operator (<)
        """
        return self.data_selection < other.data_selection

    def __le__(self, other):
        """
        Less than or equals operator (<=)
        """
        return self.data_selection <= other.data_selection

    def __eq__(self, other):
        """
        Equals operator (==)
        """
        return self.data_selection == other.data_selection

    def __ne__(self, other):
        """
        Not equals operator (!=)
        """
        return self.data_selection != other.data_selection

    def __gt__(self, other):
        """
        Greater than operator (>)
        """
        return self.data_selection > other.data_selection

    def __ge__(self, other):
        """
        Greater than or equals operator (>=)
        """
        return self.data_selection >= other.data_selection

    def __contains__(self, other):
        """
        Check whether a data_selection is a subset of the other data_selection.
        """
        return other.data_selection in self.data_selection

    def __len__(self):
        """
        Get number of selected elements.
        """
        return len(self.data_selection)

    def data(self):
        """
        Load the data associated with this DataSelection.
        """
        return self.data_selection.data()
