"""
Module with classes to specify and interact with collections of data annotations.

"""
import numpy as np
import h5py

from brain.dataformat.base import ManagedGroup, ManagedObject, RelationshipAttribute
from brain.dataformat.annotations.annotation import Annotation
from brain.dataformat.annotations.selection import DataSelection
from brain.dataformat.spec import BaseSpec, RelationshipSpec, RelationshipTargetSpec
import warnings
import filecmp

class AnnotationCollection(object):
    """
    A collection of annotations

    **Instance Variables**

    :ivar data_object: h5py.Dataset, managed object, or numpy array with the data
    :ivar annotation_types:  1D (vlen str) Dataset with the list of all available types
    :type annotation_types:  h5py.Dataset,  numpy array, or list. The dataset is 1D and of type string.
    :ivar annotation_type_indexes: 1D h5py.Dataset or numpy array with the index into the annotation_types array
                                   indicating for each annotation the corresponding annotation type.
    :ivar descriptions: List of strings with the annotation descriptions
    :type descriptions:  h5py.Dataset,  numpy array, or list. The dataset is 1D and of type string.
    :ivar selection_indexes: h5py.Dataset or numpy array with the indexes of the selections that apply. This is a
                             2D dataset of shape (#annotations, #axes+1). The axes dimension is ordered as [-1,0,1,...].
    :ivar selections:  dict of h5py.Datasets or numpy arrays with all possible selections
    :ivar collection_description: String with a description of the annotation collection

    **Filtering**

    Filtering provides means to locate annotations of interest. Filtering is performed via the
    provided set of filter functions, including:

       * `index_filter(..)` : Select all annotations with the given index.
       * `axis_filter(..)` : Find all annotations that select by a given set of axes
       * `type_filter(..)` : Find all annotations of the given type
       * `description_filer(..)` : Find all annotations with the given description
       * `description_contains_filter(..)` : Find all annotations where the description contains the given string

    Filter functions generate a 1D bool-type numpy array, indicating which annotations are selected by the filter.
    As bool arrays, results of filters may be combined using bitwise logical operations, e.g.,

        * `a & b` : `AND` : Select all annotations selected by both filters `a` and `b`
        * `a | b` : `OR` : Select all annotations that are selected by either filter `a` or `b`
        * `a ^ b` `XOR` : Exclusive or, select all annotations where the filters `a` and `b` differ
        * `~a` : `NOT` : Invert the selection of the filter, selecting all annotations not selected by `a`

    **Selecting:**

    Once we have identified a set of relevant annotations via filtering one can select
    the annotations of interest directly using standard array slicing. E.g,:

    >>> a = AnnotationCollection(..)
    >>> f = a.type_filter('event') & a.axis_filter([0])
    >>> v = a[f]

    The result of selecting annotations is reduced AnnotationCollection object.
    In addition to filter, one can also select annotations directly using
    standard data selection/slicing, e.g, `a[0:10]` to select the first 10 annotations.

    NOTE: When sub-selection is performed, all data relevant to the selected annotations
    will be loaded into memory, whereas when an AnnotationCollection is constructed
    initially it may be initialized using h5py.Dataset objects where the data resides
    in file and is loaded by the filters as needed.

    **Getting the Annotations**

    From the AnnotationCollection we can retrieve a list of all selected Annotation objects
    via the `get_annotations()` functions. This will convert the annotations from
    the collective data structures used for filtering to individual Annotation objects.
    This is typically done after the filtering is complete.

    **Merging Annotations**

    Once we have selected and retrieved the Annotation objects of interest, the
    individual Annotations may be combined using standard bitwise logical operators
    and compared using standard comparison operators (see the documentation of the
    `Annotation` class for further details. As convenience functions, the
    `AnnotationCollection` class provides a set of merge functions which will
    generate a single combined `Annotation` by merging all the selected
    annotations using a given bitwise logical operation, e.g, `merge_and(..)`,
    `merge_or`, and `merge_xor`

    **Other Operations**

        * `len` : Get the number of annotations in the collection using the standartd Python `len(a)`.

    **Example**

    >>> a = AnnotationDataGroup(...)  # Load annotation collection from file
    >>> f = a.type_filter('event') & a.axis_filter([0])  # Find all annotations that define an event on the axis 0
    >>> s = a[f]  # Select all relevant annotations
    >>> s_all = s.get_annotations()  # Get all annotations
    >>> s1 = a.merge_or()  # Define a single combined annotation

    """
    # @classmethod
    # def create_collection_from_list(cls, data_object, annotation_list, collection_description=''):
    #     """
    #     Create a new AnnotationCollection from a list of annotations.
    #
    #     :param data_object: The data object that should be annotated.
    #     :param annotation_list: The list of annotations that should be compiled into an collection of annotations.
    #         NOTE: All annotations must be applicable to the data_object given as input to this function.
    #     :param collection_description: String with a description of the annotation collection
    #
    #     :return: A new AnnotationCollection object that combines the annotations in a single collection.
    #     """
    #     all_annotations = annotation_list
    #     num_annotations = len(all_annotations)
    #     axis_list = [-1] + [index for index in range(len(data_object.shape))]
    #
    #     # Create the annotation types, type indexes, and descriptions
    #     annotation_types = []
    #     annotation_type_indexes = np.zeros(num_annotations, dtype='uint')
    #     descriptions = [None] * num_annotations
    #     for anno_index, anno in enumerate(all_annotations):
    #         try:
    #             type_index = annotation_types.index(anno.annotation_type)
    #         except ValueError:
    #             type_index = len(annotation_types)
    #             annotation_types.append(anno.annotation_type)
    #         annotation_type_indexes[anno_index] = type_index
    #         descriptions[anno_index] = anno.description
    #
    #     # Compile a list of all data selections and calculate how many selections we have for the different axes
    #     all_data_selections = [anno.data_selection for anno in all_annotations]
    #     num_selections_per_axis = np.zeros(len(axis_list))
    #     for data_selection in all_data_selections:
    #         for axis_index in data_selection.selections.keys():
    #             num_selections_per_axis[axis_index+1] += 1
    #
    #     # Initialize the dictionary of selections and selection indexes
    #     selections = {}
    #     selection_indexes = np.zeros(len(axis_list)*num_annotations).reshape(num_annotations, len(axis_list))-1
    #     for axis in axis_list:
    #         if axis >= 0:
    #             selection_shape = (num_selections_per_axis[axis+1], data_object.shape[axis])
    #             selections[axis] = np.zeros(selection_shape, dtype='bool')
    #         else:
    #             selection_shape = (0,) + data_object.shape
    #             selections[axis] = np.zeros(selection_shape, dtype='bool')
    #
    #     # Populate the selections and selection indexes with the expected data
    #     current_selection_index = np.zeros(len(axis_list))
    #     for sel_index, curr_sel in enumerate(all_data_selections):
    #         for axis_index, axis_selection in curr_sel.selections.iteritems():
    #             u_axis_index = axis_index+1
    #             selections[axis_index][current_selection_index[u_axis_index], :] = axis_selection
    #             selection_indexes[sel_index, u_axis_index] = current_selection_index[u_axis_index]
    #             current_selection_index[u_axis_index] += 1
    #
    #     # TODO: Compile properties
    #
    #     anno_collection = AnnotationCollection(data=data_object,
    #                                            annotation_type_indexes=annotation_type_indexes,
    #                                            annotation_types=annotation_types,
    #                                            descriptions=descriptions,
    #                                            selection_indexes=selection_indexes,
    #                                            selections=selections,
    #                                            collection_description=collection_description,
    #                                            properties=None)
    #
    #     return anno_collection
    #


    def __init__(self,
                 data,
                 annotation_type_indexes=None,
                 annotation_types=None,
                 descriptions=None,
                 selection_indexes=None,
                 selections=None,
                 collection_description='',
                 properties=None):
        self.data_object = data  # h5py dataset, managed object, or numpy array with the data
        self.annotation_type_indexes = annotation_type_indexes  # hpy dataset/numpy array with the annotation type index
        self.annotation_types = annotation_types  # h5py dataset of numpy array with a list of all available types
        self.descriptions = descriptions  # h5py dataset or numpy array with the annotation descriptions
        self.selection_indexes = selection_indexes  # h5py.Dataset or np.array with the indexes of the selections used
        self.selections = selections  # dict of h5py datasets or numpy arrays with the selections
        self.collection_description = collection_description  # String with the description of the collections
        self.properties = properties  # Dictionary of 1D numpy arrays used to store properties of the annotations
        if self.properties is None:
            self.properties = {}

    def __getitem__(self, selection):
        """
        Get a new AnnotationCollection for the subset of annotations selected.

        NOTE: While most other variables are sub-selects (and loaded into memory) the self.selections
        collection of selections remains unmodified. This strategy i) allows us to keep the selections
        out-of-core in the HDF5 file, and ii) avoids complex updates of references to the selections.
        """
        # If the selection is a single index, then convert it to a slice. This is to prevent
        # the selected_selection_indexes array to be reduced in dimension during subsetting
        # which can lead to problems later on as the created AnnotationCollection would not
        # look as expected.
        if isinstance(selection, int):
            selection = slice(selection, selection+1, None)

        if self.num_annotations() > 0:
            selected_annotation_type_indexes = self.annotation_type_indexes[selection]
            selected_descriptions = self.descriptions[selection]
            selected_selection_indexes = self.selection_indexes[selection, :]
            selected_selections = self.selections

            return AnnotationCollection(data=self.data_object,
                                        annotation_types=self.annotation_types,
                                        annotation_type_indexes=selected_annotation_type_indexes,
                                        descriptions=selected_descriptions,
                                        selection_indexes=selected_selection_indexes,
                                        selections=selected_selections,
                                        collection_description=self.collection_description + " (subset)")
        else:
            return AnnotationCollection(data=self.data_object,
                                        collection_description=self.collection_description + " (subset)")

    def __len__(self):
        """
        Get the number of annotations in the collection.
        """
        return self.num_annotations()

    def save(self,
             parent_object,
             object_id=None,
             force_creation=False,
             external=False,
             enable_compression=True):

        """
        This is convenient helper function to ease saving of AnnotationCollection.
        The function essentially just calls AnnotationDataGroup.create(...) for the AnnotationCollection
        with the given additional parameters


        :param parent_object: The h5py.Group, h5py.File parent object in which the managed object should be created.
                              or the filename in case that the managed object is a file that needs to be created.
                              This may also be a ManagedObject for which the corresponding h5py object
                              will be used to construct the object.
        :type parent_object: h5py.Group or String
        :param object_id: The object id to be used. This should be a unique identifier to allow users to
                          understand the origin of the data and to relate data with each other.
        :param force_creation: Force the creation of the managed object as part of the parent object, even if the
                       parent object does not specify that the managed object to be created is part of its
                       specification. (Default value: False)
        :type force_creation: bool
        :param external: Boolean indicating whether the ManagedObject should be created in an external
                       file and linked to from the parent object (True) or whether the object should be
                       created within the parent directly (False). Default value is False, i.e, the object
                       is stored as part of the parent directly without creating a new file.
                       **NOTE** The external option has not effect when the the object to be created is a file,
                       i.e,  `get_managed_object_type(...)` returns file. The behavior in this case is as follows:
                       **1)** If the object to be created is `file` and the `parent_object` is a `string`, then the file
                       will be created as usual and the value of the external option has no effect.
                       **2)** If the object to be created is a `file` and the `parent_object` is a `h5py.Group`, then
                       the file will be created externally using a filename automatically determined based on the
                       name of the parent and the format specification and an external link to the root group of the
                       new file will be created within the given parent, i.e, the behavior is as if external is True.
        :type external: bool
        :param enable_compression: Should we use compression when storing annotation results (default is True)
        :type enable_compression: bool


        :return: AnnotationDataGroup object created for the annotations object.
        """
        if self.num_annotations() > 0:
            anno_collection = self
        else:
            anno_collection = None
        return AnnotationDataGroup.create(parent_object=parent_object,
                                          object_id=object_id,
                                          force_creation=force_creation,
                                          external=external,
                                          annotation_collection=anno_collection,
                                          enable_compression=enable_compression)

    def get_unique_descriptions(self):
        """
        Get a list of unique descriptions used by the annotations in the collection.

        :returns: Numpy array of the sorted, unique description values
        """
        if self.descriptions is not None:
            return np.unique(self.descriptions[:])
        else:
            return np.unique([])

    def filter(self, filter_type, *args, **kwargs):
        """
        Generic filter function.

        This function can be used to exercise the specific filter functions by providing the name of the
        filter type rather than having to change the function. This is mainly a convenience function to
        allow filtering of annotations via a single function.

        :param filter_type: The type of filter to be applied. One of 'index', 'axis', 'type', 'type_contains',
             'description', 'description_contains', 'property', 'property_contains'. See get_filter_types(...)
             for details.
        :param args: Additional positional arguments for the filter functions
        :param kwargs: Additional keyword arguments for the the filter functions

        :raises: ValueError is raised in case that an invalid filter_type is given.

        :return: Bool array indicating for each annotation whether it has been selected (True) or not (False)
        """
        filter_functions = self.get_filter_types()
        if filter_type not in filter_functions:
            raise ValueError('Invalid filter type given')
        return filter_functions[filter_type](*args, **kwargs)

    def get_filter_types(self):
        """
        Get all filter types

        :return: Python dictionary where the keys are the names of the available filters and the values
                 are the corresponding functions for self.
        """
        return {'index': self.index_filter,
                'axis': self.axis_filter,
                'type': self.type_filter,
                'type_contains': self.type_contains_filter,
                'description': self.description_filter,
                'description_contains': self.description_contains_filter,
                'property': self.property_filter,
                'property_contains': self.property_contains_filter}

    def index_filter(self, selection):
        """
        Filter annotations based on their index.

        :param selection: Any valid selection supported by numpy, e.g., a slice object
               or an integer index. E.g., to select annotation 200 to 300 we could do
               `index_filter(slice(200,301)`. NOTE: Following standard numpy selection
               schema the upper bound is not included in this example.

        :returns: Bool array indicating for each annotation whether it has been selected (True) or not (False)

        """
        if self.num_annotations() > 0:
            relevant_selections = np.zeros((self.num_annotations()), dtype='bool')
            relevant_selections[selection] = True
            return relevant_selections
        else:
            return np.zeros((self.num_annotations(),), dtype='bool')

    def axis_filter(self, axis_list):
        """
        Get all annotations that filter by a given set of axes.

        :param axis_list: List of all axis relevant selections must filter by. Only selections
               that select by all specified axes will be retrieved. The axis may be specified
               by their integer index (recommended, -1,0,1, ... n) or by the name of the
               dimension in the data_object.

        Examples:

        >>> from brain.dataformat.brainformat.braindata import *
        >>> f= BrainDataFile('testfile_real.h5')
        >>> d = f.data().internal().ephys_data(0)
        >>> d.annotations()
        >>> a = d.annotations(0)
        >>> a.axis_filter(0)  # Filter based on a single axis index
        >>> a.axis_filter('space')  # Filter based on a single axis index using the name of the axis
        >>> a.axis_filter(['space', 1])  # Filter based on multiple axis. We here can mix axis names and indexes

        NOTE: Global selections are treated independently and are
        as such if the filter asks for axis=1 selections with
        axis=-1 (global) will not be included.

        :returns: Bool array indicating for each annotation whether it has been selected (True) or not (False)
        """
        if self.selection_indexes is not None:

            # TODO: Add option to allow -1 global selection to be resolved. See also DataSelection.axes -- restricted_only
            relevant_selections = None
            if isinstance(axis_list, int) or isinstance(axis_list, basestring):
                axis_list = [axis_list]
            # Convert any explict axis names to indicies
            try:
                dim_labels = [dim.label for dim in self.data_object.dims]
            except AttributeError:
                dim_labels = []
            if len(dim_labels) > 0:
                for i, axis in enumerate(axis_list):
                    if isinstance(axis, basestring):
                        if axis in dim_labels:
                            axis_list[i] = dim_labels.index(axis)
                        else:
                            raise ValueError("Index for axis unknown: " + str(axis))
            else:
                for axis in axis_list:
                    if isinstance(axis, basestring):
                        raise ValueError("Index for axis unknown: " + str(axis))

            # Find all selection that select by all given axes
            for axis in axis_list:
                axis_index = axis+1  # Correct the axis index as axes ordered as [-1, 0 , 1, 2, ...]
                if relevant_selections is None:
                    relevant_selections = (self.selection_indexes[:, axis_index] >= 0)
                else:
                    relevant_selections &= (self.selection_indexes[:, axis_index] >= 0)
            if relevant_selections is None:
                relevant_selections = np.zeros((self.num_annotations()), dtype='bool')
            return relevant_selections
        else:
            return np.zeros((self.num_annotations(),), dtype='bool')

    def type_filter(self, annotation_type):
        """
        Get all annotations with the given type.

        :param annotation_type: Either the string of the annotation type or integer
                                 with the index of the annotation type.

        :returns: Bool array indicating for each annotation whether it has been selected (True) or not (False)
        """
        # If the annotation type is a string then look up the index of the annotation
        if self.annotation_types is not None and self.annotation_type_indexes is not None:
            if isinstance(annotation_type, basestring):
                find_index = np.where(self.annotation_types[:] == annotation_type)[0]
                if len(find_index) == 0:
                    annotation_type_index = -1  # Set to unknown type as all indexes will be positive
                else:
                    annotation_type_index = find_index[0]
            else:
                annotation_type_index = annotation_type
            return self.annotation_type_indexes[:] == annotation_type_index
        else:
            return np.zeros((self.num_annotations(),), dtype='bool')

    def type_contains_filter(self, annotation_type):
        """
        Get all annotations where the annotation type contains the given string.

        :param annotation_type: String with annotation type substring to look for

        :returns: Bool array indicating for each annotation whether it has been selected (True) or not (False)
        """
        # In HDF5, data in VL format is stored as arbitrary-length vectors of a base type. In particular, strings
        # are stored C-style in null-terminated buffers. NumPy has no native mechanism to support this.
        # Since numpy does not support this natively, vlen strings are stored as a generic pointer type in the
        # form of the 'object' ('O'). We here need to convert the annotation_type array first to ensure we can
        # apply the char_find methods.
        if self.annotation_types is not None and self.annotation_type_indexes is not None:
            curr_annotation_types = self.annotation_types[:]
            if self.annotation_types.dtype.kind == 'O':  # We a numpy array of type object
                new_type = '|S' + str(self.annotation_types.dtype.itemsize)
                curr_annotation_types = curr_annotation_types.astype(new_type)

            matching_types = np.where(np.char.find(curr_annotation_types, annotation_type) != -1)[0]
            if matching_types.size == 0:
                return np.zeros(shape=(self.annotation_type_indexes.shape[0],), dtype='bool')
            else:
                matching_annotations = self.annotation_type_indexes[:][..., None] == matching_types[None, ...]
                return np.max(matching_annotations, axis=1)
        else:
            return np.zeros((self.num_annotations(),), dtype='bool')

    def description_filter(self, description):
        """
        Get all annotations with the given description.

        :param description: String of the description to be located.

        :returns: Bool array indicating for each annotation whether it has been selected (True) or not (False)

        """
        if self.descriptions is not None:
            return self.descriptions[:] == description
        else:
            return np.zeros((0,), dtype='bool')

    def description_contains_filter(self, description):
        """
        Get all annotations for which the description contains the given text

        :param description: String of the partial description to be found.

        :returns: Bool array indicating for each annotation whether it has been selected (True) or not (False)

        """
        if self.descriptions is not None:
            curr_descriptions = self.descriptions[:]
            # vlen strings are loaded as object arrays by h5py
            if self.descriptions.dtype.kind == 'O':  # We have a numpy object type array instead of strings
                new_type = '|S' + str(self.descriptions.dtype.itemsize)
                curr_descriptions = curr_descriptions.astype(new_type)
            return np.char.find(curr_descriptions, description) != -1
        else:
            return np.zeros((self.num_annotations(),), dtype='bool')

    def property_filter(self, key, value):
        """
        Get all annotation for which the given property has the given value

        :param key: The string key for the property to be used
        :param value: The value of the property to compare with

        :return: Bool array indicating for each annotation whether it has been selected (True) or not (False)
        """
        key_available = False if (self.properties is None) else (key in self.properties)
        if key_available:
            return self.properties[key][:] == value
        else:
            return np.zeros((self.num_annotations(),), dtype='bool')

    def property_contains_filter(self, key, value):
        """
        Get all annotations for which the property contains a given text.
        Non-string properties are handled by converting them to string first,
        but generally this query makes often only sense on string properties.

        :param key: The string key for the property to be used
        :param value: The value of the property to compare with

        :return: Bool array indicating for each annotation whether it has been selected (True) or not (False)
        """
        key_available = False if (self.properties is None) else (key in self.properties)
        if key_available:
            property_array = self.properties[key][:]
            # Convert the property array to chars
            if property_array.dtype.kind == 'O':  # We have a numpy object type array instead of strings
                new_type = '|S' + str(property_array.dtype.itemsize)
                curr_descriptions = property_array.astype(new_type)
            elif property_array.dtype.kind != 'S':
                property_array = np.char.mod('%d', property_array)
            return np.char.find(property_array, value) != -1
        else:
            return np.zeros((self.num_annotations(),), dtype='bool')

    def num_annotations(self):
        """
        Get the number of annotations in the collection.
        """
        if self.annotation_type_indexes is not None:
            return self.annotation_type_indexes.size
        else:
            return 0

    def get_annotations(self):
        """
        Get a list of all annotations.

        :returns: List of Annotation objects

        """
        annotation_list = []
        axis_list = [index for index in range(len(self.data_object.shape))] + [-1]  # List of axes
        # 1) Iterate through all annotations and construct the lit of Annotation objects
        for anno_index in range(self.num_annotations()):
            selection_ref = self.selection_indexes[anno_index, :].reshape(len(axis_list))  # Current selection refs
            anno_type = self.annotation_types[self.annotation_type_indexes[anno_index]]  # Current annotation type
            anno_description = self.descriptions[anno_index]  # Current annotation description
            if self.properties is not None:
                anno_properties = {key: self.properties[key][anno_index] for key in self.properties.keys()}
            else:
                anno_properties = None
            # Construct the list of selections
            curr_selections = {}
            for axis in axis_list:
                axis_index = axis + 1  # Correct the axis index as axes are stored in oreder of [-1, 0 , 1, 2, ...]
                if selection_ref[axis_index] >= 0:
                    curr_selections[axis] = self.selections[axis][selection_ref[axis_index]]
            # Construct the DataSelection object
            curr_data_selection = DataSelection(data_object=self.data_object,
                                                selections=curr_selections,
                                                simplify=False,
                                                collapse=False)
            # Construct the Annotation object and add it to the list
            annotation_list.append(Annotation(annotation_type=anno_type,
                                              description=anno_description,
                                              data_selection=curr_data_selection,
                                              properties=anno_properties))
        # Return the list of annotations
        return annotation_list

    def data(self, stack=False):
        """
        Get all the data associated with the current set of annotations.

        Note, we here load all the annotation_types and descriptions and selection etc. into memory.

        :param stack: stack the data into a single array. NOTE: This is only possible if the
                      selections are of the same size. If possible the selections will be
                      combined using the numpy.dstack function. A ValueError will be raised
                      if stack=True and stacking is not possible.

        :returns:

            data : A list of numpy arrays with the data (if stack==False) or a single numpy array of
                   all the data (if stack==True). NOTE: if stack==True, then the last dimension will
                   be the annotation index dimension, whereas if stack is false, then the first
                   dimensions (i.e., the list) is the annotation dimension.

            types : A numpy array of strings of the annotation types

            descriptions : A list of strings of the the annotation descriptions

            annotations : A list of all the annotations. This is the same as the output of get_annotations(...)

        """
        # 1) Compute all annotations
        annotation_list = self.get_annotations()
        # 2) Load the data associated with all annotations
        outdata = []
        for anno_obj in annotation_list:
            outdata.append(anno_obj.data())
        if stack:
            if len(outdata) > 0:
                outdata = np.dstack(outdata)
            else:
                outdata = np.empty((0,))
        # 3) Determine the type of all annotations
        if self.annotation_type_indexes is not None:
            if isinstance(self.annotation_types, list):
                out_anno_types = np.asarray(self.annotation_types)[self.annotation_type_indexes]
            else:
                out_anno_types = self.annotation_types[:][self.annotation_type_indexes]
        else:
            out_anno_types = []
        # 4) Determine the description of all annotations
        if self.descriptions is not None:
            out_descriptions = [d for d in self.descriptions[:]]
        else:
            out_descriptions = []
        # 5) Return the results
        return outdata, out_anno_types, out_descriptions, annotation_list

    def merge(self, merge_type):
        """
        Generic merge function.

        This function is provided for convenience and provides access to the
        specific merge functions by providing the merge_type as a string,
        rather than having to call different merge functions.

        :param merge_type: String indicating the type of merge to be performed. One of 'AND', 'OR', 'XOR'.
                           See also get_merge_types(...)

        :raises: ValueError is raised in case that an invalid merge type is given.

        :returns: Annotation
        """
        merge_types = self.get_merge_types()
        if merge_type not in merge_types:
            raise ValueError('Invalid merge type')
        return merge_types[merge_type]()

    def get_merge_types(self):
        """
        Get all merge types.

        :return: Dict where the keys are names of merge operations and the values are the
                 functions of self that can be used to perform the merge
        """
        return {'AND': self.merge_and,
                'OR': self.merge_or,
                'XOR': self.merge_xor}

    def merge_and(self):
        """
        Get single AnnotationObject (or None) that combines all annotations in this
        collections via logical AND.

        This is a convenience function and is equivalent to calling get_annotations(...)
        and combining all annotations in the returned list via logical & (AND).

        :returns: Annotation
        """
        annotation_list = self.get_annotations()
        if len(annotation_list) == 0:
            return None
        else:
            anno = annotation_list[0]
            for anno_index in range(1, len(annotation_list)):
                anno = anno & annotation_list[anno_index]
            return anno

    def merge_or(self):
        """
        Get single AnnotationObject (or None) that combines all annotations in this
        collections via logical OR.

        This is a convenience function and is equivalent to calling get_annotations(...)
        and combining all annotations in the returned list via logical | (OR).

        :returns: Annotation
        """
        annotation_list = self.get_annotations()
        if len(annotation_list) == 0:
            return None
        else:
            anno = annotation_list[0]
            for anno_index in range(1, len(annotation_list)):
                anno = anno | annotation_list[anno_index]
            return anno

    def merge_xor(self):
        """
        Get single AnnotationObject (or None) that combines all annotations in this
        collections via logical XOR.

        This is a convenience function and is equivalent to calling get_annotations(...)
        and combining all annotations in the returned list via logical ^ (XOR).

        :returns: Annotation
        """
        annotation_list = self.get_annotations()
        if len(annotation_list) == 0:
            return None
        else:
            anno = annotation_list[0]
            for anno_index in range(1, len(annotation_list)):
                anno = anno ^ annotation_list[anno_index]
            return anno

    def add_annotation(self, annotation):
        """
        Add a new annotation to the collection.

        *NOTE:* if the AnnotationCollection object was populated with h5py.Dataset objects,
        e.g., as is the case when using an AnnotationDataGroup instance, then the annotation
        will be written to file. If the AnnotationCollection was initialized as a pure in-memory
        collection using numpy arrays, then the arrays will only be updated in memory.

        *NOTE:* The Annotation is assumed to refer to the same data object as this
        AnnotationCollection. If this is not the case, then the annotation will be either
        reassigned to the data object of the collection (as long as the data object's shape match)
        or a ValueError is raised.

        :param annotation: The annotation to be added
        :type annotation: Annotation

        :raises: ValueError in case the annotation can not be added to the collection
        """
        # Perform basic error checking
        if not isinstance(annotation, Annotation):
            raise ValueError("The given annotation object is not of type Annotation as expected.")

        if self.data_object is None:
            raise ValueError("AnnotationCollection.data_object not initialized!!!")

        data_selection = annotation.data_selection
        new_selections = data_selection.selections
        if self.data_object.shape != data_selection.data_object.shape:
            raise ValueError("The shape of the data object of the AnnotationCollection does not " +
                             "match the data object of the new Annotation")

        # Initialize the self.selections dict if necessary
        axis_list = [-1] + [index for index in range(len(self.data_object.shape))]
        if self.selections is None:
            self.selections = {}
            for axis in axis_list:
                # Determine the dataset shape
                if axis >= 0:
                    selection_shape = (0, self.data_object.shape[axis])
                else:
                    selection_shape = (0,) + self.data_object.shape
                self.selections[axis] = np.zeros(selection_shape, dtype='bool')

        # Add the selection of the annotation to the selections and keep track of the references
        # NOTE: (len(self.data_object.shape)+1) == self.selection_indexes.shape[1] == (number of axes +1)
        num_axes = len(self.data_object.shape)+1
        selection_refs = np.zeros(shape=(num_axes,), dtype='int32') - 1
        for axis_index, axis in enumerate(axis_list):
            if axis in new_selections:
                axis_selection = self.selections[axis]
                new_shape = list(axis_selection.shape)
                new_shape[0] += 1
                try:
                    axis_selection.resize(tuple(new_shape))
                except ValueError:
                    axis_selection.resize(tuple(new_shape), refcheck=False)
                axis_selection[-1, ...] = new_selections[axis]
                selection_refs[axis_index] = axis_selection.shape[0] - 1
            else:
                pass  # Nothing to be done
        if self.selection_indexes is not None:
            self.selection_indexes.resize((self.selection_indexes.shape[0]+1, self.selection_indexes.shape[1]))
            self.selection_indexes[-1, :] = selection_refs
        else:
            self.selection_indexes = np.zeros(shape=(1, num_axes), dtype='int32')
            self.selection_indexes[-1, :] = selection_refs

        # Append the description
        if self.descriptions is not None:
            if isinstance(self.descriptions, list): #  or isinstance(self.descriptions, np.ndarray):
                self.descriptions.append(annotation.description)
            else:  # h5py.Dataset or np.ndarray
                self.descriptions.resize((self.descriptions.shape[0]+1,))
                self.descriptions[-1, ...] = annotation.description
        else:
            self.descriptions = [annotation.description, ]

        # Update the annotation type
        anno_type = annotation.annotation_type
        if self.annotation_types is not None:
            if isinstance(self.annotation_types, list):
                current_types = np.asarray(self.annotation_types[:])
            else:
                current_types = self.annotation_types[:]
            existing_type = anno_type in current_types
            if existing_type:
                type_index = np.where(current_types == anno_type)[0][0]
            else:
                if isinstance(self.annotation_types, list): # or isinstance(self.annotation_types, np.ndarray):
                    self.annotation_types.append(anno_type)
                else:  # h5py.Dataset case or np.ndarray
                    self.annotation_types.resize((self.annotation_types.shape[0]+1,))
                    self.annotation_types[-1] = anno_type
                type_index = self.annotation_types.shape[0] - 1
            self.annotation_type_indexes.resize((self.annotation_type_indexes.shape[0]+1, ))
            self.annotation_type_indexes[-1] = type_index
        else:
            self.annotation_types = [anno_type, ]
            self.annotation_type_indexes = np.zeros((1,), dtype='uint32')

        # Save the properties
        for prop_name, prop_value in annotation.properties.iteritems():
            try:
                if prop_name in self.properties:
                    prop_data = self.properties[prop_name]
                    if isinstance(prop_data, list): # or isinstance(prop_data, np.ndarray):
                        prop_data.append(prop_value)
                    else: # h5py.Dataset case or np.ndarray case
                        if isinstance(prop_data, np.ndarray):
                            prop_data = np.resize(prop_data, (prop_data.shape[0]+1, ))
                        else: # h5py.Dataset
                            prop_data.resize(prop_data, (prop_data.shape[0]+1, ))
                        prop_data[-1] = prop_value
                        self.properties[prop_name] = prop_data
                else:
                    self._create_properties_dataset(name=prop_name,
                                                    dtype=np.dtype(type(prop_value)))
                    self.properties[prop_name][-1] = prop_value
            except ValueError:
                warnings.warn('Property ' + prop_name + ' could not be saved. The data type does ' +
                              'not match and cannot be converted automatically to the type used by the collection')

        # TODO test what happens when some are int and others None or Nan in the properties array

    def _create_properties_dataset(self, name, dtype):
        """
        Internal helper function used to create a new dataset for a property and add it to
        the self.properties list. The dataset will be initalized with NAN.

        This function may be overwritten in child classes to change the construction.
        E.g., in AnalysisDataGroup we need to construct and h5py.Dataset object rather
        than an numpy array.

        This function has a side effect in that it updates self.properties

        :param name: The name of the property
        :param dtype: The numpy/h5py dtype to be used.

        """
        if name in self.properties:
            pass
        else:
            # new_property = np.empty(self.num_annotations(), dtype=dtype)
            # new_property[:] = np.nan
            self.properties[name] = [np.nan] * self.num_annotations()

    def containment_matrix(self):
        """
        Compute a binary matrix indicating for each annotation A whether it is contained in B.

        :returns: Returns a 2D, n x n numpy arrays of booleans (where n is the number of annotations).
                  The element [i,j] indicates whether i is contained in j. All elements that contain
                  i are hence given by [i,:] and all elements that i contains are given by [:,i].
        """
        dataselection_list = []
        axis_list = [index for index in range(len(self.data_object.shape))] + [-1]  # List of axes
        # 1) Iterate through all annotations and construct the list of DataSelection objects
        for anno_index in range(self.num_annotations()):
            selection_ref = self.selection_indexes[anno_index, :].reshape(len(axis_list))  # Current selection refs
            # Construct the list of selections
            curr_selections = {}
            for axis in axis_list:
                axis_index = axis + 1  # Correct the axis index as axes are stored in oreder of [-1, 0 , 1, 2, ...]
                if selection_ref[axis_index] >= 0:
                    curr_selections[axis] = self.selections[axis][selection_ref[axis_index]]
            # Construct the DataSelection object
            dataselection_list.append(DataSelection(data_object=self.data_object,
                                                    selections=curr_selections,
                                                    simplify=False,
                                                    collapse=False))
        # 2 Construct the containment matrix
        num_selections = len(dataselection_list)
        containment_matrix = np.zeros(shape=(num_selections, num_selections), dtype='bool')
        for i in range(num_selections):
            for j in range(num_selections):
                containment_matrix[i, j] = dataselection_list[i] in dataselection_list[j]

        # Return the list of annotations
        return containment_matrix


class AnnotationDataGroup(ManagedGroup, AnnotationCollection):
    """
    Managed group for storage of annotations.

    :param hdf_object: The h5py.Group object managed by this class.

    HDF5 file structure:
    -> 'data_object' --> Link to the HDF5 dataset/group we select from
    -> 'annotation_types' --> 1D Dataset with the list of all available types (vlen str)
    -> 'annotation_type_indexes' --> 1D Dataset with the index into the annotation_types array
    -> 'descriptions' --> 1D Dataset with the description of the annotations
    -> 'selection_indexes' --> (#selections, #axis+1) dataset, indicating for each axis the \
        selection that applies or -1 if none. The axes are ordered as [-1, 0, 1, 2, ...]
    -> 'selections_axis_#' --> 2D dataset per each axis with all 1D selections. + (#axis+1) \
        dataset for global selections
    -> axis_index : 1D dataset for the dimension-scale for selection_indexes
    -> collection_description: Attribute with string describing the collection

    """

    # TODO Use the axis_index dataset rather then the implicit axis construction
    # TODO Document the layout of the annotations

    def __init__(self, hdf_object):
        # 1) Call super constructor and initialize the self.hdf_object
        ManagedGroup.__init__(self,
                              hdf_object=hdf_object)
        if not isinstance(hdf_object, h5py.Group):
            raise ValueError("Expected h5py.Group object to initialize AnnotationDataGroup object.")
        if len(hdf_object.keys()) > 0:  # I.e., the group has already been populated
            # 2) Get HDF5 dataset or managed object the annotations apply to
            dataset_spec = self.get_format_specification()['datasets']

            anno_data_object = self.hdf_object[dataset_spec['data_object']['dataset']]
            try:
                managed_object = ManagedObject.get_managed_object(anno_data_object)
            except NameError:
                managed_object = None
            # 2.1) If the data object is a managed object that supports shape and slicing, then use the managed object
            if managed_object is not None:
                if hasattr(managed_object, 'shape') and hasattr(managed_object, '__getitem__'):
                    anno_data_object = managed_object
            # 3) Get array of the indexes  annotation types. 1D int dataset.
            anno_type_indexes = self.hdf_object[dataset_spec['annotation_type_indexes']['dataset']]
            # 4) Get the array of annotation types
            anno_types = self.hdf_object[dataset_spec['annotation_types']['dataset']]
            # 5) Get the array of annotation descriptions. 1D vlen string dataset.
            descriptions = self.hdf_object[dataset_spec['descriptions']['dataset']]
            # 6) 2D dataset of (#selections, #axis+1) describing for each annotation the selections that apply
            selection_refs = self.hdf_object[dataset_spec['selection_indexes']['dataset']]
            # 7) Dict of 2D datasets of per-axis 1D selections + one  (#axis+1) dataset for global selection
            axis_list = [-1] + [index for index in range(len(anno_data_object.shape))]
            data_selections = {}  # The h5py datasets are represented as selections_axis_#
            for axis in axis_list:
                selection_dataset_name = dataset_spec['selections']['prefix'] + str(axis)
                data_selections[axis] = self.hdf_object[selection_dataset_name]
            collection_description = self.hdf_object.attrs['collection_description']
            # 8) Dict of 1D datasets per property
            anno_properties = {}
            for curr_name, curr_obj in hdf_object.items():
                if curr_name.startswith(dataset_spec['properties']['prefix']) and \
                        curr_name.lstrip(dataset_spec['properties']['prefix']).isdigit():
                    try:
                        property_name = curr_obj.attrs[dataset_spec['properties']['attributes'][0]['attribute']]
                    except KeyError:
                        warnings.warn("Property dataset does not have required properties name attribute. " +
                                      "Ignoring the property.")
                        property_name = None
                    if property_name is not None:
                        anno_properties[property_name] = curr_obj

        else:  # The group is empty and will be initialized next via the populate function
            anno_data_object = None
            anno_type_indexes = None
            anno_types = None
            descriptions = None
            selection_refs = None
            data_selections = None
            collection_description = ''
            anno_properties = None
        # Initialize the data annotation collection
        AnnotationCollection.__init__(self,
                                      data=anno_data_object,
                                      annotation_type_indexes=anno_type_indexes,
                                      annotation_types=anno_types,
                                      descriptions=descriptions,
                                      selection_indexes=selection_refs,
                                      selections=data_selections,
                                      collection_description=collection_description,
                                      properties=anno_properties)

    def __getitem__(self, item):
        """
        If item is a string, then retrieve the corresponding object from the HDF5 file using the
        `ManagedGroup.__getitem__(..)` method, otherwise retrieve the corresponding AnnotationCollection
        using the implementation of `AnnotationCollection.__getitem__(..)`
        """
        if isinstance(item, basestring):
            return ManagedGroup.__getitem__(self, item)
        else:
            return AnnotationCollection.__getitem__(self, item)

    @classmethod
    def get_format_specification(cls):
        """
        Return dictionary describing the specification of the format.
        """
        spec = {'datasets': {'data_object': {'dataset': 'data_object',
                                             'prefix': None,
                                             'optional': False,
                                             'dimensions': [],
                                             'description': None,  # We don't use a description as this is a hard-link
                                             'attributes': []},
                             'annotation_types': {'dataset': 'annotation_types',
                                                  'prefix': None,
                                                  'optional': False,
                                                  'dimensions': [{'name': 'type_index',
                                                                  'unit': None,
                                                                  'optional': False,
                                                                  'dataset': None,
                                                                  'axis': 0,
                                                                  'description': 'Integer index of the type'}],
                                                  'dimensions_fixed': True,
                                                  'description': 'List of all available annotation types',
                                                  'attributes': []},
                             'annotation_type_indexes': {'dataset': 'annotation_type_indexes',
                                                         'prefix': None,
                                                         'optional': False,
                                                         'dimensions': [{'name': 'type_index',
                                                                         'unit': None,
                                                                         'optional': False,
                                                                         'dataset': None,
                                                                         'axis': 0,
                                                                         'description': 'Integer index into the ' +
                                                                                        'annotation_types array ' +
                                                                                        'indicating the type of ' +
                                                                                        'the annotation'}],
                                                         'dimensions_fixed': True,
                                                         'description': "Dataset indicating for each selection the " +
                                                                        "index of the annotation type used. The " +
                                                                        "annotation types are given in the annotation" +
                                                                        " types dataset.",
                                                         'attributes': []},
                             'descriptions': {'dataset': 'descriptions',
                                              'prefix': None,
                                              'optional': False,
                                              'dimensions': [{'name': 'annotation_index',
                                                              'unit': None,
                                                              'optional': False,
                                                              'dataset': None,
                                                              'axis': 0,
                                                              'description': 'Integer index of the annotation'}],
                                              'dimensions_fixed': True,
                                              'description': 'Dataset with the annotation descriptions.',
                                              'attributes': []},
                             'selection_indexes': {'dataset': 'selection_indexes',
                                                   'prefix': None,
                                                   'optional': False,
                                                   'dimensions': [{'name': 'annotation_index',
                                                                   'unit': None,
                                                                   'optional': False,
                                                                   'dataset': None,
                                                                   'axis': 0,
                                                                   'description': 'Integer index of the annotation'},
                                                                  {'name': 'axis_index',
                                                                   'unit': 'index',
                                                                   'optional': False,
                                                                   'dataset': 'axis_index',
                                                                   'axis': 1,
                                                                   'description': 'Integer index of the axis'}],
                                                   'dimensions_fixed': True,
                                                   'description': "Dataset indicating for each axis the index of the " +
                                                                  "selection applied to the given axis. -1 indicates " +
                                                                  "that no selection is applied along that axis. The " +
                                                                  "axis index ranges from -1 to n where -1 indicated " +
                                                                  "global selection and n is the number of axes.",
                                                   'attributes': []},
                             'selections': {'dataset': None,
                                            'prefix': 'selections_axis_',
                                            'optional': False,
                                            'dimensions': [{'name': 'selection_index',
                                                            'unit': None,
                                                            'optional': False,
                                                            'dataset': None,
                                                            'axis': 0,
                                                            'description': 'Integer index of the selection'}],
                                            'dimensions_fixed': False,
                                            'description': "Datasets with all selections for the indicated axis. " +
                                                           "Axis -1 indicates a global selection across all axes. " +
                                                           "One dataset per axis and one for global selection " +
                                                           "(-1) is mandatory.",
                                            'attributes': [{'attribute': 'axis',
                                                            'value': None,
                                                            'prefix': None,
                                                            'optional': False}]},
                             'properties': {'dataset': None,
                                            'prefix': 'property_',
                                            'optional': True,
                                            'dimensions': [{'name': 'annotation_index',
                                                            'unit': None,
                                                            'optional': False,
                                                            'dataset': None,
                                                            'axis': 0,
                                                            'description': 'Integer index of the selection'}],
                                            'dimensions_fixed': False,
                                            'description': "Datasets with a particular property for all annotations.",
                                            'attributes': [{'attribute': 'name',
                                                            'value': None,
                                                            'prefix': None,
                                                            'optional': False}]}
                            },
                'groups': {},
                'managed_objects': [],
                'attributes': [{'attribute': 'collection_description',
                                'value': None,
                                'prefix': None,
                                'optional': False}],
                'group': None,
                'prefix': "annotations_",
                'optional': True,
                'description': "Managed group for storage of a collection of annotations. Multiple annotation " +
                               "collections may typically be associated with the same data object."}
        spec = BaseSpec.from_dict(spec)

        # Add an indexes relationship pointing from anntation_type_indexes to annotation_types
        index_to_types = RelationshipSpec(
            attribute='indexes_annotation_types',
            target=RelationshipTargetSpec(dataset='annotation_types', group=None, prefix=None, axis=0),
            relationship_type='indexes',
            description='Relationship documentation that we are storing references to annotation_types',
            axis=0)
        spec['datasets']['annotation_type_indexes'].add_relationship(index_to_types)

        # Add an indexes relationship from the axis_index of the selection_indexes dataset to the selection dataset
        index_to_selections = RelationshipSpec(
            attribute='select_axis',
            target=RelationshipTargetSpec(prefix=spec['datasets']['selections']['prefix'], group=None, dataset=None),
            relationship_type='order',
            description='Relationship documentation that each column of the selection_indexes ' +\
                        'dataset refers to a different selections_axis_ dataset'
        )
        spec['datasets']['selection_indexes']['dimensions'][1].add_relationship(index_to_selections)

        # TODO Add ordering relationships between the various datasets that have an axis with just all annotations
        # Add ordering relationships from annotation_type_indexes to descriptions, selection_indexes, and property_
        # Add ordering relationships from descriptions to annotation_type_indexes, selection_indexes, and property_
        # Add ordering relationships from selection_indexes to annotation_type_indexes, descriptions, and property_
        # Add ordering relationships from property_ to annotation_type_indexes, descriptions, and selection_indexes

        return spec




    def populate(self,
                 data_object=None,
                 annotation_types=None,
                 annotation_type_indexes=None,
                 descriptions=None,
                 selection_indexes=None,
                 selections=None,
                 collection_description='',
                 annotation_collection=None,
                 annotation_properties=None,
                 enable_compression=True):
        """
        The populate method is called by the create method after the basic common setup is complete.
        The function should be used to populate the managed object (e.g., add dimensions to a datadset or
        add required datasets to a group. The populate method is passed the kwargs handed to the create
        method.

        :param data_object: The h5py.Dataset (or managed object with support for shape and slicing)
                            the Annotations refer to. May be None in case that annotation_collection is
                            provided otherwise a data_object must be provided. For h5py.Dataset and
                            ManagedObject, the data_object will be stored as a reference to the object
                            that is being annotated (i.e., the data will NOT be dublicated but simply
                            references. NOTE: If a numpy array is given then the array will be stored in the HDF5 file.
        :param annotation_types: 1D numpy array of strings (or python list of strings, or h5py dataset of string)
                            with the different possible types of annotations in this collection. May be
                            None in case that AnnotationCollection is initialized as empty.
        :param annotation_type_indexes: 1D h5py.Dataset or numpy array indicating for each annotation the index
                            of the annotation_type that applies.
        :param descriptions: h5py.Dataset or numpy array with the annotation descriptions
        :param selection_indexes: h5py.Dataset or numpy array with the indexes of the selections that apply
        :param collection_description: String with a description of the annotation collection
        :param selections: Python dict of numpy arrays or h5py.Dataset objects. The keys are ints indicating
                           the axis, which must be in the range of [-1,0,1, ... #axis]. Missing values are
                           interpreted as empty (i.e., no selections are available for those axes).
        :param annotation_collection: This parameter may be given instead of the other AnnotationDataGroup
                           specific parameters, i.e., data_object, annotation_types, annotation_type_indexes,
                           descriptions, selection_indexes, selections, collection_description. If given then
                           those parameter will be initialized from the annotation collection instead.
        :type annotation_collection: AnnotationCollection
        :param annotation_properties: Dictionary with numpy or h5py arrays of additional annotation properties.
        :type annotation_properties: Dict with string keys and 1D numpy arrays as values.
        :param enable_compression: Should we use compression when storing annotation results (default is True)
        :type enable_compression: bool

        :raises: ValueError is raised in case that the object cannot be populated.
        """
        if annotation_collection is not None:
            # Check if any of the other input parameters is given and indicate error in this case
            if data_object is not None or \
                    annotation_types is not None or \
                    annotation_type_indexes is not None or \
                    descriptions is not None or \
                    selection_indexes is not None or \
                    selections is not None or \
                    collection_description != '':
                raise ValueError('AnnotationCollection given for save collides with other input parameters.')
            data_object = annotation_collection.data_object
            annotation_types = annotation_collection.annotation_types
            annotation_type_indexes = annotation_collection.annotation_type_indexes
            descriptions = annotation_collection.descriptions
            selection_indexes = annotation_collection.selection_indexes
            selections = annotation_collection.selections
            collection_description = annotation_collection.collection_description
            annotation_properties = annotation_collection.properties

        # 1) Get the format specification
        format_spec = self.get_format_specification()
        dataset_spec = format_spec['datasets']
        # 1.1) Determine the string type to use based on the available h5py version
        try:
            str_type = h5py.special_dtype(vlen=unicode)
        except NotImplementedError:
            str_type = h5py.special_dtype(vlen=str)

        # 2) Initialize all datasets
        # 2.1) Initialize the data_object dataset
        # 2.1.1) Determine the h5py.Dataset/Group object we point to
        if isinstance(data_object, h5py.Dataset):
            data_hdf5 = data_object
            data_hdf5_file = data_object.file.filename
        elif isinstance(data_object, ManagedObject):
            if hasattr(data_object, 'shape') and hasattr(data_object, '__getitem__'):
                data_hdf5 = data_object.get_h5py()
                data_hdf5_file = data_hdf5.file.filename
            else:
                raise ValueError("The given managed object does not support data shape  and/or object slicing.")
        elif isinstance(data_object, np.ndarray):
            data_hdf5 = data_object
            data_hdf5_file = None
        else:
            raise ValueError("The given data object does not support annotation (data_object variable error).")
        num_axes = len(data_hdf5.shape)
        axis_list = np.asarray([-1] + [i for i in range(num_axes)], dtype='int32')

        # 2.1.2) Set hard link to the object we annotate
        data_same_file = True
        if data_hdf5_file is not None:
            data_same_file = filecmp.cmp(data_hdf5_file, self.hdf_object.file.filename)

        if data_same_file:
            self.hdf_object[dataset_spec['data_object']['dataset']] = data_hdf5
        else:
            self.hdf_object[dataset_spec['data_object']['dataset']] = h5py.ExternalLink(filename=data_hdf5_file,
                                                                                        path=data_object.name)
        self.data_object = data_hdf5  # Initialize the inherited data_object variable of the AnnotationCollection

        # 2.2) Initialize the annotation_types dataset as well as the instance variable of the AnnotationCollection
        if annotation_types is not None:
            if isinstance(annotation_types, list):
                annotation_types_shape = (len(annotation_types),)
            else:
                annotation_types_shape = (annotation_types.shape[0],)
        else:
            annotation_types_shape = (0,)
        # 2.2.1) Create the dataset
        self.annotation_types = self.hdf_object.create_dataset(name=dataset_spec['annotation_types']['dataset'],
                                                               shape=annotation_types_shape,
                                                               dtype=str_type,
                                                               maxshape=(None,),
                                                               chunks=True)
        # 2.2.2) Save the data
        if annotation_types is not None:
            self.annotation_types[:] = annotation_types[:]
        # 2.2.3) Create dimension label
        anno_dimspec_0 = dataset_spec['annotation_types']['dimensions'][0]
        self.annotation_types.dims[anno_dimspec_0['axis']].label = anno_dimspec_0['name']
        # 2.2.4) Add the description attribute
        self.annotation_types.attrs[ManagedObject.description_attribute_name] = \
            dataset_spec['annotation_types']['description']

        # 2.3) Initialize the 'annotation_type_indexes dataset and instance variable from AnnotationCollection
        if annotation_type_indexes is not None:
            annotation_type_indexes_shape = (annotation_type_indexes.shape[0],)
        else:
            annotation_type_indexes_shape = (0,)
        # 2.3.1) Create the dataset
        self.annotation_type_indexes = self.hdf_object.create_dataset(
            name=dataset_spec['annotation_type_indexes']['dataset'],
            shape=annotation_type_indexes_shape,
            dtype='uint32',
            maxshape=(None,),
            chunks=True,
            fillvalue=0)
        # 2.3.2) Save the data
        if annotation_type_indexes is not None:
            self.annotation_type_indexes[:] = annotation_type_indexes[:]
        # 2.3.3) Create the dimension label
        anno_type_dimspec_0 = dataset_spec['annotation_type_indexes']['dimensions'][0]
        self.annotation_type_indexes.dims[anno_type_dimspec_0['axis']].label = anno_type_dimspec_0['name']
        # 2.3.4) Create the description attribute
        self.annotation_type_indexes.attrs[ManagedObject.description_attribute_name] = \
            dataset_spec['annotation_type_indexes']['description']
        # 2.3.5) Add an indexes relationship pointing from anntation_type_indexes to annotation_types
        rel_indexes_annotation_types = RelationshipAttribute.create(
            parent_object=self.annotation_type_indexes,
            relationship=dataset_spec['annotation_type_indexes']['relationships'][0])

        # 2.4) Initialize the 'descriptions' dataset and corresponding variable inherited from AnnotationCollections
        if descriptions is not None:
            if isinstance(descriptions, list):
                descriptions_shape = (len(descriptions),)
            else:
                descriptions_shape = (descriptions.shape[0],)
        else:
            descriptions_shape = (0,)
        # 2.4.1) Create the dataset
        self.descriptions = self.hdf_object.create_dataset(
            name=dataset_spec['descriptions']['dataset'],
            shape=descriptions_shape,
            dtype=str_type,
            maxshape=(None,),
            chunks=True)
        # 2.4.2) Save the data
        if descriptions is not None:
            self.descriptions[:] = np.asarray(descriptions) #[:]
        # 2.4.3) Create the dimension label
        descriptions_dimspec_0 = dataset_spec['descriptions']['dimensions'][0]
        self.descriptions.dims[descriptions_dimspec_0['axis']].label = descriptions_dimspec_0['name']
        # 2.4.4) Create the description attributes
        self.descriptions.attrs[ManagedObject.description_attribute_name] = dataset_spec['descriptions']['description']

        # 2.5) Initialize the 'selection_indexes' dataset and corresponding variable from AnnotationCollections
        if selection_indexes is not None:
            selection_indexes_shape = selection_indexes.shape
            if selection_indexes.shape[1] != (num_axes+1):
                raise ValueError('Mismatch of shape of selection_indexes with number of dimensions of data_object.')
        else:
            selection_indexes_shape = (0, (num_axes+1))
        # 2.5.1) Create the dataset
        if enable_compression:
            self.selection_indexes = self.hdf_object.create_dataset(
                name=dataset_spec['selection_indexes']['dataset'],
                shape=selection_indexes_shape,
                dtype='int32',
                maxshape=(None, selection_indexes_shape[1]),
                compression='gzip',
                compression_opts=1,
                chunks=True,
                shuffle=True,
                fillvalue=-1)
        else:
            self.selection_indexes = self.hdf_object.create_dataset(
                name=dataset_spec['selection_indexes']['dataset'],
                shape=selection_indexes_shape,
                dtype='int32',
                maxshape=(None, selection_indexes_shape[1]),
                chunks=True,
                fillvalue=-1)
        # 2.5.2) Save the data
        if selection_indexes is not None:
            self.selection_indexes[:] = selection_indexes[:]
        # 2.5.3) Create the dimension label and axis dataset
        sel_index_dimspec = dataset_spec['selection_indexes']['dimensions']
        self.selection_indexes.dims[sel_index_dimspec[0]['axis']].label = sel_index_dimspec[0]['name']
        self.selection_indexes.dims[sel_index_dimspec[1]['axis']].label = sel_index_dimspec[1]['name']
        selection_indexes_axis1_data = axis_list
        selection_indexes_axis1 = self.hdf_object.create_dataset(name=sel_index_dimspec[1]['dataset'],
                                                                 data=selection_indexes_axis1_data)
        selection_indexes_axis1.attrs[ManagedObject.description_attribute_name] = sel_index_dimspec[1]['description']
        self.selection_indexes.dims.create_scale(selection_indexes_axis1, sel_index_dimspec[1]['unit'])
        self.selection_indexes.dims[sel_index_dimspec[1]['axis']].attach_scale(selection_indexes_axis1)
        # 2.5.4) Create the description attributes
        self.selection_indexes.attrs[ManagedObject.description_attribute_name] = \
            dataset_spec['selection_indexes']['description']
        # 2.5.5) Create the relationship attribute btween the 'axis_index' dataset and the collection
        #        selection datasets
        RelationshipAttribute.create(
            parent_object=selection_indexes_axis1,
            relationship=dataset_spec['selection_indexes']['dimensions'][1]['relationships'][0])

        # 2.6) Initialize the datasets for storing all data selections and the corresponding selections variable
        self.selections = {}
        if selections is None:
            selections = {}
        for axis in axis_list:
            # Determine the dataset shape
            if axis in selections:
                selection_shape = selections[axis].shape
            else:
                if axis >= 0:
                    selection_shape = (0, self.data_object.shape[axis])
                else:
                    selection_shape = (0,) + self.data_object.shape
            # Determine the maxhshape
            if axis >= 0:
                selection_maxshape = (None, self.data_object.shape[axis])
            else:
                selection_maxshape = (None,) + self.data_object.shape
            # Determine the data type
            selection_dtype = 'bool'
            # Determine the chunking to be used
            if axis >= 0:
                try:
                    sfrac = selection_shape[1] / 32768
                    if sfrac > 0:
                        selection_chunking = (1, selection_shape[1]/sfrac)
                    else:
                        selection_chunking = (int(32768./selection_shape[1] + 0.5), selection_shape[1])
                except ZeroDivisionError:
                    selection_chunking = True
            else:
                selection_chunking = True   # Let h5py handle the chunking for multi-dimensional selections

            # 2.6.1) Create the dataset
            selection_dataset_name = dataset_spec['selections']['prefix'] + str(axis)
            if enable_compression:
                self.selections[axis] = self.hdf_object.create_dataset(
                    name=selection_dataset_name,
                    shape=selection_shape,
                    dtype=selection_dtype,
                    maxshape=selection_maxshape,
                    compression='gzip',
                    compression_opts=1,
                    chunks=selection_chunking,
                    shuffle=True,
                    fillvalue=0)
            else:
                self.selections[axis] = self.hdf_object.create_dataset(
                    name=selection_dataset_name,
                    shape=selection_shape,
                    dtype=selection_dtype,
                    maxshape=selection_maxshape,
                    chunks=selection_chunking,
                    fillvalue=0)
            # 2.6.2) Save the selection data
            if axis in selections:
                self.selections[axis][:] = selections[axis][:]
            # 2.6.3) Create the dimension labels
            selection_dim_spec = dataset_spec['selections']['dimensions'][0]
            self.selections[axis].dims[selection_dim_spec['axis']].label = selection_dim_spec['name']
            # 2.6.4) Create the descriptions attributes
            self.selections[axis].attrs[ManagedObject.description_attribute_name] = \
                dataset_spec['selections']['description']
            # 2.6.5) Add the axis attribute
            self.selections[axis].attrs[dataset_spec['selections']['attributes'][0]['attribute']] = axis

        # 2.7 Save all annotation properties
        if annotation_properties is not None:
            self.properties = {}
            properties_dim_spec = dataset_spec['properties']['dimensions'][0]
            for prop_index, prop_key in enumerate(annotation_properties.keys()):
                self.properties[prop_key] = self.hdf_object.create_dataset(
                    name=dataset_spec['properties']['prefix'] + str(prop_index),
                    data=np.asarray(annotation_properties[prop_key][:]),
                    chunks=True,
                    maxshape=(None,))
                self.properties[prop_key].attrs[ManagedObject.description_attribute_name] = \
                    dataset_spec['properties']['description']
                self.properties[prop_key].attrs[dataset_spec['properties']['attributes'][0]['attribute']] = prop_key
                self.properties[prop_key].dims[properties_dim_spec['axis']].label = properties_dim_spec['name']

        # 2.8 Add the collections attributes
        self.hdf_object.attrs[format_spec['attributes'][0]['attribute']] = collection_description
        self.collection_description = collection_description

    def _create_properties_dataset(self, name, dtype):
        """
        Internal helper function used to create a new dataset for a property and add it to
        the self.properties list. The dataset will be initialized with NAN.

        This function is inherited from AnalysisCollection and overwritten here to
        change the construction of a numpy array to the construction of an h5py.Dataset
        for the property.

        """
        if name in self.properties:
            pass
        else:
            # Create a NAN numpy array to be stored for the property
            new_property_data = np.empty(self.num_annotations(), dtype=dtype)
            new_property_data[:] = np.nan
            new_prop_index = len(self.properties)

            # Create the h5py data structures to store the results
            dataset_spec = self.get_format_specification()['datasets']
            properties_dim_spec = dataset_spec['properties']['dimensions'][0]
            self.properties[name] = self.hdf_object.create_dataset(
                name=dataset_spec['properties']['prefix'] + str(new_prop_index),
                data=new_property_data,
                chunking=True,
                maxshape=(None,))
            self.properties[name].attrs[ManagedObject.description_attribute_name] = \
                dataset_spec['properties']['description']
            self.properties[name].attrs[dataset_spec['properties']['attributes'][0]['attribute']] = name
            self.properties[name].dims[properties_dim_spec['axis']].label = properties_dim_spec['name']
