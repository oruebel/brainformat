"""
Module with classes to help with the specification of data selections.

"""
import warnings
import numpy as np

class DataSelection(object):
    """
    A single data data_selection for a given dataset.

    **Basic Structure**

    The data_selection object defines a set of selections (one per axis).
    Axes for which no data_selection is specified are interpreted as an ALL data_selection,
    i.e., [1,1,1,...,1] is assumed. The individual selections are assumed to be
    combined via a binary AND operation, i.e., only objects selected by all
    selections are seen as being selected. A data_selection may also be applied to the
    -1 axis, indicating that the data_selection is applied to all axes. In the case
    of such a global data_selection, the data_selection is described via a bool matrix
    of the same shape as the data (rather than a vector). As such, specifying a
    global (-1) data_selection is generally something that should be avoided if possible,
    because this may result in very large data_selection arrays, as a bool value must be
    created for each data element.

    **Creating and Updating Data Selections**

    A default DataSelection my be created simply by specifying only the data object the
    selection applies to without specifying any explicit selection:

    >>> data = np.arange(100).reshape((10, 10))
    >>> d1 = DataSelection(data_object=data)

    This will create a default DataSelection with an empty selection dict, i.e.,
    `d1.selections = {}`. As missing selections are treated as ALL, this results in
    a selection data implicitly selects all objects.

    We may now change the selection, simply using array slicing. In the slicing we
    specify the axis index first, and then the elements to be changed:

    >>> d1[0, 1:3] = True  # axis=0, selection=1:3
    >>> print d1[0]
    [ True  True  True False False False False False False False]

    *NOTE:* While missing selections are treated as ALL in general, if a selection is
    missing during assignment, we will initialize the missing selection as all False.
    This divergence from the standard scheme achieves that all values outside of the
    given first assignment are not selected . This divergence is made to:
    i) allow the more intuitive usage for users of selecting elements of interest
    via assignment rather then having to define all elements that are not of
    interest and ii) to avoid the problem that elements are automatically selected
    for the user.

    *NOTE:* Instead of specifying the index of the axis, we may also use the label
    of the dimension if the data object the DataSelection applies is an h5py.Dataset
    with dimension scales. E.g, `d1['time', 1:3] = True`

    Instead of creating a default selection and then updating it, we can also directly
    create a custom selection, by directly initializing the selections dictionary
    during creation of the DataSelection object:

    >>>t = np.zeros(10, 'bool')
    >>>t[0:3] = True
    >>>d1 = DataSelection(data_object=data, selections= {0: t})

    **Accessing Axis Selection**

    To access the selections associated with the different axes we can do
    one of the following two things:

    >>> print d1[0]  # Get selection for axis 0 using array slicing
    [ True  True  True False False False False False False False]
    >>> print d1.selections[0]  # Get selection for axis 0 by accessing the selection dict directly
    [ True  True  True False False False False False False False]

    As shown above, we can retrieve the selection associated using standard slicing
    against the DataSelection directly or by slicing into the selections dict. For
    axis with an explicit selection, both methods result in the same behavior.
    However, for axes without a selection, the first approch will yield None, while
    the latter approach will result in a KeyError:

    >>> print d1[1]
    >>> print d1.selections[1]
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    KeyError: 1

    **Logical Operations / Combining Data Selection**

    DataSelection objects may be combined via bitwise binary operators:

        * `&` : AND operator defining the intersection of two DataSelections `s1 & s2`
        * `|` : OR operator defining the merge of two DataSelections `s1 | s2`
        * `^` : XOR of two data DataSelections `s1 ^ s2`
        * `~` : NOT operator inverting a DataSelection `~s1`

    **Comparing Data Selections**

        * `in` : `s1 in s2' Check if DataSelection s1 is a subset of s2 `s1 in s2`
        * '>=` : `s1 >= s2` : Check if the DataSelection s1 contains all elements of s2.
        * '>' : `s1 > s`' : Check if the DataSelection s1 contains all elements of s2 and has additional elements
        * '==' : `s1 == s2` : Check if the DataSelection s1 selects exactly the same elements as s2
        * '!=' : `s1 != s2` : Check if the DataSelection s1 selects at least in parts different elements than s2
        * '<' : `s1 < s2` : Check if the DataSelection s1 is contained in s2 but s2 selects additional elements as well
        * '<=' : `s1 <= s2` : Check if the DataSelection s1 is a subset of s2
        * `>>` : `s1 >> s2` : Check if s1 follows s2, i.e., all elements selected by s1 appear after all elements \
                              selected by s2.
        * `<<` : `s1 >> s2` : Check if s1 precedes s2, i.e., all elements selected by s1 appear before all elements \
                              selected by s2.

    **Other Selection Operations**

        * `validate()` : Validate that a data_selection is well-defined/valid
        * `len` : The python len operator functions is equivalent to calling count() (see next).
        * `count()` : Count the total number of elements selected. This is equivalent to `len(a)`
        * `simplify_selection()` : Remove unnecessary components of the data_selection.
        * `collapse_selection()` : Collapse the data_selection to a single global data_selection.

    :ivar selections: Dict of numpy bool vector (True/False) indicating which values are selected
                      for a given axis. The axis is the key and data_selection vectors are the values.
                      Use -1 for a complex data_selection that is applied to all axes. As soon as a
                      a global -1 data_selection is present the selection could also be collapsed to
                      just a single global selection. See DataSelection.collapse_selection(...)

    :ivar data_object: The object to which the data_selection is applied to. The object must support
                      the .shape attribute and support h5py/numpy array slicing.
    """
    def __init__(self, data_object, selections=None, simplify=False, collapse=False):
        """
        Initialize a new DataSelection object.

        :param selections: Dict of numpy bool vector (True/False) indicating which values are selected
                      for a given axis. The axis is the key and data_selection vectors are the values.
                      Use -1 for a complex data_selection that is applied to all axes. As soon as a
                      a global -1 data_selection is present could also be collapsed to a single
                      global data_selection. Use None or an empty dict {} to create a basic selection
                      of the whole data.
        :param data_object: The object to which the data_selection is applied to. The object must support
                      the .shape attribute and support h5py/numpy array slicing.
        :param simplify: Simplify the incoming data_selection if possible (see also simplify_selection(..))
        :param collapse: Collapse the data_selection if possible (see also collapse_selection(..) with global_only=True)

        """
        # Save the data_selection properties
        if selections is not None:
            self.selections = selections
        else:
            self.selections = {}
        self.data_object = data_object
        # Simplify and collapse the data_selection if requested
        if simplify:
            self.simplify_selection()
        if collapse:
            self.collapse_selection(global_only=True)

    def __setitem__(self, key, value):
        """
        Define the selection for a given axis using the following syntax.

        s[axis_index, value_selection] = True

        The right-hand assignment value must be a bool or bool-array. The axis_index
        must be an integer indicating a valid axis. The selection may be any
        valid data selection along the given axis.

        Note, if no selection exists yet for the given axis, then all values
        outside of the given assignment will be set to False. This is in contrast
        to the fact that non-existent selections are generally treated as all 1.
        This divergence is made to: i) allow the more intuitive usage for users of
        selecting elements of interest via assignment rather then having to define
        all elements that are not of interest and ii) avoid the problem that
        elements are automatically selected for the user.
        """
        # Check if the given key/value are valid
        if not isinstance(key, tuple) or len(key) != 2:
            raise KeyError("Invalid key for assignment to DataSelection. Expected tuple of (axis_index, selection)")
        axis_index = key[0]
        axis_selection = key[1]
        # Look up the axis if it is given by name
        if isinstance(axis_index, basestring):
            try:
                dim_labels = [dim.label for dim in self.data_object.dims]
            except AttributeError:
                dim_labels = []
            if axis_index in dim_labels:
                axis_index = dim_labels.index(axis_index)

        if not isinstance(axis_index, int):
            raise KeyError("Axis index must be an integer.")
        num_dims = len(self.data_object.shape)
        if axis_index < -1 or axis_index >= num_dims:
            raise KeyError("Axis index out-of-bounds.")

        # Check if the given axis already has a selection
        if axis_index not in self.selections:
            if axis_index >= 0:
                new_selection = np.zeros(shape=(self.data_object.shape[axis_index],), dtype='bool')
            else:  # axis_index == -1
                new_selection = np.zeros(shape=self.data_object.shape, dtype='bool')
            new_selection[axis_selection] = value
            self.selections[axis_index] = new_selection
        else:
            # Assign the given selection
            self.selections[axis_index][axis_selection] = value

    def __getitem__(self, item):
        """
        Get the selection for the given axis. If no selection is present
        for the specified axis, then None is returned instead
        """
        if item in self.selections:
            return self.selections[item]
        elif hasattr(self.data_object, 'dims'):
            try:
                dim_labels = [dim.label for dim in self.data_object.dims]
                if item in dim_labels:
                    dim_index = dim_labels.index(item)
                    return self.selections[dim_index]
                else:
                    return None
            except AttributeError:
                return None
        else:
            return None

    def __and__(self, other):
        """
        Return a new DataSelection that defines a logical AND of the two selections.
        Selections are combined on a per-axis basis.

        NOTE: The new data_selection will always be defined on the data_object of the left-hand
              DataSelection A of (A and B).

        NOTE: If only one of the two selections defines a data_selection for a given axes
        then the single data_selection that is defined will be used, implicitly interpreting
        the missing data_selection as an ALL [1,1,1....1] data_selection.

        :param other: The right-hand DataSelection object B of the A & B operation.
        :type other: DataSelection

        """
        if isinstance(other, DataSelection):
            # Check if the shapes of the data objects match
            if self.data_object.shape != other.data_object.shape:
                raise AttributeError("Axis mismatch between data objects the data_selection is applied to.")
            if not self.validate() or not other.validate():
                raise ValueError("Invalid data_selection.")
            all_axes = set(self.axes() + other.axes())  # Get all axes that have explicit selections
            new_selection = {}  # Create empty new data_selection
            # Iterate over all selections
            for axis in all_axes:
                # Merge the two selections if both objects define a data_selection for the axis
                if axis in self.axes() and axis in other.axes():
                    new_selection[axis] = self.selections[axis] & other.selections[axis]
                # Keep the left-hand data_selection if only it defines an explicit data_selection for the axis
                elif axis in self.axes():
                    new_selection[axis] = self.selections[axis]
                # Keep the right-hand data_selection if only it defines an explicit data_selection for the axis
                elif axis in other.axes():
                    new_selection[axis] = other.selections[axis]
            # Create a new DataSelection object and simplify and collapse the data_selection if possible
            return DataSelection(selections=new_selection,
                                 data_object=self.data_object,
                                 simplify=True,
                                 collapse=True)
        else:
            raise ValueError('AND operations not supported for objects other than DataSelections')

    def __or__(self, other):
        """
        Return a new DataSelection that defines the logical OR of the two selections.

        NOTE: The new data_selection will always be defined on the data_object of the left-hand
              DataSelection A of (A or B).

        NOTE: Due to the complexity of the resulting data_selection, the returned DataSelection
        may define a complex, global data_selection for axis -1. Only in the case where
        both selections define just a single data_selection on the same axis do we keep
        a simple data_selection. Global selections may require large memory as we need
        to store one bool for each element in the dataset.

        :param other: The right-hand DataSelection object B of the A & B operation.
        :type other: DataSelection

        """
        if isinstance(other, DataSelection):
            # Check if the shapes of the data objects match
            if self.data_object.shape != other.data_object.shape:
                raise AttributeError("Axis mismatch between data objects the data_selection is applied to.")
            if not self.validate() or not other.validate():
                raise ValueError("Invalid data_selection.")
            all_axes = list(set(self.axes() + other.axes()))  # Get all axes with an explicit data_selection
            new_selection = {}
            if -1 in all_axes or len(all_axes) > 1:  # If at least one of the selections defines a global data_selection
                # Create new collapsed selections and merge them with or
                left_hand = self.__collapse_selection__(selection_dict=self.selections,
                                                        data_shape=self.data_object.shape,
                                                        global_only=False)[-1]
                right_hand = self.__collapse_selection__(selection_dict=other.selections,
                                                         data_shape=self.data_object.shape,
                                                         global_only=False)[-1]
                new_selection = {-1: left_hand | right_hand}
            elif len(all_axes) == 0:  # Both selections select all data
                new_selection = {}  # Select everything
            elif len(all_axes) == 1:  # One or both selections define a data_selection on a single dimension
                axis = all_axes[0]
                if axis in self.axes() and axis in other.axes():
                    # Create a new data_selection where the two selections are merged
                    new_selection = {axis: self.selections[axis] | other.selections[axis]}
                else:
                    # One defines a data_selection for the axis and the other selects everything
                    new_selection = {}  # Select everything
            return DataSelection(selections=new_selection,
                                 data_object=self.data_object,
                                 simplify=False,
                                 collapse=False)
        else:
            raise ValueError('OR operations not supported for object other than DataSelections')

    def __xor__(self, other):
        """
        Return a new data data_selection that defines an XOR between the two selections.
        NOTE: If an data_selection does not define a data_selection for an axis, then it is
        interpreted as having an ALL data_selection, i.e., [1,1,1,...1]. As such for
        axes for which only one side defines an explicit data_selection, the operation
        equivalent ot an invert/negation of the data_selection that exists.
        NOTE: Due to the complexity of the XOR this operation typically result in a global
        -1 axis data_selection in order to be able to represent the resulting data_selection,
        even if the inputs are all just per-axis selections. Only in the case where
        both selections define just a single data_selection on the same axis do we keep
        a simple data_selection. Global selections may require large memory as we need
        to store one bool for each element in the dataset.
        """
        if isinstance(other, DataSelection):
            # Check that the data_selection shapes match
            if self.data_object.shape != other.data_object.shape:
                raise AttributeError("Axis mismatch between data objects the data_selection is applied to.")
            if not self.validate() or not other.validate():
                raise ValueError("Invalid data_selection.")
            # Get all axes for which we have selections
            all_axes = list(set(self.axes() + other.axes()))
            if (len(all_axes) > 1) or (-1 in all_axes):
                left_hand = self.__collapse_selection__(selection_dict=self.selections,
                                                        data_shape=self.data_object.shape,
                                                        global_only=False)[-1]
                right_hand = self.__collapse_selection__(selection_dict=other.selections,
                                                         data_shape=self.data_object.shape,
                                                         global_only=False)[-1]
                new_selection = {-1: left_hand ^ right_hand}
            elif len(all_axes) == 1:
                new_selection = {}
                axis = all_axes[0]
                if axis in self.axes() and axis in other.axes():
                    new_selection[axis] = self.selections[axis] ^ other.selections[axis]
                elif axis in self.axes():
                    new_selection[axis] = ~self.selections[axis]
                elif axis in other.axes():
                    new_selection[axis] = ~other.selections[axis]
            else:  # i.e. we do not have any explicit data_selection specified.
                # Create a new data_selection that selects nothing
                data_shape = self.data_object.shape
                num_axes = len(data_shape)
                new_selection = {axis: np.zeros(data_shape[axis], dtype='bool') for axis in range(len(num_axes))}
            return DataSelection(selections=new_selection,
                                 data_object=self.data_object,
                                 simplify=False,
                                 collapse=False)
        else:
            raise ValueError('XOR operations not supported for object other than DataSelections')

    def __rshift__(self, other):
        """
        `a >> b` : Identify if a follows b.

        This checks for all selections applied whether all elements selected by `a` appear
        after the elements selected by `b`. This means both `a >> b` and `b >> a` can be
        False at the same time in the case that either a and b overlap in their selection ranges.
        """
        lower_bounds_self, upper_bounds_self = self.axis_bounds()
        lower_bounds_other, upper_bounds_other = other.axis_bounds()
        all_axes = list(set(self.axes(restricted_only=True, bounds=(lower_bounds_self, upper_bounds_self)) +
                            other.axes(restricted_only=True, bounds=(lower_bounds_other, upper_bounds_other))))
        return np.all(lower_bounds_self[all_axes] > upper_bounds_other[all_axes])

    def __lshift__(self, other):
        """
        `a << b` : Identify if a precedes b.

        This checks for all selections applied whether all elements selected by `a` appear
        before the elements selected by `b`. This means both `a >> b` and `b >> a` can be
        False at the same time in the case that either a and b overlap in their selection ranges.
        """
        lower_bounds_self, upper_bounds_self = self.axis_bounds()
        lower_bounds_other, upper_bounds_other = other.axis_bounds()
        all_axes = list(set(self.axes(restricted_only=True, bounds=(lower_bounds_self, upper_bounds_self)) +
                            other.axes(restricted_only=True, bounds=(lower_bounds_other, upper_bounds_other))))
        return np.all(upper_bounds_self[all_axes] < lower_bounds_other[all_axes])

    def __invert__(self):
        """
        Return a new DataSelection object that inverts the current data_selection.

        NOTE: This may result in a complex, global data_selection. Only in the case where we have no or only
        have a single data_selection applied to one axes can we maintain a simple per-axis data_selection.
        Global selections may require large memory as we need to store
        one bool for each element in the dataset.

        NOTE: The data_selection of this data_selection will be simplified first in an attempt to reduce
        the number of axes selections used with the goal to keep a simple per-axis data_selection
        object if possible.
        """
        if not self.validate():
            raise ValueError("Invalid data_selection.")
        self.simplify_selection()
        num_axes = len(self.axes())
        new_selection = {}
        if num_axes == 0:
            # Define a new data_selection that selects no objects
            data_shape = self.data_object.shape
            new_selection = {axis: np.zeros(data_shape[axis], dtype='bool') for axis in range(len(num_axes))}
        if num_axes == 1:
            new_selection = {k: ~v for k, v in self.selections.iteritems()}  # The for loop is only 1 iteration
        else:
            # Define a new global data_selection that inverts the current data_selection
            new_selection.update(self.__collapse_selection__(selection_dict=self.selections,
                                                             data_shape=self.data_object.shape,
                                                             global_only=False))
            new_selection[-1] = ~(new_selection[-1])
        return DataSelection(selections=new_selection,
                             data_object=self.data_object,
                             simplify=False,
                             collapse=False)

    def __lt__(self, other):
        """
        Less than operator (<)
        """
        return self.__comparison_operator__(other=other,
                                            operator='lt')

    def __le__(self, other):
        """
        Less than or equals operator (<=)
        """
        return self.__comparison_operator__(other=other,
                                            operator='le')

    def __eq__(self, other):
        """
        Equals operator (==)
        """
        return self.__comparison_operator__(other=other,
                                            operator='eq')

    def __ne__(self, other):
        """
        Not equals operator (!=)
        """
        return self.__comparison_operator__(other=other,
                                            operator='ne')

    def __gt__(self, other):
        """
        Greater than operator (>)
        """
        return self.__comparison_operator__(other=other,
                                            operator='gt')

    def __ge__(self, other):
        """
        Greater than or equals operator (>=)
        """
        return self.__comparison_operator__(other=other,
                                            operator='ge')

    def __contains__(self, other):
        """
        Check whether a data_selection is a subset of the other data_selection.
        """
        return self.__comparison_operator__(other=other,
                                            operator='in')

    def __len__(self):
        """
        Get number of selected elements.
        """
        return self.count()

    def __comparison_operator__(self, other, operator):
        """
        Internal helper function used to implement a series of comparison operators.

        NOTE: These operations checks are not just comparisons of one selection selecting
              more or less items then the other, but are true overlap containment checks.
              E.g., a<b means that b must select all times that a selects and b must
              select additional items that a does not select. This also means that as
              soon as a selects a record that is not in b, a<b will be False.

        :param other: The other DataSelection object
        :type other: DataSelection
        :param operator: String indicating the comparison operator to be used, which include:
                         'in', 'ge' is >= , 'gt' is >, 'eq' is ==, 'ne is !=, 'le' is <=, 'lt' is <.
        :type: str or unicode

        """
        # Check if we can compare against the give object
        if isinstance(other, DataSelection):
            # Not equal ne is implemented by inverting the result of equals check
            if operator == 'ne':
                return not self.__comparison_operator__(other=other, operator='eq')

            # If we have a global selection in any of the two objects then we need to
            # create global selections for both of two and compare them directly
            if -1 in other.axes() or -1 in self.axes():
                my_selection = self.__collapse_selection__(selection_dict=self.selections,
                                                           data_shape=self.data_object.shape,
                                                           global_only=False)[-1]
                other_selection = self.__collapse_selection__(selection_dict=other.selections,
                                                              data_shape=self.data_object.shape,
                                                              global_only=False)[-1]
                my_count = my_selection.sum()
                other_count = other_selection.sum()
                combined_count = (my_selection & other_selection).sum()
                if operator == 'in' or operator == 'ge':
                    return combined_count == other_count
                elif operator == 'lt':
                    return (combined_count == my_count) and (my_count < other_count)
                elif operator == 'gt':
                    return (combined_count == other_count) and (my_count > other_count)
                elif operator == 'eq':
                    return (combined_count == other_count) and (my_count == other_count)
                elif operator == 'ne':  # This is handled separetely above
                    pass  # return not ((combined_count==other_count) and (my_count==other_count))
                elif operator == 'le':
                    return combined_count == my_count
                else:
                    raise ValueError("Unsupported comparison operator given.")
            # If we only have per-axis selections defined then we can check this by comparing
            # the selections individually without creating a large global selection
            else:
                # 1) Compute all axes used
                all_axes = list(set(self.axes() + other.axes()))

                # 2) If no selections are specified, ie., both are selecting everything, hence both are equal
                if len(all_axes) == 0:
                    return operator in ['eq', 'le', 'ge', 'in']

                # 2) Compute the result by comparing the selections of all axes
                results = np.zeros(len(all_axes), dtype='bool')
                logical_result = True
                for index, axis in enumerate(all_axes):
                    # 2.1) Contains and greater equal
                    if operator == 'in' or operator == 'ge':
                        if axis not in self.axes():
                            continue
                        elif axis not in other.axes():
                            if self.selections[axis].sum() < self.data_object.shape[axis]:
                                logical_result = False
                                break
                        else:
                            other_count = other.selections[axis].sum()
                            post_count = (self.selections[axis] & other.selections[axis]).sum()
                            if post_count != other_count:
                                logical_result = False
                                break
                    # 2.2 Less than
                    elif operator == 'lt':
                        if axis not in self.axes():
                            if other.selections[axis].sum() < self.data_object.shape[axis]:
                                logical_result = False
                                break
                        elif axis not in other.axes():
                            continue
                        else:
                            my_count = self.selections[axis].sum()
                            other_count = other.selections[axis].sum()
                            post_count = (self.selections[axis] & other.selections[axis]).sum()
                            if not ((post_count == my_count) and (my_count < other_count)):
                                logical_result = False
                                break
                    # 2.3 Greater than
                    elif operator == 'gt':
                        if axis not in self.axes():
                            if other.selections[axis].sum() < self.data_object.shape[axis]:
                                results[index] = True
                        elif axis not in other.axes():
                            if self.selections[axis].sum() < self.data_object.shape[axis]:
                                return False
                            else:
                                results[index] = False
                        else:
                            my_count = self.selections[axis].sum()
                            other_count = other.selections[axis].sum()
                            post_count = (self.selections[axis] & other.selections[axis]).sum()
                            if (my_count == other_count) and (post_count == other_count):
                                results[index] = False
                            elif post_count != other_count:
                                return False
                            elif (my_count > other_count) and (post_count == other_count):
                                results[index] = True
                            else:
                                results[index] = False
                    # 2.4 Equals
                    elif operator == 'eq':
                        if axis not in self.axes():
                            if other.selections[axis].sum() != self.data_object.shape[axis]:
                                logical_result = False
                                break
                        elif axis not in other.axes():
                            if self.selections[axis].sum() != self.data_object.shape[axis]:
                                logical_result = False
                                break
                        else:
                            my_count = self.selections[axis].sum()
                            other_count = other.selections[axis].sum()
                            post_count = (self.selections[axis] & other.selections[axis]).sum()
                            if not ((my_count == post_count) and (my_count == other_count)):
                                logical_result = False
                                break
                    # 2.5 Not equals. Nothing to be done here. Implemented above by negating the equals (eq, ==) check
                    elif operator == 'ne':
                        pass
                    # 2.6 Less equals
                    elif operator == 'le':
                        if axis not in self.axes():
                            if other.selections[axis].sum() != self.data_object.shape[axis]:
                                logical_result = False
                                break
                        elif axis not in other.axes():
                            pass
                        else:
                            my_count = self.selections[axis].sum()
                            post_count = (self.selections[axis] & other.selections[axis]).sum()
                            if not post_count == my_count:
                                logical_result = False
                                break
                    else:
                        raise ValueError("Unsupported comparison operator given.")
                if operator == 'gt':
                    logical_result = np.any(results)
                return logical_result
        else:
            raise ValueError('IN operations not supported for objects other than DataSelections')

    @staticmethod
    def __collapse_selection__(selection_dict, data_shape, global_only=True):
        """
        Internal helper function used to collapse a set of selections. A data_selection may be
        collapsed to a single global data_selection if a global data_selection is already present.

        :param selection_dict: DataSelection.selections dictionary describing for all
               axes (keys) the selected objects (values).
        :type selection_dict: dict
        :param data_shape: Tuple with the shape of the data to be selected
        :type data_shape: tuple
        :param global_only: Boolean indicating whether the data_selection should always be
               collapsed to a single global data_selection (False) or only in case that a
               global data_selection is already present (True). Default is True.
        :type global_only: bool
        """
        selected_axes = selection_dict.keys()
        num_axes = len(data_shape)
        if global_only and -1 not in selected_axes:
            return selection_dict
        else:
            if -1 in selected_axes:
                global_selection = selection_dict[-1]
            else:
                global_selection = np.ones(shape=data_shape,
                                           dtype='bool')
            for axis, select in selection_dict.iteritems():
                if axis >= 0:
                    expansion = [None for _ in range(num_axes)]
                    expansion[axis] = slice(None)
                    global_selection *= select[tuple(expansion)]
            return {-1: global_selection}

    def axis_bounds(self):
        """
        Compute the smallest and largest selected index along all axes.

        :returns: This function returns a tuple of two lists, one with the lower axis bounds and one with the
                  upper axis bounds.
        """
        lower_axis_bounds = np.zeros(shape=(len(self.data_object.shape), ), dtype='int64')  # Init lower bounds as 0
        upper_axis_bounds = np.asarray(self.data_object.shape) - 1  # Init the upper bounds as max index
        for axis, select in self.selections.iteritems():
            if axis >= 0:
                lower_axis_bounds[axis] = np.argmax(select)
                upper_axis_bounds[axis] = select.size - np.argmax(select[::-1])
        # If we have a global selection we need to compute the bounds for it and compare
        if -1 in self.axes():
            for axis in range(len(self.data_object.shape)):
                collapse_axis = tuple([i for i in range(len(self.data_object.shape)) if i != axis])
                numpy_version = np.version.version.split('.')  # Check numpy versions
                # Let numpy handle the collapse of the matrix if possible (numpy version >= 1.7 does this)
                if int(numpy_version[0]) > 1 or (int(numpy_version[0]) == 1 and int(numpy_version[1]) >= 7):
                    collapsed_selection = np.any(self.selections[-1], axis=tuple(collapse_axis))
                else:  # If the numpy version is too old then we'll do it ourselves
                    collapsed_selection = self.selections[-1]
                    for caxis in reversed(collapse_axis):
                        collapsed_selection = np.max(collapsed_selection, axis=caxis)
                current_lower_bound = np.argmax(collapsed_selection)
                current_upper_bound = collapsed_selection.size - np.argmax(collapsed_selection[::-1])
                if current_lower_bound > lower_axis_bounds[axis]:
                    lower_axis_bounds[axis] = current_lower_bound
                if current_upper_bound < upper_axis_bounds[axis]:
                    upper_axis_bounds[axis] = current_upper_bound
        return lower_axis_bounds, upper_axis_bounds

    def collapse_selection(self, global_only=True):
        """
        Collapse the data_selection to a single global data_selection.

        :param global_only: Boolean indicating whether the data_selection should always be
               collapsed to a single global data_selection (False) or only in case that a
               global data_selection is already present (True). Default is True.
        :type global_only: bool
        """
        self.selections = self.__collapse_selection__(selection_dict=self.selections,
                                                      data_shape=self.data_object.shape,
                                                      global_only=global_only)

    def simplify_selection(self, aggressive=False):
        """
        Simplify the data selection.

        Remove unnecessary entries from the data_selection dict, i.e., entries that
        select all elements for a given axis. If aggressive is set to True then
        further optimizations may be made. E.g.,if at least one selection for any given
        axis indicates that no values should be selected, then all other selection have
        no effect as no elements will be selected either way, hence, the selection can be
        simplified to a single empty selection. This is just an example, other optimizations
        may be taken as well.

        :param aggressive: If set to True the function will not just remove unnecessary
                           entries but further try to optimize the selection.


        """
        num_elements = np.asarray(self.data_object.shape).prod(axis=None)
        for axis, select in self.selections.iteritems():
            if axis >= 0:
                if select.sum() == self.data_object.shape[axis]:
                    self.selections.pop(axis, None)
            elif select.sum == num_elements:  # i.e., axis == -1
                self.selections.pop(axis, None)

        if aggressive:
            # Check if we have any selection that selects no values at all
            empty_selection = False
            for axis, select in self.selections.iteritems():
                if select.sum() == 0:
                    empty_selection = True
            # If we have an empty selection, then replace it with an optimized selection
            if empty_selection:
                self.selections = {}
                array_data_shape = np.asarray(self.data_object.shape)
                first_non_zero_axis = np.argmax(array_data_shape > 0)
                self[first_non_zero_axis, :] = False

    def axes(self, restricted_only=False, bounds=None):
        """
        Get the list of axes that are sub-selected by this selections.
        This may be an empty list.

        :param restricted_only: If set to true, then only axes that are actually
                                restricted by the selection are returned. This means
                                axes where we find selected values along the whole
                                axes are not included in the selection. This also
                                means, that -1 is removed from the axes and resolved
                                to identify the actually restricted axes.
        :param bounds: Optional input used to indicate the lower and upper bounds
                       selected along a given axis. If set to None, then the bounds
                       will be computed using self.axis_bounds.
        :type bounds: Tuple of two lists indicating the lower and upper bounds. Same as
                      output of self.axis_bounds

        :returns: List of integers with the axes indices.
        """
        if restricted_only:
            all_axes = []
            for axis, selection in self.selections.iteritems():
                if axis >= 0 and not np.all(selection):
                    all_axes.append(axis)
                elif axis == -1:
                    if bounds is not None:
                        lower_bounds, upper_bounds = bounds
                    else:
                        lower_bounds, upper_bounds = self.axis_bounds()
                    for i in range(len(lower_bounds)):
                        if lower_bounds[i] > 0 and upper_bounds[i] < self.data_object.shape[axis]:
                            all_axes.append(i)

                elif axis < -1:
                    KeyError('Invalid axis index smaller -1 in list of selections.')
            return all_axes
        else:
            return self.selections.keys()

    def count(self):
        """
        Get the number of elements selected by the data_selection.
        Dimensions without an explicit data_selection are counted as
        having an ALL data_selection. This is equivalent to calling len() on
        the object.
        """
        axis_counts = self.counts()
        if -1 not in axis_counts:
            counts_array = np.asarray(axis_counts.values())
            return counts_array.prod(axis=None)

        else:
            new_selection = self. __collapse_selection__(selection_dict=self.selections,
                                                         data_shape=self.data_object.shape,
                                                         global_only=False)[-1]
            return new_selection.sum()

    def counts(self):
        """
        Get the number of elements selected by the individual parts of
        the selections defined in the self.selections object.
        Dimensions without an explicit data_selection are interpreted
        as having an ALL data_selection (i.e, the length of that axes
        is returned)

        :returns: Dictionary of {axis_index: count}. Axis -1 indicates
                  the presence of a global data_selection.

        """
        num_selected = {axis: size for axis, size in enumerate(self.data_object.shape)}
        for axis, select in self.selections.iteritems():
            num_selected[axis] = select.sum()
        return num_selected

    def validate(self):
        """
        Check whether the data_selection is valid.
        """
        data_shape = self.data_object.shape
        num_dims = len(data_shape)
        for axis, select in self.selections.iteritems():
            if axis >= num_dims:
                warnings.warn("Axis index out of range.")
                return False
            elif axis >= 0:
                if select.size != data_shape[axis]:
                    warnings.warn("Selection length does not match dimension size.")
                    return False
            elif axis == -1:
                if select.shape != data_shape:
                    warnings.warn("Selection shape does not match data shape.")
                    return False
            else:
                warnings.warn("Invalid axis index in data_selection.")
                return False
        return True

    def data(self):
        """
        Load the data associated with this DataSelection.
        """
        select_axes = self.axes()
        num_axes = len(select_axes)
        if num_axes == 0:  # No selections are specified. Load the full data.
            return self.data_object[:]
        elif -1 in select_axes:  # We have a global selection. Collapse all selection and do one global load.
            if num_axes > 1:
                select = self.__collapse_selection__(selection_dict=self.selections,
                                                     data_shape=self.data_object.shape,
                                                     global_only=False)[-1]
            else:
                select = self.selections[-1]
            try:
                return self.data_object[select]
            except UnboundLocalError:
                return np.empty(0)
        elif num_axes == 1:  # We have single axis selection. We can do this in one data load
            select = [slice(None) for _ in range(len(self.data_object.shape))]
            select[select_axes[0]] = self.selections[select_axes[0]]
            try:
                return self.data_object[tuple(select)]
            except UnboundLocalError:
                return np.empty(0)
        else:  # We have multiple single axes selections. h5py does not allow us to do this in one load.
            # Sort the data selections by selectivity
            import operator
            axis_counts = self.counts()
            # Compute the number of elements that will be loaded by each selection (if applied alone on the full data)
            selected_elements = {}
            for axis, count in axis_counts.iteritems():
                temp_shape = np.asarray(self.data_object.shape)
                temp_shape[axis] = count
                selected_elements[axis] = np.sum(temp_shape)
            # Sort the selection in increasing order, i.e., perform the data load that loads the fewest elements first
            sorted_axes = sorted(selected_elements.iteritems(), key=operator.itemgetter(1), reverse=False)
            select = [slice(None) for _ in range(len(self.data_object.shape))]
            smallest_axis = sorted_axes.pop(0)[0]
            select[smallest_axis] = self.selections[smallest_axis]
            data = self.data_object[tuple(select)]
            # Now that we have a numpy array (and no longer an h5py object) we can load the remaining data all at once
            select[smallest_axis] = slice(None)
            for axis in sorted_axes:
                select[axis[0]] = self.selections[axis[0]]
            return data[tuple(select)]

