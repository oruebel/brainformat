"""
Module with the base classes used for specification of HDF5 file formats and HDF5 object modules.

"""
import h5py
import os
import warnings
import json
import numpy as np

from brain.dataformat.spec import BaseSpec, RelationshipSpec, RelationshipTargetSpec


################################################
# ManagedObject                                #
################################################
class ManagedObject(object):
    """
    Abstract base class defining the base API for
    classes responsible for managing a specific
    hdf5 h5py.Group or h5py.Dataset object.

    Functions to be implemented by derived class:

        * get_managed_object_type : Overwrite in case that the derived class manages a dataset
        * get_format_specification : Overwrite to specify the format
        * populate: Overwrite to implement the creation of the object

    :ivar type_attribute_name: Name of the attribute used to store the information about the management class.
    :ivar description_attribute_name: Name of the attribute used to store the human-readable descriptions of
               the purpose and content of the class.
    :ivar object_id_attribute_name: Name of the attribute used to store an optional object id which may be
               used to reference the data in a persistent fashion (e.g. DOI associated with the given data object)
    :ivar hdf_object: The HDF5 object managed by the object
    :ivar ...: All attributes of the hdf_object set object are exposed  also via this class through
               the __getattr__ function and can be used as usual.

    """

    type_attribute_name = 'format_type'
    description_attribute_name = 'format_description'
    object_id_attribute_name = 'object_id'
    format_spec_attribute_name = 'format_specification'

    def __init__(self, hdf_object):
        """
        Initialize the management object.

        :param hdf_object: The h5py.Group, h5py.File, or h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        self.hdf_object = hdf_object
        if self.get_managed_object_type() == 'group' and not isinstance(self.hdf_object, h5py.Group):
            raise ValueError('The given hdf_object is not a group as expected.')
        if self.get_managed_object_type() == 'dataset' and not isinstance(self.hdf_object, h5py.Dataset):
            raise ValueError('The given hdf_object is not a dataset as expected.')
        if self.get_managed_object_type() == 'file' and not isinstance(self.hdf_object, h5py.File):
            raise ValueError('The given hdf_object is not a file as expected.')

    def __getattr__(self, item):
        try:
            return super(ManagedObject, self).__getattribute__(item)
        except AttributeError:
            attrobj = getattr(self.hdf_object, item)
            managed_attrobj = None
            if item != 'filename':
                try:
                    managed_attrobj = self.get_managed_object(attrobj)
                except (ValueError, NameError):
                    pass
            if managed_attrobj is not None:
                return managed_attrobj
            else:
                return attrobj

    def get_h5py(self):
        """
        Get the h5py object managed. This a convenience function that
        simply returns self.hdf_object.
        """
        return self.hdf_object

    def get_filename(self, absolute_path=False):
        """
        Get the name of the file containing the managed object.
        Alternatively we may also use self.file.filename

        :param absolute_path: Set to True to retrieve an absolute path
        :type absolute_path: bool
        """
        if absolute_path:
            return os.path.abspath(self.hdf_object.file.filename)
        else:
            return self.hdf_object.file.filename

    def close(self):
        """
        Close the HDF5 file associated with the managed object.
        """
        if self.hdf_object is not None:
            self.hdf_object.file.close()

    @classmethod
    def create(cls,
               parent_object,
               object_id=None,
               dataset_args=None,
               force_creation=False,
               external=False,
               object_name=None,
               **kwargs):
        """
        Create new managed object in the given parent group.
        The type of object created is decided by the get_managed_object_type(...) f
        function defined implemented by derived classes (default is 'group'). The
        The functions creates the new object and assigns common attributes. It then
        creates the manager object and calls the corresponding populate(...) function
        to initialize the new object.

        **NOTE** See the populate method of the corresponding derived class for details on the
        additional keyword arguments.

        :param parent_object: The h5py.Group, h5py.File parent object in which the managed object should be created.
                              or the filename in case that the managed object is a file that needs to be created.
                              This may also be a ManagedObject for which the corresponding h5py object
                              will be used to construct the object.
        :type parent_object: h5py.Group or String
        :param object_id: The object id to be used. This should be a unique identifier to allow users to
                          understand the origin of the data and to relate data with each other.
        :param dataset_args: In case the managed object is a 'dataset' (see get_managed_object_type),
                       then this dict argument can be used to specify additional arguments for the
                       h5py.Group.require_dataset function.
        :param force_creation: Force the creation of the managed object as part of the parent object, even if the
                       parent object does not specify that the managed object to be created is part of its
                       specification. (Default value: False)
        :type force_creation: bool
        :param external: Boolean indicating whether the ManagedObject should be created in an external
                       file and linked to from the parent object (True) or whether the object should be
                       created within the parent directly (False). Default value is False, i.e, the object
                       is stored as part of the parent directly without creating a new file.
                       **NOTE** The external option has not effect when the the object to be created is a file,
                       i.e,  `get_managed_object_type(...)` returns file. The behavior in this case is as follows:
                       **1)** If the object to be created is `file` and the `parent_object` is a `string`, then the file
                       will be created as usual and the value of the external option has no effect.
                       **2)** If the object to be created is a `file` and the `parent_object` is a `h5py.Group`, then
                       the file will be created externally using a filename automatically determined based on the
                       name of the parent and the format specification and an external link to the root group of the
                       new file will be created within the given parent, i.e, the behavior is as if external is True.
        :type external: bool
        :param object_name: In cases where no naming conventing is prescribed by the format we can provide a name
                       for the object.
        :param kwargs: Additional keyword arguments for the populate function implemented in the
                       derived classes (see in derived classes for details).

        :returns: Instance of the derived ManagedObject responsible for management of
                  the newly create h5py.Group or h5py.Dataset object.

        :raises: ValueError is raised in case that a conflicting object should exist, an illegal type is encountered,
                 or if the creation of the object is not explicitly specified as permitted in the specification.
        """
        # 1) Get the specification for the object
        spec = cls.get_format_specification()

        # 2) Check if the new object is allowed to be part of the parent object
        if not force_creation:  # Do not check format compliance if the user enforces the creation of the object
            if not isinstance(parent_object, basestring):
                parent_managed_object = cls.get_managed_object(parent_object)
                if isinstance(parent_managed_object, ManagedObject):
                    parent_spec = parent_managed_object.get_format_specification()
                    allowed_addition = False
                    if "ManagedObject" in parent_spec['managed_objects']:
                        allowed_addition = True
                    else:
                        for mo in parent_spec['managed_objects']:
                            if cls.__name__ == mo['format_type']:
                                allowed_addition = True
                                break
                    if not allowed_addition:
                        raise ValueError("The given parent object does not specify that: " +
                                         cls.__name__ + " as and allowed type.")
                else:
                    warnings.warn('Skipping verification of format compliance. The given parent object is not managed.')
            elif isinstance(parent_object, basestring) and cls.get_managed_object_type() == 'file':
                if 'file_prefix' in spec and spec['file_prefix'] is not None:
                    if not os.path.basename(parent_object).startswith(spec['file_prefix']):
                        raise ValueError("Filename %s not compliant with specification. Expected prefix: %s " %
                                         (os.path.basename(parent_object), spec['file_prefix']))
                if 'file_extension' in spec and spec['file_extension'] is not None:
                    if not os.path.basename(parent_object).endswith(spec['file_extension']):
                        raise ValueError("Filename %s not compliant with specification. Expected extension: %s " %
                                         (os.path.basename(parent_object), spec['file_prefix']))

        # 3) Set the parent_object variable, i.e.,  determine the h5py object to be used to construct the object.
        #    NOTE: A new file (ie., h5py.File) may be created for the parent_object if necessary.
        # 3.1) If the parent object is a managed object, then get the corresponding h5py object
        if isinstance(parent_object, ManagedObject):
            parent_object = parent_object.hdf_object
        # 3.2) If the parent object is a dataset, then the parent object to the group object it is in
        if isinstance(parent_object, h5py.Dataset):
            parent_object = parent_object.parent
        # 3.3) If the parent object is an HDF5 File, Group, or String
        if isinstance(parent_object, h5py.File):
            parent_object = parent_object['/']
        elif isinstance(parent_object, h5py.Group):
            pass  # Nothing to be done, we already have the correct parent object
        elif isinstance(parent_object, basestring) and external:
            if cls.get_managed_object_type() != 'file':
                parent_object = h5py.File(parent_object, 'a')   # Create the parent file object
        elif isinstance(parent_object, basestring) and not external and cls.get_managed_object_type() == 'file':
            pass
        else:
            raise ValueError('Cannot create object. Incorrect parent type %s' % str(type(parent_object)))

        # 4) the the object_name variable, i.e, determine the name of the object to be created
        # 4.1) We are a group so we need to determine the object name from the 'group'
        #      or 'prefix' key in the specification
        if cls.get_managed_object_type() == 'group':
            if object_name is None: # Create the group name
                object_name = spec['group']
                if object_name is None:
                    object_prefix = spec['prefix']
                    object_index = len(cls.get_all(parent_group=parent_object))
                    object_name = object_prefix + str(object_index)
            else:  # Check that the group name is valid
                if spec['group'] is not None:
                    if object_name != spec['group'] and not force_creation:
                        raise ValueError("The given object_name does not match the object's specification")
                if spec['prefix'] is not None:
                    if not object_name.startswith(spec['prefix']) and not force_creation:
                        raise ValueError("The given object_name does not match the object's specification")
        # 4.2) We are a dataset so we need to determine the object name from the 'dataset' or 'prefix'
        #      key in the specification
        elif cls.get_managed_object_type() == 'dataset':
            if object_name is None: # Create the dataset name
                object_name = spec['dataset']
                if object_name is None:
                    object_prefix = spec['prefix']
                    object_index = len(cls.get_all(parent_group=parent_object))
                    object_name = object_prefix + str(object_index)
            else:  # Validate that the name is valid
                if spec['dataset'] is not None:
                    if object_name != spec['dataset'] and not force_creation:
                        raise ValueError("The given object_name does not match the object's specification")
                if spec['prefix'] is not None:
                    if not object_name.startswith(spec['prefix']) and not force_creation:
                        raise ValueError("The given object_name does not match the object's specification")

        # 4.3) We are a file so we need to determine the filename either from the parent_object string or
        #      determine the name based on the group or prefix key
        elif cls.get_managed_object_type() == 'file':
            # 4.3.1) If the parent_object is a string then ignore the external option simply use the parent_object as
            #        the object_name
            if isinstance(parent_object, basestring):
                object_name = parent_object
            # 4.3.2) We do not have a name given for the managed file object, but the parent_object is group, so
            #        we need to construct a name for the file.
            else:
                object_name = parent_object.file.filename
                if spec['group'] is None and spec['prefix'] is None:
                    raise ValueError("The ManagedObject " + str(cls) + " does not support linking. " +
                                     "Missing group/prefix in specification")
                if spec['group'] is None:
                    object_prefix = spec['prefix']
                    object_index = len(cls.get_all(parent_group=parent_object))
                    object_name += object_prefix + str(object_index)
                else:
                    object_name += spec['group']
        # 4.4: TODO Make sure that the object does not already exist!!!

        # 5) Check if we need to create a new file for external storage
        external_parent = None
        external_filename = None
        if external:
            # 5.1) We are asked to create a managed object of type 'file'. Ignore the external option.
            if cls.get_managed_object_type() == 'file' and isinstance(parent_object, basestring):
                pass
            # 5.2) We were told to create the managed object externally and all we were given was a string
            #      to indicate the name of the file to be used. I.e., we need to create the ManagedObjectFile.
            elif isinstance(parent_object, basestring):
                # 5.2.2) Open the parent file if it exists
                if os.path.exists(parent_object):
                    try:
                        parent_file = h5py.File(parent_object, mode='a')
                    except IOError as e:
                        raise ValueError("Parent file object could not be opened due to an IOError " + str(e))
                else:  # 5.2.1) Create a new parent file
                    parent_file = ManagedObjectFile.create(parent_object=parent_object,
                                                           mode='a',
                                                           external=False)
                parent_object = parent_file["/"]
            else:
                external_filename = os.path.abspath(parent_object.file.filename).rstrip('.h5')
                external_filename += parent_object.name.replace("/", "__")
                external_filename += "__" + object_name + ".h5"
                external_parent_file = ManagedObjectFile.create(parent_object=external_filename,
                                                                mode='a',
                                                                external=False)
                external_parent = external_parent_file.get_h5py()

        # 6) Create the h5py.Group, Dataset, File object for the managed object
        # 6.1) Create the group
        if cls.get_managed_object_type() == 'group':
            # 6.1.1) Create the group in the external file container and add an external link in the parent
            if external_parent:
                hdfobj = external_parent.require_group(name=object_name)
                parent_object[object_name] = h5py.ExternalLink(os.path.basename(external_filename),
                                                               "/"+object_name)
            # 6.1.2) Create the group directly in the parent object
            else:
                hdfobj = parent_object.require_group(name=object_name)
        # 6.2) Create the dataset
        elif cls.get_managed_object_type() == 'dataset':
            # 6.1.2) Create the dataset in the external file container and add an external link in the parent
            if external_parent:
                hdfobj = external_parent.require_dataset(name=object_name, **dataset_args)
                parent_object[object_name] = h5py.ExternalLink(os.path.basename(external_filename),
                                                               "/"+object_name)
            # 6.1.3) Create the dataset directly in the parent object
            else:
                if 'data' in dataset_args:
                    hdfobj = parent_object.create_dataset(name=object_name, **dataset_args)
                else:
                    hdfobj = parent_object.require_dataset(name=object_name, **dataset_args)
        # 6.3) Create the file
        elif cls.get_managed_object_type() == 'file':
            # 6.3.1) Create the new manged file object
            hdfobj = h5py.File(object_name, 'a')
            # 6.3.2) Create an external link to files root if needed.
            if not isinstance(parent_object, basestring):  # I.e, the parent_object is an h5py.Group object
                if spec['group'] is None and spec['prefix'] is None:
                    raise ValueError("The ManagedObject " + str(cls) +
                                     " does not support linking. Missing group/prefix in specification")
                object_name = spec['group']
                if object_name is None:
                    object_prefix = spec['prefix']
                    object_index = len(cls.get_all(parent_group=parent_object))
                    object_name = object_prefix + str(object_index)
                parent_object[object_name] = h5py.ExternalLink(os.path.basename(hdfobj.filename), "/")
            else:
                pass

        else:
            raise ValueError("The object type specified is not supported as managed object. " +
                             "Allowed types: 'group', 'dataset'")

        # 7) Set the required attributes
        # 7.1 Set the type attribute
        hdfobj.attrs[cls.type_attribute_name] = cls.__name__
        # 7.2) Set the object is attribute
        if object_id is not None:
            hdfobj.attrs[cls.object_id_attribute_name] = object_id
        # 7.3) Create the description attribute for the object (this should always be
        #      available if the specification if complete)
        if 'description' in spec:
            hdfobj.attrs[cls.description_attribute_name] = spec['description']
        # 7.4) Set the format specification attribute
        hdfobj.attrs[cls.format_spec_attribute_name] = json.dumps(spec)

        # 8) Create the manager object for the new HDF5 object and populate it
        # 8.1) Get an instance of the manager class
        managerobj = cls(hdf_object=hdfobj)
        # 8.2) Call the populate function of the object
        try:
            managerobj.populate(**kwargs)
        except ValueError:
            # An error occurred while creating the object. Clean up.
            if cls.get_managed_object_type() == 'file':
                warnings.warn('An error occurred while creating the file. Removing the file.')
                os.remove(unicode(parent_object))
            else:
                try:
                    del parent_object[object_name]
                    warnings.warn('An error occurred while populating the group/dataset. ' +
                                  'Removed the object from file.')
                except:
                    warnings.warn('An error occurred while populating the group/dataset. ' +
                                  'Removal of the object from file failed.')
            raise

        # 9) Return the populated manager object
        return managerobj

    @classmethod
    def get_all(cls, parent_group, get_h5py=True):
        """
        Get all objects that are managed by the current class contained in the parent group.

        :param parent_group: The h5py.Group, ManagedGroup, or ManagedFile
                             object for which all objects managed by the current class
                             should be retrieved.
        :type parent_group: h5py.Group, ManagedGroup, or ManagedFile
        :param get_h5py: Boolean indicating whether we want to get the h5py objects (True) or the
                         instances of the manager class for the objects (False). Default is True.
        :type get_h5py: bool

        :returns: List of managed h5py.Group, h5py.Dataset objects managed by the current class or
                  list of ManagedObject instances responsible for managing the found groups/datasets.

        """
        # Get the true h5py.Group object from the ManagedObject
        if isinstance(parent_group, ManagedGroup):
            parent_group = parent_group.hdf_object
        elif isinstance(parent_group, ManagedFile):
            parent_group = parent_group.hdf_object['/']

        # Compute the list of managed objects
        managed_objects = []
        if isinstance(parent_group, h5py.Group):
            for curr_name, curr_obj in parent_group.items():
                temp_file = None
                # Try to resolve external links, the h5py items function does not automatically resolve external links
                if curr_obj is None:
                    try:
                        curr_obj = parent_group[curr_name]
                    except (KeyError, IOError):
                        temp_link = parent_group.get(curr_name, getlink=True, getclass=False)
                        if isinstance(temp_link, h5py.ExternalLink):
                            warnings.warn('Cannot resolve ExternalLink for: %(filename)s:%(path)s.' %
                                          {'filename': temp_link.filename, 'path': temp_link.path} +
                                          ' The link may be broken (e.g,. the file no longer exists' +
                                          ' or the file may already be open, preventing h5py from' +
                                          ' resolving the link.')
                        else:
                            warnings.warn('Cannot open %s' % curr_name)
                        del temp_link
                    # if curr_obj is None:
                    #     temp_link = parent_group.get(curr_name, getlink=True, getclass=False)
                    #     if isinstance(temp_link, h5py.ExternalLink):
                    #         try:
                    #             temp_file = h5py.File(temp_link.filename, 'r')
                    #             curr_obj = temp_file[temp_link.path]
                    #         except IOError:
                    #             warnings.warn('Cannot resolve ExternalLink for: %(filename)s:%(path)s.' %
                    #                           {'filename': temp_link.filename, 'path': temp_link.path} +
                    #                           ' The link may be broken (e.g,. the file no longer exists' +
                    #                           ' or the file may already be open, preventing h5py from' +
                    #                           ' resolving the link.')
                # If we were able to get the object, then check its type and add it to the list if necessary
                if curr_obj is not None:
                    if cls.is_managed_by(curr_obj):
                        managed_objects.append(curr_obj)
                # Close any file we may have opened
                if temp_file is not None:
                    temp_file.close()
        else:
            warnings.warn("ManagedObject.get_all parent_group parameter does not identify a hdf5 group.")
        # Either return the list of h5py objects
        if get_h5py:
            return managed_objects
        # Or generate a list of the instances of ManagedObjects responsible for the found objects
        else:
            return [cls.get_managed_object(h5py_object) for h5py_object in managed_objects]

    @classmethod
    def is_managed_by(cls, hdf_object):
        """
        Check whether the given hdf_object is managed by the current class.

        :param hdf_object: The hdf5 object to be checked

        :returns: Boolean indicating whether the object is managed by the current class.
        """
        # Check if the object is managed
        if not cls.is_managed(hdf_object):
            return False
        if isinstance(hdf_object, ManagedObject):
            hdf_object = hdf_object.get_h5py()
        # Check if the hdf_object attribute matches our class
        if hdf_object.attrs[cls.type_attribute_name] == cls.__name__:
            # Check whether the prefix is correct. If not, then warn the user.
            objectname = os.path.basename(hdf_object.name)
            spec = cls.get_format_specification()
            prefix = spec['prefix']
            if prefix is not None:
                if not objectname.startswith(prefix):
                    warnings.warn('Managed object %s  managed by %s has incorrect prefix' %
                                  (hdf_object.name, cls.__name__))
            elif 'group' in spec:
                if objectname != spec['group']:
                    warnings.warn('Managed group %s managed by %s has incorrect name' %
                                  (hdf_object.name, cls.__name__))
            elif 'dataset' in spec:
                if objectname != spec['dataset']:
                    warnings.warn('Managed dataset %s managed by %s has incorrect name' %
                                  (hdf_object.name, cls.__name__))
            return True
        else:
            return False

    @classmethod
    def is_managed(cls, hdf_object):
        """
        Check whether the given hdf object is managed by a brain file API class.

        :param hdf_object: The hdf5 object to be checked (dataset or group)

        :returns: Boolean indicating whether the given hdf5 object is managed or not.
        """
        if isinstance(hdf_object, ManagedObject):
            hdf_object = hdf_object.get_h5py()
        return cls.type_attribute_name in hdf_object.attrs.keys()

    @classmethod
    def get_managed_object(cls, hdf_object):
        """
        Get an instance of the ManagedObject class for
        managing the given hdf_object. None is returned in case
        that the given hdf_object is not a managed object.

        :param hdf_object: The h5py.Dataset, h5py.File, or h5py.Group for which the
               corresponding instance of the relevant derived class of
               ManagedObject should be generated. If a ManagedObject
               instance is provided as input, then the hdf_object will be returned.
               **NOTE**: The behavior fo the function is undefined for objects
               other then the mentioned h5py data objects.
               **NOTE**: A string to a valid HDF5 data file is allowed for convenience, in
               which case the file will be opened in append `mode=a` if possible and if that
               fails in read-only mode 'r'. A warning will be issued if the file could only
               be opened in read-only mode.

        :returns: Instance of ManagedObject or None in case the
                  object is not managed or the manager object cannot be constructed.
        :raises NameError: In case that the indicated format_type class was not found.
        :raises ValueError: In case that an invalid hdf_object is given

        """
        import types
        # 1) If the given hdf_object is already a ManagedObject then simply return it
        if isinstance(hdf_object, ManagedObject):
            return hdf_object

        # 2) If we have a string give, then we assume it is a file path and we try to open the file
        if isinstance(hdf_object, basestring):
            if os.path.isfile(hdf_object):
                try:
                    hdf_object = h5py.File(hdf_object, mode='a')
                except IOError:
                    try:
                        hdf_object = h5py.File(hdf_object, mode='r')
                        warnings.warn("File could only be opened in read-only mode.")
                    except IOError as e:
                        warnings.warn("The file could not be opened or did not identify a valid HDF5 file" + str(e))
        # We could not resolve the string
        if isinstance(hdf_object, basestring):
            raise ValueError("The input hdf_object did not identify a valid HDF5 object.")

        # 3) Construct the ManagedObject for the given h5py object

        try:
            format_type = hdf_object.attrs[cls.type_attribute_name]
        except AttributeError:
            return None
        except KeyError:
            return None

        brainformat_class = cls.get_manager_class(format_type)

        if brainformat_class is not None and isinstance(brainformat_class, (types.ClassType, types.TypeType)):
            brainformat_object = brainformat_class(hdf_object=hdf_object)
            return brainformat_object
        else:
            return None

    @classmethod
    def get_manager_class(cls, format_type):
        """
        Based on the name of the class, get the Python Class object for the manager class
        :param format_type: Name of the class specified in the type attribute of the format. (cls.__name_)
        :return: Class object or None

        :raises NameError if the given class could not be found
        """
        import sys
        try:
            import brain
            derived_formats = brain.dataformat.get_derived_formats()
            format_module = None
            for curr_format in derived_formats:
                if format_type.startswith(curr_format):
                    ftype = format_type.split(".")[-1]
                    format_module = format_type.rstrip("."+ftype)
                    format_type = ftype
            # Get the class that corresponds to the given name
            try:  # Try to get from our namespace
                brainformat_class = getattr(sys.modules[__name__], format_type)
            except AttributeError:  # Try to get from the module namespace
                try:
                    brainformat_class = getattr(sys.modules[cls.__module__], format_type)
                except AttributeError:  # Try to import from derived modules
                    import importlib
                    # Check the format_module that has been indicated first (if once has been set)
                    brainformat_class = None
                    if format_module is not None:
                        try:
                            currlib = importlib.import_module(format_type)
                            brainformat_class = getattr(currlib, format_type)
                        except AttributeError:
                            pass
                        except ImportError:
                            pass
                    else:  # We don't have an explicit format specification so check all that are registered
                        for curr_format in derived_formats:
                            currlib = importlib.import_module(curr_format)
                            try:
                                brainformat_class = getattr(currlib, format_type)
                                break
                            except AttributeError:
                                pass
            return brainformat_class
        except AttributeError:
            raise NameError(format_type+" doesn't exist or is not a class.")

    def check_format_compliance(self, current_only=True):
        """
        Check if the HDF5 object assigned to the given instance
        of a subclass of ManagedObject is compliant with
        the corresponding format specification. Warnings are raised
        for every non-format compliance found. Use e.g,
        "with warnings.catch_warnings(record=True) as w:"
        when calling the function to record all warnings.

        :param current_only: If current only is set then the current object is validated.
                             Set to false in order to force validation of all objects of the
                             current type contained in the parent group of the current
                             object. The current object is self.hdf_object.

        :returns: Boolean indicating whether the hdf object is compliant with the format.
        """
        # Check if all required attributes are present
        attrs_keys = self.hdf_object.attrs.keys()
        compliance = self.type_attribute_name in attrs_keys
        compliance &= self.description_attribute_name in attrs_keys
        compliance &= self.format_spec_attribute_name in attrs_keys
        compliance &= self.format_spec_attribute_name in attrs_keys
        if not compliance:
            warnings.warn('BrainFormat type attribute %s missing' % self.type_attribute_name)
            compliance = False

        # A file is validated much in the same way a group is
        if self.get_managed_object_type() == 'group':
            if current_only:
                compliance &= self.check_group_format_compliance_single(group_spec=self.get_format_specification(),
                                                                        hdf_group=self.hdf_object)
            else:
                compliance &= self.check_group_format_compliance_contained(group_spec=self.get_format_specification(),
                                                                           parent_group=self.hdf_object.parent)
        elif self.get_managed_object_type() == 'dataset':
            if current_only:
                compliance &= self.check_dataset_format_compliance_single(dataset_spec=self.get_format_specification(),
                                                                          hdf_dataset=self.hdf_object)
            else:
                compliance &= self.check_dataset_format_compliance_contained(dataset_spec=self.get_format_specification(),
                                                                             parent_group=self.hdf_object.parent)
        elif self.get_managed_object_type() == 'file':
            file_prefix = self.get_format_specification()['file_prefix']
            file_extension = self.get_format_specification()['file_extension']
            if file_prefix is not None:
                if not os.path.basename(self.hdf_object.file.filename).startswith(file_prefix):
                    warnings.warn('Filename %s does not start with expected prefix: %s' %
                                  (os.path.basename(self.hdf_object.file.filename), str(file_prefix)))
                    compliance = False
            if file_extension is not None:
                if not os.path.basename(self.hdf_object.file.filename).endswith(file_extension):
                    warnings.warn('Filename %s does not end with expected extension: %s' %
                                  (os.path.basename(self.hdf_object.file.filename), str(file_extension)))
                    compliance = False
            # A file is essentially validated in the same way a group is
            compliance &= self.check_group_format_compliance_contained(group_spec=self.get_format_specification(),
                                                                       parent_group=self.hdf_object)
        else:
            raise ValueError("The object type specified is not supported as managed object. " +
                             "Allowed types: 'group', 'dataset'")
        return compliance

    @classmethod
    def check_group_format_compliance_single(cls, group_spec, hdf_group):
        """
        Internal helper function used to check a given hdf_group
        against a given group specification. Warnings
        are raised for every non-format compliance found.
        This function used the check_format_compliance(..) function.

        :param group_spec: The group specification
        :param hdf_group: Th group object to be validated against the specification.

        :returns: Boolean indicating compliance.
        """
        # Check if the specification of the group is basically compliant
        compliance = True
        # For file specs we allow the group and prefix to be both None. Essentially indicating
        # that the file should not be externally linked to
        is_file_spec = 'file_prefix' in group_spec or 'file_extension' in group_spec
        if group_spec['group'] is not None and group_spec['prefix'] is not None:
            warnings.warn('ILLEGAL FORMAT SPEC: Only one of group or prefix may be given for group specifications')
            compliance = False
        elif group_spec['group'] is None and group_spec['prefix'] is None:
            if not is_file_spec:
                warnings.warn('ILLEGAL FORMAT SPEC: Either group or prefix must be set for group specification')
                compliance = False
        if is_file_spec and ('file_prefix' not in group_spec or 'file_extension' not in group_spec):
            warnings.warn('ILLEGAL FORMAT SPEC: Both file_prefix and file_extension must be in file specification.')
            compliance = False
        if not compliance:
            return compliance
        return cls.check_group_format_compliance(group_spec=group_spec,
                                                 group_list=[hdf_group])

    @classmethod
    def check_group_format_compliance_contained(cls, group_spec, parent_group):
        """
        Internal helper function used to check all relevant groups in a
        parent object against a given group specification. Warnings
        are raised for every non-format compliance found. This function uses the
        check_format_compliance(..) function.

        :param group_spec: The group specification
        :param parent_group: The parent object that should contain one or more
               object with the given group specification.
        :type parent_group: h5py.Group or h5py.File

        :returns: Boolean indicating compliance.
        """
        group_object_keys = parent_group.keys()
        compliance = True

        # Check if the specification of the group is basically compliant
        is_file_spec = 'file_prefix' in group_spec or 'file_extension' in group_spec
        if group_spec['group'] is not None and group_spec['prefix'] is not None:
            warnings.warn('ILLEGAL FORMAT SPEC: Only one of group or prefix may be given for group specifications')
            compliance = False
        elif group_spec['group'] is None and group_spec['prefix'] is None:
            if not is_file_spec:
                warnings.warn('ILLEGAL FORMAT SPEC: Either group or prefix must be set for group specification')
                compliance = False
        if is_file_spec and 'file_prefix' not in group_spec or 'file_extension' not in group_spec:
            warnings.warn('ILLEGAL FORMAT SPEC: Both file_prefix and file_extension must be in file specification.')
            compliance = False
        if not compliance:
            return compliance

        # Find all relevant groups
        relevant_groups = []
        if group_spec['group'] is not None:
            try:
                relevant_groups.append(parent_group[group_spec['group']])
            except KeyError:
                pass
        elif group_spec['prefix'] is not None:
            for currkey in group_object_keys:
                # Check all groups with the given prefix that end in a number. Checking if
                # it ends in a number is to avoid false checks for different group types
                # for which the prefix is a subset of the other.
                if currkey.startswith(group_spec['prefix']) and currkey.lstrip(group_spec['prefix']).isdigit():
                    if isinstance(parent_group[currkey], h5py.Group):
                        relevant_groups.append(parent_group[currkey])

        # If we are checking a file-specification and the parent is a file, then we need to check its compliance as well
        if is_file_spec and isinstance(parent_group, h5py.File):
            relevant_groups.append(parent_group)

        # We did not find any relevant objects and the object was specified as mandatory
        if len(relevant_groups) == 0 and not group_spec['optional']:
            warnings.warn('Required group not found: name=%s, prefix=%s' %
                          (str(group_spec['group']), str(group_spec['prefix'])))
            compliance = False
        if not compliance:
            return compliance

        # Check the compliance of all relevant groups with the format
        return cls.check_group_format_compliance(group_spec=group_spec,
                                                 group_list=relevant_groups)

    @classmethod
    def check_group_format_compliance(cls, group_spec, group_list):
        """
        Internal helper function used to check all groups in a
        parent object against a given group specification. Warnings
        are raised for every non-format compliance found.

        :param group_spec: The group specification
        :param group_list: List of hdf group object to be validated against the given specification.

        :returns: Boolean indicating compliance.

        """
        compliance = True
        # Check all relevant group objects for format compliance
        for group_object in group_list:
            # Check compliance of all datasets
            if 'datasets' in group_spec:
                for object_key, object_spec in group_spec['datasets'].iteritems():
                    compliance &= cls.check_dataset_format_compliance_contained(dataset_spec=object_spec,
                                                                                parent_group=group_object)
            # Check compliance of all subgroups
            if 'groups' in group_spec:
                for object_key, object_spec in group_spec['groups'].iteritems():
                    compliance &= cls.check_group_format_compliance_contained(group_spec=object_spec,
                                                                              parent_group=group_object)
            # Check compliance of all attributes
            if 'attributes' in group_spec:
                for attr_spec in group_spec['attributes']:
                    compliance &= cls.check_attribute_format_compliance(attribute_spec=attr_spec,
                                                                        hdf_object=group_object)
            # Check compliance of all managed objects contained
            if 'managed_objects' in group_spec:
                for managed_spec in group_spec['managed_objects']:
                    compliance &= cls.check_contained_managed_objects_format_compliance(managed_spec=managed_spec,
                                                                                        parent_group=group_object)
        return compliance

    @classmethod
    def check_contained_managed_objects_format_compliance(cls, managed_spec, parent_group):
        """
        Internal helper function used to check all managed objects in a
        parent object against a given managed object specification. Warnings
        are raised for every non-format compliance found.

        :param managed_spec: The managed specification
        :param parent_group: The parent object that should contain one or more
               object with the given managed object specification.

        :returns: Boolean indicating compliance.

        """
        # Check if the dataset exists
        compliance = True
        # Find all relevant objects
        relevant_objects = []
        for curr_name, curr_obj in parent_group.items():
            # Try to resolve external links, the h5py items function does not automatically resolve external links
            if curr_obj is None:
                try:
                    curr_obj = parent_group[curr_name]
                except (IOError, KeyError):
                    temp_link = parent_group.get(curr_name, getlink=True, getclass=False)
                    if isinstance(temp_link, h5py.ExternalLink):
                        warnings.warn('Cannot resolve ExternalLink for: %(filename)s:%(path)s.' %
                                      {'filename': temp_link.filename, 'path': temp_link.path} +
                                      ' The link may be broken (e.g,. the file no longer exists' +
                                      ' or the file may already be open, preventing h5py from' +
                                      ' resolving the link.')
                    else:
                        warnings.warn('Cannot open object %s' % curr_name)
                    del temp_link
                    break
            try:
                if managed_spec['format_type'] == 'ManagedObject' or \
                        curr_obj.attrs[cls.type_attribute_name] == managed_spec['format_type']:
                    relevant_objects.append(curr_obj)
            except KeyError:
                pass
        if len(relevant_objects) == 0 and not managed_spec['optional']:
            warnings.warn('No required managed object of type %s found' % managed_spec['format_type'])
            compliance = False
        if not compliance:
            return compliance

        # Check if the managed object is compliant
        for hdf_object in relevant_objects:
            try:
                manager_object = cls.get_managed_object(hdf_object=hdf_object)
            except NameError:
                manager_object = None
                compliance = False
            if manager_object is not None:
                compliance &= manager_object.check_format_compliance()
            else:
                warnings.warn('Unable to instantiate manager object for hdf_object')
                compliance = False
        return compliance

    @classmethod
    def check_dataset_format_compliance_single(cls, dataset_spec, hdf_dataset):
        """
        Internal helper function used to check a given hdf dataset
        against a given dataset specification. Warnings
        are raised for every non-format compliance found.

        :param dataset_spec: The dataset specification
        :param hdf_dataset: The hdf dataset to be validated against the specification.

        :returns: Boolean indicating compliance.

        """
        compliance = True
        # Check if the specification of the group is basically compliant
        if dataset_spec['dataset'] is not None and dataset_spec['prefix'] is not None:
            warnings.warn('ILLEGAL FORMAT SPEC: Only one of dataset or prefix may be given for group specifications')
            compliance = False
        elif dataset_spec['dataset'] is None and dataset_spec['prefix'] is None:
            warnings.warn('ILLEGAL FORMAT SPEC: Either dataset or prefix must be set for group specification')
            compliance = False
        if not compliance:
            return compliance
        # Check format compliance for the dataset
        return cls.check_dataset_format_compliance(dataset_spec=dataset_spec,
                                                   dataset_list=[hdf_dataset])

    @classmethod
    def check_dataset_format_compliance_contained(cls, dataset_spec, parent_group):
        """
        Internal helper function used to check all relevant datasets in a
        parent object against a given dataset specification. Warnings
        are raised for every non-format compliance found.

        :param dataset_spec: The dataset specification
        :param parent_group: The parent object that should contain one or more
               object with the given dataset specification.

        :returns: Boolean indicating compliance.

        """
        # Check if the dataset exists
        group_object_keys = parent_group.keys()
        compliance = True

        # Check if the specification of the group is basically compliant
        if dataset_spec['dataset'] is not None and dataset_spec['prefix'] is not None:
            warnings.warn('ILLEGAL FORMAT SPEC: Only one of dataset or prefix may be given for group specifications')
            compliance = False
        elif dataset_spec['dataset'] is None and dataset_spec['prefix'] is None:
            warnings.warn('ILLEGAL FORMAT SPEC: Either dataset or prefix must be set for group specification')
            compliance = False
        if not compliance:
            return compliance

        # Find all relevant datasets
        relevant_datasets = []
        if dataset_spec['dataset'] is not None:
            try:
                relevant_datasets.append(parent_group[dataset_spec['dataset']])
            except KeyError:
                pass
        elif dataset_spec['prefix'] is not None:
            for currkey in group_object_keys:
                if currkey.startswith(dataset_spec['prefix']):
                    if isinstance(parent_group[currkey], h5py.Dataset):
                        relevant_datasets.append(parent_group[currkey])

        if len(relevant_datasets) == 0:
            if not dataset_spec['optional']:
                warning_str = 'Required dataset not found: name=%s, prefix=%s' % (str(dataset_spec['dataset']),
                                                                                  str(dataset_spec['prefix']))
                warnings.warn(warning_str)
                compliance = False
        if not compliance:
            return compliance

        # Check format compliance for all relevant datasets
        return cls.check_dataset_format_compliance(dataset_spec=dataset_spec,
                                                   dataset_list=relevant_datasets)

    @classmethod
    def check_dataset_format_compliance(cls, dataset_spec, dataset_list):
        """
        Internal helper function used to check all datasets in a given
        list against a given dataset specification. Warnings
        are raised for every non-format compliance found.

        :param dataset_spec: The dataset specification
        :param dataset_list: The list of datasets to be validated agains the specification..

        :returns: Boolean indicating compliance.

        """
        compliance = True
        # Check all relevant group objects for format compliance
        for curr_dataset in dataset_list:
            # Check if the dataset is required to have dimension scales and if those are compliant
            if 'dimensions' in dataset_spec:
                # Check if the number of dimensions is specified correctly
                expected_min_num_dims = -1
                for dim_spec in dataset_spec['dimensions']:
                    if dim_spec['axis'] > expected_min_num_dims:
                        expected_min_num_dims = dim_spec['axis']
                if expected_min_num_dims > -1:
                    expected_min_num_dims += 1  # The axis indices
                if expected_min_num_dims > len(curr_dataset.shape):
                    warnings.warn('Dataset expected to have at least %s dimensions found %s' %
                                  (expected_min_num_dims, len(curr_dataset.shape)))
                if expected_min_num_dims != -1:
                    if expected_min_num_dims != len(curr_dataset.shape):
                        dimensions_fixed = True
                        if 'dimensions_fixed' in dataset_spec:
                            dimensions_fixed = dataset_spec['dimensions_fixed']
                        if dimensions_fixed:
                            warnings.warn('Specification indicates %s dimensions found %s for %s' %
                                          (expected_min_num_dims, len(curr_dataset.shape), curr_dataset.name))
                # Iterate through all dimension scales and check for format compliance
                for dim_spec in dataset_spec['dimensions']:
                    # If the dimensions specification is not empty
                    if len(dim_spec) > 0:
                        dim_index = dim_spec['axis']
                        # Check if units stored in the label are correct
                        if len(curr_dataset.dims) <= dim_index:
                            if not dim_spec['optional']:
                                warnings.warn("Missing required data dimensions: " + str(dim_index))
                                compliance = False
                            dim_name = ''
                        else:
                            dim_name = curr_dataset.dims[dim_index].label
                        if dim_name != dim_spec['name']:
                            if dim_name == '' and dim_spec['optional']:
                                # Ignore the empty dimensions scale since it is optional
                                continue
                            if dim_name == '' and dim_spec['name'] is None:
                                # Ignore the empty dimensions scale, since we only require that the dim exists
                                continue
                            elif dim_name == '' and not dim_spec['optional']:
                                warnings.warn('Missing dimension scale for axis %i of dataset %s' %
                                              (dim_index, curr_dataset.name))
                            else:
                                warnings.warn("Label/units for axis %i of dataset %s are %s but expected %s" %
                                              (dim_index, curr_dataset.name, dim_name, dim_spec['name']))
                            compliance = False
                        # Get the dataset associated with the dimensions scale if it exists
                        if dim_spec['unit'] is not None:
                            try:
                                dim_scale = curr_dataset.dims[dim_spec['axis']][dim_spec['unit']]
                            except:
                                dim_scale = None
                                if not dim_spec['optional']:  # or dim_spec['dataset'] is not None:
                                    warnings.warn('Missing dimension scale dataset %s for %s' %
                                                  (unicode(dim_spec['dataset']), curr_dataset.name))
                                    compliance = False
                        else:
                            dim_scale = None
                        # Check if the name of the dim_scale dataset is correct
                        if dim_scale:  # If we have a dataset for the dimension-scale
                            if os.path.basename(dim_scale.name) != dim_spec['dataset']:
                                warnings.warn("Name of dimension scale dataset expected to be %s found %s" %
                                              (dim_spec['name'], os.path.basename(dim_scale.name)))
                                compliance = False
                        # Check if name description associated with the dimension scale is correct
                        if dim_spec['unit'] is not None:
                            if dim_spec['unit'] not in curr_dataset.dims[dim_index].keys()[0]:
                                if not dim_spec['optional']:
                                    warnings.warn("Unit of dimension scale expected to be %s found %s" %
                                                  (unicode(dim_spec['unit']), curr_dataset.dims[dim_index].keys()[0]))
                                    compliance = False
                    # If the dimensions scale specification is empty
                    else:
                        if len(curr_dataset.dims[dim_index]) > 0:
                            warnings.warn('Found dimension scale for dim  %s of dataset %s expected empty' %
                                          (str(dim_index), curr_dataset.name))
            # Check if all additional attributes are compliant
            if 'attributes' in dataset_spec:
                if dataset_spec['attributes'] is not None:
                    for attr_index, attr_spec in enumerate(dataset_spec['attributes']):
                        compliance &= cls.check_attribute_format_compliance(attribute_spec=attr_spec,
                                                                            hdf_object=curr_dataset)
        return compliance

    @classmethod
    def check_attribute_format_compliance(cls, attribute_spec, hdf_object):
        """
        Internal helper function used to check all attributes of a
        hdf5 object against a given attribute specification. Warnings
        are raised for every non-format compliance found.

        :param attribute_spec: The attribute specification
        :param hdf_object: The object with which the attributes with the
               given attribute specification are associated with.

        :returns: Boolean indicating compliance.

        """
        compliance = True
        attrs_object_keys = hdf_object.attrs.keys()

        # Find all relevant attributes
        # Check if the specification of the group is basically compliant
        if attribute_spec['attribute'] is not None and attribute_spec['prefix'] is not None:
            warnings.warn('ILLEGAL FORMAT SPEC: Only one of attribute or prefix may be given for group specifications')
            compliance = False
        elif attribute_spec['attribute'] is None and attribute_spec['prefix'] is None:
            warnings.warn('ILLEGAL FORMAT SPEC: Either attribute or prefix must be set for group specification')
        if not compliance:
            return compliance

        # Find all relevant attributes
        relevant_attributes = []
        if attribute_spec['attribute'] is not None:
            try:
                relevant_attributes.append(hdf_object.attrs[attribute_spec['attribute']])
            except KeyError:
                pass
        elif attribute_spec['prefix'] is not None:
            for currkey in attrs_object_keys:
                if currkey.startswith(attribute_spec['prefix']):
                    relevant_attributes.append(hdf_object.attrs[currkey])

        if len(relevant_attributes) == 0 and not attribute_spec['optional']:
            warnings.warn('Required attribute not found: name=%s, prefix=%s' %
                          (str(attribute_spec['attribute']), str(attribute_spec['prefix'])))
            compliance = False
        if not compliance:
            return compliance

        # Check the values of all relevant attributes
        for attr_val in relevant_attributes:
            if attribute_spec['value'] is not None:
                if attr_val != attribute_spec['value']:
                    warnings.warn('Attribute %s has value %s expected value is %s' %
                                  (attribute_spec['attribute'], str(attr_val), attribute_spec['value']))
                    compliance = False
        return compliance

    @classmethod
    def get_num_dimensions_from_dataset_specification(cls, dataset_spec):
        """
        Get the number of dimensions for a dataset based on the specifcation
        of dimension scales.

        :param dataset_spec: The specification of the dataset

        :returns: None in case that no dimensions scales are given and the dimensionality of the
                  dataset is not fixed. Returns a tuple of two integer indicating the minimum number
                  of dimensions and the maximum number of dimensions.

        """
        if 'dimensions' in dataset_spec:
            max_axis_index = 0
            min_axis_index = 0
            for dim_spec in dataset_spec['dimensions']:
                if dim_spec['axis'] > max_axis_index:
                    max_axis_index = dim_spec['axis']
                if not dim_spec['optional']:
                    if dim_spec['axis'] > min_axis_index:
                        min_axis_index = dim_spec['axis']
            min_max_dims = (min_axis_index+1, max_axis_index+1)
            return min_max_dims
        else:
            return None

    @classmethod
    def get_format_specification_recursive(cls):
        """
        Recursively construct the format specification for the current
        class including the specification of all included managed objects.
        The specification of managed objects are inserted in the `groups`
        and `dataset` dicts respectively and are accordingly removed from
        the managed_objects lists (i.e., the `managed_objects` list is empty after
        all replacements have been completed.). The result is a full specification
        for the current class.  The function also adds the
        specification for the `format_type`, `description`, 'format_specification`,
        and `object_id` attributes as those are implicitly defined by the
        ManageObject class, and are implicitly part of the specification of all managed
        object (even though the specific specification of the different objects usually
        does not explicitly declare them).

        :returns: Python dict with the full format specification for the
                  current managed object class. The specification may be converted
                  to JSON using json.dumps.
        """
        import sys
        spec = cls.get_format_specification()

        # 1) Iterate through all managed objects and insert their specification in the datasets and groups lists
        if 'managed_objects' in spec:
            for managed_object_spec in spec['managed_objects']:
                # Get the managed class and its specification
                try:
                    brainformat_class = getattr(sys.modules[__name__], managed_object_spec['format_type'])
                except AttributeError:
                    brainformat_class = getattr(sys.modules[cls.__module__], managed_object_spec['format_type'])
                brainformat_class_spec = brainformat_class.get_format_specification()
                # Define the key for the managed object
                object_key = None
                if 'group' in brainformat_class_spec:
                    object_key = brainformat_class_spec['group']
                elif 'dataset' in brainformat_class_spec:
                    object_key = brainformat_class_spec['dataset']
                if object_key is None:
                    object_key = brainformat_class_spec['prefix']
                if object_key is None:
                    object_key = "_"
                # Determine whether we need to insert the specification of the contained
                # managed object in the list of datasets or groups
                if issubclass(brainformat_class, ManagedGroup):
                    spec_key = 'groups'
                elif issubclass(brainformat_class, ManagedDataset):
                    spec_key = 'datasets'
                elif issubclass(brainformat_class, ManagedFile):
                    spec_key = 'groups'
                else:  # This should never be the case if the library is used properly
                    spec_key = None
                    warnings.warn('Contained managed object is neither a group, file, nor dataset. Ignoring %s' %
                                  str(managed_object_spec))
                # Compute the specification of the managed object and insert it in the list
                if spec_key is not None:
                    # Add a list of datasets or groups if missing
                    if spec_key not in spec:
                        spec[spec_key] = {}
                    # Ensure that we do not overwrite an existing object and find a unique key for the new specification
                    if object_key in spec[spec_key]:
                        index = 0
                        main_key = object_key
                        while object_key in spec[spec_key]:
                            object_key = main_key+"_"+str(index)
                            index += 1
                    # Insert the specification of the managed object in the full specification
                    spec[spec_key][object_key] = brainformat_class.get_format_specification_recursive()
                    spec[spec_key][object_key]['optional'] = managed_object_spec['optional']
            # Clear the list of managed objects
            spec['managed_objects'] = []

        # 2) Add the mandatory attributes specified by ManagedObject to the specification
        # 2.1) Add the type and description attribute
        if 'attributes' not in spec:
            spec['attributes'] = []
        type_attr_spec = {'attribute': cls.type_attribute_name,
                          'value': cls.__name__,
                          'prefix': None,
                          'optional': False}
        spec['attributes'].append(type_attr_spec)
        # 2.2) Add the description attribute
        if 'description' in spec:
            descr_attr_val = spec['description']
        else:
            descr_attr_val = ""
        descr_attr_spec = {'attribute': cls.description_attribute_name,
                           'value': descr_attr_val,
                           'prefix': None,
                           'optional': False}
        spec['attributes'].append(descr_attr_spec)
        # 2.3) Add the object id attribute
        object_id_attr_spec = {'attribute': cls.object_id_attribute_name,
                               'value': None,
                               'prefix': None,
                               'optional': True}
        spec['attributes'].append(object_id_attr_spec)
        # 2.4) Add the format specification attribute
        format_spec_attr_spec = {'attribute': cls.format_spec_attribute_name,
                                 'value': None,
                                 'prefix': None,
                                 'optional': False}
        spec['attributes'].append(format_spec_attr_spec)

        return spec

    def has_object_id(self):
        """
        Check whether the object has an id assigned to it.
        """
        return self.get_object_id() is not None

    def get_object_id(self):
        """
        Get the optional id of the object.

        :returns: The id of the object or None if no id exists.
        """
        if self.object_id_attribute_name in self.hdf_object.attrs.keys():
            return self.hdf_object.attrs[self.object_id_attribute_name]
        else:
            return None

    def set_object_id(self, object_id):
        """
        Define the id of the object.

        :param object_id: The object id to be used. If None is given, then the object id will be deleted.
        :type object_id: str, unicode, or None
        """
        if object_id is not None:
            self.hdf_object.attrs[self.object_id_attribute_name] = object_id
        else:
            del self.hdf_object.attrs[self.object_id_attribute_name]

    def get_format_specification_from_file(self):
        """
        Get the format specification as given in the file.

        :returns: Python dict with the format specification as given in the file or None if not available.
        """
        if self.format_spec_attribute_name in self.hdf_object.attrs.keys():
            return json.loads(self.hdf_object.attrs[self.format_spec_attribute_name])
        else:
            return None

    def format_specification_changed(self):
        """
        Check whether the format specification as given in the file is the same as the
        format specification given by the current instance of the managed object.
        """
        if self.format_spec_attribute_name in self.hdf_object.attrs.keys():
            return self.hdf_object.attrs[self.format_spec_attribute_name] == self.get_format_specification()
        else:
            return False

    @classmethod
    def get_format_specification(cls):
        """
        Return dictionary describing the specification of the format.
        """
        raise NotImplementedError('The get_format_specification method must be implemented by the child class')

    def populate(self, **kwargs):
        """
        The populate method is called by the create method after the basic common setup is complete.
        The function should be used to populate the managed object (e.g., add dimensions to a datadset or
        add required datasets to a group. The populate method is passed the kwargs handed to the create
        method.

        :param kwargs: Any additional keyword arguments supported by the
                       specific implementation of the populate method.
        """
        raise NotImplementedError('The populate method must be implemented by the child class')

    @classmethod
    def get_managed_object_type(cls):
        """
        Get whether a group or dataset is managed by the class.
        The default implementation assumes that a group is managed.
        The method must be overwritten in derived classes that manage
        datasets.

        :returns: String 'group' or 'dataset' or 'file' indicating whether a group, file, or
                  dataset is managed by the class.

        """
        raise NotImplementedError('The object needs to specify its type.')


################################################
# ManagedGroup                        #
################################################
class ManagedGroup(ManagedObject):
    """
    Base class for specification of managed h5py.Group objects.

    :ivar hdf_object: See ManagedObject

    Implicit attributes available via the h5py.Group object

    :ivar attrs: HDF5 Attributes for this group. Same as self.hdf_object.attrs.
    :ivar id: The groups's low-level identifer; an instance of h5py.GroupID.
              Same as self.hdf_object.id
    :ivar ref: An HDF5 object reference pointing to this group. See using object
              references as part of the h5py docs. Same as self.hdf_object.ref.
    :ivar regionref: A proxy object allowing you to interrogate region references.
               See using region references as part of the h5py docs. Same as
               self.hdf_object.regionref.
    :ivar name: String giving the full path to this group.
    :ivar file: The BrainDataFile object for the file instance in which the
                group resides. If the file is not a managed BrainDataFile then
                file is set to the h5py.File object instead.
    :ivar parent: The ManagedGroup object for the group instance
                containing this group. If the parent group is not a managed group
                then parent will be set to the corresponding h5py.Group object instead.

    Functions to be implemented by derived class:

        * `get_format_specification(..)` : Overwrite to specify the format. See ManagedObject.
        * `populate(..)` : Overwrite to implement the creation of the object. See ManagedObject.

    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group or h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(ManagedGroup, self).__init__(hdf_object)

    @classmethod
    def get_managed_object_type(cls):
        return 'group'

    def __getitem__(self, item):
        """
        Enable slicing into the group.
        """
        hdfobj = self.hdf_object[item]
        manobj = self.get_managed_object(hdfobj)
        if manobj is not None:
            return manobj
        else:
            return hdfobj

    def __contains__(self, item):
        """
        Dict-like containership testing. name may be a relative or absolute path.
        """
        return item in self.hdf_object

    def keys(self):
        """
        Get the names of directly attached group members. See h5py.Group
        """
        return self.hdf_object.keys()

    def values(self):
        """
        Get the objects contained in the group (Group and Dataset instances).
        Broken soft or external links show up as None.
        """
        return self.hdf_object.values()

    def items(self):
        """
        Get (name, value) pairs for object directly attached to this group.
        Values for broken soft or external links show up as None.
        """
        return self.hdf_object.items()


################################################
# ManagedDataset                               #
################################################
class ManagedDataset(ManagedObject):
    """
    Base class for specification of managed h5py.Dataset objects.

    :ivar hdf_object: See ManagedObject

    Functions to be implemented by derived class:

        * `get_format_specification(..)` : Overwrite to specify the format. See ManagedObject.
        * `populate(..)` : Overwrite to implement the creation of the object. See ManagedObject.

    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group or h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(ManagedDataset, self).__init__(hdf_object)

    @classmethod
    def get_managed_object_type(cls):
        return 'dataset'

    def __getitem__(self, item):
        """
        Numpy-style slicing to read data.
        """
        return self.hdf_object[item]

    def __setitem__(self, key, value):
        """
        NumPy-style slicing to write data
        """
        self.hdf_object[key] = value


################################################
# ManagedFile                                  #
################################################
class ManagedFile(ManagedObject):
    """
    Base class for specification of managed h5py.File objects.

    :ivar hdf_object: h5py.File or h5py.Group indicating the HDF5 file or string indicating the HDF5 file ot be opened.
                      See also ManagedObject.hdf_object

    Functions to be implemented by derived class:

        * `get_format_specification(..)` : Overwrite to specify the format. See ManagedObject.
        * `populate(..)` : Overwrite to implement the creation of the object. See ManagedObject.

    """
    def __init__(self, hdf_object, mode='r'):
        """
        :param hdf_object: This can be either the h5py.File object, a h5py.Group or h5py.Dataset
                           contained in the file of interest, or a string indicating the name
                           the file to be opened.
        :param mode: Used only if hdf_object is a string and a file is opened anew. Indicate
                     the mode in which a file should be opened, e.g., 'r', 'w', 'a'. See
                     the h5py.File documentation for details.
        """
        self.hdf_object = None
        if isinstance(hdf_object, h5py.File):
            self.hdf_object = hdf_object
        elif isinstance(hdf_object, h5py.Group) or isinstance(hdf_object, h5py.Dataset):
            self.hdf_object = hdf_object.file
        elif isinstance(hdf_object, basestring):
            self.hdf_object = h5py.File(hdf_object, mode=mode)
        super(ManagedFile, self).__init__(self.hdf_object)

    @classmethod
    def get_managed_object_type(cls):
        return 'file'

    def flush(self):
        """
        Flush the HDF5 file.
        """
        if isinstance(self.hdf_object, h5py.File):  # This should always be the case
            self.hdf_object.flush()
        elif self.hdf_object is not None:
            warnings.warn('The hdf_object managed is not an h5py.File object as expected. File not closed')

    def file_stats(self, root='/'):
        """
        Traverse the file to compute file related statistics, e.g., the number of objects
        of a given type etc.

        :param root: String indicating the root object starting from which we should compute the file statistics.
                     Default value is "/", i.e., starting from the root itself
        :return: Dict with a list of all object types:

            * 'datasets' : List of all names of all datasets
            * 'groups' : List of all names of all groups
            * 'attributes' : List of all attributes (excluding relationship attributes). Each attribute is described
                             by a dict with: 1) 'parent', the name of the parent object, 2) 'name' the name of the
                             attribute, and 3) 'value', the value of the attribute.
            * 'relationships : List of all relationship attributes. Each attribute is described
                             by a dict with: 1) 'parent', the name of the parent object, 2) 'name' the name of the
                             attribute, and 3) 'value', the value of the attribute.
            * 'managed' : List of names of all managed objects (these will also appear in the datasets or groups lists).

        """
        filestats = {}
        filestats['datasets'] = []
        filestats['attributes'] = []
        filestats['groups'] = []
        filestats['relationships'] = []
        filestats['managed'] = []
        filestats['others'] = []

        def update_stats(name, obj):
            """
            Callback function used in conjunction with the visititems function to compile
            statistics for the file

            :param name: the name of the object in the file
            :param obj: the hdf5 object itsel
            """
            if isinstance(obj, h5py.Dataset):
                filestats['datasets'].append(os.path.join(root, name))
            elif isinstance(obj, h5py.Group):
                filestats['groups'].append(os.path.join(root, name))
            else:
                filestats['others'].append(os.path.join(root, name))
            if ManagedObject.is_managed(obj):
                filestats['managed'].append(os.path.join(root, name))

            # Visit all attributes of the object
            for attr_name, attr_value in obj.attrs.iteritems():
                attr_dict = {'parent': os.path.join(root, name),
                             'name': attr_name,
                             'value': attr_value}
                if attr_name.startswith(RelationshipAttribute.RELATIONSHIP_ATTRIBUTE_PREFIX):
                    filestats['relationships'].append(attr_dict)
                else:
                    filestats['attributes'].append(attr_dict)

        self.hdf_object[root].visititems(update_stats)
        if root == '/':
            update_stats(name='/', obj=self.hdf_object['/'])

        return filestats


################################################
# ManagedObjectFile                            #
################################################
class ManagedObjectFile(ManagedFile):
    """
    Container file used to store a single managed object in a separate external file that
    is then linked to other parent files. This container is used as part of the external
    storage feature available for all ManagedObject implementations.

    NOTE: Parent files will link to the managed object contained in the file and NOT to the
          root group of the ManagedObjectFile container.
    """
    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format.
        """
        return {'datasets': {},
                'groups': {},
                'managed_objects': [{'format_type': 'ManagedObject', 'optional': True}],
                'attributes': [],
                'group': "/",
                'prefix': None,
                'file_prefix': None,
                'file_extension': '.h5',
                'optional': False,
                'description': 'Container file used for external storage of managed objects. ' +
                               'This container file is used to allow modular files where different ' +
                               'components of a file are stored in separate files that are linked via' +
                               'hard links.'}

    def populate(self, **kwargs):
        """
        Populate the managed object file.
        """
        pass  # Nothing to be done.

    def __getitem__(self, item):
        """
        Enable slicing into the root group of the file
        """
        hdfobj = self.hdf_object[item]
        manobj = self.get_managed_object(hdfobj)
        if manobj is not None:
            return manobj
        else:
            return hdfobj


################################################
# RelationshipAttribute                        #
################################################
class RelationshipAttribute(object):
    """
    Class used to help with the creation and management of special attributes used to encode relationships.

    For Index Map Relationships we use the following axis types in the specification:

    **INDEXING_AXIS**: The axis in the map dataset of index map relationships that encodes array indexes

    **STACK_AXIS**: The axis in the map dataset of index map relationships along which the set of
                    indexes are stacked, i.e., when multiple indices in the target correspond to
                    a single index in the source.'

    :cvar RELATIONSHIP_ATTRIBUTE_PREFIX: The name prefix that is prepended to the attribute name to
                indicate that the attribute defines a relationship.
    :cvar INDEX_MAP_RELATIONSHIP_POSTFIX: Dict where the values are the postfix names that are appended to
                relationship attributes to indicate which part of an index map relationship they encode.

    """
    RELATIONSHIP_ATTRIBUTE_PREFIX = 'RELATIONSHIP_ATTR_'

    INDEX_MAP_RELATIONSHIP_POSTFIX = {'MAP_TO_TARGET': '_IMR_MAP_TO_TARGET',
                                      'MAP_TO_SOURCE': '_IMR_MAP_TO_SOURCE',
                                      'SOURCE_TO_MAP': '_IMR_SOURCE_TO_MAP',
                                      'SOURCE_TO_TARGET': '_IMR_SOURCE_TO_TARGET'}
    @classmethod
    def find_relationships(cls, source, target=None, **kwargs):
        """
        Find all relationships from the given source that satisfy all additional criteria
        :param source: h5py.Dataset or h5py.Group object for which we want to look up relationships
        :param target: h5py.Dataset pr h5py.Group to which we want to related to
        :param kwargs: Additional kyword arguments with RelationshipSpec key, value pairs to be used for
                       filtering. E.g., adding relationship_type='order' selects only relationships that
                       have a relationship_type of 'order.'
        :return:
        """
        # Get all candiate relationships
        candidate_rels = RelationshipAttribute.get_all_relationships(parent_object=source)

         # Filter by target
        if target is not None:
            for k in list(candidate_rels.keys()):
                if candidate_rels[k].target.name != target.name and \
                        os.path.abspath(candidate_rels[k].target.file.filename) == os.path.abspath(target.file.filename):
                    candidate_rels.pop(k)

        # Filter by relationship_type
        for filter_key, filter_value in kwargs.iteritems():
            for k in list(candidate_rels.keys()):
                if candidate_rels[k].relationship_spec[filter_key] != filter_value:
                    candidate_rels.pop(k)
        return candidate_rels


    @classmethod
    def __get_source_h5py_from_parent__(cls,
                                        parent_object):
        """
        Given a parent object specified as h5py or ManagedObject, retrieve the
        h5py.Group or h5py.Dataset object for relationships.

        :param parent_object: The source object with which the relationship should be associated.
        :type parent_object: h5py.Group, h5py.Dataset, h5py.File, ManagedObject

        :raises: ValueError is raised if the parent_object cannot be resolved
        """
        source = parent_object
        if isinstance(parent_object, ManagedObject):
            source = parent_object.get_h5py()
        elif isinstance(parent_object, h5py.File):
            source = parent_object['/']
        if not isinstance(source, h5py.Group) and not isinstance(source, h5py.Dataset):
            raise ValueError('The parent_object could not be resolved to an h5py.Group or h5py.Dataset')
        return source

    @classmethod
    def create_index_map_relationship(cls,
                                      name,
                                      map_object,
                                      source_object,
                                      target_object,
                                      source_axis=None,
                                      target_axis=None,
                                      map_indexing_axis=None,
                                      map_stack_axis=None,
                                      user_description=None,
                                      user_properties=None,
                                      mapping_properties=None):
        """
        Helper function used to create complex mapped relationships, i.e., relationships
        that involve multiple datasets correlated via an index map.

        Example index map relationship::

            source <----order---- map ----indexes----> target
                \                 /
                 \-----order---->/

        If the user also specifies additional user properties and a description for the mapped relationship,
        then those will are stored as an additional `user` relationship between the source and the target:

        Example index map relationship with user data::

            source <----order---- map ----indexes----> target
                \                 /                     /
                 \-----order---->/                     /
                  \                                   /
                   \-------------user--------------->/


        :param name: The name fir the relationship.
        :param map_object: The HDF5 object that contains the map between the source and target
        :param source_object: The source HDF5 object from which we map from
        :param target_object: The target HDF5 object to which we map to
        :param source_axis: The source axis (or list of axes) to which the relationship applies to (Default=None)
        :param target_axis: The target axis (or list of axes) to which the relationship applies to (Default=None)
        :param map_indexing_axis: The axis of the map dataset that identifies the indicies into the target data. This
            is useful if the mapping is between multi-dimensional datasets and we need to identify which
            axis identifies the tuples into the target. (Default=None)
        :param map_stack_axis: The axis of the map dataset that identifies the stack of mappings in case that
            This is useful if the mapping has to encode one to many relationships and we, hence, need to store
            many mapping values for each source element. (Default=None)
        :param user_description: Description of the relationship between the source and target.
            The description is associated with the optional user-relationship between the
            source and target.(Default=None)
        :param user_properties: Additional properties describing the relationship between A and B. The user_properties
            are associated with the optional user-relationship between the source and target (Default=None)
        :param mapping_properties: Additional properties describing the mapping. This is typically additional
            information about how the map between the source and target was constructed. This information will be
            associated with the relationship between the map and the target. The keys 'MAP_INDEXING_AXIS' and
            'MAP_STACK_AXIS' are reserved to record the optional axis that store the indices and stack to
            store 1 to many mappings.

        :return: Tuple of four RelationshipAttribute objects:

            * `rel_map_to_target` : the indexes relationship of the map to the target
            * `rel_map_to_source` : the order relationship of the map to the source
            * `rel_source_to_map` : the order relationship of the source to the map
            * `rel_source_to_target` : the user relationship between source and the target. May be None if \
               neither user_description nor user_properties are set.

        """
        postfix = RelationshipAttribute.INDEX_MAP_RELATIONSHIP_POSTFIX
        # Create the relationship between the map and the target
        rel_map_to_target = RelationshipAttribute.create(parent_object=map_object,
                                                         target_object=target_object,
                                                         target_axis=target_axis,
                                                         attribute=name+postfix['MAP_TO_TARGET'],
                                                         relationship_type='indexes',
                                                         description='The source defines a map from ' +
                                                                     str(source_object.name) + 'to the ' +
                                                                     'target of this relationship',
                                                         properties=mapping_properties,
                                                         axis={'INDEXING_AXIS': map_indexing_axis,
                                                               'STACK_AXIS': map_stack_axis})
        # Create the relationship between the map and the source
        map_to_source_axis = None
        if map_indexing_axis is not None or map_stack_axis is not None:
            map_to_source_axis = range(0, len(map_object.shape))
            if map_indexing_axis is not None:
                map_to_source_axis.remove(map_indexing_axis)
            if map_stack_axis is not None:
                map_to_source_axis.remove(map_stack_axis)
        rel_map_to_source = RelationshipAttribute.create(parent_object=map_object,
                                                         target_object=source_object,
                                                         target_axis=source_axis,
                                                         attribute=name+postfix['MAP_TO_SOURCE'],
                                                         relationship_type='order',
                                                         description='The source defines a map from the ' +
                                                                     'target of this relationship to' +
                                                                     str(target_object.name),
                                                         axis=map_to_source_axis)
        # Create the relationship between the source and the map
        rel_source_to_map = RelationshipAttribute.create(parent_object=source_object,
                                                         target_object=map_object,
                                                         target_axis=map_to_source_axis,
                                                         attribute=name+postfix['SOURCE_TO_MAP'],
                                                         relationship_type='order',
                                                         description='The target of this relationship defined a map' +
                                                                     'from the source of this relationship to' +
                                                                     str(target_object.name),
                                                         axis=source_axis)
        # Create the user relationship between the source and target
        rel_source_to_target = None
        if user_description is not None or user_properties is not None:
            user_rel_description = user_description if user_description is not None else ''
            rel_source_to_target = RelationshipAttribute.create(parent_object=source_object,
                                                                target_object=target_object,
                                                                target_axis=target_axis,
                                                                attribute=name+postfix['SOURCE_TO_TARGET'],
                                                                relationship_type='user',
                                                                description=user_rel_description,
                                                                axis=source_axis,
                                                                properties=user_properties)
        # Return the relationships we created above
        return rel_map_to_target, rel_map_to_source, rel_source_to_map, rel_source_to_target

    @classmethod
    def create(cls,
               parent_object,
               relationship=None,
               target_object=None,
               target_axis=None,
               attribute=None,
               relationship_type=None,
               description=None,
               prefix=None,
               axis=None,
               optional=False,
               properties=None):
        """
        Create an new attribute for the parent_object storing the given
        relationship.

        :param parent_object: The source object with which the relationship should be associated.
        :type parent_object: h5py.Group, h5py.Dataset, h5py.File, ManagedObject

        :param relationship: The RelationshipSpec object with the specification of the relationship
        :type relationship: brain.dataformat.spec.RelationshipSpec

        Optional Parameters:

        Instead of specifying the relationship directly we may also provide the data required to
        construct the specification of the relationship instead which are:

        :param target_object: The target HDF5 object we want to point to from the source.
            ManagedObject API class instances are also permissible. (required if relationship=None)
        :param attribute: The name of the relationship attribute (a standard prefix may be prepended in storage)
            (required if relationship=None)
        :param target_axis: The axis of the target object we want to relate with. Only
            relevant when relating to datasets
        :param relationship_type: The type of the relationship. One of RelationshipSpec.RELATIONSHIP_TYPES.keys()
            (required if relationship=None)
        :param description: Textual description of the relationship. (required if relationship=None)
        :param prefix: Prefix of the attribute used for storage. Must be None in case that attribute is
            specified. (a prefix may be prependend in storage) (Default=None)(Optional)
        :param axis: Optional axis of the source to which the relationship applies to.
            (Default=None, all data)(Optional)
        :param optional: Is the relationship optional. (Default=False)(Optional)
        :param properties: Optional dictionary (must by JSON serializable) with additional user properties
            associated with the relationship. (Default=None)(Optional)

        :return: New RelationshipAttribute object that encodes the relationship
        """
        # from numpy import dtype

        # Check if we have conflicting parameter
        if relationship is not None and (target_object is not None or attribute is not None
                                         or relationship_type is not None or description is not None or
                                         prefix is not None or axis is not None or properties is not None):
            raise ValueError('Colliding input parameters. The relationship may only be specified either via the' +
                             ' relationship parameter or via the the optional parameters to create the spec, ' +
                             ' but not both')

        # Determine the source of the relationship with which we should associate the relationship
        source = cls.__get_source_h5py_from_parent__(parent_object)

        # Create the description of the relationship if not given
        if relationship is None:
            if attribute is None or relationship_type is None or description is None or target_object is None:
                raise ValueError('attribute, relationship_type, and description parameter must be set when the ' +
                                 'relationship specification is omitted')
            target_spec = RelationshipTargetSpec.from_objects(source_object=parent_object,
                                                              target_object=target_object,
                                                              target_axis=target_axis)
            relationship = RelationshipSpec(attribute=attribute,
                                            target=target_spec,
                                            relationship_type=relationship_type,
                                            description=description,
                                            prefix=prefix,
                                            axis=axis,
                                            optional=optional,
                                            properties=properties)

        # Check if the relationship is valid
        if relationship['axis'] is not None and not isinstance(source, h5py.Dataset):
            raise ValueError('Axis may only be defined for sources that point to an HDF5 dataset')

        # Determine the name for the attribute
        attribute_name = relationship['attribute']
        if relationship['prefix'] is not None:
            num_rel_attribs = 0
            for attrib_name in source.attr.keys():
                if attrib_name.startswith(relationship['prefix']):
                    num_rel_attribs += 1
            attribute_name = relationship['prefix'] + str(num_rel_attribs)
        attribute_name = cls.RELATIONSHIP_ATTRIBUTE_PREFIX + attribute_name

        # Encode the relationship as JSON
        attribute_value = relationship.to_json()

        # Add the attribute to the parent object
        parent_object.attrs[attribute_name] = attribute_value

        # Create a new RelationshipAttribute instance for the new attribute and return it
        return RelationshipAttribute(parent_object=parent_object,
                                     relationship_spec=relationship)

    @classmethod
    def get_all_relationships(cls,
                              parent_object):
        """
        Get a dict of all relationships defined for the given parent object

        :param parent_object: The source object with which the relationship should be associated.
        :type parent_object: h5py.Group, h5py.Dataset, h5py.File, ManagedObject

        :returns: Dict of RelationshipAttribute objects describing all valid relationships. The
                  keys are the names of the relationship and the values are the RelationshipAttribute objets

        """
        # Determine the source of the relationship with which we should associate the relationship
        source = cls.__get_source_h5py_from_parent__(parent_object)

        # Get all relationships
        all_relationships = {}
        for attribute_name, attribute_value in source.attrs.iteritems():
            if attribute_name.startswith(cls.RELATIONSHIP_ATTRIBUTE_PREFIX):
                try:
                    relationship_spec = BaseSpec.from_json(attribute_value)
                except:
                    warnings.warn('Relationship attribute with invalid spec at: '
                                  + parent_object.name + ":" + attribute_name)
                    relationship_spec = None
                if isinstance(relationship_spec, RelationshipSpec):
                    relationship_name = attribute_name.lstrip(cls.RELATIONSHIP_ATTRIBUTE_PREFIX)
                    relationship_value = RelationshipAttribute(parent_object=parent_object,
                                                               relationship_spec=relationship_spec)
                    all_relationships[relationship_name] = relationship_value

        # Return the found valid relationships
        return all_relationships

    @classmethod
    def get_relationship(cls,
                         parent_object,
                         name):
        """
        Get the relationship with the given name defined for the given parent object.

        :param parent_object: The source object with which the relationship should be associated.
        :type parent_object: h5py.Group, h5py.Dataset, h5py.File, ManagedObject
        :param name: The string name of the relationship

        :returns: RelationshipAttribute object describing the relationship or None

        """
        all_relationships = cls.get_all_relationships(parent_object=parent_object)
        attr_name = name.lstrip(cls.RELATIONSHIP_ATTRIBUTE_PREFIX)
        if name in all_relationships:
            return all_relationships[name]
        elif attr_name in all_relationships:
            return all_relationships[attr_name]
        else:
            return None

    @classmethod
    def get_index_map_relationship_names(cls, parent_object):
        """
        Get a list of all unique names of index map relationships

        :param parent_object: The HDF5 object for which we want to look up relationships
        :return: List of names of index map relationships
        """
        all_relationships = RelationshipAttribute.get_all_relationships(parent_object=parent_object)
        imr_relationship_names = []
        for attr_name, attr_value in all_relationships.iteritems():
            for postfix in RelationshipAttribute.INDEX_MAP_RELATIONSHIP_POSTFIX.values():
                if attr_name.endswith(postfix):
                    imr_rel_name = attr_name
                    imr_rel_name = imr_rel_name.lstrip(RelationshipAttribute.RELATIONSHIP_ATTRIBUTE_PREFIX)
                    imr_rel_name = imr_rel_name.rstrip(postfix)
                    imr_relationship_names.append(imr_rel_name)
                    break
        # Remove any duplicates
        imr_relationship_names = list(set(imr_relationship_names))

        # Return the list of index map relationships
        return imr_relationship_names

    @classmethod
    def get_index_map_relationship(cls,
                                   parent_object,
                                   relationship_name):
        """
        For a given parent_object dataset find all relationships that pertain to the
        index map relationship with the given name

        :param parent_object: The HDF5 map dataset
        :param relationship_name: The name of the index map relationship

        :return: Dict of RelationshipAttribute objects with the following entries:

            * 'MAP_TO_TARGET': The indexes relationship from the map to the target (None if not found)
            * 'MAP_TO_SOURCE': The order relationship from the map to the source (None if not found)
            * 'SOURCE_TO_MAP': The order relationship from the source to map (None if inverse==False or not found)
            * 'SOURCE_TO_TARGET': The user relationship from the source to target (None if inverse==False or not found)

        """
        # Get all relationships
        all_relationships = RelationshipAttribute.get_all_relationships(parent_object=parent_object)
        out_dict = {'MAP_TO_TARGET': None,
                    'MAP_TO_SOURCE': None,
                    'SOURCE_TO_MAP': None,
                    'SOURCE_TO_TARGET': None}
        # Get all relationships that point from the map to target and source
        for attr_name, attr_value in all_relationships.iteritems():
            if relationship_name in attr_name:
                if attr_name.endswith(RelationshipAttribute.INDEX_MAP_RELATIONSHIP_POSTFIX['MAP_TO_TARGET']):
                    out_dict['MAP_TO_TARGET'] = attr_value
                elif attr_name.endswith(RelationshipAttribute.INDEX_MAP_RELATIONSHIP_POSTFIX['MAP_TO_SOURCE']):
                    out_dict['MAP_TO_SOURCE'] = attr_value
        # Get all relationships that point from the source to map and target
        if out_dict['MAP_TO_SOURCE'] is not None:
            source_candidate = out_dict['MAP_TO_SOURCE'].target
        else:
            source_candidate = parent_object
        all_source_relationships = RelationshipAttribute.get_all_relationships(source_candidate)
        for attr_name, attr_value in all_source_relationships.iteritems():
            if relationship_name in attr_name:
                if attr_name.endswith(RelationshipAttribute.INDEX_MAP_RELATIONSHIP_POSTFIX['SOURCE_TO_MAP']):
                    out_dict['SOURCE_TO_MAP'] = attr_value
                if attr_name.endswith(RelationshipAttribute.INDEX_MAP_RELATIONSHIP_POSTFIX['SOURCE_TO_TARGET']):
                    out_dict['SOURCE_TO_TARGET'] = attr_value

        # Check if we only found the 'SOURCE_TO_MAP' relationship but not the `MAP_TO_SOURCE` and `MAP_TO_TARGET`
        # relationships. If this is case then we need to locate our map and look at get_index_map_relationship
        # for the map_object instead
        if out_dict['SOURCE_TO_MAP'] is not None and out_dict['MAP_TO_SOURCE'] is None:
            inverse_rels = RelationshipAttribute.get_index_map_relationship(
                parent_object=out_dict['SOURCE_TO_MAP'].target,
                relationship_name=relationship_name)
            # If our index map relationship is correct then this should technically never be the case
            if inverse_rels['SOURCE_TO_MAP'] is None:
                inverse_rels['SOURCE_TO_MAP'] = out_dict['SOURCE_TO_MAP']
            out_dict = inverse_rels

        return out_dict

    def __init__(self,
                 parent_object,
                 relationship_spec,
                 globals_dict=None,
                 target_index=None):
        """
        :param parent_object: The source object with which the relationship should be associated.
        :type parent_object: h5py.Group, h5py.Dataset, h5py.File, ManagedObject

        :param relationship_spec: The RelationshipSpec object with the specification of the relationship
        :type relationship: brain.dataformat.spec.RelationshipSpec

        :param globals_dict: Dictionary of dicts describing the global paths. The keys in the main dict
            describe the main GlobalTargetClass namespace and the keys in the contained dicts describe
            the GlobalTargetKey name of the target. The values of the secondary dicts are the paths.
            E.g, {'BRAINformat': {'descriptors': '/descriptors', 'internal_data': '/data/internal'}

        :param target_index: To ease static definition of targets the target_spec may include a prefix_all
            parameter to indicate a list of objects the relationship is allowed to point to. In this case
            we need specify which of the objects in the list of target we want this link to go to.
        """
        self.source = self.__get_source_h5py_from_parent__(parent_object)
        self.relationship_spec = RelationshipSpec(**relationship_spec)
        self.target_spec = RelationshipTargetSpec(**relationship_spec['target'])
        self.target = self.get_target(parent_object=self.source,
                                      target_spec=self.target_spec,
                                      globals_dict=globals_dict,
                                      target_index=target_index)
        self.source_axis = self.relationship_spec['axis']
        self.target_axis = self.target_spec['axis']
        self.relationship_type = self.relationship_spec['relationship_type']
        # self.slice_option = 'load_data'

    def __getitem__(self, item):
        """
        By specifying the slicing operation into the source, get the corresponding selection for the target.

        NOTE: None is returned in case that no values match the selection in the target

        :raises: NotImplementedError is raised in the case ambiguous situations that are not well-defined,
            e.g., equivalent or order relationships between groups?

        :raises ValueError: if the item cannot be found

        :raises KeyError: if the item cannot be found
        """
        if self.source_axis is None:
            source_selection = item
        else:
            source_selection = []
            # If we are not indexing the other dataset then the self.source_axis defines the list of axis we relate to
            if self.relationship_type != 'indexes':
                source_axes = self.source_axis if isinstance(self.source_axis, list) else [self.source_axis, ]
            # If we have and indexes relationship, then the source_axis encodes the axes that are used to
            # define the indexes and we need to remove them from the list of axes
            else:
                source_axes = range(0, len(self.source.shape))
                if isinstance(self.source_axis, int):
                    source_axes.remove(self.source_axis)
                elif isinstance(self.source_axis, list):
                    for axis_t in self.source_axis:
                        if axis_t is not None:
                            source_axes.remove(axis_t)
                elif isinstance(self.source_axis, dict):
                    for axis_t in self.source_axis.values():
                        if axis_t is not None:
                            source_axes.remove(axis_t)

            # Define the selection for the source based on which axes have relationships
            item_index = 0
            for dim in range(len(self.source.shape)):
                if dim not in source_axes or item_index == -1:
                    source_selection.append(slice(None))
                else:
                    if isinstance(item, tuple):
                        source_selection.append(item[item_index])
                        item_index += 1
                        if item_index >= len(item):
                            item_index = -1
                    else:
                        source_selection.append(item)
                        item_index = -1
            source_selection = tuple(source_selection)

        # target_all_selection = ()
        # if isinstance(self.target, h5py.Dataset):
        #     target_selection = tuple([slice(None) for _ in range(len(self.target.shape))])

        # The source dataset contains indices into the target dataset
        if self.relationship_type == 'indexes':
            if not isinstance(self.source, h5py.Dataset):
                raise NotImplementedError('indexes relationship can only be resolved if the source is a dataset')
            selection = self.source[source_selection]
            return selection

        # The source object selects certain parts of the target object based on the values
        elif self.relationship_type == 'indexes_values':
            if self.target_axis is not None:
                raise NotImplementedError('Indexes relationships with target axis specified cannot be resolved yet')
            if isinstance(self.target, h5py.Dataset) and isinstance(self.source, h5py.Dataset):
                return self.target[:] == self.source[source_selection]
            elif isinstance(self.target, h5py.Group) and isinstance(self.source, h5py.Dataset):
                return self.source[source_selection]
            elif isinstance(self.target, h5py.Group) and isinstance(self.source, h5py.Group):
                return item
            elif isinstance(self.target, h5py.Dataset) and isinstance(self.source, h5py.Group):
                return self.target[:] == item
            else:
                raise NotImplementedError('Slicing for indexes_values relationship not available for group')

        # The target and source contain values with the same encoding so that values in the two objects
        # can be directly compared
        elif self.relationship_type == 'shared_encoding' or self.relationship_type == 'shared_ascending_encoding':
            if isinstance(self.source, h5py.Group) and isinstance(self.target, h5py.Dataset):
                return self.target[:] == item
            elif isinstance(self.source, h5py.Group) and isinstance(self.target, h5py.Group):
                return item
            elif isinstance(self.source, h5py.Dataset) and isinstance(self.target, h5py.Dataset):
                # Perform a min/max range selection, rather than a values equals selection
                if self.relationship_type == 'shared_ascending_encoding' and \
                        isinstance(item, slice) and item.step is None and \
                        not isinstance(self.target, h5py.Group) and not isinstance(self.source, h5py.Group) and \
                        len(self.target.shape) <= 1 and len(self.source.shape) <= 1:
                    if self.target_axis is not None:
                        temp_axis = [self.target_axis,] if not isinstance(self.target_axis, list) else self.target_axis
                        if len(temp_axis) != len(self.target.shape):
                            raise NotImplementedError('shared_ordered encoding with partial target axis set not supported')
                    if self.source_axis is not None:
                        temp_axis = [self.source_axis,] if not isinstance(self.source_axis, list) else self.source_axis
                        if len(temp_axis) != len(self.source.shape):
                            raise NotImplementedError('shared_ordered encoding with partial source axis set not supported')
                    source_data = self.source[source_selection]
                    start_value = source_data.min()
                    stop_value = source_data.max()
                    target_data = self.target[:]
                    start_index = np.argmax(target_data >= start_value)
                    stop_index = np.argmin(target_data <= stop_value)
                    if start_index == stop_index:  # No values match
                        return None  # Return an empty selection
                    else:
                        return slice(start_index, stop_index, None)
                # Select values that are equal to each other
                else:
                    selection = np.unique(self.source[source_selection])
                    if len(selection.shape) == 0:
                        selection = [selection, ]
                    out_selection = np.zeros(shape=self.target.shape, dtype='bool')
                    target_data = self.target[:]
                    for selection_value in selection:
                        out_selection |= (target_data == selection_value)
                    return out_selection

        # The ordering of objects matches between the datasets along the given axis
        elif self.relationship_type == 'order' or self.relationship_type == 'equivalent':
            if isinstance(self.source, h5py.Dataset) and isinstance(self.target, h5py.Dataset):
                return item
            elif isinstance(self.source, h5py.Group) and isinstance(self.target, h5py.Group):
                if isinstance(item, basestring) or isinstance(item, int) or isinstance(item, slice):
                    source_keys = sorted(self.source.keys())
                    target_keys = sorted(self.target.keys())
                    if isinstance(item, basestring):
                        source_index = source_keys.index(item)
                        if source_index >= len(target_keys):
                            raise ValueError('Selection for target out of bounds')
                        return target_keys[source_index]
                    elif isinstance(item, int):
                        return target_keys[item]
                    elif isinstance(item, slice):
                        return target_keys[item]
                    else:
                        raise NotImplementedError(str(type(item)) + ' selection not supported for groups')

                else:
                    raise NotImplementedError(str(self.relationship_type) + " selection only possibel between" +
                                              "groups if selection is performed by name, index, or slice. ")
                return item
            else:
                raise not NotImplementedError(str(self.relationship_type) +
                                              ' selection between groups to datasets and vice versa not defined')
        else:
            raise ValueError('Unkown relationship type')

        raise NotImplementedError('Not implemented yet')

    @classmethod
    def get_target(cls,
                   parent_object,
                   target_spec,
                   globals_dict=None,
                   dict_encode=False,
                   target_index=None):
        """
        Get the target HDF5 object defined by the target specification relative to the given source HDF5 object.

        :param parent_object: The source object with which the relationship should be associated.
        :type parent_object: h5py.Group, h5py.Dataset, h5py.File, ManagedObject

        :param target_spec: The RelationshipTargetSpec specification of the target
        :type target_spec: brain.dataformat.spec.RelationshipTargetSpec

        :param globals_dict: Dictionary of dicts describing the global paths. The keys in the main dict
            describe the main GlobalTargetClass namespace and the keys in the contained dicts describe
            the GlobalTargetKey name of the target. The values of the secondary dicts are the paths.
            E.g, {'BRAINformat': {'descriptors': '/descriptors', 'internal_data': '/data/internal'}

        :param dict_encode: Encode the output as a python dict. This is useful when the target_spec only
            defines a prefix and we want a dict of just the object with that prefix, rather than the target
            group that contains the objects. (Default=False)

        :param target_index: To ease static definition of targets the target_spec may include a prefix_all
            parameter to indicate a list of objects the relationship is allowed to point to. In this case
            we need specify which of the objects in the list of target we want this link to go to.

        :raises: ValueError is raised in case that the target object cannot be located
        """
        source = cls.__get_source_h5py_from_parent__(parent_object=parent_object)
        if not isinstance(target_spec, RelationshipTargetSpec):
            try:
                target_spec = RelationshipTargetSpec(**target_spec)
            except:
                raise ValueError('target_spec is not a valid RelationshipTargetSpec object')
        if target_spec['prefix_all'] is not None and target_index is None:
            raise ValueError('The provided target specification points to multiple objects. Target_index is required')

        # Determine the tarsget file
        target_file = parent_object.file if target_spec['filename'] is None else h5py.File(target_spec['filename'], 'r')

        # Determine the target path
        target_path = ""
        # Resolve the global_path
        if target_spec['global_path'] is not None:
            # Check if we have an absolute path within the file
            try:
                target_parent = target_file[target_spec['global_path']]
            except KeyError:
                target_parent = None
            if target_parent is not None:
                target_path = target_spec['global_path']
            # Resolve the indexed global path that points to a location in the globals_dict
            elif ':' in target_spec['global_path']:
                if globals_dict is None or not isinstance(globals_dict, dict):
                    raise ValueError('Cannot resolve global target')
                global_target_class = target_spec['global_path'].split(':')[0]
                global_target_key = target_spec['global_path'].lstrip(global_target_class)
                if global_target_class in globals_dict:
                    if global_target_key in globals_dict[global_target_class]:
                        target_path = globals_dict[global_target_class][global_target_key]
                    else:
                        raise ValueError('Could not locate global target')
                else:
                    ValueError('Could not locate global target')
        # We don't have a global path so we are indexing localy within the parten object of our source
        else:
            target_path = parent_object.parent.name

        # If we are linking to a dataset
        if target_spec['dataset'] is not None:
            target_path = os.path.join(target_path, target_spec['dataset'])
            target_object = target_file[target_path]
            if dict_encode:
                return {target_object.name: target_object}
            else:
                return target_object
        # If we are linking to a group
        elif target_spec['group'] is not None:
            target_path = os.path.join(target_path, target_spec['group'])
            target_object = target_file[target_path]
            if target_spec['prefix'] is None:
                if dict_encode:
                    return {target_object.name: target_object}
                else:
                    return target_object
        # If we are linking to a series of objects via the prefix
        elif target_spec['prefix'] is not None:
            target_object = target_file[target_path]
            if dict_encode:
                target_dict = {}
                for target_name, target_object in target_object.iteritems():
                    if target_name.startswith(target_spec['prefix']):
                        target_dict[target_name] = target_object
                return target_dict
            else:
                return target_object
        elif target_spec['prefix_all'] is not None:
            target_path = os.path.join(target_path, target_spec['prefix_all']) + str(target_index)
            target_object = target_file[target_path]
            if dict_encode:
                return {target_object.name: target_object}
            else:
                return target_object

        raise ValueError('The target could not be determined')


