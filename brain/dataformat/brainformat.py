"""
Module for specification of the BRAIN file format API.

"""
import warnings

import numpy as np
import json
import os
import h5py

from brain.dataformat.base import ManagedFile, ManagedGroup, ManagedObject, ManagedObjectFile, ManagedDataset, RelationshipAttribute
from brain.dataformat.annotations.selection import DataSelection
from brain.dataformat.annotations.annotation import Annotation
from brain.dataformat.annotations.collection import AnnotationDataGroup, AnnotationCollection
from brain.dataformat.spec import DatasetSpec, AttributeSpec, BaseSpec, GroupSpec, ManagedSpec, RelationshipSpec


################################################
# ManagedFile                                  #
################################################
class BrainDataFile(ManagedFile):
    """
    Class for management of HDF5 brain files.

    :ivar hdf_object: See ManagedFile

    Implicit instance variables (i.e., these are mapped names but not stored explicitly)

    :ivar data_#: Same as self.data(#). Usually use data_0
    :ivar descriptors_#: Same as self.descriptors(#). Usually use descriptors_0

    """
    def __init__(self, hdf_object, mode='r'):
        """
        :param hdf_object: This can be either the h5py.File object, a h5py.Group or h5py.Dataset
                           contained in the file of interest, or a string indicating the name
                           the file to be opened.
        :param mode: Used only if hdf_object is a string and a file is opened anew. Indicate
                     the mode in which a file should be opened, e.g., 'r', 'w', 'a'. See
                     the h5py.File documentation for details.
        """
        super(BrainDataFile, self).__init__(hdf_object=hdf_object, mode=mode)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format.
        """
        return {'datasets': {},
                'groups': {},
                'managed_objects': [{'format_type': 'BrainDataData', 'optional': False},
                                    {'format_type': 'BrainDataDescriptors', 'optional': False}],
                'attributes': [],
                'group': None,
                'prefix': "entry_",
                'file_prefix': None,
                'file_extension': '.h5',
                'optional': False,
                'description': 'Managed BRAIN file.'}

    def populate(self, **kwargs):
        """
        Populate the Brain file with the Data and Descriptors group.
        """
        parent_object = self.hdf_object['/']
        BrainDataData.create(parent_object=parent_object)
        BrainDataDescriptors.create(parent_object=parent_object)
        self.flush()

    def __getitem__(self, item):
        """
        Enable slicing into the file
        """
        hdfobj = self.hdf_object[item]
        manobj = self.get_managed_object(hdfobj)
        if manobj is not None:
            return manobj
        else:
            return hdfobj

    def data(self, index=0):
        """
        Get the data managed object with the data. In
        principal the current format defines that there should be always
        exactly one descriptors group. However, multiple could be supported
        so this function is prepared to handle this in case the format should
        change in the future.

        :param index: Optional input parameter (should be always 0 for now)
                      to define which data group should be retrieved.
                      Currently their should be always exactly one.

        :returns: BrainDataData object or None
        """
        dataobjs = BrainDataData.get_all(self.hdf_object['/'])
        if len(dataobjs) == 0:
            return None
        else:
            return BrainDataData(dataobjs[index])

    def descriptors(self, index=0):
        """
        Get the descriptors managed object with the descriptors. In
        principal the current format defines that there should be always
        exactly one descriptors group. However, multiple could be supported
        so this function is prepared to handle this in case the format should
        change in the future.

        :param index: Optional input parameter (should be always 0 for now)
                      to define which descriptors group should be retrieved.
                      Currently their should be always exactly one.

        :returns: BrainDataDescriptors object or None
        """
        descriptorobjs = BrainDataDescriptors.get_all(self.hdf_object['/'])
        if len(descriptorobjs) == 0:
            return None
        else:
            return BrainDataDescriptors(descriptorobjs[index])

    def __getattr__(self, item):
        if item.startswith('data_'):
            indexstr = item.lstrip('data_')
            if indexstr.isdigit():
                return self.data(int(indexstr))
            else:
                raise AttributeError("'BrainDataFile' has not attribute" + item)
        elif item.startswith('descriptors_'):
            indexstr = item.lstrip('descriptors_')
            if indexstr.isdigit():
                return self.descriptors(int(indexstr))
            else:
                raise AttributeError("'BrainDataFile' has not attribute" + item)
        else:
            try:
                return super(BrainDataFile, self).__getattribute__(item)
            except AttributeError:
                return super(BrainDataFile, self).__getattr__(item)


################################################
# BrainDataMultiFile                           #
################################################
class BrainDataMultiFile(ManagedObjectFile):
    """
    Container file used to store a collection of BrainDataFile collections of neural data. This container is
    used to organize multiple  experiments, sessions, etc. stored in separate files into a single collection,
    allow a user to open a single file and interact with the collection of files as if they were all stored
    in the same file.

    NOTE: This container typically stores external links to other BrainDataFile objects. However, the
          container may also be self-contained.
    """
    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format.
        """
        return {'datasets': {},
                'groups': {},
                'managed_objects': [{'format_type': 'BrainDataFile', 'optional': True}],
                'attributes': [],
                'group': None,
                'prefix': None,
                'file_prefix': None,
                'file_extension': '.h5',
                'optional': False,
                'description': 'Container file used to organize multiple BrainDataFile objects into a larger ' +
                               'data collection, e.g., to create a collection of recording session or experiments ' +
                               'allowing users to more seamlessly interact with many related files.'}

    def add_object(self, hdf_object, relative_link=True, force_creation=False):
        """
        Add the given BrainDataFile object to the collection.

        :param hdf_object: The object to be added to the collection. This may either be: i) and instance of
                           BrainDataFile, ii) and instance of h5py.Group pointing to a BrainDataFile object,
                           iii) a tuple of two strings (filename, path) or) a dict {'filename':..., 'path':...}
                           describing the location of the file and the path in the hdf5 file. **NOTE**
        :param relative_link: Should we use relative links (i.e., relative to the BrainDataMultiFile where we
                       are adding the link (True) or should we use absolute paths to external files (False). Default
                       value is True.
        :type relative_link: bool
        :param force_creation: Force the creation of the link, even if it is not explicitly permitted by the format
                       specification and/or the link cannot be verified. E.g, we may not be able to always open
                       the linked object to verify that the link actually points to a BrainDataFile managed object
                       or other support object.
        :type force_creation: bool


        :raises ValueError: A ValueError is raised in case that the description of the link is invalid.
        :raises IOError: In case that the link cannot be established

        """
        # 1) Determine the filename and path variable with the settings for the link
        if isinstance(hdf_object, tuple):
            if len(hdf_object) != 2:
                raise ValueError('Invalid description of the link given in hdf_object because len(hdf_object) != 2')
            filename, path = hdf_object
        elif isinstance(hdf_object, dict):
            if 'filename' not in hdf_object or 'path' not in hdf_object:
                raise ValueError('Missing filename and/or path key in hdf_object dict.')
            filename = hdf_object['filename']
            path = hdf_object['path']
        elif isinstance(hdf_object, ManagedObject):
            filename = hdf_object.get_filename(absolute_path=True)
            path = hdf_object.get_h5py().name
        elif isinstance(hdf_object, h5py.Group):
            filename = os.path.abspath(hdf_object.file.filename)
            path = hdf_object.name
        else:
            raise ValueError("Invalid type for hdf_object.")

        # 2) Verify that the filename and path are valid
        if not force_creation:
            if not os.path.exists(filename):
                ValueError("The link was requested to point to an invalid file path: " + filename)
            elif not os.path.isfile(filename):
                ValueError("The requested link does not point to a file: " + filename)

        # 3) Verify that the filename and path follow the format specification, i.e., point to a BrainDataFile object
        if not force_creation:
            temp_file = None
            if isinstance(hdf_object, h5py.Group):
                try:
                    hdf_object = self.get_managed_object(hdf_object)
                except (NameError, ValueError):
                    hdf_object = None
            elif isinstance(hdf_object, tuple) or isinstance(hdf_object, dict):
                try:
                    temp_file = h5py.File(filename, 'r')
                    hdf_object = self.get_managed_object(temp_file[path])
                except (NameError, ValueError):
                    hdf_object = None
                except (KeyError, IOError):
                    raise ValueError('The target_ephys_object object could not be opened.')
            if not isinstance(hdf_object, BrainDataFile):
                raise ValueError('The link did not point to a BrainDataFile as required by the format specification')
            if temp_file:
                temp_file.close()

        # 4) Turn the link into a relative link if necessary or use an absolute link
        if relative_link:
            abs_filename = os.path.abspath(filename)  # Absolute path to the linked file
            my_filename = self.get_filename(absolute_path=True)  # Absolute path to this file
            common_dir_prefix = os.path.commonprefix([os.path.dirname(abs_filename),
                                                      os.path.dirname(my_filename)])
            filename = os.path.relpath(path=abs_filename,
                                       start=common_dir_prefix)
        else:
            filename = os.path.abspath(filename)

        # 5) Construct a name for the link
        # 5.1) Construct a name based on the specification of BrainDataFile
        brain_data_file_spec = BrainDataFile.get_format_specification()
        if brain_data_file_spec['group'] is None:   # TODO Fix name depending on how we handle group names for files
            object_prefix = brain_data_file_spec['prefix']
            object_index = len(BrainDataFile.get_all(parent_group=self.hdf_object))
            link_name = object_prefix + str(object_index)
            # If get_all was not able to open an external link, then our index might be wrong
            if link_name in self.hdf_object.keys():
                existing_keys = self.hdf_object.keys()
                for i in range(object_index, object_index+10000):
                    if object_prefix + str(i) not in existing_keys:
                        link_name = object_prefix + str(i)
                        break
        else:
            link_name = brain_data_file_spec['group']
        # 5.2 Check if the link name is valid
        if link_name in self.hdf_object.keys():
            raise ValueError("Could not create a new link. %s already specified in the file.")
        # 6) Create the external link
        self.hdf_object[link_name] = h5py.ExternalLink(filename=filename,
                                                       path=path)


################################################
# BrainDataInternalData                        #
################################################
class BrainDataData(ManagedGroup):
    """
    Class for management of the `data` group for storage of brain recordings data.

    :ivar hdf_object: See ManagedGroup

    Implicit instance variables (i.e., these are mapped names but not stored explicitly)

    :ivar internal_#: Same as self.internal(#). Usually use internal_0
    :ivar external_#: Same as self.external(#). Usually use external_0

    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataData, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format.
        """
        return {'datasets': {},
                'groups': {},
                'managed_objects': [{'format_type': 'BrainDataInternalData', 'optional': False},
                                    {'format_type': 'BrainDataExternalData', 'optional': False}],
                'attributes': [],
                'group': "data",
                'prefix': None,
                'optional': False,
                'description': 'Managed group for storage of brain data (internal and external).'}

    def populate(self, **kwargs):
        """
        Populate the Brain file with the Data and Descriptors group.
        """
        parent_object = self.hdf_object
        BrainDataInternalData.create(parent_object=parent_object)
        BrainDataExternalData.create(parent_object=parent_object)

    def internal(self, index=0):
        """
        Get the internal managed object with the internal data. In
        principal the current format defines that there should be always
        exactly one internal group. However, multiple could be supported
        so this function is prepared to handle this in case the format should
        change in the future.

        :param index: Optional input parameter (should be always 0 for now)
                      to define which internal group should be retrieved.
                      Currently their should be always exactly one.

        :returns: BrainDataInternalData object or None
        """
        internalobjs = BrainDataInternalData.get_all(self.hdf_object)
        if len(internalobjs) == 0:
            return None
        else:
            return BrainDataInternalData(internalobjs[index])

    def external(self, index=0):
        """
        Get the external managed object with the external data. In
        principal the current format defines that there should be always
        exactly one external group. However, multiple could be supported
        so this function is prepared to handle this in case the format should
        change in the future.

        :param index: Optional input parameter (should be always 0 for now)
                      to define which external group should be retrieved.
                      Currently their should be always exactly one.

        :returns: BrainDataExternalData object or None
        """
        externalobjs = BrainDataExternalData.get_all(self.hdf_object)
        if len(externalobjs) == 0:
            return None
        else:
            return BrainDataExternalData(externalobjs[index])

    def __getattr__(self, item):
        if item.startswith('internal_'):
            indexstr = item.lstrip('internal_')
            if indexstr.isdigit():
                return self.internal(int(indexstr))
            else:
                raise AttributeError("'BrainDataData' has not attribute " + item)
        elif item.startswith('external_'):
            indexstr = item.lstrip('external_')
            if indexstr.isdigit():
                return self.external(int(indexstr))
            else:
                raise AttributeError("'BrainDataData' has not attribute " + item)
        else:
            try:
                return super(BrainDataData, self).__getattribute__(item)
            except AttributeError:
                return super(BrainDataData, self).__getattr__(item)


###################################
#  Ephys Manager                  #
###################################
class EphysManager(object):
    """
    Base helper class for subclasses that need to manage BrainDataEphys and BrainDataEphysProcessed objects
    """
    def __init__(self, **kwargs):
        """
        :param kwargs: Pass any arguments through to super
        """
        try:
            # In cas our super is ManagedObject and we need to pass hdf_object through
            super(EphysManager, self).__init__(**kwargs)
        except:
            # We are the last one in the super call hierarchy and we can't pass arguments through to object
            super(EphysManager, self).__init__()

    def num_ephys_data(self):
        """
        Get the number of ephys datasets available.
        """
        return len(BrainDataEphys.get_all(self.hdf_object))

    def num_ephys_data_processed(self):
        """
        Get the number of processed ephys datasets available.
        """
        return len(BrainDataEphysProcessed.get_all(self.hdf_object))

    def ephys_data(self, index=0):
        """
        Get the ephys managed object with the ephys data.

        :param index: Optional input parameter to define the index of the ephys dataset.

        :returns: BrainDataEcoG object or None
        """
        ephysobjs = BrainDataEphys.get_all(self.hdf_object)
        if len(ephysobjs) == 0:
            return None
        else:
            return BrainDataEphys(ephysobjs[index])

    def ephys_data_processed(self, index=0):
        """
        Get the `ephys_processed_` managed object with the processed ephys data.

        :param index: Optional input parameter to define the index of the processed ephys dataset.

        :returns: BrainDataEcoGProcessed object or None
        """
        ephysobjs = BrainDataEphysProcessed.get_all(self.hdf_object)
        if len(ephysobjs) == 0:
            return None
        else:
            return BrainDataEphysProcessed(ephysobjs[index])

    def __getattr__(self, item):
        if item.startswith('ephys_data_processed_'):
            indexstr = item.lstrip('ephys_data_processed_')
            if indexstr.isdigit():
                return self.ephys_data_processed(int(indexstr))
            else:
                raise AttributeError("'BrainDataInternalData' has no attribute " + item)
        elif item.startswith('ephys_data_'):
            indexstr = item.lstrip('ephys_data_')
            if indexstr.isdigit():
                return self.ephys_data(int(indexstr))
            else:
                raise AttributeError("'BrainDataInternalData' has not attribute " + item)
        else:
            try:
                return super(EphysManager, self).__getattribute__(item)
            except AttributeError:
                return super(EphysManager, self).__getattr__(item)


#########################################
#  Stimulus Manager
########################################
class CollectionManager(object):
    """
    Base helper class for subclasses that need to manage BrainDataStimuluss
    """
    def __init__(self, **kwargs):
        """
        :param kwargs: Pass any arguments through to super
        """
        try:
            # In cas our super is ManagedObject and we need to pass hdf_object through
            super(CollectionManager, self).__init__(**kwargs)
        except:
            # We are the last one in the super call hierarchy and we can't pass arguments through to object
            super(CollectionManager, self).__init__()

    def num_collection_data(self):
        """
        Get the number of ephys datasets available.
        """
        return len(BrainDataCollection.get_all(self.hdf_object))

    def collection_data(self, index=0):
        """
        Get the ephys managed object with the ephys data.

        :param index: Optional input parameter to define the index of the ephys dataset.

        :returns: BrainDataEcoG object or None
        """
        stimulusobjs = BrainDataCollection.get_all(self.hdf_object)
        if len(stimulusobjs) == 0:
            return None
        else:
            return BrainDataCollection(stimulusobjs[index])

    def __getattr__(self, item):
        if item.startswith('collection_'):
            indexstr = item.lstrip('collection_')
            if indexstr.isdigit():
                return self.stimulus_data(int(indexstr))
            else:
                raise AttributeError("'BrainDataInternalData' has no attribute " + item)
        else:
            try:
                return super(CollectionManager, self).__getattribute__(item)
            except AttributeError:
                return super(CollectionManager, self).__getattr__(item)

################################################
# BrainDataInternalData                        #
################################################
class BrainDataInternalData(ManagedGroup, EphysManager, CollectionManager):
    """
    Class for management of the 'internal' group for storage of brain recordings data.

    :ivar hdf_object: See ManagedGroup

    Implicit instance variables (i.e., these are mapped names but not stored explicitly)

    :ivar ephys_data_#: Same as self.ephys_data(#) where # is the index of the raw ephys dataset.
    :ivar ephys_data_processed_#: Same as self.ephys_data_processed(#) where # is the index of the processed ephys dataset.

    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataInternalData, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the group.
        """
        spec = GroupSpec(group='internal',
                         prefix=None,
                         optional=False,
                         description='Managed group for storage of a collection of internal brain data.')
        spec.add_managed(ManagedSpec(format_type='BrainDataEphys', optional=True))
        spec.add_managed(ManagedSpec(format_type='BrainDataEphysProcessed', optional=True))
        spec.add_managed(ManagedSpec(format_type='BrainDataCollection', optional=True))
        return spec

    def populate(self, **kwargs):
        """
        Populate the 'data' group with all required elements.
        """
        pass

    def __getattr__(self, item):
        try:
            return EphysManager.__getattr__(self, item)
        except:
            try:
                return CollectionManager.__getattr__(self, item)
            except:
                try:
                    return ManagedGroup.__getattribute__(self, item)
                except AttributeError:
                    return ManagedGroup.__getattr__(self, item)


#########################################
#  Stimulus Manager
########################################
class StimulusManager(object):
    """
    Base helper class for subclasses that need to manage BrainDataStimuluss
    """
    def __init__(self, **kwargs):
        """
        :param kwargs: Pass any arguments through to super
        """
        try:
            # In cas our super is ManagedObject and we need to pass hdf_object through
            super(StimulusManager, self).__init__(**kwargs)
        except:
            # We are the last one in the super call hierarchy and we can't pass arguments through to object
            super(StimulusManager, self).__init__()

    def num_stimulus_data(self):
        """
        Get the number of ephys datasets available.
        """
        return len(BrainDataStimulus.get_all(self.hdf_object))

    def stimulus_data(self, index=0):
        """
        Get the ephys managed object with the ephys data.

        :param index: Optional input parameter to define the index of the ephys dataset.

        :returns: BrainDataEcoG object or None
        """
        stimulusobjs = BrainDataStimulus.get_all(self.hdf_object)
        if len(stimulusobjs) == 0:
            return None
        else:
            return BrainDataStimulus(stimulusobjs[index])

    def __getattr__(self, item):
        if item.startswith('stimulus_'):
            indexstr = item.lstrip('stimulus_')
            if indexstr.isdigit():
                return self.stimulus_data(int(indexstr))
            else:
                raise AttributeError("'BrainDataInternalData' has no attribute " + item)
        else:
            try:
                return super(StimulusManager, self).__getattribute__(item)
            except AttributeError:
                return super(StimulusManager, self).__getattr__(item)

################################################
# BrainDataExternalData                        #
################################################
class BrainDataExternalData(ManagedGroup,
                            StimulusManager):
    """
    Class for management of the 'external' group for storage of recordings external to the brain.
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataExternalData, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the group.
        """
        spec = GroupSpec(group='external',
                         prefix=None,
                         optional=False,
                         description='Managed group for storage of external data related to the internal brain data.')
        # Add stimulus managed object
        stimulus_spec = ManagedSpec(format_type="BrainDataStimulus", optional=True)
        spec.add_managed(stimulus_spec)
        return spec

    def populate(self, **kwargs):
        """
        Populate the 'data' group with all required elements.
        """
        pass

    def __getattr__(self, item):
        try:
            return StimulusManager.__getattr__(self, item)
        except:
            try:
                return ManagedGroup.__getattribute__(self, item)
            except AttributeError:
                return ManagedGroup.__getattr__(self, item)


################################################
# BrainDataDescriptors                         #
################################################
class BrainDataDescriptors(ManagedGroup):
    """
    Class for management of the `descriptors` group for storage of data descriptions and metadata.

    :ivar hdf_object: See ManagedFile

    Implicit instance variables (i.e., these are mapped names but not stored explicitly)

    :ivar static_#: Same as self.static(#) where # is the index of the static descriptor group.
                    The current format assumes a single static group, i.e., only static_0 is
                    typically valid.
    :ivar dynamic_#: Same as self.dynamic(#) where # is the index of the static descriptor group.
                    The current format assumes a single static group, i.e., only static_0 is
                    typically valid.
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataDescriptors, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the group.
        """
        return {'datasets': {},
                'groups': {},
                'managed_objects': [{'format_type': 'BrainDataStaticDescriptors', 'optional': False},
                                    {'format_type': 'BrainDataDynamicDescriptors', 'optional': False}],
                'attributes': [],
                'group': "descriptors",
                'prefix': None,
                'optional': False,
                'description': 'Managed group for storage of a collection of brain data descriptors.'}

    def populate(self, **kwargs):
        """
        Populate the 'descriptors' group with all required elements.
        """
        parent_object = self.hdf_object
        BrainDataDynamicDescriptors.create(parent_object=parent_object)
        BrainDataStaticDescriptors.create(parent_object=parent_object)

    def static(self, index=0):
        """
        Get the static descriptors managed object with the static descriptor data.
        In principal the current format defines that there should be always
        exactly one static descriptor group. However, multiple could be supported
        so this function is prepared to handle this in case the format should
        change in the future.

        :param index: Optional input parameter to define the index of static descriptors.
                      Default is index=0.

        :returns: BrainDataEcoGProcessed object or None
        """
        staticobjs = BrainDataStaticDescriptors.get_all(self.hdf_object)
        if len(staticobjs) == 0:
            return None
        else:
            return BrainDataStaticDescriptors(staticobjs[index])

    def dynamic(self, index):
        """
        Get the dynamic descriptors managed object with the dynamic descriptor data.
        In principal the current format defines that there should be always
        exactly one dynamic descriptor group. However, multiple could be supported
        so this function is prepared to handle this in case the format should
        change in the future.

        :param index: Optional input parameter to define the index of dynamic descriptors.
                      Default is index=0.

        :returns: BrainDataEcoGProcessed object or None
        """
        dynamicobjs = BrainDataDynamicDescriptors.get_all(self.hdf_object)
        if len(dynamicobjs) == 0:
            return None
        else:
            return BrainDataDynamicDescriptors(dynamicobjs[index])

    def __getattr__(self, item):
        if item.startswith('static_'):
            indexstr = item.lstrip('static_')
            if indexstr.isdigit():
                return self.static(int(indexstr))
            else:
                raise AttributeError("'BrainDataDescriptors' has not attribute" + item)
        elif item.startswith('dynamic_'):
            indexstr = item.lstrip('dynamic_')
            if indexstr.isdigit():
                return self.dynamic(int(indexstr))
            else:
                raise AttributeError("'BrainDataDescriptors' has not attribute" + item)
        else:
            try:
                return super(BrainDataDescriptors, self).__getattribute__(item)
            except AttributeError:
                return super(BrainDataDescriptors, self).__getattr__(item)


#########################################
#  Instrument Manager
########################################
class InstrumentManager(object):
    """
    Base helper class for subclasses that need to manage BrainDataStaticDescriptorInstrument
    """
    def __init__(self, **kwargs):
        """
        :param kwargs: Pass any arguments through to super
        """
        try:
            # In cas our super is ManagedObject and we need to pass hdf_object through
            super(InstrumentManager, self).__init__(**kwargs)
        except:
            # We are the last one in the super call hierarchy and we can't pass arguments through to object
            super(InstrumentManager, self).__init__()

    def num_instrument_data(self):
        """
        Get the number of instrument datasets available.
        """
        return len(BrainDataStaticDescriptorInstrument.get_all(self.hdf_object))

    def instrument_data(self, index=0):
        """
        Get the instruemnt managed object with the ephys data.

        :param index: Optional input parameter to define the index of the ephys dataset.

        :returns: BrainDataStaticDescriptorInstrument object or None
        """
        instrumentobjs = BrainDataStaticDescriptorInstrument.get_all(self.hdf_object)
        if len(instrumentobjs) == 0:
            return None
        else:
            return BrainDataStaticDescriptorInstrument(instrumentobjs[index])

    def __getattr__(self, item):
        if item.startswith('instrument_'):
            indexstr = item.lstrip('instrument_')
            if indexstr.isdigit():
                return self.instrument_data(int(indexstr))
            else:
                raise AttributeError("'BrainDataStatisDescriptors' has no attribute " + item)
        else:
            try:
                return super(InstrumentManager, self).__getattribute__(item)
            except AttributeError:
                return super(InstrumentManager, self).__getattr__(item)


################################################
# BrainDataStaticDescriptors                   #
################################################
class BrainDataStaticDescriptors(ManagedGroup,
                                 InstrumentManager):
    """
    Class for management of the 'static' group for storage of static data descriptors.
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataStaticDescriptors, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the group.
        """
        spec = GroupSpec(group="static",
                         prefix=None,
                         optional=False,
                         description='Managed group for storage of static descriptors.')
        instrument_spec = ManagedSpec(format_type="BrainDataStaticDescriptorInstrument", optional=True)
        spec.add_managed(instrument_spec)
        metadata_spec = ManagedSpec(format_type="BrainDataMetadataGroup", optional=True)
        spec.add_managed(metadata_spec)
        return spec

    def populate(self, **kwargs):
        """
        Populate the 'data' group with all required elements.
        """
        pass

    def __getattr__(self, item):
        try:
            return InstrumentManager.__getattr__(self, item)
        except:
            try:
                return ManagedGroup.__getattribute__(self, item)
            except AttributeError:
                return ManagedGroup.__getattr__(self, item)


################################################
# BrainDataMetadataDataset                     #
################################################
class BrainDataMetadataDataset(ManagedDataset):
    """
    Dataset for storing a piece of metadata
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataMetadataDataset, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the dataset
        """
        spec = DatasetSpec(dataset=None,
                           prefix=None,
                           description="Metadata storage dataset",
                           optional=False,
                           attributes=None,
                           dimensions=None,
                           dimensions_fixed=None,
                           primary=None,
                           relationships=None)
        spec.add_attribute(AttributeSpec(attribute='unit',
                                         prefix=None,
                                         optional=True,
                                         description="Attribute describing the units of the metadata"))
        spec.add_attribute(AttributeSpec(attribute='ontology',
                                         prefix=None,
                                         optional=True,
                                         description="Attribute describing the ontology used for the metadata"))
        spec.add_attribute(AttributeSpec(attribute='user_description',
                                         prefix=None,
                                         optional=True,
                                         description="Attribute describing the purpose of the dataset"))
        return spec

    def populate(self, unit=None, ontology=None, user_description=None):
        """
        Populate the metadata data object

        :param unit: The unit of the metadata object
        :param ontology: The ontology to be used for the object.
        :param user_description: Optional string with the user's description of the data
        """
        spec = BaseSpec.from_dict(self.get_format_specification())
        if unit is not None:
            unit_attr = BaseSpec.from_dict(spec.get_attribute('unit'))
            unit_attr['value'] = unit
            unit_attr.create_attribute(parent_object=self.hdf_object)
        if ontology is not None:
            ontology_attr = BaseSpec.from_dict(spec.get_attribute('ontology'))
            ontology_attr['value'] = ontology
            ontology_attr.create_attribute(parent_object=self.hdf_object)
        if user_description is not None:
            user_attr = BaseSpec.from_dict(spec.get_attribute('user_description'))
            user_attr['value'] = user_description
            user_attr.create_attribute(parent_object=self.hdf_object)


################################################
# BrainDataMetadataGroup                      #
################################################
class BrainDataMetadataGroup(ManagedGroup):
    """
    Group for storing arbitrary metadata collection
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataMetadataGroup, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the dataset
        """
        spec = GroupSpec(group=None,
                         prefix=None,
                         description="Metadata storage group")
        spec.add_managed(ManagedSpec(format_type="BrainDataMetadataDataset", optional=True))
        return spec

    def populate(self):
        """
        Populate the metadata data object
        """
        pass


################################################
# BrainDataStimulus            #
################################################
class BrainDataStimulus(ManagedGroup):
    """
    Group for storing static metadata about a stimulus
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataStimulus, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the dataset
        """
        spec = GroupSpec(group=None,
                         prefix="stimulus_",
                         description="Group stroing metadata about a stimulus")
        type_spec = DatasetSpec(dataset='type',
                                prefix=None,
                                description="The type of stimulus",
                                optional=False)
        spec.add_dataset(type_spec)
        spec.add_managed(ManagedSpec(format_type="BrainDataMetadataDataset", optional=True))
        return spec

    def populate(self, type):
        """
        :param type: The type of stimulus used
        """
        spec = BaseSpec.from_dict(self.get_format_specification())
        type_spec = spec.get_dataset(dataset='type')
        self.hdf_object[type_spec['dataset']] = type
        self.hdf_object[type_spec['dataset']].attrs[ManagedObject.description_attribute_name] = type_spec['description']


################################################
# BrainDataStaticDescriptorInstrument          #
################################################
class BrainDataStaticDescriptorInstrument(ManagedGroup):
    """
    Group for storing static metadata about an instrument
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataStaticDescriptorInstrument, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the dataset
        """
        spec = GroupSpec(group=None,
                         prefix="instrument_",
                         description="Group storing metadata about an instrument")
        type_spec = DatasetSpec(dataset='type',
                                prefix=None,
                                description="The type of instrument",
                                optional=False)
        spec.add_dataset(type_spec)
        layout_spec = DatasetSpec(dataset='layout_index',
                                  prefix=None,
                                  optional=True,
                                  description="Dataset describing the instrument layout of different recording channels.")
        spec.add_dataset(layout_spec)
        layout_loc_spec = DatasetSpec(dataset='layout_locations',
                                      prefix=None,
                                      optional=True,
                                      description="Dataset describing the location of the different recording channels in space.")
        spec.add_dataset(layout_loc_spec)
        spec.add_managed(ManagedSpec(format_type="BrainDataMetadataDataset", optional=True))
        return spec

    def populate(self, type, layout_index=None, layout_locations=None):
        """
        :param type: The type of stimulus used
        """
        spec = BaseSpec.from_dict(self.get_format_specification())
        type_spec = spec.get_dataset(dataset='type')
        self.hdf_object[type_spec['dataset']] = type
        self.hdf_object[type_spec['dataset']].attrs[ManagedObject.description_attribute_name] = type_spec['description']

        if layout_index is not None:
            layout_index_spec = spec.get_dataset(dataset='layout_index')
            self.hdf_object[layout_index_spec['dataset']] = layout_index
            self.hdf_object[layout_index_spec['dataset']].attrs[ManagedObject.description_attribute_name] = \
                layout_index_spec['description']

        if layout_locations is not None:
            layout_locations_spec = spec.get_dataset(dataset='layout_locations')
            self.hdf_object[layout_locations_spec['dataset']] = layout_locations
            self.hdf_object[layout_locations_spec['dataset']].attrs[ManagedObject.description_attribute_name] = \
                layout_locations_spec['description']


################################################
# BrainDataStaticDescriptors                   #
################################################
class BrainDataDynamicDescriptors(ManagedGroup):
    """
    Class for management of the 'dynamic' group for storage of dynamic data descriptors.
    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataDynamicDescriptors, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the group.
        """
        spec = GroupSpec(group='dynamic',
                         prefix=None,
                         optional=False,
                         description='Managed group for storage of static descriptors.')
        metadata_spec = ManagedSpec(format_type="BrainDataMetadataGroup", optional=True)
        spec.add_managed(metadata_spec)
        return spec

    def populate(self, **kwargs):
        """
        Populate the 'data' group with all required elements.
        """
        pass


################################################
# BrainDataCollection                            #
################################################
class BrainDataCollection(ManagedGroup, EphysManager):
    """
    Class for management of the 'modality_' group for storage of brain recordings from a particular modality.

    :ivar hdf_object: See ManagedGroup

    Implicit instance variables (i.e., these are mapped names but not stored explicitly)

    :ivar ephys_data_#: Same as self.ephys_data(#) where # is the index of the raw ephys dataset.
    :ivar ephys_data_processed_#: Same as self.ephys_data_processed(#) where # is the index of the processed ephys dataset.

    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataCollection, self).__init__(hdf_object=hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format of the group.
        """
        spec = GroupSpec(group=None,
                         prefix='collection_',
                         optional=True,
                         description='Container for storing a collection of related datasets, e.g. '+
                                     'from a single session or modality')
        spec.add_managed(ManagedSpec(format_type='BrainDataEphys', optional=True))
        spec.add_managed(ManagedSpec(format_type='BrainDataEphysProcessed', optional=True))
        return spec

    def populate(self, **kwargs):
        """
        Populate the 'data' group with all required elements.
        """
        pass

    def __getattr__(self, item):
        try:
            return EphysManager.__getattr__(self, item)
        except:
            try:
                return ManagedGroup.__getattribute__(self, item)
            except AttributeError:
                return ManagedGroup.__getattr__(self, item)

################################################
# BrainDataEphys                                #
################################################
class BrainDataEphys(ManagedGroup):
    """
    Class for management of managed h5py.Group  objects structured to store Ephys brain recording data.

    :ivar hdf_object: See ManagedObject
    :ivar auto_expand: Expand the dataset upon write automatically, i.e., if a write points to
        elements that are out of bounds of the current dataset size, then the dataset size is
        changed (if possible) to accommodate the write to the new location.

    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group or h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataEphys, self).__init__(hdf_object)
        self.auto_expand = False

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format.
        """
        return {'datasets': {'ephys_data': {'dataset': 'raw_data',             # dataset key is mandatory may be None
                                           'prefix': None,                    # prefix key is mandatory may be None
                                           'optional': False,                 # optional key is mandatory
                                           'primary': True,                   # this is a primary dataset for analysis
                                           # dimensions key is optional. If specified we assume that the number of
                                           # dimensions is fixed and that a scale is specified for all dimensions,
                                           # even if that specification is empty, i.e., no fixed scale is given.
                                           # NOTE: if multiple scales are defined for the same axis, then the name
                                           #       for those axis must be the same, while the unit key may differ
                                           #       between scales for the same dimension
                                           'dimensions': [{'name': 'space',     # Mandatory
                                                           'unit': 'id',        # Mandatory
                                                           'optional': False,   # Mandatory
                                                           'dataset': 'electrode_id',  # Mandatory. Set to None if
                                                                                       # the axis should only be labeled
                                                                                       # but not data be associated
                                                                                       # with the scale
                                                           'axis': 0,           # Mandatory
                                                           'description': 'Id of the recording electrode'},  # Mandatory
                                                          {'name': 'time', # use name = 'None' for axis without a scale
                                                           'unit': 'ms',
                                                           'optional': False,
                                                           'dataset': 'time_axis',
                                                           'axis': 1,
                                                           'description': 'Sample time in ms'},
                                                          {'name': 'space',
                                                           'unit': 'region name',
                                                           'optional': True,
                                                           'dataset': 'anatomy_name',
                                                           'axis': 0,
                                                           'description': 'Name of region location of the electrodes'},
                                                          {'name': 'space',
                                                           'unit': 'region id',
                                                           'optional': True,
                                                           'dataset': 'anatomy_id',
                                                           'axis': 0,
                                                           'description': 'Integer id of the region location ' +
                                                                          'of the electrodes'}],
                                           'dimensions_fixed': True,
                                           'description': 'Dataset with the Ephys recordings data',  # Mandatory
                                           'attributes': [{'attribute': 'unit',   # Mandatory but may be empty
                                                           'value': 'Volt',  # Mandatory. Use None for user-defined
                                                           'prefix': None,        # Mandatory
                                                           'optional': False}]},   # Mandatory
                             'sampling_rate': {'dataset': 'sampling_rate',
                                               'prefix': None,
                                               'optional': False,
                                               'attributes': [{'attribute': 'unit',
                                                               'value': 'Hz',
                                                               'prefix': None,
                                                               'optional': False}],
                                               'description': 'Sampling rate in Hz'},
                             'layout': {'dataset': 'layout',
                                        'prefix': None,
                                        'optional': True,
                                        'dimensions': [],
                                        'attributes': [],
                                        'description': 'The physical layout of the electrodes.'}},
                'groups': {},           # Mandatory
                'managed_objects': [{'format_type': 'AnnotationDataGroup',
                                     'optional': True}],  # e.g.,{'format_type': 'BrainDataEphys', 'optional': True}
                'attributes': [],  # e.g, [{'attribute': 'unit', 'value': Hz, 'prefix': None, 'optional': False}]
                'group': None,          # Mandatory (use 'dataset' in case of a managed dataset
                'prefix': "ephys_data_",
                'optional': False,
                'description': 'Managed group for storage of raw Ephys recordings.'}
                # For file descriptions set the group="/" . If prefix is given it is assumed that all
                # names file of that type must start with prefix.

    def populate(self,
                 ephys_data=None,
                 ephys_data_shape=None,
                 ephys_data_type=None,
                 sampling_rate=None,
                 electrode_ids=None,
                 start_time=0,
                 time_data=None,
                 layout=None,
                 anatomy_names=None,
                 anatomy_ids=None,
                 format_spec=None,
                 chunks=True,
                 compression=None,
                 compression_opts=None):
        """
        The populate method is called by the create method after the basic common setup is complete.
        The function should be used to populate the managed object (e.g., add dimensions to a datadset or
        add required datasets to a group. The populate method is passed the kwargs handed to the create
        method.

        :param ephys_data: The neurallogical recordings data to be written. If present then ephys_data_shape
                           and ephys_data_dtype are ignored.
        :type ephys_data: 2D numpy array of [#channels, #samplesPerChannel]
        :param ephys_data_shape: Shape of the neural data set. Required if ephys_data is not present. If
                           ephys_data_shape is ignored.
        :type ephys_data_shape: 2D tuple indicating the number of recordings and samples per recording.
        :param ephys_data_type: Datatype of the neural data set. Required if ephys_data is not present. If
                           ephys_data_shape is ignored.
        :type ephys_data_type: numpy.dtype or string indicating the dtype
        :param sampling_rate: Sampling rate in Hz
        :type sampling_rate: int or float
        :param electrode_ids: 1D Numpy array of length ephys_data_shape[0] with the electrode ids. If None, then
            the array will be set to np.arange(ephys_data_shape[0]).
        :type electrode_ids: 1D numpy array or None
        :param start_time: Start time of the recording, usually in the form of time since epoch
                           (default behavior by time.time())
                           The Unix epoch (or Unix time or POSIX time or Unix timestamp) is the number of seconds
                           that have elapsed since January 1, 1970 (midnight UTC/GMT), not counting leap seconds (
                           in ISO 8601: 1970-01-01T00:00:00Z). Literally speaking the epoch is Unix time 0
                           (midnight 1/1/1970), but 'epoch' is often used as a synonym for 'Unix time'.
        :type start_time: int or float
        :param time_data: The dimensions scale data associated with the time axis. If None (default), then
            the sampling_rate  and start_time will be used ot initialize the time_data
        :param layout: The spatial layout of the ephys_data.
        :type layout: numpy array of ints describing for each channel index its location in the layout matrix.
                      Use -1 to indicate unoccupied channels. This strategy allows complex layouts to be described
                      via a rectangular matrix.
        :param anatomy_names: 1D array of strings with the name of the region an electrode is located in.
        :param anatomy_ids: 1D array of ints with the integer id's of the regions the electrode. Note,
                            if anatomy_names is given and anatomy_ids is empty, then the array will be
                            autogenerated by creating an index for the unique values in the anatomy_names array.
        :param format_spec: Format specification. Usually this is None, indicating that the specification defined
                            by self.get_format_specification should be used. In some cases, derived classes will
                            expand the base class specification or use the same specification, simply with
                            different names and values. To allow the base classes to reuse the populate function,
                            the function allows a different format specification to be provided as input.
                            CAUTION! The format_spec must be compliant with the structure described by the specification
                            for this class in order for the populate function to work correctly.
        :param chunks: User-defined chunking for the ephys dataset. This is by default set to True, in which case
            h5py will automatically determine a chunking. Chunking is used to enable dynamic expansion of the data
            in the time dimension. User defined chunkings should be a tuple containing for each dimensions the
            size of the chunk. Typically this is a tuple of length 2, but derived classes may expand the
            dimensionality of the ephys dataset.
        :param compression: String with the currently applied compression filter, or None if compression is not
            enabled for the EcOG dataset. Typical compression options are 'gzip' (available with all HDF5
            installations), 'lzf' (available with all h5py installations, 'szip' (may not be available
            with all installations)
        :param compression_opts: Options for the compression filter. E.g., in the case of gzip an integer between
            0 and 9 may be given to set the compression level.

        :raises: ValueError is raised in case that the object cannot be populated.
        """

        # Create the neural dataset
        if not format_spec:
            format_spec = self.get_format_specification()
        # Set the format specification attribute
        self.hdf_object.attrs[ManagedObject.format_spec_attribute_name] = json.dumps(format_spec)
        data_spec = format_spec['datasets']
        if ephys_data is not None:
            ephys_data_shape = ephys_data.shape
        # Check that the number of dimensions of the dataset are valid
        expected_num_dims = self.get_num_dimensions_from_dataset_specification(data_spec)
        if expected_num_dims is not None:
            if expected_num_dims[0] > len(ephys_data.shape):
                raise ValueError('ephys_data must have at least %s dimensions found %s' %
                                 (str(expected_num_dims[0]), str(len(ephys_data.shape))))
            if expected_num_dims[1] < len(ephys_data.shape):
                raise ValueError('ephys_data must have at most %s dimensions found %s' %
                                 (str(expected_num_dims[1]), str(len(ephys_data.shape))))
        # Create the maximum shape for the dataset
        if chunks:
            ephys_data_maxshape = [ephys_data_shape[0], None]
            for additional_dim in range(2, len(ephys_data_shape)):
                ephys_data_maxshape.append(ephys_data_shape[additional_dim])
            ephys_data_maxshape = tuple(ephys_data_maxshape)
        else:
            ephys_data_maxshape = None

        # Generate the dataset either by setting the data or generating an empty dataset of the given shape
        if ephys_data is not None:
            ephys_dataset = self.hdf_object.create_dataset(name=data_spec['ephys_data']['dataset'],
                                                          data=ephys_data,
                                                          dtype=ephys_data.dtype,
                                                          chunks=chunks,
                                                          compression=compression,
                                                          compression_opts=compression_opts,
                                                          maxshape=ephys_data_maxshape,
                                                          fillvalue=np.nan)  # If int type is used then nan will be 0
        elif ephys_data_shape is not None and ephys_data_type is not None:
            ephys_dataset = self.hdf_object.require_dataset(name=data_spec['ephys_data']['dataset'],
                                                           shape=ephys_data_shape,
                                                           dtype=ephys_data_type,
                                                           chunks=chunks,
                                                           compression=compression,
                                                           compression_opts=compression_opts,
                                                           maxshape=ephys_data_maxshape,
                                                           fillvalue=np.nan)  # If int type is used then nan will be 0
        else:
            raise ValueError("If ephys_data is not set then ephys_data_shape and ephys_data_dtype are required.")

        # Create the basic attributes
        ephys_dataset.attrs[ManagedObject.description_attribute_name] = data_spec['ephys_data']['description']
        ephys_data_attr_spec = data_spec['ephys_data']['attributes'][0]
        if ephys_data_attr_spec['value'] is not None:
            ephys_dataset.attrs[ephys_data_attr_spec['attribute']] = ephys_data_attr_spec['value']
        else:
            ephys_dataset.attrs[ephys_data_attr_spec['attribute']] = ""

        # Create the time axis data
        if time_data is None:
            if sampling_rate:
                sample_period = 1000. / sampling_rate # (1000000.0 / sampling_rate)  # Sample period in ns
                stop_time = start_time + sample_period*ephys_data_shape[1]
                time_data = np.arange(start_time, stop_time, sample_period)
            else:
                time_data = np.empty(ephys_data_shape[1], dtype='float')
                time_data[:] = np.nan
        else:
            if time_data.shape[0] != ephys_dataset.shape[data_spec['ephys_data']['dimensions'][1]['axis']]:
                warnings.warn('The time_data dataset provided does not match the length of' +
                              ' the time axis of the Ephys dataset')


        self.add_dimension_scale(scale=time_data,
                                 dataset_name=data_spec['ephys_data']['dimensions'][1]['dataset'],
                                 unit=data_spec['ephys_data']['dimensions'][1]['unit'],
                                 description=data_spec['ephys_data']['dimensions'][1]['description'],
                                 axis=data_spec['ephys_data']['dimensions'][1]['axis'],
                                 fillvalue=np.nan,
                                 chunks=True,
                                 relationship_axes=0,
                                 maxshape=(None,))


        # Write the time axis data
        # time_dataset = self.hdf_object.create_dataset(name=data_spec['ephys_data']['dimensions'][1]['dataset'],
        #                                               data=time_data,
        #                                               fillvalue=np.nan,   # If int type is used then nan will be 0
        #                                               chunks=True,
        #                                               maxshape=(None,))
        # time_dataset.attrs[ManagedObject.description_attribute_name] = \
        #     data_spec['ephys_data']['dimensions'][1]['description']

        # Create spatial coordinate map
        if electrode_ids is None:
            electrode_ids = np.arange(ephys_dataset.shape[0])
            electrode_ids_fill = -1
        else:
            if electrode_ids.dtype == float:
                electrode_ids_fill = np.nan
            elif str(electrode_ids.dtype).startswith('|S'):
                electrode_ids_fill = ''
            else:
                electrode_ids_fill = -1

        self.add_dimension_scale(scale=electrode_ids,
                                 dataset_name=data_spec['ephys_data']['dimensions'][0]['dataset'],
                                 unit=data_spec['ephys_data']['dimensions'][0]['unit'],
                                 axis=data_spec['ephys_data']['dimensions'][0]['axis'],
                                 description=data_spec['ephys_data']['dimensions'][0]['description'],
                                 fillvalue=electrode_ids_fill,
                                 chunks=True,
                                 relationship_axes=0,
                                 maxshape=(None,))


        # electrode_id_dataset = self.hdf_object.create_dataset(name=data_spec['ephys_data']['dimensions'][0]['dataset'],
        #                                                       data=electrode_ids,
        #                                                       fillvalue=electrode_ids_fill, #np.nan,
        #                                                       chunks=True,
        #                                                       maxshape=(None,))
        # electrode_id_dataset.attrs[ManagedObject.description_attribute_name] = \
        #     data_spec['ephys_data']['dimensions'][0]['description']

        # Save the units for the different dimensions as the label of the dimensions
        eid_axis_index = data_spec['ephys_data']['dimensions'][0]['axis']
        time_axis_index = data_spec['ephys_data']['dimensions'][1]['axis']
        ephys_dataset.dims[eid_axis_index].label = data_spec['ephys_data']['dimensions'][0]['name']
        ephys_dataset.dims[time_axis_index].label = data_spec['ephys_data']['dimensions'][1]['name']
        # Create the scales for the different dimensions from the corresponding datasets created above
        # ephys_dataset.dims.create_scale(electrode_id_dataset, data_spec['ephys_data']['dimensions'][0]['unit'])
        # ephys_dataset.dims.create_scale(time_dataset, data_spec['ephys_data']['dimensions'][1]['unit'])
        # # Attach the scales to the dataset
        # ephys_dataset.dims[eid_axis_index].attach_scale(electrode_id_dataset)
        # ephys_dataset.dims[time_axis_index].attach_scale(time_dataset)

        # Save the sampling rate
        if sampling_rate:
            self.hdf_object[data_spec['sampling_rate']['dataset']] = sampling_rate
        else:
            self.hdf_object[data_spec['sampling_rate']['dataset']] = np.nan
        sr_dataset = self.hdf_object[data_spec['sampling_rate']['dataset']]
        sr_dataset.attrs[ManagedObject.description_attribute_name] = data_spec['sampling_rate']['description']
        sr_unit_attr_spec = data_spec['sampling_rate']['attributes'][0]
        sr_dataset.attrs[sr_unit_attr_spec['attribute']] = sr_unit_attr_spec['value']

        # Save the layout dataset if available
        if layout is not None:
            if isinstance(layout, h5py.Dataset):
                self.hdf_object[data_spec['layout']['dataset']] = h5py.SoftLink(layout.name)
            else:
                self.hdf_object[data_spec['layout']['dataset']] = layout
            layout_dataset = self.hdf_object[data_spec['layout']['dataset']]
            layout_dataset.attrs[ManagedObject.description_attribute_name] = data_spec['layout']['description']

        # Create/save the annotations
        if anatomy_names is not None or anatomy_ids is not None:
            my_annotations = AnnotationDataGroup.create(parent_object=self.hdf_object,
                                                        data_object=ephys_dataset,
                                                        collection_description='anatomy')

            # Initialize the anatomies ids array if needed. Note annotations need to be initialized first
            # as the anatomy will be stored as both dimension scales and as annotations.
            self.anatomy(anatomy_names=anatomy_names,
                         anatomy_ids=anatomy_ids,
                         format_spec=format_spec,
                         annotation_data_group=my_annotations)

    def add_dimension_scale(self,
                            scale,
                            unit,
                            axis,
                            dataset_name=None,
                            description=None,
                            relationship_axes=None,
                            copy_scale=False,
                            check_duplicate_names=False,
                            **kwargs):
        """
        Add a new description for one of the dimensions that is not part
        of the format specification. This functionality is provided to enable
        users to flexible add new descriptions for dimensions without having
        to update the core format.

        NOTE: The dimension is assumed to exists.

        :param scale: The numpy data array with the dimension description
        :type scale: 1D numpy array with the same length as the dimension.
        :param unit: String describing the units for the scale
        :type unit: basestring
        :param axis: Integer index indicating the axis the scale should be
                     associated with. This may also be string with the name
                     of the dimension/axis, in which case the index is determined
                     based on the given name.
        :param dataset_name: Optional string indicating the name for the dataset
                in which the scale array should be stored. If set to None (default)
                then a name will be automatically determined.
        :type dataset_name: basestring
        :param description: Optional string with the additional description of the
            the scale to help other users with interpretation of the data.
        :param relationship_axes: Indicate which axes of the scale are related via an 'order' relationship
               to the target object.
        :param copy_scale: If scale is and h5py.Dataset, should we leave it in place or should we copy it here?
        :param kwargs: Additional keyword arguments to be passed to the h5py.create_dataset during creation
               of the dimension scale
        :raises ValueError: A ValueError is raised in case the dimension scale
                does not match the expected format, e.g., the size of the
                scale does not match the size of the dataset.

        :return: The h5py.Dataset object created for the scale

        """
        # Get the objects we need
        main_dataset = self.ephys_data(get_hdf=True)
        main_group = self.hdf_object
        existing_keys = main_group.keys()
        create_order_relationship = relationship_axes is not None
        curr_scales = self.dims(axis=axis, get_hdf=False) if create_order_relationship else []

        # 1) Check if all input parameters are valid
        # 1.1 Check if the unit parameter is a string
        if not isinstance(unit, basestring):
            raise ValueError("Unit parameter is not a string")
        # 1.2 Check if the dataset_name is a string and the dataset is not in use yet
        if dataset_name is not None:
            if not isinstance(dataset_name, basestring):
                raise ValueError('dataset_name parameter is not a string')
            if dataset_name in existing_keys:
                raise ValueError('The given dataset_name is already used.')
        # 1.3 Check if the axis parameter is valid
        if isinstance(axis, basestring):
            axis = self.get_axis_id_by_name(axis)
            if axis is None:
                ValueError("Could not determine the index of the axis based on the given name.")

        if axis >= len(main_dataset.shape):
            raise ValueError("The index of the indicated axis is out of bounds.")
        # 1.4 Check if the scale dataset is a valid array
        if scale.shape[0] != main_dataset.shape[axis]:
            raise ValueError("The length of the first dimension of the scale does not match the length of the" +
                             " dimension/axis of the main data.")

        # 2 Create a name for the dataset_name if not set
        if dataset_name is None:
            dim_name = ""
            try:
                dim_name = main_dataset.dims[axis].label
            except:
                pass
            dataset_name = dim_name + "_" + unit

            # 2.1 Make sure the dataset_name does not exist and append an index to the name to make it unique
            if dataset_name in existing_keys:
                named_keys = [nk for nk in existing_keys if nk.startswith(dataset_name)]
                dataset_name += str(len(named_keys))
                if dataset_name in existing_keys:
                    raise ValueError("Could not determine an apporbriate dataset name. Set the dataset_name parameter.")

        # 3 Create the dataset for the dimensions scale
        if isinstance(scale, h5py.Dataset) and not copy_scale:
            scale_dataset = scale
            # 4 Create the optional description for the dimension scale
            if description is not None:
                scale_dataset.attrs[ManagedObject.description_attribute_name+'_scale_'+str(axis)+'_'+main_dataset.name] = description
        else:
            scale_dataset = main_group.create_dataset(name=dataset_name,
                                                      data=scale if not isinstance(scale, h5py.Dataset) else scale[...],
                                                      **kwargs)
            # 4 Create the optional description for the dimension scale
            if description is not None:
                scale_dataset.attrs[ManagedObject.description_attribute_name] = description

        # 5 Associate the scale with the dataset
        main_dataset.dims.create_scale(scale_dataset,
                                       unit)
        # 6 Attache the scale
        main_dataset.dims[axis].attach_scale(scale_dataset)

        # 7 Create the optional relationship between the dimension scale and the object
        if create_order_relationship:
            rel_name = 'order_' + scale_dataset.name + '_' + main_dataset.name
            create_attribute = True
            if check_duplicate_names:
                if RelationshipAttribute.RELATIONSHIP_ATTRIBUTE_PREFIX + rel_name not in scale_dataset.attrs.keys():
                    warnings.warn("Duplicated relationship name found: " + rel_name)
                    create_attribute = False
            if create_attribute:
                RelationshipAttribute.create(parent_object=scale_dataset,
                                                     target_object=main_dataset,
                                                     target_axis=axis,
                                                     axis=relationship_axes,
                                                     relationship_type='order',
                                                     description=str(description) + " (dimensions scale relationship)",
                                                     attribute=rel_name)

            rel_name = 'order_' + main_dataset.name  + '_' + scale_dataset.name
            create_attribute = True
            if check_duplicate_names:
                if RelationshipAttribute.RELATIONSHIP_ATTRIBUTE_PREFIX + rel_name not in main_dataset.attrs.keys():
                    warnings.warn("Duplicated relationship name found: " + rel_name)
                    create_attribute = False
            if create_attribute:
                RelationshipAttribute.create(parent_object=main_dataset,
                                             target_object=scale_dataset,
                                             target_axis=relationship_axes,
                                             axis=axis,
                                             relationship_type='order',
                                             description=str(description) + " (dimensions scale relationship)",
                                             attribute=rel_name)
            # Check which other dimension scales have order relationships with the current axis and add
            # order relationships for those as well
            for s in curr_scales:
                curr_s = s['dataset']
                curr_rels = RelationshipAttribute.get_all_relationships(curr_s)
                for rel in curr_rels.values():
                    if rel.relationship_type == 'order' and \
                            (rel.target.name == main_dataset.name and \
                             rel.target.file.filename == main_dataset.file.filename) and \
                            (rel.target_axis == axis or rel.target_axis == [axis,]):
                        rel_name = 'order_' + scale_dataset.name + "_" + curr_s.name
                        create_attribute = True
                        if check_duplicate_names:
                            if RelationshipAttribute.RELATIONSHIP_ATTRIBUTE_PREFIX + rel_name not in scale_dataset.attrs.keys():
                                warnings.warn("Duplicated relationship name found: " + rel_name)
                                create_attribute = False
                        if create_attribute:
                            RelationshipAttribute.create(parent_object=scale_dataset,
                                                         target_object=curr_s,
                                                         target_axis=rel.source_axis,
                                                         axis=relationship_axes,
                                                         relationship_type='order',
                                                         description='Secondary order relationship due to dimensions scale relationship with shared target ' + main_dataset.name,
                                                         attribute=rel_name)
                        rel_name = 'order_' + curr_s.name  + '_' + scale_dataset.name
                        create_attribute = True
                        if check_duplicate_names:
                            if RelationshipAttribute.RELATIONSHIP_ATTRIBUTE_PREFIX + rel_name not in curr_s.attrs.keys():
                                warnings.warn("Duplicated relationship name found: " + rel_name)
                                create_attribute = False
                        if create_attribute:
                            RelationshipAttribute.create(parent_object=curr_s,
                                                         target_object=scale_dataset,
                                                         target_axis=relationship_axes,
                                                         axis=rel.source_axis,
                                                         relationship_type='order',
                                                         description='Secondary order relationship due to dimensions scale relationship with shared target ' + main_dataset.name,
                                                         attribute=rel_name)
                        else:
                            pass #print rel_name + " already exists"

        # 8 Return out dataset
        return scale_dataset


    def get_axis_id_by_name(self, name):
        """
        Get the integer index for the dimensions/axis with the given name

        :param name: Name of the given dimension
        :type name: basestring

        :returns: Integer index for the given dimension/axis or None if
                  if an unknown name is given.
        """

        # Find the index of the axis with the given name
        for dimid, dim in enumerate(self.ephys_data(get_hdf=True).dims):
            if dim.label == name:
                return dimid
        return None

    def dims(self, get_hdf=True, axis=None):
        """
        Get the h5py Dimension scales associated with the ephys dataset

        :param get_hdf: Get the h5py.DimensionScale associated with the
                        ephys dataset if set to True (Default). If set to
                        False a list of lists of  dicts with a summary of
                        the dimensions is returned instead which is formatted as follows:
                        [axis_index][scale_index]. Each dict then
                        contains the name, unit, dataset, and axis information
                        for the dimensions scale.

        :param axis: Optional integer indicating that only the dimension for a
                     given axis should be returned. Optionally this may also be
                     a string with the name of the given dimension.
                     Default value is None, ie., the dimensions for all axes are returned.

        :returns List containing for each dimensions a list of dicts describing the
                 'name', 'unit', 'dataset' and 'axis' for the dimensions scale.
                 If the axis input parameter is set, then a single list of dicts is
                 returned instead.
        """
        dimensions = self.ephys_data().dims
        if isinstance(axis, basestring):
            axis_index = self.get_axis_id_by_name(axis)
            if axis_index is None:
                raise ValueError("Could not determine the index for the axis with name " + axis)
            else:
                axis = axis_index

        if get_hdf:
            if axis is None:
                return dimensions
            else:
                return dimensions[axis]
        else:
            dim_list = [[] for _ in range(len(self.ephys_data().shape))]
            for dimid, dim in enumerate(dimensions):
                for subscale_index, subscale in enumerate(dim.keys()):
                    subscale_dict = {'name': dim.label,
                                     'unit': subscale,
                                     'dataset': dim[subscale_index],
                                     'axis': dimid,
                                     'description': ''}
                    if ManagedObject.description_attribute_name in dim[subscale].attrs:
                        subscale_dict['description'] = dim[subscale].attrs[ManagedObject.description_attribute_name]
                    dim_list[dimid].append(subscale_dict)


            if axis is None:
                return dim_list
            else:
                return dim_list[axis]

    def anatomy(self,
                anatomy_names=None,
                anatomy_ids=None,
                format_spec=None,
                annotation_data_group=None,
                get_hdf=True):
        """
        Get the anatomy information associated with the dataset.

        :param anatomy_names: 1D array of strings with the name of the region an electrode is located in.
                            By setting this parameter we can add an anatomical description to a dataset
                            if it does not has one.
        :param anatomy_ids: 1D array of ints with the integer id's of the regions the electrode. Note,
                            if anatomy_names is given and anatomy_ids is empty, then the array will be
                            autogenerated by creating an index for the unique values in the anatomy_names array.
                            By setting this parameter we can add an anatomical description to a dataset
                            if it does not has one.
        :param format_spec: Format specification. Usually this is None, indicating that the specification defined
                            by self.get_format_specification should be used. In some cases, derived classes will
                            expand the base class specification or use the same specification, simply with
                            different names and values. To allow the base classes to reuse the populate function,
                            the function allows a different format specification to be provided as input.
                            CAUTION! The format_spec most be compliant with the structure described by the specification
                            for this class in order for the populate function to work correctly.
        :param annotation_data_group: Optional AnnotationDataGroup object used to store anatomy description as
                            annotations. This is only relevant when either the anatomy_names and/or anatomy_ids
                            parameter is specified. If the annotation_data_group is given then the anatomy will
                            automatically be transformed to annotations and added to the given annotations object.
        :type annotation_data_group: AnnotationDataGroup
        :param get_hdf: Get the value in numpy format (set to False) or the
                         h5py dataset for the layout data (set to True).
                         Default value is True.
        :type get_hdf: bool

        :raises KeyError: A KeyError is generated if a new anatomy_names array or anatomy_ids array is
                          given but a prior description already exists.

        :returns: 1D Numpy array indicating for each recording the name of the region. The
                  function also returns a second 1D numpy indicating for each recording the index
                  of the region the electrode was located in. Note, if any of the anatomy
                  arrays are missing, then None will be returned instead of the anatomy array.
        """
        # Create the anatomy data storage if possible
        if anatomy_names is not None or anatomy_ids is not None:
            if self.has_anatomy():
                raise KeyError("An anatomy description is already defined for the dataset.")
            # Initialize the anatomies ids array if needed
            if anatomy_names is not None and anatomy_ids is None:
                unique_names = np.unique(anatomy_names)
                anatomy_ids = np.zeros(shape=(anatomy_names.size, ),
                                       dtype='int')
                for aindex, aname in enumerate(unique_names):
                    anatomy_ids[anatomy_names == aname] = aindex

            # Get the format specification and ephys dataset
            if not format_spec:
                format_spec = self.get_format_specification()
            data_spec = format_spec['datasets']
            ephys_dataset = self.ephys_data()

            # Create the anatomy_names scale
            if anatomy_names is not None:
                anatomy_axis_index = data_spec['ephys_data']['dimensions'][2]['axis']
                anatomy_name_dataset = self.hdf_object.create_dataset(
                    name=data_spec['ephys_data']['dimensions'][2]['dataset'],
                    data=anatomy_names,
                    chunks=True,
                    maxshape=(None,))
                ephys_dataset.dims.create_scale(anatomy_name_dataset, data_spec['ephys_data']['dimensions'][2]['unit'])
                ephys_dataset.dims[anatomy_axis_index].attach_scale(anatomy_name_dataset)
                anatomy_name_dataset.attrs[ManagedObject.description_attribute_name] = \
                    data_spec['ephys_data']['dimensions'][2]['description']

            # Create the anatomy_ids scale
            if anatomy_ids is not None:
                anatomy_axis_index = data_spec['ephys_data']['dimensions'][3]['axis']
                anatomy_ids_dataset = self.hdf_object.create_dataset(
                    name=data_spec['ephys_data']['dimensions'][3]['dataset'],
                    data=anatomy_ids,
                    chunks=True,
                    maxshape=(None,))
                ephys_dataset.dims.create_scale(anatomy_ids_dataset, data_spec['ephys_data']['dimensions'][3]['unit'])
                ephys_dataset.dims[anatomy_axis_index].attach_scale(anatomy_ids_dataset)
                anatomy_name_dataset.attrs[ManagedObject.description_attribute_name] = \
                    data_spec['ephys_data']['dimensions'][3]['description']

            # Create an annotation for each unique anatomical description
            if annotation_data_group is not None:
                if anatomy_names is not None:
                    anatomy_array = anatomy_names
                else:
                    anatomy_array = anatomy_ids
                unique_anatomy_names = np.unique(anatomy_array)
                for aname in unique_anatomy_names:
                    selection_object = DataSelection(data_object=annotation_data_group.data_object)
                    selection_object[0, :] = anatomy_array == aname  # Equivalent to: selection_object['space'. :] = ..
                    anno = Annotation(annotation_type='anatomy',
                                      description=unicode(aname),
                                      data_selection=selection_object)
                    annotation_data_group.add_annotation(annotation=anno)
            self.hdf_object.file.flush()

        # Retrieve the anatomy data and return it
        anatomy_names = None
        anatomy_ids = None
        dimension_spec = self.get_format_specification()['datasets']['ephys_data']['dimensions']
        anatomy_names_spec = dimension_spec[2]
        anatomy_ids_spec = dimension_spec[3]
        space_axes = self.ephys_data().dims[anatomy_names_spec['axis']]
        if anatomy_names_spec['unit'] in space_axes.keys():
            anatomy_names = space_axes[anatomy_names_spec['unit']]
            if not get_hdf:
                anatomy_names = anatomy_names[:]
        if anatomy_ids_spec['unit'] in space_axes.keys():
            anatomy_ids = space_axes[anatomy_ids_spec['unit']]
            if not get_hdf:
                anatomy_ids = anatomy_ids[:]
        return anatomy_names, anatomy_ids

    def has_anatomy(self):
        """
        Check whether an anatomy description is available for the Ephys dataset.

        :returns: Boolean indicating whether an anatomy description by name and or id is available.
        """
        anames, aids = self.anatomy()
        return (anames is not None) or (aids is not None)

    def ephys_data(self, get_hdf=True):
        """
        Get the h5py.Dataset object of the ephys data.

        For get_hdf==True this is the same as self['raw_data'] and self['ephys_data'].

        For get_hdf==False this is the same as self['raw_data'][:] and self['ephys_data'][:]

        :param get_hdf: Get the value in numpy format (set to False) or the
                         h5py dataset for the sampling rate (set to True).
                         Default value is True.

        :returns: h5py.Dataset if get_hdf is True. numpy array of the full data
                  if get_hdf is False. None in case that the ephys data should be
                  missing
        """
        format_spec = self.get_format_specification()
        ephys_data_name = format_spec['datasets']['ephys_data']['dataset']
        try:
            if get_hdf:
                return self[ephys_data_name]
            else:
                return self[ephys_data_name][:]
        except KeyError:
            return None

    def sampling_rate(self, get_hdf=True):
        """
        Get the h5py.Dataset object of the sampling rate data. Same as
        self['sampling_rate']

        :param get_hdf: Get the value in numpy format (set to False) or the
                         h5py dataset for the sampling rate (set to True).
                         Default value is True.

        :returns: h5py.Dataset if get_hdf is True. Float if get_hdf is False.
                  None in case the sampling_rate data should be missing.

        """
        format_spec = self.get_format_specification()
        sr_data_name = format_spec['datasets']['sampling_rate']['dataset']
        try:
            if get_hdf:
                return self[sr_data_name]
            else:
                return self[sr_data_name][()]
        except KeyError:
            return None

    def layout(self, get_hdf=True, new_layout=None):
        """
        Get the h5py.Dataset of the layout of the ephys_data. Same as self['layout']

        :param get_hdf: Get the value in numpy format (set to False) or the
                         h5py dataset for the layout data (set to True).
                         Default value is True.
        :param new_layout: If a layout dataset has not been defined previously then
                         we can generate a new layout by providing an according
                         numpy data array (or h5py.Dataset) to this parameter.
        :returns: h5py.Dataset if get_hdf is True. Numpy array if get_hdf is False.
                  None in case the layout data does not exist.

        """
        format_spec = self.get_format_specification()
        layout_data_name = format_spec['datasets']['layout']['dataset']
        if new_layout is not None:
            if layout_data_name not in self.hdf_object.keys():
                self.hdf_object[layout_data_name] = new_layout[:]
            else:
                raise KeyError('A layout is already defined for the dataset. You may update the h5py object directly.')
        try:
            if get_hdf:
                return self[layout_data_name]
            else:
                return self[layout_data_name][:]
        except KeyError:
            return None

    def has_layout(self):
        """
        Check whether a layout description is available for the Ephys dataset.

        :returns: Boolean indicating whether a layout dataset is available.
        """
        return self.layout() is not None

    def create_ephys_annotation_collection(self,
                                          collection_description,
                                          infile=True):
        """
        Create a new, empty collection of annotations for the ephys dataset
        :param collection_description: The description for the annotation collection
        :param infile: Create the collection in file directly (default=True) or in memory only.
            If set to False, then an AnnotationCollection will be created in memory. If we want
            the in-memory collection to be saved to file, then we need to call the AnnotationCollection.save(..)
            function. If set to True (default), then an infile AnnotationDataGroup will be created
            and all changes will be saved to file.

        :return: New AnnotationDataGroup stored in the HDF5 file if infile==True.
            New AnnotationCollection available in memory only if infile==False.
        """
        if infile:
            my_annotations = AnnotationDataGroup.create(parent_object=self.hdf_object,
                                                        data_object=self.ephys_data(get_hdf=True),
                                                        collection_description=collection_description)
        else:
            my_annotations = AnnotationCollection(data=self.ephys_data(get_hdf=True),
                                                  collection_description=collection_description)
        return my_annotations

    def annotations(self, index=None):
        """
        Get the requested annotation object.

        :param index: The index of the annotation object to be retrieved.

        :returns: If index is specified to a value >=0 then a single AnnotationDataGroup object is returned
                  (or None if the index is invalid). Otherwise a list of all AnnoationDataGroup object is returned.
        """
        annotation_spec = AnnotationDataGroup.get_format_specification()
        annotation_prefix = annotation_spec['prefix']
        if index >= 0:
            annotation_name = annotation_prefix + str(index)
            if annotation_name in self.hdf_object.keys():
                return self.get_managed_object(hdf_object=self.hdf_object[annotation_name])
            else:
                return None
        else:
            return AnnotationDataGroup.get_all(parent_group=self.hdf_object,
                                               get_h5py=False)

    def num_annotations(self):
        """
        Get the number of annotation objects associated with the Ephys data.
        """
        return len(self.annotations())

    def has_annotations(self):
        """
        Check whether annotations exists for the Ephys dataset.
        """
        return self.num_annotations() > 0

    def set_auto_expand(self, auto_expand):
        """
        Define whether the ephys dataset should be automatically expanded upon write.
        """
        self.auto_expand = auto_expand

    def __getitem__(self, item):
        """
        Read data.
        """
        try:
            if item == 'ephys_data':
                format_spec = self.get_format_specification()
                item = format_spec['datasets']['ephys_data']['dataset']
            return self.hdf_object[item]
        except (KeyError, AttributeError):
            if isinstance(item, str) or isinstance(item, unicode):
                raise KeyError('Invalid key given')
            else:
                format_spec = self.get_format_specification()
                ephys_data_name = format_spec['datasets']['ephys_data']['dataset']
                return self.hdf_object[ephys_data_name][item]

    def __setitem__(self, key, value):
        """
        Write data.
        """
        # Check whether the given key has managed name

        # if key not in self.allowed_data_keys:
        #    if self.enforce_format:
        #        raise KeyError('None format compliant key specified for data write.')
        #    else:
        #        warnings.warn('None format compliant key specified for data write.' +
        #                      'The dataset will be added but the resulting file is ' +
        #                      'no longer fully compliant with the Brain Format.')

        # 1) We want to slice into the ephys dataset
        if isinstance(key, tuple) or isinstance(key, slice):

            # Get the ephys dataset
            format_spec = self.get_format_specification()
            ephys_data_name = format_spec['datasets']['ephys_data']['dataset']
            ephys_dataset = self.hdf_object[ephys_data_name]

            # Check if all dimensions are large enough and expand the dataset if needed
            if self.auto_expand:
                check_dims = key if isinstance(key, tuple) else (key,)
                for axis_index, selection in enumerate(check_dims):
                    axis_length = ephys_dataset.shape[axis_index]
                    selection_max = 0
                    if isinstance(selection, slice):
                        if selection.stop is not None:
                            selection_max = selection.stop-1
                        else:
                            selection_max = axis_length-1
                    if isinstance(selection, int):
                        selection_max = selection
                    if isinstance(selection, list) or isinstance(selection, np.ndarray):
                        selection_max = np.max(selection)
                    if selection_max >= axis_length:
                        # Resize the data as well as all the dimension scales associated with that axis
                        self.__resize_axis__(ephys_dataset=ephys_dataset,
                                             axis_index=axis_index,
                                             new_axis_size=selection_max+1)

            # Write the data to the ephys dataset
            ephys_dataset[key] = value
        # 2) We want to retrieve a dataset from the managed group based on the dataset's name
        elif isinstance(key, str) or isinstance(key, unicode):
            if key == 'ephys_data':
                key = self.get_format_specification()['datasets']['ephys_data']['dataset']
            self.hdf_object[key] = value

    def __resize_axis__(self, ephys_dataset, axis_index, new_axis_size):
        ephys_dataset.resize(new_axis_size, axis=axis_index)
        axis_dim_scales = ephys_dataset.dims[axis_index]
        for scale in axis_dim_scales.keys():
            if len(axis_dim_scales[scale].shape) == 1:
                axis_dim_scales[scale].resize(new_axis_size, 0)

    def __getattr__(self, item):
        if item == 'shape':
            format_spec = self.get_format_specification()
            item = format_spec['datasets']['ephys_data']['dataset']
            return self.hdf_object[item].shape
        else:
            try:
                return super(BrainDataEphys, self).__getattribute__(item)
            except AttributeError:
                return super(BrainDataEphys, self).__getattr__(item)

################################################
# BrainDataEphysProcessed                       #
################################################
class BrainDataEphysProcessed(BrainDataEphys):
    """
    Class for management of h5py.Group objects structured to store processed Ephys brain recording data.

    :ivar hdf_object: See ManagedObject and BrainDataEphys.

    """
    def __init__(self, hdf_object):
        """
        :param hdf_object: The h5py.Group or h5py.Dataset object managed by the current instance.

        :raises: ValueError in case the given hdf_object does not match the type expected by the class
                 as indicated byt the get_managed_object_type() function.
        """
        super(BrainDataEphysProcessed, self).__init__(hdf_object)

    @classmethod
    def get_format_specification(cls):
        """
        Get dictionary describing the format.

        This function adapts the specification of the BrainDataEphys class.

        :returns: Dictionary with the format specification.

        """
        # Update the specification from the base class, i.e., BrainDataEphys.
        spec = super(BrainDataEphysProcessed, cls).get_format_specification()
        # Update the prefix for the group
        spec['prefix'] = 'ephys_data_processed_'
        # Set the description and name for the main dataset
        spec['datasets']['ephys_data']['dataset'] = 'processed_data'
        spec['description'] = 'Managed group for storage of processed Ephys recordings.'
        # Make the value of 'units' attribute to None indicating that the value is user-defined.
        spec['datasets']['ephys_data']['attributes'][0]['value'] = None
        # Add optional band attribute to indicate the bands that have been processed
        spec['datasets']['ephys_data']['attributes'].append({'attribute': 'original_name',
                                                            'value': None,
                                                            'prefix': None,
                                                            'optional': True})
        # Update the name of the electrodes dataset since we may have different meanings for space
        spec['datasets']['ephys_data']['dimensions'][0]['dataset'] = 'spatial_id'
        # Add dimension for frequency bands in the processed data
        spec['datasets']['ephys_data']['dimensions'].append({'name': 'channels',
                                                            'unit': 'Hz',
                                                            'optional': True,
                                                            'dataset': 'frequency_bands',
                                                            'axis': 2,
                                                            'description': 'Frequency bands of the channels'})
        # Add dimension for token data as third dimension
        spec['datasets']['ephys_data']['dimensions'].append({'name': 'channels',
                                                            'unit': 'token id',
                                                            'optional': True,
                                                            'dataset': 'token_id',
                                                            'axis': 2,
                                                            'description': 'Integer Id of the token type'})
        # Add dimension for token data as third dimension
        spec['datasets']['ephys_data']['dimensions'].append({'name': 'channels',
                                                            'unit': 'token name',
                                                            'optional': True,
                                                            'dataset': 'token_name',
                                                            'axis': 2,
                                                            'description': 'Name of the token type'})
        return spec

    def populate(self,
                 ephys_data=None,
                 ephys_data_shape=None,
                 ephys_data_type=None,
                 ephys_data_units=None,
                 sampling_rate=16,
                 spatial_ids=None,
                 start_time=0,
                 layout=None,
                 anatomy_names=None,
                 anatomy_ids=None,
                 original_name=None,
                 frequency_bands=None,
                 token_names=None,
                 token_ids=None,
                 compression=None,
                 compression_opts=None):
        """
        The populate method is called by the create method after the basic common setup is complete.
        The function should be used to populate the managed object (e.g., add dimensions to a datadset or
        add required datasets to a group. The populate method is passed the kwargs handed to the create
        method.

        :param ephys_data: The neurallogical recordings data to be written. If present then ephys_data_shape
                           and ephys_data_dtype are ignored.
        :type ephys_data: 2D numpy array of [#channels, #samplesPerChannel]
        :param ephys_data_shape: Shape of the neural data set. Required if ephys_data is not present. If
                           ephys_data_shape is ignored.
        :type ephys_data_shape: 2D tuple indicating the number of recordings and samples per recording.
        :param ephys_data_type: Datatype of the neural data set. Required if ephys_data is not present. If
                           ephys_data_shape is ignored.
        :type ephys_data_type: numpy.dtype or string indicating the dtype
        :param ephys_data_units: String indicating the units used for the processed Ephys recordings.
        :type ephys_data_units: String
        :param sampling_rate: Sampling rate in Hz
        :type sampling_rate: int or float
        :param spatial_ids: 1D Numpy array of length ephys_data_shape[0] with the spatial ids (e.g, of the electrodes).
            If None, then the array will be set to np.arange(ephys_data_shape[0]).
        :param start_time: Start time of the recording in time since epoch (default behavior by time.time())
                           The Unix epoch (or Unix time or POSIX time or Unix timestamp) is the number of seconds
                           that have elapsed since January 1, 1970 (midnight UTC/GMT), not counting leap seconds (
                           in ISO 8601: 1970-01-01T00:00:00Z). Literally speaking the epoch is Unix time 0
                           (midnight 1/1/1970), but 'epoch' is often used as a synonym for 'Unix time'.
        :type start_time: int64
        :param layout: The spatial layout of the ephys_data
        :type layout: numpy array of ints describing for each channel index its location in the layout matrix.
        :param anatomy_names: 1D array of strings with the name of the region an electrode is located in.
        :param anatomy_ids: 1D array of ints with the integer id's of the regions the electrode. Note,
                            if anatomy_names is given and anatomy_ids is empty, then the array will be
                            created automatically.
        :param original_name: Original name of the dataset as specified by the user
        :type original_name: String
        :param frequency_bands: Numpy array indicating the center of the frequency bands stored in the
                                third dimension of the processed data array. May be None.
        :type frequency_bands: 1D Numpy array fo length ephys_data.shape[2] (or ephys_data_shape[2], whichever applies).
                               Default value is None, in which case the dimensions scale for the third dimension is
                               omitted (as it is optional in the specification).
        :param token_ids: 1D numpy array of ints with the ids of tokens (optional). If set to None and token_names
                             is given, then the token_ids are initialized automatically.
        :param token_names: 1D numpy array of strings with the names of the tokens (optional). Note, tokens
                            are stored as dimensions scales as well as annotations in a separate annotation object.
        :param compression: String with the currently applied compression filter, or None if compression is not
            enabled for the EcOG dataset. Typical compression options are 'gzip' (available with all HDF5
            installations), 'lzf' (available with all h5py installations, 'szip' (may not be available
            with all installations)
        :param compression_opts: Options for the compression filter. E.g., in the case of gzip an integer between
            0 and 9 may be given to set the compression level.

        :raises: ValueError is raised in case that the object cannot be populated.
        """
        # 1) Initialize the object using the base Ephys functionality
        spec = self.get_format_specification()
        super(BrainDataEphysProcessed, self).populate(ephys_data=ephys_data,
                                                     ephys_data_shape=ephys_data_shape,
                                                     ephys_data_type=ephys_data_type,
                                                     sampling_rate=sampling_rate,
                                                     electrode_ids=spatial_ids,
                                                     start_time=start_time,
                                                     layout=layout,
                                                     anatomy_names=anatomy_names,
                                                     anatomy_ids=anatomy_ids,
                                                     format_spec=spec,
                                                     compression=compression,
                                                     compression_opts=compression_opts)
        ephys_dataset = self.hdf_object[spec['datasets']['ephys_data']['dataset']]

        # 2) Define the units attribute (emit warning if not given as the attribute should not be optional)
        unit_attr_spec = spec['datasets']['ephys_data']['attributes'][0]
        if ephys_data_units is not None:
            ephys_dataset.attrs[unit_attr_spec['attribute']] = ephys_data_units
        else:
            warnings.warn('Units for processed Ephys data are empty.')

        # 3) Define the original name attribute (do not warn as it is optional
        if original_name is not None:
            orig_name_attr_spec = spec['datasets']['ephys_data']['attributes'][1]
            ephys_dataset.attrs[orig_name_attr_spec['attribute']] = original_name

        # 4) Add the optional dimension scale for the frequency bands
        band_dim_spec = spec['datasets']['ephys_data']['dimensions'][-3]
        if len(ephys_dataset.shape) > band_dim_spec['axis']:
            ephys_dataset.dims[band_dim_spec['axis']].label = band_dim_spec['name']
            if frequency_bands is not None:
                self.add_dimension_scale(scale=frequency_bands,
                                 dataset_name=band_dim_spec['dataset'],
                                 unit=band_dim_spec['unit'],
                                 description=band_dim_spec['description'],
                                 axis=band_dim_spec['axis'],
                                 fillvalue=np.nan,
                                 chunks=True,
                                 relationship_axes=0,
                                 maxshape=(None,))

        # 5) Deal with the token data
        # 5.1) Create the token ids if not specified by the user
        if token_names is not None and token_ids is None:
            unique_names = np.unique(token_names)
            token_ids = np.zeros(shape=(token_names.size, ), dtype='int')
            for tindex, tname in enumerate(unique_names):
                token_ids[token_names == tname] = tindex

        # 5.2) Add the optional dimension scale for the token ids
        if token_ids is not None:
            # TODO use add_dimension_scale instead to automatically create additional relationships
            token_ids_dim_spec = spec['datasets']['ephys_data']['dimensions'][-2]
            token_ids_dataset = self.hdf_object.create_dataset(name=token_ids_dim_spec['dataset'],
                                                               data=token_ids,
                                                               chunks=True,
                                                               maxshape=(None,))
            ephys_dataset.dims.create_scale(token_ids_dataset, token_ids_dim_spec['unit'])
            ephys_dataset.dims[token_ids_dim_spec['axis']].attach_scale(token_ids_dataset)
            token_ids_dataset.attrs[ManagedObject.description_attribute_name] = token_ids_dim_spec['description']

        # 5.3) Add the optional dimension scale for the token names
        if token_names is not None:
            # TODO use add_dimension_scale instead to automatically create additional relationships
            token_names_dim_spec = spec['datasets']['ephys_data']['dimensions'][-1]
            token_names_dataset = self.hdf_object.create_dataset(name=token_names_dim_spec['dataset'],
                                                                 data=token_names,
                                                                 chunks=True,
                                                                 maxshape=(None,))
            ephys_dataset.dims.create_scale(token_names_dataset, token_names_dim_spec['unit'])
            ephys_dataset.dims[token_names_dim_spec['axis']].attach_scale(token_names_dataset)
            token_names_dataset.attrs[ManagedObject.description_attribute_name] = token_names_dim_spec['description']

        # 5.4) Create annotations for each unique token if necessary
        if token_names is not None or token_ids is not None:
            my_annotations = AnnotationDataGroup.create(parent_object=self.hdf_object,
                                                        data_object=ephys_dataset,
                                                        collection_description='tokens')
            token_annotation_array = token_ids
            if token_names is not None:
                token_annotation_array = token_names
            # Save selection for each token type
            unique_token_names = np.unique(token_annotation_array)
            for tname in unique_token_names:
                selection_object = DataSelection(data_object=my_annotations.data_object)
                selection_object[2, :] = token_annotation_array == tname  # Same as: selection_object['bands'. :] = ..
                anno = Annotation(annotation_type='multi_token',
                                  description=unicode(tname),
                                  data_selection=selection_object)
                my_annotations.add_annotation(annotation=anno)
            # Save a selection for each individual token
            for tindex, tname in enumerate(token_annotation_array):
                selection_object = DataSelection(data_object=my_annotations.data_object)
                selection_object[2, :] = np.zeros(token_annotation_array.size, dtype='bool')
                selection_object[2, tindex] = True
                anno = Annotation(annotation_type='single_token',
                                  description=unicode(tname),
                                  data_selection=selection_object)
                my_annotations.add_annotation(annotation=anno)

            self.hdf_object.file.flush()

    def original_name(self):
        """
        Get the original name specificed by the user for the dataset.

        :return: String indicating the original name of the dataset
                 specified by the user or None in case no user-defined
                 name was specified.
        """
        spec = self.get_format_specification()
        orig_name_attr_spec = spec['datasets']['ephys_data']['attributes'][1]
        orig_name_attr = orig_name_attr_spec['attribute']
        ephys_dataset = self.hdf_object[spec['datasets']['ephys_data']['dataset']]
        if orig_name_attr in ephys_dataset.attrs:
            return ephys_dataset.attrs[orig_name_attr]
        else:
            return None
