"""
Module with helper functions for creations of format modules, i.e., ManagedObject-based
class API's and formal format specifications.

The spec module provides a range of helper data structures that ease the iterative creation
for format specification for all data primitives (i.e., files, groups, datasets, managed, attributes, dimensions, etc.).
Using the data structures makes the creation of specification easier as format components can be
added step-by-step and add the same time the provided data structures ensure that the format specfications are
valid and document-standard-compliant. The modules also provide helper functions, e.g., to convert specification
documents from/to json or to compile format specification documents directly from a python API, e.g.:

>>> from brain.dataformat.spec import FormatDocument
>>> import brain.dataformat.brainformat as brainformat
>>> json_spec = FormatDocument.from_api(module_object=brainformat).to_json()

"""
import json
import os
import h5py


class BaseSpec(dict):
    """
    Base dictionary class usef for specificaiton of data formats

    :ivar valid_key: List of allowed key values for the dictionary. Set to None if all keys are allowed.
    """

    def __init__(self):
        super(BaseSpec, self).__init__()
        self.valid_keys = []

    def __setitem__(self, key, value):
        """
        Overwrite the setting of key value pairs to ensure that only keys that are
        part of the spec can be set.

        :param key: The key to set
        :param value: The value the key should be set to
        """
        if self.valid_keys is not None:
            if key not in self.valid_keys:
                raise ValueError(unicode(key) + ' is not a valid key for ' + unicode(self.__class__.__name__) +
                                 '. Valid keys are: ' + unicode(self.valid_keys))
        super(BaseSpec, self).__setitem__(key, value)

    def to_json(self, pretty=False):
        """
        Convert a given specification to json.

        :param pretty: Should a pretty formatting be used when encoding the JSON.

        :return: JSON string for the current specification
        """
        if pretty:
            return json.dumps(self, sort_keys=True, indent=4, separators=(',', ': '))
        else:
            return json.dumps(self)

    @staticmethod
    def from_json(spec):
        """
        Load a specification from json
        :param spec: The json string with the spec to be loaded
        :return: The specification loaded as a python dict
        """
        spec_dict = json.loads(spec)
        return BaseSpec.from_dict(spec=spec_dict)

    @staticmethod
    def from_dict(spec):
        """
        Create an instance of the given specification class from the given dict.
        :param spec: The dict with the given specification
        :return: Instance of the approbriate specification class object (e.g, DatasetSpec) for the given dict
        """
        if 'file_prefix' in spec:
            spec_object = FileSpec(**spec)
        elif 'group' in spec:
            spec_object = GroupSpec(**spec)
        elif 'target' in spec and 'relationship_type' in spec:
            spec_object = RelationshipSpec(**spec)
        elif 'attribute' in spec:
            spec_object = AttributeSpec(**spec)
        elif 'dataset' in spec and 'prefix' in spec:
            spec_object = DatasetSpec(**spec)
        elif 'unit' in spec:
            spec_object = DimensionSpec(**spec)
        elif 'format_type' in spec:
            spec_object = ManagedSpec(**spec)
        else:
            try:
                spec_object = FormatDocument(**spec)
            except ValueError:
                raise ValueError('Unknown specification type or invalid specification')
        return spec_object


class FormatDocument(BaseSpec):
    """
    A format specification document is a dict where the keys are the names of all Managed Types and
    the values are BaseSpecs with the format specifications.
    """
    def __init__(self, **kwargs):
        super(FormatDocument, self).__init__()
        self.valid_keys = None
        for managed_type, managed_spec in kwargs:
            self[managed_type] = managed_spec

    def __setitem__(self, key, value):
        """
        Overwrite the setting of key value pairs to ensure that only values that are
        BaseSpec are allowed and that all keys are strings.

        :param key: String with the managed type name
        :param value: The value the key should be set to
        """
        if not isinstance(key, basestring):
            raise ValueError('Managed object type key names must be strings')
        if not isinstance(value, BaseSpec):
            if isinstance(value, basestring):
                value = BaseSpec.from_json(value)
            elif isinstance(value, dict):
                value = BaseSpec.from_dict(value)
            else:
                raise ValueError('Value does not define a valid format specification')
        super(FormatDocument, self).__setitem__(key=key,
                                                value=value)

    @staticmethod
    def from_api(module_object=None,
                 class_dict=None):
        """
        Generate a format specification document form an API, i.e,  given a module_object and/or
        dictionary of classes compile a FormatDocument dict of all managed object types and
        their specification.

        :param module_object: Full name of the module or the module object itself with the format classes.
            (Default value is None)
        :param class_dict: Dictionary where the keys are names (i.e, the managed type) and the values
            are the corresponding class
        :param as_json: Boolean indicating whether the output document should be converted to JSON.
            (Default value is False)
        :param pretty: In the case that as_json==True, should a pretty formatting be used when encoding the JSON.


        :returns: Python dictionary or JSON document  where the keys are the mangaed types and the values are the
            dictionaries of the format spec.
        """
        from brain.dataformat.base import ManagedObject
        import sys
        import inspect
        # Compile the list of all classes
        classes = {}
        if isinstance(module_object, basestring):
            classes = inspect.getmembers(sys.modules['brain.dataformat.brainformat'], inspect.isclass)
        elif module_object is not None:
            classes = inspect.getmembers(module_object, inspect.isclass)
        if class_dict is not None:
            classes.update(class_dict)

        # Compile the specification document
        spec_document = FormatDocument()
        for class_object in classes:
            if issubclass(class_object[1], ManagedObject):
                try:
                    spec_document[class_object[0]] = class_object[1].get_format_specification()
                except NotImplementedError:
                    pass
        # Return the resulting document
        return spec_document


class DatasetSpec(BaseSpec):
    """
    Specification of a dataset
    """
    def __init__(self,
                 dataset,
                 prefix,
                 description,
                 optional=False,
                 attributes=None,
                 dimensions=None,
                 dimensions_fixed=None,
                 primary=None,
                 relationships=None):
        """

        :param dataset: The name of the dataset or None if prefix is specified. In advanced cases, this may also
            be a DatasetSpec.
        :param prefix:  The name of the prefix of None if dataset is specified
        :param description: The human-readable description of the dataset
        :param optional: Boolean indicating whether the dataset is optional. (Default=False)
        :param attributes: List of AttributeSpec objects with the attributes or None if no attributes are specified.
            (Default=None)
        :param dimensions: List of DimensionSpec objects with the dimensiosn or None if no dimensions are specified.
            (Default=None)
        :param dimensions_fixed: Optional bool indicating whether the number of dimensions is fixed. Default value
            is None, indicating that the value should not be set. If not set, then this parameter is implicitly
            assumed to be True if dimensions are specified and False if no dimensions are specified
        :param primary: Optional bool indicating whether this is a primary dataset. Default value is None, indicating
            that the key should not be set. If not set, then the value is implicitly assumed to be False.
        :param relationships: Optional list of relationship specifications describing relationships of this dataset
            to other objects. Set ot None, if no relationships are specified, in which case an empty list will
            be created. (Default=None) NOTE: The key may also be omitted if no relationships are specified

        """
        super(DatasetSpec, self).__init__()
        # Define the list of valid keys
        self.valid_keys = ['dataset',
                           'prefix',
                           'description',
                           'dimensions',
                           'dimensions_fixed',
                           'attributes',
                           'optional',
                           'primary',
                           'relationships']

        # Ensure that the name spec is valid
        #if dataset is not None and prefix is not None:
        #    raise ValueError('Either dataset or prefix must be not None but not both')
        if isinstance(dataset, dict):
            dataset = DatasetSpec(**dataset)
        if isinstance(dataset, basestring) and "/" in dataset:
            raise ValueError("Illegal name for dataset. '/' not allowed as part of dataset name.")

        # Ensure that the attributes are valid
        my_attributes = attributes
        if my_attributes is None:
            my_attributes = []
        elif isinstance(attributes, AttributeSpec):
            my_attributes = [my_attributes, ]
        elif isinstance(attributes, list):
            my_attributes = [AttributeSpec(**attr) for attr in attributes]
        elif isinstance(attributes, dict):
            my_attributes = [AttributeSpec(**attributes), ]
        else:
            raise ValueError('Invalid attributes given')

        # Ensure that the dimensions are valid
        my_dimensions = dimensions
        if my_dimensions is None:
            my_dimensions = []
        elif isinstance(attributes, DimensionSpec):
            my_dimensions = [my_dimensions, ]
        elif isinstance(attributes, list):
            my_dimensions = [DimensionSpec(**dim) for dim in dimensions]
        elif isinstance(attributes, dict):
            my_dimensions = [DimensionSpec(**dimensions), ]
        else:
            raise ValueError('Invalid dimensions given')

        # Ensure that the relationships are valid
        my_relationships = relationships
        if my_relationships is None:
            my_relationships = []
        elif isinstance(relationships, RelationshipSpec):
            my_relationships = [my_relationships, ]
        elif isinstance(relationships, list):
            my_relationships = [RelationshipSpec(**rel) for rel in relationships]
        elif isinstance(relationships, dict):
            my_relationships = [RelationshipSpec(**relationships), ]
        else:
            raise ValueError('Invalid relationships given')

        self['dataset'] = dataset
        self['prefix'] = prefix
        self['description'] = unicode(description) if description is not None else description
        self['optional'] = bool(optional)
        self['attributes'] = my_attributes
        self['dimensions'] = my_dimensions
        self['relationships'] = my_relationships
        if primary is not None:
            self['primary'] = bool(primary)
        if dimensions_fixed is not None:
            self['dimensions_fixed'] = bool(dimensions_fixed)

    def get_attribute(self, attribute=None, prefix=None):
        """
        Get the specification of the attribute with the given attribute name and/or prefix
        :param attribute: Name of the attribute
        :param prefix: Prefix of the attribute
        :return: AttributeSpec object or None if not found
        """
        for a in self['attributes']:
            if (a['attribute'] == attribute or attribute is None) and (a['prefix'] == prefix or prefix is None):
                return a
        return None

    def add_attribute(self, attribute):
        """
        Add a new attribute to the dataset

        :param attribute: AttributeSpec object with the specification of the attribute
        :type attribute: AttributeSpec

        :raises: ValueError is raised if the given attribute sppec is invalid
        """
        if not isinstance(attribute, AttributeSpec):
            if isinstance(attribute, dict):
                attribute = AttributeSpec(**attribute)
            else:
                raise ValueError("The given attribute does not define a valid attribute")
        self['attributes'].append(attribute)

    def add_dimension(self, dimension):
        """
        Add a new dimensions to the dataset

        :param dimension: DimensionSpec object with the specification for the dataset
        :type dimension: DimensionSpec

        :raises: ValueError is raised if the given dimensions specification is invalid or collides with an
            existing dimensions specification. E.g, dimensions specifications for the same axis are expected
            to always have the same name as the name is used to address the dimension.
        """
        # Ensure that the scale has the correct format
        if not isinstance(dimension, DimensionSpec):
            if isinstance(dimension, dict):
                dimension = DimensionSpec(**dimension)
            else:
                raise ValueError("The given dimension object is not a valid dimension specification")
        # Ensure that the name of the dimension scale does not collide with any other existing dimensions
        for dim_spec in self['dimensions']:
            if dim_spec['axis'] == dimension['axis']:
                if dim_spec['name'] != dimension['name']:
                    raise ValueError('Another dimension scale with a different ' +
                                     'dimension name already exists for the same axis')

        self['dimensions'].append(dimension)

    def add_relationship(self, relationship):
        """
        Add a new relationship to the dataset

        :param relationship: RelationshipSpec object with the specification of the relatioship
        :type relationship: RelationshipSpec

        :raises: ValueError is raised if the given relationship specification is invalid.
        """
        if not isinstance(relationship, RelationshipSpec):
            if isinstance(relationship, dict):
                relationship = RelationshipSpec(**relationship)
            else:
                raise ValueError("The given relationship does not define a valid RelationshipSpec")
        self['relationships'].append(relationship)


class GroupSpec(BaseSpec):
    """
    Specification of a group
    """
    def __init__(self,
                 group,
                 prefix,
                 description,
                 datasets=None,
                 groups=None,
                 managed_objects=None,
                 attributes=None,
                 optional=False,
                 relationships=None):
        """

        :param group: The name of the group or None if prefix is specified
        :param prefix: The name of the prefix of None if group is specified
        :param description: The human-readable description of the group
        :param datasets: Dictionary of DatasetSpec objects with the datasets contained in the group, or
            None if no datasets are specified. (Default=None)
        :param groups: Dict of GroupSpec objects with the groups contained in the group, or None if no groups
            are specified. (Default=None)
        :param managed_objects: List of ManagedSpec objects with the managed objects, or None if no managed objects
            are specified. (Default=None)
        :param attributes: List of AttributeSpec objects with the attributes or None if no attributes are specified.
            (Default=None)
        :param optional: Boolean indicating whether the dataset is optional. (Default=False)
        :param relationships: Optional list of relationship specifications describing relationships of this group
            to other objects. Set ot None, if no relationships are specified, in which case an empty list will
            be created. (Default=None) NOTE: The key may also be omitted if no relationships are specified

        """
        super(GroupSpec, self).__init__()
        # Define the list of valid keys
        self.valid_keys = ['group',
                           'prefix',
                           'description',
                           'datasets',
                           'groups',
                           'managed_objects',
                           'attributes',
                           'optional',
                           'relationships']

        # Ensure that the attributes are valid
        my_attributes = attributes
        if my_attributes is None:
            my_attributes = []
        elif isinstance(attributes, AttributeSpec):
            my_attributes = [my_attributes, ]
        elif isinstance(attributes, list):
            my_attributes = [AttributeSpec(**attr) for attr in attributes]
        elif isinstance(attributes, dict):
            my_attributes = [AttributeSpec(**attributes), ]
        else:
            raise ValueError('Invalid attributes given')

        # Ensure that the datasets
        my_datasets = datasets
        if isinstance(datasets, dict) and 'dataset' in datasets and 'prefix' in datasets:
            try:
                my_datasets = DatasetSpec(**datasets)
            except:
                pass
        if my_datasets is None:
            my_datasets = {}
        elif isinstance(datasets, DatasetSpec):
            if my_datasets['dataset'] is not None:
                my_datasets = {my_datasets['dataset']: my_datasets}
            else:
                my_datasets = {my_datasets['prefix']: my_datasets}
        elif isinstance(datasets, dict):
            my_datasets = {dataset_key: DatasetSpec(**dataset_value)
                           for dataset_key, dataset_value in datasets.iteritems()}
        else:
            raise ValueError('Invalid datasets given')

        # Ensure that the managed_objects are valid
        my_managed_objects = managed_objects
        if my_managed_objects is None:
            my_managed_objects = []
        elif isinstance(managed_objects, ManagedSpec):
            my_managed_objects = [my_managed_objects, ]
        elif isinstance(managed_objects, list):
            my_managed_objects = [ManagedSpec(**attr) for attr in managed_objects]
        elif isinstance(managed_objects, dict):
            my_managed_objects = [ManagedSpec(**managed_objects), ]
        else:
            raise ValueError('Invalid managed_objects given')

        # Ensure that the groups are valid
        my_groups = groups
        if isinstance(groups, dict) and 'group' in groups and 'prefix' in groups:
            try:
                my_groups = GroupSpec(**groups)
            except:
                pass
        if my_groups is None:
            my_groups = {}
        elif isinstance(groups, GroupSpec):
            if groups['group'] is not None:
                my_groups = {groups['group']: my_groups}
            else:
                my_groups = {groups['prefix']: my_groups}
        elif isinstance(groups, dict):
            my_groups = {group_key: GroupSpec(**group_value)
                         for group_key, group_value in groups.iteritems()}
        else:
            raise ValueError('Invalid groups given')

        # Ensure that the relationships are valid
        my_relationships = relationships
        if my_relationships is None:
            my_relationships = []
        elif isinstance(relationships, RelationshipSpec):
            my_relationships = [my_relationships, ]
        elif isinstance(relationships, list):
            my_relationships = [RelationshipSpec(**rel) for rel in relationships]
        elif isinstance(relationships, dict):
            my_relationships = [RelationshipSpec(**relationships), ]
        else:
            raise ValueError('Invalid relationships given')

        # Save all input parameters
        self['group'] = group
        self['prefix'] = prefix
        self['description'] = description if description is not None else description
        self['datasets'] = my_datasets
        self['groups'] = my_groups
        self['managed_objects'] = my_managed_objects
        self['attributes'] = my_attributes
        self['optional'] = optional
        self['relationships'] = my_relationships

    def get_dataset(self, dataset=None, prefix=None):
        """
        Get the specification of the group with the given group name and/or prefix
        :param dataset: Name of the attribute
        :param prefix: Prefix of the attribute
        :return: AttributeSpec object or None if not found
        """
        for k, v in self['datasets'].iteritems():
            if (v['dataset'] == dataset or dataset is None) and (v['prefix'] == prefix or prefix is None):
                return v
        return None

    def add_dataset(self, dataset, key=None):
        """
        Add a new dataset to the group spec

        :param dataset: DatasetSpec object with the specification of the dataset
        :type dataset: DatasetSpec
        :param key: The key under which the dataset spec should be stored. May be None, in which case
            the name or prefix of the dataset are used (depending on which one is provided)
        :type key: string

        """
        if not isinstance(dataset, DatasetSpec):
            if isinstance(dataset, dict):
                dataset = DatasetSpec(**dataset)
            else:
                raise ValueError("The given dataset does not define a valid dataset specification")
        if key is None:
            key = dataset['dataset'] if dataset['dataset'] is not None else dataset['prefix']
        if not isinstance(key, basestring):
            raise ValueError("The key must be a string.")
        self['datasets'][key] = dataset

    def get_attribute(self, attribute=None, prefix=None):
        """
        Get the specification of the attribute with the given attribute name and/or prefix
        :param attribute: Name of the attribute
        :param prefix: Prefix of the attribute
        :return: AttributeSpec object or None if not found
        """
        for a in self['attributes']:
            if (a['attribute'] == attribute or attribute is None) and (a['prefix'] == prefix  or prefix is None):
                return a
        return None

    def add_attribute(self, attribute):
        """
        Add a new attribute to the group spec

        :param attribute: AttributeSpec object with the specification of the attribute
        :type attribute: AttributeSpec
        """
        if not isinstance(attribute, AttributeSpec):
            if isinstance(attribute, dict):
                attribute = AttributeSpec(**attribute)
            else:
                raise ValueError("The given attribute does not define a valid attribute")
        self['attributes'].append(attribute)

    def get_group(self, group=None, prefix=None):
        """
        Get the specification of the group with the given group name and/or prefix
        :param group: Name of the attribute
        :param prefix: Prefix of the attribute
        :return: AttributeSpec object or None if not found
        """
        for a in self['groups']:
            if (a['group'] == group or group is None) and (a['prefix'] == prefix or prefix is None):
                return a
        return None

    def add_group(self, group):
        """
        Add a new group to the group spc

        :param group: GroupSpec object with the specification of the group
        :type group: GroupSpec
        """
        if not isinstance(group, GroupSpec):
            if isinstance(group, dict):
                group = GroupSpec(**group)
            else:
                raise ValueError("The given group does not define a valid group specification")
        self['groups'].append(group)

    def add_managed(self, managed_object):
        """
        Add a new managed object to the group spec

        :param managed_object: ManagedSpec object with the specification of the group
        :type managed_object: ManagedSpec
        """
        if not isinstance(managed_object, ManagedSpec):
            if isinstance(managed_object, dict):
                managed_object = ManagedSpec(**managed_object)
            else:
                raise ValueError("The given managed_object does not define a valid managed object specification")
        self['managed_objects'].append(managed_object)

    def add_relationship(self, relationship):
        """
        Add a new relationship to the dataset

        :param relationship: RelationshipSpec object with the specification of the relatioship
        :type relationship: RelationshipSpec

        :raises: ValueError is raised if the given relationship specification is invalid.
        """
        if not isinstance(relationship, RelationshipSpec):
            if isinstance(relationship, dict):
                relationship = RelationshipSpec(**relationship)
            else:
                raise ValueError("The given relationship does not define a valid RelationshipSpec")
        self['relationships'].append(relationship)


class FileSpec(GroupSpec):
    """
    Specification of a file
    """
    def __init__(self,
                 description,
                 file_prefix=None,
                 file_extension=None,
                 group=None,
                 prefix=None,
                 datasets=None,
                 groups=None,
                 managed_objects=None,
                 attributes=None,
                 optional=False):
        """
        :param description: The human-readable description of the group
        :param file_prefix: Prefix for names of files of this type. Set to None to indicate that arbitrary filenames
            may be used. (Default=None)
        :param file_extension: The file extension that must be used for files of this type. Set to None to indicate
            that arbitrary file extensions may be used (Default=None)
        :param group: Name for external links to the root group of the file. Only one of prefix or group may be
            set. If both group and prefix are set to None, then no external links may generated to the file
            (i.e, the specification describes a pure container). (Default=None)
        :param prefix: Name prefix for external links to the root group of the file. Only one of prefix or group may be
            set. If both group and prefix are set to None, then no external links may generated to the file
            (i.e, the specification describes a pure container). (Default=None)
        :param datasets: List of DatasetSpec objects with the datasets contained in the group, or None if no datasets
            are specified. (Default=None)
        :param groups: List of GroupSpec objects with the groups contained in the group, or None if no groups
            are specified. (Default=None)
        :param managed_objects: List of ManagedSpec objects with the managed objects, or None if no managed objects
            are specified. (Default=None)
        :param attributes: List of AttributeSpec objects with the attributes or None if no attributes are specified.
            (Default=None)
        :param optional: Boolean indicating whether the file/group is optional. (Default=False)
        """
        super(FileSpec, self).__init__(description=description,
                                       group=group,
                                       prefix=prefix,
                                       datasets=datasets,
                                       groups=groups,
                                       managed_objects=managed_objects,
                                       attributes=attributes,
                                       optional=optional)
        self.valid_keys.append('file_prefix')
        self.valid_keys.append('file_extension')
        self['file_prefix'] = file_prefix
        self['file_extension'] = file_extension


class ManagedSpec(BaseSpec):
    """
    Specification of a contained Managed Object
    """
    def __init__(self,
                 format_type,
                 optional=False):
        """
        :param format_type: The class type of the ManagedObject
        :param optional: Boolean indicating whether the Managed Object is optional. (Default=False)
        """
        super(ManagedSpec, self).__init__()
        self.valid_keys = ['format_type',
                           'optional']
        self['format_type'] = format_type
        self['optional'] = optional


class AttributeSpec(BaseSpec):
    """
    Specification of an attribute
    """
    def __init__(self,
                 attribute,
                 prefix,
                 value=None,
                 optional=False,
                 description=None):
        """
        :param attribute: Fixed name for the attribute. May be None in case that a prefix is specified,
        :param prefix: Prefix for the attribute. The prefix is typically appended by a number (or other unique value)
            allowing multiple instanced of the attribute to be assigned to the same object.
        :param value: Value of the attribute. May be None in case that value for teh attribute is user-defined.
            (Default=None)
        :param optional: Boolean indicating whether the Attribute is optional. (Default=False)
        :param description: Optional description of the attribute (not save to file) (Default=None)

        """
        super(AttributeSpec, self).__init__()
        self.valid_keys = ['attribute',
                           'prefix',
                           'value',
                           'optional',
                           'description']
        self['attribute'] = attribute
        self['prefix'] = prefix
        self['value'] = value
        self['optional'] = optional
        if description is not None:
            self['description'] = description

    def create_attribute(self, parent_object):
        """
        Create the attribute based on the given specfication
        :param parent_object:  The parent object with which the attribute should be associated
        :type parent_object: h5py.Group, h5py.Dataset, ManagedGroup, ManagedDataset
        """
        from brain.dataformat.base import ManagedObject
        from numpy import nan
        if self['attribute'] is None and self['prefix'] is None:
            raise ValueError('Missing attribute name in the specification')
        parent = parent_object if not isinstance(parent_object, ManagedObject) else parent_object.hdf_object
        name = self['attribute']
        if name is None:
            name = self['prefix']
            index = len([i for i in parent.keys()])
            name += str(index)
        value = self['value'] if self['value'] is not None else nan
        parent_object.attrs[name] =value


class DimensionSpec(BaseSpec):
    """
    Specification of a dimension scale
    """

    def __init__(self,
                 name,
                 unit,
                 dataset,
                 axis,
                 description,
                 optional=False,
                 relationships=None):
        """
        :param name: The name of the dimensions scale. NOTE: if multiple dimensions scales are associated with
            the same axis of a dataset, then their names must be identical while their unit keys may differ.
            If a dimension is required but does not have a dimension scale, then set the name to None.
            In this case unit and dataset should be None as well.

        :param unit: The units in which the dimension is expressed. May be None in case that only a name
            for the dimensions should be specified but no actual dimension scale. NOTE: if a dataset is
            specified then unit must be set as well as this is used to address the dataset.

        :param optional: Boolean indicating whether the object is optional or mandatory.

        :param dataset : The HDF5 dataset with the values for the dimension scale. May be None, in
            case that no actual axis scale should be specified, but rather only the dimensions should
            be labeled, but no actual dimensions scale should be created.

        :param axis : Unsigned Integer indicating the axis/dimension the dimension scale is associated with. (Mandatory)

        :param description: Description of the dimensions scale. The description is associated with the
            dataset as the format description attribute (i.e., if the dataset is not set to None). (Mandatory)

        :param relationships: Optional list of relationship specifications describing relationships of the dataset
            associated with the dimensions scale to other objects. Set ot None, if no relationships are specified,
            in which case an empty list will be created. (Default=None) NOTE: The key may also be omitted if
            no relationships are specified. NOTE: If relationships are specified, then the `dataset` key must be set.
        """
        super(DimensionSpec, self).__init__()
        self.valid_keys = ['name',
                           'unit',
                           'dataset',
                           'axis',
                           'description',
                           'optional',
                           'relationships']

        # Ensure that the names, unit, dataset do not collide
        if name is None and (unit is not None or dataset is not None):
            raise ValueError('Unit or dataset may not be specified if name is set to None')
        if unit is None and dataset is not None:
            raise ValueError('Unit is required if dataset is set, as the unit is used to address the dataset')
        if relationships is not None and dataset is None:
            if len(relationships) > 0:
                raise ValueError('Relationships for dimensions scales can only be defined if the dataset is set')

        # Ensure that the relationships are valid
        my_relationships = relationships
        if my_relationships is None:
            my_relationships = []
        elif isinstance(relationships, RelationshipSpec):
            my_relationships = [my_relationships, ]
        elif isinstance(relationships, list):
            my_relationships = [RelationshipSpec(**rel) for rel in relationships]
        elif isinstance(relationships, dict):
            my_relationships = [RelationshipSpec(**relationships), ]
        else:
            raise ValueError('Invalid relationships given')

        # Save the data values
        self['name'] = name
        self['unit'] = unit
        self['dataset'] = dataset
        self['axis'] = axis
        self['description'] = description
        self['optional'] = optional
        self['relationships'] = my_relationships

    def add_relationship(self, relationship):
        """
        Add a new relationship to the dataset

        :param relationship: RelationshipSpec object with the specification of the relatioship
        :type relationship: RelationshipSpec

        :raises: ValueError is raised if the given relationship specification is invalid.
        """
        if not isinstance(relationship, RelationshipSpec):
            if isinstance(relationship, dict):
                relationship = RelationshipSpec(**relationship)
            else:
                raise ValueError("The given relationship does not define a valid RelationshipSpec")
        self['relationships'].append(relationship)


class RelationshipSpec(BaseSpec):
    """
    Specification of a relationship between datasets
    """

    RELATIONSHIP_TYPES = {
        'indexes': 'The source dataset contains indices into the target dataset.',
        'indexes_values': 'The source object selects certain parts of the target object based on the values',
        'shared_encoding': 'The target and source contain values with the same encoding so that values in the two objects can be directly compared',
        'shared_ascending_encoding': 'Same as `shared_encoding` but the source and target datasets are expected to be sorted by value (e.g., in the case of time)',
        'order': 'The ordering of objects matches between the datasets along the given axis.',
        'equivalent': 'The source and target object store different values but encode the same data.',
        'user': 'Arbitrary user-defined relationship'}

    def __init__(self,
                 attribute,
                 target,
                 relationship_type,
                 description,
                 prefix=None,
                 axis=None,
                 optional=False,
                 properties=None):
        """

        :param attribute: The name of the relationship attribute (a standard prefix may be prepended in storage)
        :param target: Dictionary specifying the target object of the relationship. See also
            `RelationshipTargetSpec` for further details.
        :param relationship_type: The type of the relationship. One of RelationshipSpec.RELATIONSHIP_TYPES.keys()
        :param description: Textual description of the relationship
        :param prefix: Prefix of the attribute used for storage. Must be None in case that attribute is
            specified. (a prefix may be prependend in storage) (Default=None)
        :param axis: Optional axis (or list of axis) of the source to which the relationship applies to.
            (Default=None, all data). In the case of `indexes` relationships, axis may be used to identify
            the dimension that defines the indicies (e.g, if we index a 2D dataset then we may have a 20x2
            dataset containing 20 two-dimensional indicies, where the second axis defines the indicies).
            In special case, this may also be a dict to encode additional
            meaning for different axis. E.g., in the case mapping relationships we use this to encode which
            axis is used to store the mu
        :param optional: Is the relationship optional. (Default=False)
        :param properties: Optional dictionary (must by JSON serializable) with additional user properties
            associated with the relationship. (Default=None)
        """
        super(RelationshipSpec, self).__init__()
        self.valid_keys = ['attribute',
                           'prefix',
                           'target',
                           'axis',
                           'relationship_type',
                           'optional',
                           'description',
                           'properties']
        # Validate the relationship specification
        if relationship_type not in self.get_relationship_types():
            raise ValueError('Invalid relationship type specified')
        if axis is not None and axis < 0:
            raise ValueError('The axis must be None or positive byt not negative')
        if isinstance(target, RelationshipTargetSpec):
            pass
        elif isinstance(target, dict):
            target = RelationshipTargetSpec(**target)
        if attribute is not None and prefix is not None:
            raise ValueError('Only one of name or prefix may be not None')
        if attribute is None and prefix is None:
            raise ValueError('One of attribute or prefix must be not None')

        # Save the data
        self['attribute'] = attribute
        self['prefix'] = prefix
        self['target'] = target
        self['relationship_type'] = relationship_type
        self['description'] = description
        self['axis'] = axis
        self['optional'] = optional
        self['properties'] = properties

    @classmethod
    def get_relationship_types(cls):
        """
        Get a list of available relationship types.

        This is a convenience function and is equivialant ot RelationshipSpec.RELATIONSHIP_TYPES.keys()
        """
        return RelationshipSpec.RELATIONSHIP_TYPES.keys()


class RelationshipTargetSpec(BaseSpec):
    """
    Specification of the target of a relationship
    """
    def __init__(self,
                 dataset=None,
                 group=None,
                 prefix=None,
                 prefix_all=None,
                 axis=None,
                 filename=None,
                 global_path=None):
        """
        :param dataset: The name of the dataset the relationship points to. This is usually a relative name within
            the current specification. May be None if `group` or `prefix` are defined. One and only one of `dataset`,
            `group`, `prefix`, 'prefix_all'  must be not None.
        :param group: The name of the group the relationship points to. This is usually a relative name within the
            current specification. May be None if `group` or `prefix` are defined. One and only one of `dataset`,
            `group`, `prefix`, 'prefix_all'  must be not None.
        :param prefix: The name prefix of the dataset or group the relationship points to . This is usually a relative
            name within the current specification. When the prefix parameter is used, then relationship points to the
            collection of all objects with the given prefix.  May be None if `group` or `prefix` are defined.
            One and only one of `dataset`, `group`, `prefix`, 'prefix_all' must be not None.
        :param prefix_all: The name prefix of the dataset or group the relationship points to . This is usually
            a relative name within the current specification. When the prefix_all parameter is used, then the
            relationship points to each object with the given prefix, i.e., the relationship specifies multiple
            targets. This is used in the context of static specifications to allow one to specify a series of
            targets where the number of targets of the relationship is not known a priori.
            May be None if `group` or `prefix` are defined.
            One and only one of `dataset`, `group`, `prefix`, 'prefix_all' must be not None.
        :param axis: The index  of the axis (or list of axis) we point to (or a name if the relationship points
            to a particular dataset) or None. (Default=None)
        :param filename: The path to the file with the object we are pointing to of different from the source file.
            (Default=None)
        :param global_path: Typically we try to define Managed Objects in a self-contained fashion, i.e., all
            data related to object should be available in the same managed objects. However, in some cases it
            is useful to store repeatably-used and shared data in central locations. The global key allows us
            to specify the location of global targets that we want to point to. The `dataset`, `group`, `prefix`
            keys are still honored, i.e, the global key only gives us the base location where the object is located.

             * `<path>` The global key may be an absolute path, in which case the path will start with `/`. \
                This form of description is often used when a user adds custom relationships to the file.
             * `<GlobalTargetClass>:<GlobalKey>` `GlobalTargetSpace indicated the name of the dictionary \
                (i.e, namespace) for the global targets and the `GlobalKey` is the name of the particular global \
                target, where the value associated with the key is expected to be the absolute path within the \
                file to the global target.
             * `None`  indicating that we are indexing a local structure within the parent group of the source object

        """
        super(RelationshipTargetSpec, self).__init__()
        self.valid_keys = ['global_path',
                           'dataset',
                           'group',
                           'prefix',
                           'prefix_all',
                           'axis',
                           'filename']

        if not(global_path is None or global_path.startswith('/') or len(global_path.split(':')) == 2):
            raise ValueError('Invalid global path specification')
        if dataset is not None and (prefix is not None or group is not None or prefix_all is not None):
            raise ValueError('Target not uniquely specified')
        if prefix is not None and (dataset is not None or group is not None or prefix_all is not None):
            raise ValueError('Target not uniquely specified')
        if group is not None and (prefix is not None or dataset is not None or prefix_all is not None):
            raise ValueError('Target not uniquely specified')
        if prefix_all is not None and (prefix is not None or group is not None or dataset is not None):
            raise ValueError('Target not uniquely specified')
        if dataset is None and prefix is None and group is None and prefix_all is None:
            raise ValueError('No target dataset, group, or prefix is specified')
        if group is not None and axis is not None:
            raise ValueError('Axis may not be specified for targets pointing to a group')
        self['global_path'] = global_path
        self['dataset'] = dataset
        self['group'] = group
        self['prefix'] = prefix
        self['prefix_all'] = prefix_all
        self['axis'] = axis
        self['filename'] = filename

    @classmethod
    def from_objects(cls,
                     target_object,
                     source_object,
                     target_axis=None):
        """
        Given an HDF5 object, create a corresponding target description.

        The function tries to create relative target if possible (i.e,. if the two
        objects are contained within the same parent). If this is not possible, then an
        absolute target path will be used.

        :param target_object: The target HDF5 object we want to point to from the source.
            ManagedObject API class instances are also permissible.
        :param source_object: The source HDF5 object with which we want to associate the
            relationship. I.e., the target is typically defined relative to the parent
            of the source object. ManagedObject API class instances are also permissible.
        :param target_axis: The axis of the target object we want to relate with. Only
            relevant when relating to datasets

        """
        from brain.dataformat.base import ManagedObject
        # Get the correct h5py objects
        target = target_object if not isinstance(target_object, ManagedObject) else target_object.hdf_object
        source = source_object if not isinstance(source_object, ManagedObject) else source_object.hdf_object

        # Make sure that we have permissible sources and targets
        if not (isinstance(target, h5py.Group) or isinstance(target, h5py.Dataset)):
            raise ValueError('Target must be an h5py Group or Dataset')
        if not (isinstance(source, h5py.Group) or isinstance(source, h5py.Dataset)):
            raise ValueError('Source must be an h5py Group or Dataset')

        # Make sure that the target_axis is valid
        if not isinstance(target, h5py.Dataset) and target_axis is not None:
            raise ValueError("target_axis may only be set when the target is a valid HDF5 dataset ")
        if target_axis is not None and target_axis >= target.shape:
            raise ValueError("target_axis out of bounds")

        # Check if we can find a common prefix to the parent
        common_prefix = os.path.commonprefix([source.name, target.name])
        split_common_prefix = common_prefix.split('/')
        common_prefix = common_prefix.rstrip(split_common_prefix[-1])
        if len(common_prefix) > 1 and common_prefix.endswith('/'):
            common_prefix = common_prefix.rstrip('/')
        # Option 1) We found a common prefix and can create a relative target description
        if common_prefix == source.parent.name:
            relative_target_path = os.path.relpath(target.name, source.parent.name)
            group_val = relative_target_path if isinstance(target, h5py.Group) else None
            dataset_val = relative_target_path if isinstance(target, h5py.Dataset) else None
            global_path_val = None
        # Option 2) We found the common_prefix in the list of global paths
        else:
            global_path_val, object_name = os.path.split(target.name)
            group_val = object_name if isinstance(target, h5py.Group) else None
            dataset_val = object_name if isinstance(target, h5py.Dataset) else None

        # Check if the source and target point to the same file
        try:
            same_file = os.path.samefile(source.file.filename, target.file.filename)
        except:  # The samefile function is not available on Windows
            same_file = os.stat(source.file.filename) == os.stat(target.file.filename)
        target_filename = None if same_file else os.path.abspath(target.file.filename)

        # Return the spec for the target relative to the source
        return RelationshipTargetSpec(group=group_val,
                                      dataset=dataset_val,
                                      prefix=None,
                                      axis=target_axis,
                                      filename=target_filename,
                                      global_path=global_path_val)


class APIGenerator(object):
    """
    Class used to auto-generate API's based on a given format specification
    """
    @staticmethod
    def create_api(format_spec, filename=None):
        """

        :param format_spec: The format specification for which we should auto-generate a basis API that we
            can extend and customize
        :param filename: The name of the file the API should be saved to or None if the API should not be
            a saved but generated in-memory only

        :return: Class object of the generated API class for the spec.
        """
        raise NotImplementedError('The API auto-generator has not been implemented yet')


