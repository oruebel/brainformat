"""
Thin wrapper to ease reading of WAV files
"""

class WAVFile(object):
    """
    Simple helper class to help with reading WAV files

    :ivar filename: Name of the wave file
    :ivar data: The data read from the wav file
    :ivar sampling_rate: The sampling rate read from the wav file
    """

    def __init__(self, filename, read_on_creation=True):
        """
        :param filename: The name of the input WAV file
        :param read_on_creation: Boolean indicating whether we want to immediately read the WAV data
        """
        self.__filename = filename
        self.data = None
        self.sampling_rate = None
        if read_on_creation:
            self.read()

    def read(self):
        """
        Read the WAV data and update

        self.data and self.sampling_rate variables
        :return: self.data, self.sampling rate
        """
        import scipy.io.wavfile as sio_wave
        self.sampling_rate, self.data = sio_wave.read(self.__filename, mmap=False)
        return self.data, self.sampling_rate

