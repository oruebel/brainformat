"""
Tools module for conversion of brain data ot the BrainData HDF5 format.

"""
import warnings
import os
import sys
import logging

from brain.dataformat.brainformat import BrainDataFile, BrainDataEphys, BrainDataEphysProcessed
from brain.readers.htkcollection import HTKCollection


logger = logging.getLogger(__name__)


class ConvertBrainData(object):
    """
    Class used for conversion of brain data to the BrainData HDF5 format.
    """
    def __init__(self, htk_raw=None, htk_processed=None, output=None, print_status=True, **kwargs):
        """
        Create the conversion object and define all input arguments.

        :param htk_raw: List of string of the location of all raw htk Ephys recordings.
        :param htk_processed: List of strings of the locations of all processed htk Ephys recordings.
        :param output: The output data file.
        :param print_status: Boolean indicating whether read status should printed to standard out
        :param kwargs: Unused. Specified only to catch any illegal arguments.

        """
        if htk_raw is not None:
            self.htk_raw = htk_raw
        else:
            self.htk_raw = []
        if htk_processed is not None:
            self.htk_processed = htk_processed
        else:
            self.htk_processed = []
        self.print_status = print_status
        self.output = output
        self.log_settings()
        if len(kwargs) > 0:
            warnings.warn('Unused arguments: %s' % str(kwargs))

            exit()

    def log_settings(self):
        """
        Print file convert settings to standard out using print.
        """
        logger.info("Convert Settings:")
        logger.info("-----------------")
        logger.info("    * Raw Data:")
        for i in self.htk_raw:
            logger.info("        * " + unicode(i))
        logger.info("    * Processed Data:")
        for i in self.htk_processed:
            logger.info("        * " + unicode(i))
        logger.info("    * Output: ")
        logger.info("            * " + unicode(self.output))
        logger.info("")

    def convert(self):
        """
        Execute the conversion of the data based on the arguments
        defined by the instance variables (see __init__ )
        """
        logger.info("Convert Progress:")
        logger.info("-----------------")
        outfile = None
        if self.output is not None:
            if os.path.exists(self.output):
                outfile = BrainDataFile(self.output)
            else:
                outfile = BrainDataFile.create(self.output)
            logger.info("Created output HDF5 file: " + unicode(self.output))
        else:
            warnings.warn('No output file specified via --output')
            exit()

        if self.htk_raw is not None:
            for htk_dict in self.htk_raw:
                htkdir = htk_dict['htk_dir']
                anatomy_file = htk_dict['anatomy_file']
                logger.info("Reading "+htkdir)
                htkfile = HTKCollection(directory=htkdir,
                                        anatomy_file=anatomy_file)
                htkdata = htkfile.read_data(print_status=self.print_status)
                if htkfile.num_bands == 1:
                    htkdata = htkdata.reshape((htkdata.shape[0], htkdata.shape[1]))
                internal_grp = outfile.data().internal()
                if htkfile.has_anatomy():
                    anatomy_names = htkfile.get_anatomy_map(len(htkfile.htk_files), htkfile.anatomy)
                else:
                    anatomy_names = None
                logger.info("Writing "+htkdir+" data to file.")
                BrainDataEphys.create(parent_object=internal_grp,
                                     ephys_data=htkdata,
                                     sampling_rate=htkfile.sample_rate,
                                     anatomy_names=anatomy_names,
                                     layout=htkfile.layout)
                del htkfile
                del htkdata
        if self.htk_processed is not None:
            for htk_dict in self.htk_processed:
                htkdir = htk_dict['htk_dir']
                bands_file = htk_dict['bands_file']
                anatomy_file = htk_dict['anatomy_file']
                logger.info("Reading processed HTK data: "+htkdir)
                if 'Real' in htkdir:
                    realdir = htkdir
                    imagdir = htkdir.replace('Real', 'Imag', 1)
                elif 'Imag' in htkdir:
                    realdir = htkdir.replace('Imag', 'Real', 1)
                    imagdir = htkdir
                else:
                    realdir = htkdir
                    imagdir = None

                if imagdir is None:
                    htkfile_real = HTKCollection(directory=realdir,
                                                 bands_file=bands_file,
                                                 anatomy_file=anatomy_file)
                    htkdata = htkfile_real.read_data(print_status=self.print_status)
                else:
                    htkfile_real = HTKCollection(directory=realdir,
                                                 bands_file=bands_file,
                                                 anatomy_file=anatomy_file)
                    data_real = htkfile_real.read_data(print_status=self.print_status)
                    htkfile_imag = HTKCollection(directory=imagdir)
                    data_imag = htkfile_imag.read_data(print_status=self.print_status)
                    if data_real.shape != data_imag.shape:
                        warnings.warn('Imaginary and real data components do not match.' +
                                      ' Converting using the command-line dir only')
                        if realdir == htkdir:
                            htkdata = data_real
                        else:
                            htkdata = data_imag
                    else:
                        logger.info('Found matching real and imaginary components for ' + htkdir +
                                    '. Combining the two to a complex processed dataset')
                        htkdata = data_real + 1j*data_imag

                internal_grp = outfile.data().internal()
                original_name = os.path.basename(htkdir)
                if htkfile_real.has_anatomy():
                    anatomy_names = htkfile_real.get_anatomy_map(len(htkfile_real.htk_files),
                                                                     htkfile_real.anatomy)
                else:
                    anatomy_names = None

                if len(original_name) == 0:
                    original_name = os.path.basename(os.path.split(htkdir)[0])
                logger.info("Writing processed HTK data to file.")
                BrainDataEphysProcessed.create(parent_object=internal_grp,
                                              ephys_data=htkdata,
                                              sampling_rate=htkfile_real.sample_rate,
                                              layout=htkfile_real.layout,
                                              ephys_data_units='Volts',
                                              original_name=original_name,
                                              anatomy_names=anatomy_names,
                                              frequency_bands=htkfile_real.bands)
                del htkdata

    @staticmethod
    def print_help():
        """
        Print user help to standard out using print.
        """
        print "python convert_braindata.py <arguments>"
        print ""
        print "Arguments:"
        print "----------"
        print "--output <file> : The output HDF5 file (mandatory)"
        print "--htk_raw <dir> <anatomy>:"
        print "     <dir> : directory of the raw HTK data collection. "
        print "     <anatomy> : Anatomy file describing the division of the "
        print "                 data into spatial regions (optional)"
        print "--htk_processed <dir> <bands> <anatomy> :"
        print "     <dir> : directory of the processed HTK data collection. "
        print "     <bands> : .mat file describing the frequency bands (optional)"
        print "     <anatomy> : Anatomy file describing the division of the "
        print "                 data into spatial regions (optional)"
        print "--log <level> : Define the level of logging to be used."
        print "    <level> : One of DEBUG, INFO, WARNING, ERROR, CRITICAL"

    @staticmethod
    def parse_arguments(argv=None):
        """
        Parse all input arguments for the data conversion.

        :param argv: The list of the input arguments. sys.argv is used by default
                     if argv is set to None (default value is None)
        :returns: The function returns a tuple with the following components:

            * arg_dict : Dictionary with the arguments for ConvertBrainData
            * arg_error: Boolean indicating whether an error occurred during parsing of arguments
            * logging_level: The level at which logging should be performed

        """
        if argv is None:
            argv = sys.argv
        arg_dict = {}
        i = 1
        arg_error = False
        logging_level = logging.DEBUG
        while i < (len(argv)):
            current_arg = argv[i]
            if current_arg == '--htk_raw':
                i += 1
                if i < len(argv):
                    htkr = {'htk_dir': argv[i],
                            'anatomy_file': None}
                    i += 1
                    if i < len(argv):
                        if not argv[i].startswith('--'):
                            htkr['anatomy_file'] = argv[i]
                            i += 1
                    if 'htk_raw' in arg_dict:
                        arg_dict['htk_raw'].append(htkr)
                    else:
                        arg_dict['htk_raw'] = [htkr]

                else:
                    arg_error = True
                    warnings.warn('Expected value following --htk_raw')
            elif current_arg == '--htk_processed':
                i += 1
                if i < len(argv):
                    htkp = {'htk_dir': argv[i],
                            'anatomy_file': None}
                    i += 1
                    if i < len(argv):
                        if not argv[i].startswith('--') and not ('anatomy' in argv[i]):
                            htkp['bands_file'] = argv[i]
                            i += 1
                    if not 'bands_file' in htkp:
                        htkp['bands_file'] = None
                    if i < len(argv):
                        if not argv[i].startswith('--'):
                            htkp['anatomy_file'] = argv[i]
                            i += 1

                    if 'htk_processed' in arg_dict:
                        arg_dict['htk_processed'].append(htkp)
                    else:
                        arg_dict['htk_processed'] = [htkp]
                else:
                    arg_error = True
                    warnings.warn('Expected value following --htk_processed')
            elif current_arg == '--output':
                i += 1
                if i < len(argv):
                    arg_dict['output'] = argv[i]
                    i += 1
                else:
                    arg_error = True
                    warnings.warn('Expected output file value following --output')
            elif current_arg == '--help':
                ConvertBrainData.print_help()
                exit(0)
            elif current_arg == '--log':
                i += 1
                if i < len(argv):
                    if argv[i] == 'DEBUG':
                        logging_level = logging.DEBUG
                    elif argv[i] == 'INFO':
                        logging_level = logging.INFO
                    elif argv[i] == 'WARNING':
                        logging_level = logging.WARNING
                    elif argv[i] == 'ERROR':
                        logging_level = logging.ERROR
                    elif argv[i] == 'CRITICAL':
                        logging_level = logging.CRITICAL
                    else:
                        arg_error = True
                        warnings.warn('Unrephysnized logging level specified: '+argv[i])
                    i += 1
                else:
                    arg_error = True
                    warnings.warn('Expected logging level following --log')

            else:
                arg_error = True
                warnings.warn('Unrephysnized input argument %s' % current_arg)

        # Define whether we should print read status to screen
        arg_dict['print_status'] = logging_level in [logging.DEBUG, logging.INFO]

        return arg_dict, arg_error, logging_level


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.captureWarnings(True)
    argument_dict, argument_error, log_level = ConvertBrainData.parse_arguments()
    logging.basicConfig(level=log_level)
    if argument_error:
        ConvertBrainData.print_help()
        exit(0)
    ConvertBrainData(**argument_dict).convert()

