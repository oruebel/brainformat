"""
Tools module for conversion of brain data ot the BrainData HDF5 format.

"""
import os
import logging
import argparse
import numpy as np
from scipy import misc
from ast import literal_eval
import scipy.io as sio

from brain.dataformat.brainformat import \
    BrainDataFile, \
    BrainDataEphys, \
    BrainDataEphysProcessed, \
    BrainDataStimulus, \
    BrainDataStaticDescriptorInstrument, \
    BrainDataMetadataDataset, \
    BrainDataMetadataGroup, \
    BrainDataCollection
from brain.dataformat.base import RelationshipAttribute
from brain.readers.htkcollection import \
    HTKCollection,\
    HTKFile
from brain.readers.wavfile import WAVFile


# Question 1: Which polytride layout was used (2 or 3 columns?)
# Question 2: Which Chip layout was used (64-channel R or S?)
# Question 3: Do we have the time-aligned stimulus array so that we can generate the plots that Kris would like to see?

# Question 4: What are the right bands for Wvlt_70to170_1band and MUA
# Question 5: Which name should we use to descripe the type of ephys chip and polytrode used?

# Example:
# python convert_rat_data.py \
#           --raw_dirs '/Volumes/Data Drive/brain/R18_B12/RawHTK' \
#           --stimulus_trigger_mark '/Volumes/Data Drive/brain/R18_B12/mrk11.htk' \
#           --stimulus_type "White Noise" \
#           --stimulus_signal '/Volumes/Data Drive/brain/Stimulus/stimulus_signal_03202013.wav' \
#           --stimulus_audio '/Volumes/Data Drive/brain/R18_B12/aud11.htk' \
#           --stimulus_trigger '/Volumes/Data Drive/brain/Stimulus/stimulus_trigger_03202013.wav' \
#           --surgical_notes '/Volumes/Data Drive/brain/SurgicalNotes/RAT18' \
#           --grid_geometry_image '/Volumes/Data Drive/brain/Geometry/64ch_layout.png'\
#           --grid_orientation R \
#           --processed_dirs '/Volumes/Data Drive/brain/R18_B12/Wvlt_4to1200_54band_CAR1' '/Volumes/Data Drive/brain/R18_B12/Wvlt_70to170_1band' '/Volumes/Data Drive/brain/R18_B12/MUA' \
#           --bands_files '/Volumes/Data Drive/brain/cfs.4_1200.54Wvl.mat' guess '[1200,]' \
#           --processed_notes 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 54 different frequencies logarithmically spaced between 4 and 1200Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 1 frequency logarithmically spaced between 70 and 170Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 1 frequency logarithmically spaced between 70 and 170Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' \
#           --output test.h5
#
# python convert_rat_data.py --raw_dirs '/Volumes/Data Drive/brain/R18_B12/RawHTK' --stimulus_trigger_mark '/Volumes/Data Drive/brain/R18_B12/mrk11.htk' --stimulus_type "White Noise" --stimulus_signal '/Volumes/Data Drive/brain/Stimulus/stimulus_signal_03202013.wav'  --stimulus_audio '/Volumes/Data Drive/brain/R18_B12/aud11.htk' --stimulus_trigger '/Volumes/Data Drive/brain/Stimulus/stimulus_trigger_03202013.wav' --surgical_notes '/Volumes/Data Drive/brain/SurgicalNotes/RAT18' --grid_geometry_image '/Volumes/Data Drive/brain/Geometry/64ch_layout.png' --grid_orientation R --processed_dirs '/Volumes/Data Drive/brain/R18_B12/Wvlt_4to1200_54band_CAR1' '/Volumes/Data Drive/brain/R18_B12/Wvlt_70to170_1band' '/Volumes/Data Drive/brain/R18_B12/MUA' --bands_files '/Volumes/Data Drive/brain/cfs.4_1200.54Wvl.mat' guess '[1200,]' --processed_notes 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 54 different frequencies logarithmically spaced between 4 and 1200Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 1 frequency logarithmically spaced between 70 and 170Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 1 frequency logarithmically spaced between 70 and 170Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' --output '/Volumes/Data Drive/test.h5'
#

# TODO Add function to ephys data read to create pandas dataframe for the dataset
# TODO Add metadata about the preprocessing
# TODO Add MUA spiking indices data
# TODO Amplitude/Frequency Tuning curves for each electrode
# TODO Create figure of three surve plots of a narrow time-frame showing the raw, MUA spiking, and MUA smoothed data
# TODO Create frequency/apmplitude mean respones bin plot for all electrodes and lay them out based on the grid and polytrode locations

logger = logging.getLogger(__name__)


class RawDescriptionDefaultHelpArgParseFormatter(argparse.ArgumentDefaultsHelpFormatter,
                                                 argparse.RawDescriptionHelpFormatter):
    """
    Simple derived formatter class for use with argparse. This formatter combines the default
    argparse.ArgumentDefaultsHelpFormatter and argparse.RawDescriptionHelpFormatter
    for formatting arguments and help descriptions.

    """
    pass


class InstrumentLayout(object):

    @staticmethod
    def polytrode(ncols=2):
        """
        Get the layout for the polytrode

        :param ncols: Integer indicating the number of columns in the polytrode. One of 2,3.
        :return: array with the layout index for the polytrode
        """
        if ncols == 3:
            matlab_array = np.asarray([[np.nan, 17, np.nan],
                                       [10    , 16, 23    ],
                                       [9     , 18, 24    ],
                                       [8     , 15, 25    ],
                                       [7     , 19, 26    ],
                                       [6     , 14, 27    ],
                                       [5     , 20, 28    ],
                                       [4     , 13, 29    ],
                                       [3     , 21, 30    ],
                                       [2     , 12, 31    ],
                                       [1     , 22, 32    ],
                                       [np.nan, 11, np.nan],
                                       ])
        elif ncols == 2:
            matlab_array = np.asarray([[20, 18],
                                   [16, 14],
                                   [12, 10],
                                   [8 , 6 ],
                                   [4 , 2 ],
                                   [22, 24],
                                   [26, 28],
                                   [30, 32],
                                   [31, 29],
                                   [27, 25],
                                   [23, 21],
                                   [1 , 3 ],
                                   [5 , 7 ],
                                   [9 , 11],
                                   [13, 15],
                                   [17, 19],])
        else:
            raise ValueError("Ncols must be one of 2 or 3")
        python_array = matlab_array-1
        return python_array

    @staticmethod
    def polytrode_position_in_grid(orientiation, xspacing=0.2, yspacing=0.2):
        """
        The location of the polytrode with respect to the ephys grid

        :param orientation: Char with the channel orientation of the ephys grid. One of 'S' or 'R'

        :return: Two numpy arrays of two floats. The first array is the (x,y) index location in the grid, and the
                 second array is the spatial (x,y) location is the spatial location.
        """
        index_location = np.asarray([7,0.5])
        spatial_location = np.asarray([index_location[0]*yspacing, index_location[1]*xspacing])
        return index_location, spatial_location

    @staticmethod
    def grid(orientation, xspacing=0.2, yspacing=0.2):
        """

        :param orientation: Char with the channel orientation. One of 'S' or 'R'
        :param xspacing: The spacing to be used to compute the electrodes x positions
        :param yspacing: The spacing to be used to compute the electrodes y positions
        :return:
        """
        # Grid index
        matlab_array = np.asarray([[15, 13, 11, 9 , 7 , 5 , 3 , 1 ],
                                   [16, 14, 12, 10, 8 , 6 , 4 , 2 ],
                                   [32, 30, 28, 26, 24, 22, 20, 18],
                                   [31, 29, 27, 25, 23, 21, 19, 17],
                                   [47, 45, 43, 41, 39, 37, 35, 33],
                                   [48, 46, 44, 42, 40, 38, 36, 34],
                                   [64, 62, 60, 58, 56, 54, 52, 50],
                                   [63, 61, 59, 57, 55, 53, 51, 49]])
        if orientation == 'R':
            pass
        elif orientation == 'S':
            matlab_array = matlab_array[:, ::-1]
        python_array = matlab_array-1

        # Grid position
        position_array = np.zeros(shape=(8, 8, 2), dtype='float')
        for i in range(8):
            position_array[i, :, 0] = np.arange(0, 8) * yspacing
            position_array[:, i, 1] = np.arange(0, 8) * xspacing

        return python_array, position_array


class ConvertRatData(object):
    """
    Class used to convert the rat data
    """

    GUESS_KEYS = ['guess', 'Guess', 'g', 'G']  # String keys used to indicate where we should guess frequency bands.

    def __init__(self,
                 output,
                 raw_dirs,
                 processed_dirs,
                 processed_notes,
                 bands_files,
                 stimulus_amplitude,
                 stimulus_frequency,
                 stimulus_signal,
                 stimulus_trigger,
                 stimulus_audio,
                 stimulus_trigger_mark,
                 stimulus_amplitude_processed,
                 stimulus_frequency_processed,
                 stimulus_trigger_mark_processed,
                 stimulus_type,
                 surgical_notes_dir,
                 grid_geometry_image,
                 grid_orientation='R',
                 grid_xspacing=0.2,
                 grid_yspacing=0.2,
                 polytrode_ncols=2,
                 polytrode_position=None):
        """

        :param output: The output HDF5 file to be used
        :param raw_dirs: List of path of directories with raw HTK recordings
        :param processed_dirs: List of paths of directories with processed HTK recordings
        :param processed_notes: List of strings with notes describing how the processed data was derived from the raw data.
        :param bands_files: List of paths to files with the frequency bands from the processsing. Must have the same
                    length as processed_dirs. Use 'guess' to indicate that we should guess the frequency bands based
                    on the filename. Use 'None' to indicate that no bands should be used for the channel.
        :param stimulus_amplitude: Path to the time-aligned HTK file with the stimulus amplitude
        :param stimulus_frequency: Path to the time-aligned HTK file with the stimulus frequency
        :param stimulus_signal: Path to the WAV file with the stimulus signal
        :param stimulus_trigger:  Path to the WAV file with the stimulus trigger
        :param stimulus_type: String indicating the type of stimulus
        :param stimulus_audio:  Path to the WAV file with the stimulus audio
        :param stimulus_trigger_mark:  Path to the WAV file with the stimulus markers
        :param surgical_notes_dir:  Path to the directory with the surgical notes
        :param grid_geometry_image:  Path to the file with the image of teh geometry of the chip
        :param grid_orientation: R or S depending describing the orientation of the chip
        :param grid_xspacing: Floating value indicating the spacing in x between electrodes in the grid
        :param grid_yspacing: Floating value indicating the spacing in y between electrodes in the grid
        :param polytrode_ncols: Number of columns of the polytrode
        :param polytrode_position: Index location of the polytrode in the grid
        """
        self.output = output
        self.raw_dirs = raw_dirs
        self.stimulus_signal = stimulus_signal
        self.stimulus_amplitude = stimulus_amplitude
        self.stimulus_amplitude_sample_rate_base = 10000
        self.stimulus_amplitude_unit = 'Volt'
        self.stimulus_frequency = stimulus_frequency
        self.stimulus_frequency_sample_rate_base = 10000
        self.stimulus_frequency_unit = 'Hz'
        self.stimulus_trigger = stimulus_trigger
        self.stimulus_audio = stimulus_audio
        self.stimulus_audio_sample_rate_base = 10000
        self.stimulus_type = stimulus_type
        self.stimulus_trigger_mark = stimulus_trigger_mark
        self.stimulus_trigger_mark_sample_rate_base = 10000
        self.stimulus_trigger_mark_unit = 'Volt'
        self.stimulus_trigger_mark_processed = stimulus_trigger_mark_processed
        self.stimulus_trigger_mark_processed_unit = 'Volt'
        self.stimulus_amplitude_processed = stimulus_amplitude_processed
        self.stimulus_amplitude_processed_unit = 'Volt'
        self.stimulus_frequency_processed = stimulus_frequency_processed
        self.stimulus_frequency_processed_unit = 'Hz'
        self.surgical_notes_dir = surgical_notes_dir

        self.grid_postfix = np.arange(1, 65)
        self.gird_sample_rate_base = 10000
        self.grid_geometry_image = grid_geometry_image
        self.grid_type = "ephys Grid"  # TODO Set proper chip name
        self.grid_layout, self.grid_locations = InstrumentLayout.grid(orientation=grid_orientation,
                                                                      xspacing=grid_xspacing,
                                                                      yspacing=grid_yspacing)

        self.polytrode_type = "ephys Polytrode"      # TODO Set proper polytrode name
        self.polytrode_postfix = np.arange(97, 129)
        self.polytrode_sample_rate_base = 10000
        self.polytrode_layout = InstrumentLayout.polytrode(ncols=polytrode_ncols)
        self.polytrode_to_grid_index_location, self.polytrode_to_grid_spatial_location = \
            InstrumentLayout.polytrode_position_in_grid(orientiation=grid_orientation,
                                                        xspacing=grid_xspacing,
                                                        yspacing=grid_yspacing)
        if polytrode_position:
            self.polytrode_to_grid_index_location = polytrode_position
            self.polytrode_to_grid_spatial_location = np.asarray([polytrode_position[0]*grid_yspacing,
                                                                  polytrode_position[1]*grid_xspacing])

        self.processed_dirs = processed_dirs
        if processed_notes is not None:
            if len(processed_notes) != len(self.processed_dirs):
                raise ValueError('We need to provide as many notes as there are processed datasets or None.')
        self.processed_notes = processed_notes
        self.bands_files = [i if i not in self.GUESS_KEYS else None for i in bands_files]
        self.bands_guess = [i in self.GUESS_KEYS for i in bands_files]
        # Check if the user provided and explicit list of bands for any of the files us it instead
        for i, v in enumerate(self.bands_files):
            if v is not None:
                try:
                    bands = literal_eval(v)
                    if bands is None:
                        self.bands_guess[i] = False
                        self.bands_files[i] = None
                    elif not isinstance(bands, basestring):
                        self.bands_guess[i] = bands
                        self.bands_files[i] = None
                except SyntaxError:
                    pass

        self.processed_sample_rate_base = [10000] * len(self.processed_dirs)

        self.print_status = True

    def log_settings(self):
        """
        Print file convert settings to standard out using print.
        """
        for k, v in vars(self).iteritems():
            logger.info(k + " = " + str(v))

    def create_output_file(self):
        """
        Internal helper function used to create the BrainDataFile for self.output
        :return: BrainDataFile object or None
        """
        if self.output is not None:
            if os.path.exists(self.output):
                outfile = BrainDataFile(self.output, mode='a')
            else:
                outfile = BrainDataFile.create(self.output)
            logger.info("Created output HDF5 file: " + unicode(self.output))
        else:
            logger.warning('No output file specified via --output. Performing dry run.')
            outfile = None
        return outfile

    def write_instrument_metadata(self, outfile):
        """

        :param outfile: The BrainDataFile output file (or None in case no write should be performed)
        :return: Tuple of two BrainDataStaticDescriptorInstrument objects, one for the ephys grid and one for the
                 polytrode, respectively. Both objects may be None if outfile is set to None.
        """
        static_descriptors_grp = None if outfile is None else outfile.descriptors().static()
        if outfile:
            logger.info("Creating ephys grid instrument metadata")
            ephys_grid_instrument = BrainDataStaticDescriptorInstrument.create(parent_object=static_descriptors_grp,
                                                                              type=self.grid_type,
                                                                              layout_index=self.grid_layout,
                                                                              layout_locations=self.grid_locations)
            # Read and write the grid_geometry_image
            if self.grid_geometry_image is not None:
                ggi_dataset = BrainDataMetadataDataset.create(parent_object=ephys_grid_instrument,
                                                              object_name=os.path.basename(self.grid_geometry_image),
                                                              dataset_args={'data': misc.imread(self.grid_geometry_image)},
                                                              user_description='Image of the layout of the instruments geometry',
                                                              unit=None,
                                                              ontology=None)
                ggi_dataset.hdf_object.attrs['CLASS'] = 'IMAGE'
                ggi_dataset.hdf_object.attrs['IMAGE_VERSION'] = '1.2'
                ggi_dataset.hdf_object.attrs['IMAGE_SUBCLASS'] = 'IMAGE_TRUECOLOR'

            # Write the polytrode instrument metadata
            logger.info("Creating ephys polytrode instrument metadata")
            ephys_poly_instrument = BrainDataStaticDescriptorInstrument.create(parent_object=static_descriptors_grp,
                                                                              type=self.polytrode_type,
                                                                              layout_index=self.polytrode_layout,
                                                                              layout_locations=None)
            _ = BrainDataMetadataDataset.create(parent_object=ephys_poly_instrument,
                                                object_name='polytrode_to_grid_index_location',
                                                dataset_args={'data': self.polytrode_to_grid_index_location},
                                                unit=None,
                                                ontology=None,
                                                user_description='O-based X/Y index location of the polytrode within the ephys grid')
            _ = BrainDataMetadataDataset.create(parent_object=ephys_poly_instrument,
                                                object_name='polytrode_to_grid_spatial_location',
                                                dataset_args={'data': self.polytrode_to_grid_spatial_location},
                                                unit=None,
                                                ontology=None,
                                                user_description='X/Y spatial location of the polytrode within the ephys grid')
            return ephys_grid_instrument, ephys_poly_instrument
        else:
            return None, None

    def write_stimulus_metadata(self, outfile):
        """
        Write the stimulus metadata given by self.stimulus_type, self.stimulus_signal, self.stimulus_trigger,
        self.stimulus_audio,  self.stimulus_trigger_mark, self.stimulus_amplitude, self.stimulus_frequncy


        :param outfile: The BrainDataFile output file (or None in case no write should be performed)
        :return:  Tuple of the following objects:

            * stimulus : BrainDataStimulus :  object with all the stimulus data
            * stimulus_signal_obj:  BrainDataMetadataDataset  : object with the self.stimulus_signal WAV data
            * stimulus_trigger_obj : BrainDataMetadataDataset : object with the self.stimulus_trigger WAV data
            * stimulus_audio_obj : BrainDataMetadataDataset : object with the self.stimulus_audio HTK data
            * stimulus_trigger_mark_obj : BrainDataMetadataDataset : object with the self.stimulus_trigger_mark HTK data
            * stimulus_amplitude_obj: BrainDataMetadataDataset : object with the self.stimulus_amplitude HTK data
            * stimulus_frequency_obj: BrainDataMetadataDataset : object with the self.stimulus_frequency HTK data

        """
        external_data_grp = None if outfile is None else outfile.data().external()

         # Write the stimulus data
        if outfile:
            logger.info("Creating stimulus metadata")
            stimulus = BrainDataStimulus.create(parent_object=external_data_grp,
                                                type=self.stimulus_type)
        # Convert stimulus signal WAV file
        if self.stimulus_signal is not None:
            logger.info("  Stimulus metadata: adding stimulus signal data")
            stimulus_signal_wavfile = WAVFile(self.stimulus_signal, read_on_creation=True)
            if outfile:
                stimulus_signal_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_signal',
                    dataset_args={'data': stimulus_signal_wavfile.data.squeeze()},
                    unit='audio',
                    ontology=None)
                stimulus_signal_obj.hdf_object.attrs['sampling_rate'] = stimulus_signal_wavfile.sampling_rate
            del stimulus_signal_wavfile
        else:
            stimulus_signal_obj = None

        # Convert stimulus trigger HTK file
        if self.stimulus_trigger is not None:
            logger.info("  Stimulus metadata: adding stimulus trigger data")
            stimulus_trigger_wavfile = WAVFile(self.stimulus_trigger, read_on_creation=True)
            if outfile:
                stimulus_trigger_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_trigger',
                    dataset_args={'data': stimulus_trigger_wavfile.data.squeeze()},
                    unit='audio',
                    ontology=None)
                stimulus_trigger_obj.hdf_object.attrs['sampling_rate'] = stimulus_trigger_wavfile.sampling_rate
            del stimulus_trigger_wavfile
        else:
            stimulus_trigger_obj = None

        # Convert stimulus audio HTK file
        if self.stimulus_audio is not None:
            logger.info("  Stimulus metadata: adding stimulus audio data")
            aud_htk = HTKFile(self.stimulus_audio,
                              sample_rate_base=self.stimulus_audio_sample_rate_base)
            aud_htkdata = aud_htk.read_data()
            if outfile:
                stimulus_audio_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_audio',
                    dataset_args={'data': aud_htkdata.squeeze()},
                    unit='audio',
                    ontology=None)
                stimulus_audio_obj.hdf_object.attrs['sampling_rate'] = aud_htk.sample_rate
            del aud_htkdata
            del aud_htk
        else:
            stimulus_audio_obj = None

        # Convert stimulus trigger mark
        if self.stimulus_audio is not None:
            logger.info("  Stimulus metadata: adding stimulus trigger mark data")
            stm_htk = HTKFile(self.stimulus_trigger_mark,
                              sample_rate_base=self.stimulus_audio_sample_rate_base)
            stm_htkdata = stm_htk.read_data()
            if outfile:
                stimulus_trigger_mark_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_trigger_mark',
                    dataset_args={'data': stm_htkdata.squeeze()},
                    unit=self.stimulus_trigger_mark_unit,
                    ontology=None)
                stimulus_trigger_mark_obj.hdf_object.attrs['sampling_rate'] = stm_htk.sample_rate
            del stm_htkdata
            del stm_htk
        else:
            stimulus_trigger_mark_obj = None

        # Convert stimulus frequency data
        if self.stimulus_audio is not None:
            logger.info("  Stimulus metadata: adding stimulus frequency data")
            stm_htk = HTKFile(self.stimulus_frequency,
                              sample_rate_base=self.stimulus_frequency_sample_rate_base)
            stm_htkdata = stm_htk.read_data()
            if outfile:
                stimulus_frequency_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_frequency',
                    dataset_args={'data': stm_htkdata.squeeze()},
                    unit=self.stimulus_frequency_unit,
                    ontology=None)
                stimulus_frequency_obj.hdf_object.attrs['sampling_rate'] = stm_htk.sample_rate
            del stm_htkdata
            del stm_htk
        else:
            stimulus_frequency_obj = None

        # Convert stimulus amplitude data
        if self.stimulus_audio is not None:
            logger.info("  Stimulus metadata: adding stimulus amplitude data")
            stm_htk = HTKFile(self.stimulus_amplitude,
                              sample_rate_base=self.stimulus_amplitude_sample_rate_base)
            stm_htkdata = stm_htk.read_data()
            if outfile:
                stimulus_amplitude_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_amplitude',
                    dataset_args={'data': stm_htkdata.squeeze()},
                    unit=self.stimulus_amplitude_unit,
                    ontology=None)
                stimulus_amplitude_obj.hdf_object.attrs['sampling_rate'] = stm_htk.sample_rate
            del stm_htkdata
            del stm_htk
        else:
            stimulus_amplitude_obj = None

        # Convert the stimulus amplitude data that is time aligned with the processed data
        if self.stimulus_amplitude_processed is not None:
            logger.info("  Stimulus metadata: adding processed stimulus amplitude")
            stm_proc_mat = sio.loadmat(self.stimulus_amplitude_processed)
            if outfile:
                temp  = stm_proc_mat['stimAmps'].squeeze()
                # TODO Fix missing values in time-aligned processed stimulus data
                dat = np.zeros(shape=(temp.size+4,), dtype=temp.dtype)
                dat[:] = np.nan
                dat[4:] = temp
                stimulus_amplitude_processed_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_amplitude_processed',
                    dataset_args={'data': dat},
                    unit=self.stimulus_amplitude_processed_unit,
                    ontology=None
                )
                stimulus_amplitude_processed_obj.hdf_object.attrs['sampling_rate'] = stm_proc_mat['TfsN'].squeeze()[()]

        # Convert the stimulus data that is time aligned with the processed data
        if self.stimulus_frequency_processed is not None:
            logger.info("  Stimulus metadata: adding processed stimulus frequency")
            stm_proc_mat = sio.loadmat(self.stimulus_frequency_processed)
            if outfile:
                temp = stm_proc_mat['stimFrqs'].squeeze()
                # TODO Fix missing values in time-aligned processed stimulus data
                dat = np.zeros(shape=(temp.size+4,), dtype=temp.dtype)
                dat[:] = np.nan
                dat[4:] = temp
                stimulus_frequency_processed_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_frequency_processed',
                    dataset_args={'data': dat},
                    unit=self.stimulus_frequency_processed_unit,
                    ontology=None
                )
                stimulus_frequency_processed_obj.hdf_object.attrs['sampling_rate'] = stm_proc_mat['TfsN'].squeeze()[()]

        # Convert the stimulus data that is time aligned with the processed data
        if self.stimulus_trigger_mark_processed is not None:
            logger.info("  Stimulus metadata: adding processed stimulus trigger_mark")
            stm_proc_mat = sio.loadmat(self.stimulus_trigger_mark_processed)
            if outfile:
                temp = stm_proc_mat['mrk'].squeeze()
                # TODO Fix missing values in time-aligned processed stimulus data
                dat = np.zeros(shape=(temp.size+4,), dtype=temp.dtype)
                dat[:] = np.nan
                dat[4:] = temp
                stimulus_trigger_mark_processed_obj = BrainDataMetadataDataset.create(
                    parent_object=stimulus,
                    object_name='stimulus_trigger_mark_processed',
                    dataset_args={'data': dat},
                    unit=self.stimulus_trigger_mark_processed_unit,
                    ontology=None
                )
                stimulus_trigger_mark_processed_obj.hdf_object.attrs['sampling_rate'] = stm_proc_mat['TfsN'].squeeze()[()]

        return stimulus, stimulus_signal_obj, stimulus_trigger_obj, stimulus_audio_obj, \
               stimulus_trigger_mark_obj, stimulus_frequency_obj, stimulus_amplitude_obj, \
               stimulus_trigger_mark_processed_obj, stimulus_frequency_processed_obj, stimulus_amplitude_processed_obj

    def write_surgical_notes(self, outfile):
        """
        Helper function used to write the surgical notes data given by self.surgical_notes_dir

        :param outfile: The BrainDataFile output file (or None in case no write should be performed)
        :return:  BrainDataMetadataGroup with the surgical notes data or None in case no data has been written
        """
        # Write the surgical notes data
        surgical_notes_obj = None
        static_descriptors_grp = None if outfile is None else outfile.descriptors().static()
        if self.surgical_notes_dir:
            logger.info('Converting surgical notes:')
            if outfile:
                surgical_notes_obj = BrainDataMetadataGroup.create(
                    parent_object=static_descriptors_grp,
                    object_name='surgical_notes')
            for objname in os.listdir(self.surgical_notes_dir):
                objpath = os.path.join(self.surgical_notes_dir, objname)
                if os.path.isfile(objpath):
                    try:
                        objdata = misc.imread(objpath)
                        asimage = True
                        logger.info('  Saving ' + objpath + ' as image array')
                    except:
                        logger.info('  Saving ' + objpath + ' as raw string')
                        tempfile = open(objpath, 'r')
                        objdata = tempfile.read()
                        asimage = False
                        tempfile.close()
                    if outfile:
                        try:
                            temp_meta = BrainDataMetadataDataset.create(
                                        parent_object=surgical_notes_obj,
                                        object_name=os.path.splitext(objname)[0],
                                        dataset_args={'data': objdata},
                                        unit=None,
                                        ontology=None)
                            if asimage:
                                temp_meta.hdf_object.attrs['CLASS'] = 'IMAGE'
                                temp_meta.hdf_object.attrs['IMAGE_VERSION'] = '1.2'
                                temp_meta.hdf_object.attrs['IMAGE_SUBCLASS'] = 'IMAGE_TRUECOLOR'
                        except:
                            logger.warning("Save of " + objpath + "failed.")
        return surgical_notes_obj

    def create_collection_objects(self, outfile):
        """
        Create the collection storage object targets for the grid and poly instrument data and relate them via
        a user relationship with the instrument used

        :param outfile: The BrainDataFile output file (or None in case no write should be performed)
        :return: Tuple of (ephys_grid_collection, ephys_poly_collection) with the BrainDataCollection storage targets.
                 (None,None) may be returned if the objects were not created, e.g., if outfile was None.
        """
        if outfile is not None:
            internal_data_grp = outfile.data().internal()
            ephys_grid_collection = BrainDataCollection.create(parent_object=internal_data_grp)
            ephys_poly_collection = BrainDataCollection.create(parent_object=internal_data_grp)
            return ephys_grid_collection, ephys_poly_collection
        else:
            return None, None

    def write_raw_data(self,
                       outfile,
                       ephys_grid_collection,
                       ephys_poly_collection,
                       ephys_grid_instrument,
                       ephys_poly_instrument,
                       stimulus_dimensions):
        """
        Write the raw ephys grid and polytrode data

        :param outfile: The BrainDataFile output file (or None in case no write should be performed)
        :param ephys_grid_collection: BrainDataCollection object where the grid data should be collected
        :param ephys_grid_collection: BrainDataCollection object where the poly data should be collected
        :param ephys_grid_instrument: The BrainDataStaticDescriptorInstrument object with the instrument metadata
                                     for the grid data
        :param ephys_poly_instrument: The BrainDataStaticDescriptorInstrument object with the instrument metadata
                                     for the polytrode data
        :param stimulus_dimensions: List of HDF5 datasets with time-aligned stimulus h5py.Datasets (with unit attribute)
                                    data that should be associated with the raw data as dimension scales
        :return: Tuple of two lists. The first list has all BrainDataEphys objects for the grid data. The second
                 list has all BrainDataEphys objects for the polytrode data.
        """
        # Write the raw HTK data
        logger.info("Creating raw HTK data")
        grid_ephys = []
        poly_ephys = []
        internal_grp = None if outfile is None else outfile.data().internal()
        for htkdir in self.raw_dirs:
            # Read the stimulus trigger mark, amplitude, frequency files if available
            # if self.stimulus_trigger_mark:
            #     stm_htk = HTKFile(self.stimulus_trigger_mark,
            #                       sample_rate_base=self.stimulus_trigger_mark_sample_rate_base)
            #     stm_htkdata = stm_htk.read_data()
            # else:
            #     stm_htk = None
            #     stm_htkdata = None
            #
            # if self.stimulus_amplitude:
            #     amp_htk = HTKFile(self.stimulus_amplitude,
            #                       sample_rate_base=self.stimulus_amplitude_sample_rate_base)
            #     amp_htkdata = amp_htk.read_data()
            # else:
            #     amp_htk = None
            #     amp_htkdata = None
            #
            # if self.stimulus_frequency:
            #     frq_htk = HTKFile(self.stimulus_frequency,
            #                       sample_rate_base=self.stimulus_frequency_sample_rate_base)
            #     frq_htkdata = frq_htk.read_data()
            # else:
            #     frq_htk = None
            #     frq_htkdata = None

            # Read the grid HTK data
            logger.info("  Reading ephys Grid from " + str(htkdir))
            gridhtk = HTKCollection(htkdir,
                                    noblock=True,
                                    postfix=self.grid_postfix,
                                    layout=self.grid_layout,
                                    sample_rate_base=self.gird_sample_rate_base)
            htkdata = gridhtk.read_data(print_status=self.print_status)
            # Write the grid HTK data
            if outfile:
                logger.info("  Writing ephys Grid from " + str(htkdir))
                raw_ephys_grid = BrainDataEphys.create(parent_object=internal_grp if ephys_grid_collection is None else ephys_grid_collection,
                                                     ephys_data=htkdata,
                                                     sampling_rate=gridhtk.sample_rate,
                                                     anatomy_names=None,
                                                     layout=None) # ephys_grid_instrument['layout_index'])
                if stimulus_dimensions is not None:
                    for sd in stimulus_dimensions:
                        raw_ephys_grid.add_dimension_scale(scale=sd,
                                                           unit=sd.attrs['unit'] ,
                                                           axis=1,
                                                           dataset_name=None,
                                                           description=None,
                                                           relationship_axes=0)

                grid_ephys.append(raw_ephys_grid)
            del gridhtk
            del htkdata

            # Read the polytride HTK data
            logger.info("  Reading ephys Polytrode from " + str(htkdir))
            polyhtk = HTKCollection(htkdir,
                                    noblock=True,
                                    layout=self.polytrode_layout,
                                    postfix=self.polytrode_postfix,
                                    sample_rate_base=self.polytrode_sample_rate_base)
            htkdata = polyhtk.read_data(print_status=self.print_status)
            # Write the polytrode HTK data
            if outfile:
                logger.info("  Writing ephys Polytrode from " + str(htkdir))
                raw_ephys_poly = BrainDataEphys.create(parent_object=internal_grp if ephys_poly_collection is None else ephys_poly_collection,
                                                     ephys_data=htkdata,
                                                     sampling_rate=polyhtk.sample_rate,
                                                     anatomy_names=None,
                                                     layout=None) #ephys_poly_instrument['layout_index'])
                if stimulus_dimensions is not None:
                    for sd in stimulus_dimensions:
                        raw_ephys_poly.add_dimension_scale(scale=sd,
                                                           unit=sd.attrs['unit'] ,
                                                           axis=1,
                                                           dataset_name=None,
                                                           description=None,
                                                           relationship_axes=0)
                poly_ephys.append(raw_ephys_poly)
            del polyhtk
            del htkdata

        return grid_ephys, poly_ephys

    def write_processed_data(self,
                             outfile,
                             ephys_grid_collection,
                             ephys_poly_collection,
                             ephys_grid_instrument,
                             ephys_poly_instrument,
                             stimulus_dimensions):
        """
        Write the processed ephys grid and polytrode data

        :param outfile: The BrainDataFile output file (or None in case no write should be performed)
        :param ephys_grid_collection: BrainDataCollection object where the grid data should be collected
        :param ephys_grid_collection: BrainDataCollection object where the poly data should be collected
        :param ephys_grid_instrument: The BrainDataStaticDescriptorInstrument object with the instrument metadata
                                     for the grid data
        :param ephys_poly_instrument: The BrainDataStaticDescriptorInstrument object with the instrument metadata
                                     for the polytrode data
        :param stimulus_dimensions: List of HDF5 datasets with time-aligned stimulus h5py.Datasets (with unit attribute)
                                    data that should be associated with the raw data as dimension scales
        :return: Tuple of two lists. The first list has all BrainDataEphys objects for the grid data. The second
                 list has all BrainDataEphys objects for the polytrode data.
        """
         # Write the raw HTK data
        logger.info("Creating processed HTK data")
        grid_ephys = []
        poly_ephys = []
        internal_grp = None if outfile is None else outfile.data().internal()
        for i in range(len(self.processed_dirs)):
            htkdir = self.processed_dirs[i]
            logger.info("  Reading processed ephys Grid from " + str(htkdir))
            gridhtk = HTKCollection(directory=htkdir,
                                    bands_file=self.bands_files[i],
                                    guess_bands=self.bands_guess[i],
                                    sample_rate_base=self.processed_sample_rate_base[i],
                                    noblock=True,
                                    layout=self.grid_layout,
                                    postfix=self.grid_postfix)
            htkdata = gridhtk.read_data(print_status=self.print_status)
            # Write the grid HTK data
            if outfile:
                logger.info("  Writing processed ephys Grid from " + str(htkdir))
                original_name = os.path.basename(htkdir)
                if len(original_name) == 0:
                    original_name = os.path.basename(os.path.split(htkdir)[0])

                processed_ephys_grid = BrainDataEphysProcessed.create(
                    parent_object=internal_grp if ephys_grid_collection is None else ephys_grid_collection,
                    ephys_data=htkdata,
                    sampling_rate=gridhtk.sample_rate,
                    anatomy_names=None,
                    ephys_data_units='Volts',
                    frequency_bands=np.asarray(gridhtk.bands),
                    original_name=original_name,
                    layout=None) #ephys_grid_instrument['layout_index'])
                if stimulus_dimensions is not None:
                    num_stim_axis = 0
                    for sd in stimulus_dimensions:
                        if sd.shape[0] == processed_ephys_grid.ephys_data().shape[1]:
                            processed_ephys_grid.add_dimension_scale(scale=sd,
                                                                     unit=sd.attrs['unit'] ,
                                                                     axis=1,
                                                                     dataset_name=None,
                                                                     description=None,
                                                                     relationship_axes=0)
                            num_stim_axis += 1
                    logger.info("    Added %i stimulus scales to %s" % (num_stim_axis, processed_ephys_grid.hdf_object.name))
                grid_ephys.append(processed_ephys_grid)
            del gridhtk
            del htkdata

            logger.info("  Reading processed ephys Polytrode from " + str(htkdir))
            polyhtk = HTKCollection(directory=self.processed_dirs[i],
                                    bands_file=self.bands_files[i],
                                    guess_bands=self.bands_guess[i],
                                    sample_rate_base=self.processed_sample_rate_base[i],
                                    noblock=True,
                                    layout=self.polytrode_layout,
                                    postfix=self.polytrode_postfix)
            htkdata = polyhtk.read_data(print_status=self.print_status)
            # Write the grid HTK data
            if outfile:
                logger.info("  Writing processed ephys Polytrode from " + str(htkdir))
                original_name = os.path.basename(htkdir)
                if len(original_name) == 0:
                    original_name = os.path.basename(os.path.split(htkdir)[0])

                processed_ephys_poly = BrainDataEphysProcessed.create(
                    parent_object=internal_grp if ephys_poly_collection is None else ephys_poly_collection,
                    ephys_data=htkdata,
                    sampling_rate=polyhtk.sample_rate,
                    anatomy_names=None,
                    ephys_data_units='Volts',
                    frequency_bands=np.asarray(polyhtk.bands),
                    original_name=original_name,
                    layout=None) # ephys_poly_instrument['layout_index'])
                if stimulus_dimensions is not None:
                    num_stim_axis = 0
                    for sd in stimulus_dimensions:
                        if sd.shape[0] == processed_ephys_poly.ephys_data().shape[1]:
                            processed_ephys_poly.add_dimension_scale(scale=sd,
                                                                     unit=sd.attrs['unit'] ,
                                                                     axis=1,
                                                                     dataset_name=None,
                                                                     description=None,
                                                                     relationship_axes=0)
                            num_stim_axis += 1
                    logger.info("    Added %i stimulus scales to %s" % (num_stim_axis, processed_ephys_grid.hdf_object.name))
                poly_ephys.append(processed_ephys_poly)
            del polyhtk
            del htkdata

            # Relate processed data with user relationship if they have the same start name
            for j in range(len(self.processed_dirs)):
                if htkdir.startswith(self.processed_dirs[j]):
                    RelationshipAttribute.create(parent_object=processed_ephys_grid,
                                                 relationship_type='user',
                                                 target_object=grid_ephys[j],
                                                 axis=None,
                                                 target_axis=None,
                                                 attribute='derived_from_rel_'+processed_ephys_grid.name+'_'+grid_ephys[j].name,
                                                 description='Relationship describing that a processed dataset was derived from another dataset')

        return grid_ephys, poly_ephys

    def relate_grid_and_polytrode_instruments(self, ephys_grid_instrument, ephys_poly_instrument):
        """
        Create a user relationship between the two instruments

        :param ephys_grid_instrument: BrainDataStaticDescriptorsInstrument object with the ephys grid instrument data
        :param ephys_poly_instrument:  BrainDataStaticDescriptorsInstrument object with the polytrode instrument data
        :return:
        """
        RelationshipAttribute.create(ephys_poly_instrument,
                                     relationship_type='user',
                                     target_object=ephys_grid_instrument,
                                     axis=None,
                                     target_axis=None,
                                     attribute='polytrode_to_grid_location',
                                     description='Relationship describing the location of the polytrode within the Ephys grid based on the x/y index and relative spatial location.',
                                     properties={'polytrode_to_grid_index_location': self.polytrode_to_grid_index_location.tolist(),
                                                 'polytrode_to_grid_spatial_location': self.polytrode_to_grid_spatial_location.tolist()})


    def relate_raw_data_time_scales(self, grid_ephys, poly_ephys):
        """
        Create order relationships between:
         1. all pairwise combinations of the time-axis dimensions scales of the raw grid and polytrode datasets
         2. all dimensions scales and the time axis of the oposing raw dataset
         3. all time axes of the raw datasets themself
        Create shared_ascending_encoding relationship between:
         1. all pairwise combindations of the time only dimensions scale of the raw grid and polytrode datasets

         :param grid_ephys: The list of grid ephys datasets
         :param poly_ephys: The list of poly ephys datasets


        """
        logger.info("Relating raw data time scales")
        ephys_data = grid_ephys + poly_ephys  # Relate all to all
        for i in range(len(ephys_data)-1):  # Relate all ephys datasets
            e1 = ephys_data[i]
            e1_ed = e1.ephys_data()
            for j in range((i+1), len(ephys_data)):  # Relate them with all other ephys dataset we have not visitied yet
                e2 = ephys_data[j]
                e2_ed = e2.ephys_data()
                # Relate all time dimensions scales of e1 with all scales of e2, and relate the scales with the datasets themself as well
                for e1_time_dim in e1.dims(axis=1, get_hdf=False):
                    for e2_time_dim in e2.dims(axis=1, get_hdf=False):
                        e1_dim_dataset = e1_time_dim['dataset']
                        e2_dim_dataset = e2_time_dim['dataset']
                        # If the two dimensions scales have the same name they are related via shared encoding and
                        # in the case of time shared_ascending_encoding relationships
                        if os.path.basename(e1_dim_dataset.name) == os.path.basename(e2_dim_dataset.name):
                            rtype = 'shared_ascending_encoding' \
                                if os.path.basename(e1_dim_dataset.name) == 'time_axis' \
                                else 'shared_encoding'
                            RelationshipAttribute.create(
                                parent_object=e1_dim_dataset,
                                target_object=e2_dim_dataset,
                                target_axis=0,
                                axis=0,
                                relationship_type = rtype,
                                description='Shared encoding relationship due to matching data for simultaneously recorded raw darta ',
                                attribute= rtype + '_' + e1_dim_dataset.name + '_' + e2_dim_dataset.name)
                        # Order relationship between the dimensions scale datasets directly
                        RelationshipAttribute.create(
                            parent_object=e1_dim_dataset,
                            target_object=e2_dim_dataset,
                            target_axis=0,
                            axis=0,
                            relationship_type='order',
                            description='Tertiary order relationship due to time dimensions scale relationship between raw data ',
                            attribute='order_' + e1_dim_dataset.name + '_' + e2_dim_dataset.name)
                        RelationshipAttribute.create(
                            parent_object=e2_dim_dataset,
                            target_object=e1_dim_dataset,
                            target_axis=0,
                            axis=0,
                            relationship_type='order',
                            description='Tertiary order relationship due to time dimensions scale relationship between raw data ',
                            attribute='order_' + e2_dim_dataset.name + '_' + e1_dim_dataset.name)
                        # Order relationship between  axis=1 of the polytrode dataset and the grid dimension scale
                        RelationshipAttribute.create(
                            parent_object=e1_dim_dataset,
                            target_object=e2_ed,
                            target_axis=1,
                            axis=0,
                            relationship_type='order',
                            description='Tertiary order relationship due to time dimensions scale relationship between raw data ',
                            attribute='order_' + e1_dim_dataset.name + '_' + e2_ed.name)
                        RelationshipAttribute.create(
                            parent_object=e2_ed,
                            target_object=e1_dim_dataset,
                            target_axis=0,
                            axis=1,
                            relationship_type='order',
                            description='Tertiary order relationship due to time dimensions scale relationship between raw data ',
                            attribute='order_' + e2_ed.name + '_' + e1_dim_dataset.name)
                         # Order relationship between  axis=1 of the grid dataset and the polytrode dimension scale
                        RelationshipAttribute.create(
                            parent_object=e2_dim_dataset,
                            target_object=e1_ed,
                            target_axis=1,
                            axis=0,
                            relationship_type='order',
                            description='Tertiary order relationship due to time dimensions scale relationship between raw data ',
                            attribute='order_' + e2_dim_dataset.name + '_' + e1_ed.name)
                        RelationshipAttribute.create(
                            parent_object=e1_ed,
                            target_object=e2_dim_dataset,
                            target_axis=0,
                            axis=1,
                            relationship_type='order',
                            description='Tertiary order relationship due to time dimensions scale relationship between raw data ' + e1.hdf_object.name,
                            attribute='order_' + e1_ed.name + '_' + e2_dim_dataset.name)

    def relate_raw_data_blocks_by_order(self, grid_ephys, poly_ephys):
        """
        Relate all raw ephys datasets by order relationships along their dimensions

        :param grid_ephys: List of all ephys BrainDataEphys data objects
        :param poly_ephys: List of all BrainDataEphys polytrode data objects
        :return:
        """
        logger.info("Relating raw data blocks")
        # Relate all grids with all grids
        if len(grid_ephys) > 1:
            for i in range(len(grid_ephys)-1):
                for j in range(i+1, len(grid_ephys)):
                    di = grid_ephys[i].ephys_data()
                    dj = grid_ephys[j].ephys_data()
                    RelationshipAttribute.create(
                        parent_object=di,
                        target_object=dj,
                        target_axis=[0,1],
                        axis=[0,1],
                        relationship_type='order',
                        description='Relationship describing that times and electrodes  are ordered in the same way',
                        attribute='order_time_eid_' + di.name + "_" + dj.name)
                    RelationshipAttribute.create(
                        parent_object=dj,
                        target_object=di,
                        target_axis=[0,1],
                        axis=[0,1],
                        relationship_type='order',
                        description='Relationship describing that times and electrodes  are ordered in the same way',
                        attribute='order_time_eid_' + dj.name + "_" + di.name)

        # Relate all polytrodes with all polytrodes
        if len(poly_ephys) > 1:
            for i in range(len(poly_ephys)-1):
                for j in range(i+1, len(poly_ephys)):
                    di = poly_ephys[i].ephys_data()
                    dj = poly_ephys[j].ephys_data()
                    RelationshipAttribute.create(
                        parent_object=di,
                        target_object=dj,
                        target_axis=[0,1],
                        axis=[0,1],
                        relationship_type='order',
                        description='Relationship describing that times and electrodes  are ordered in the same way',
                        attribute='order_time_eid_' + di.name + "_" + dj.name)
                    RelationshipAttribute.create(
                        parent_object=dj,
                        target_object=di,
                        target_axis=[0,1],
                        axis=[0,1],
                        relationship_type='order',
                        description='Relationship describing that times and electrodes  are ordered in the same way',
                        attribute='order_time_eid_' + dj.name + "_" + di.name)

        # Relate all grids with all polytrodes
        for g in grid_ephys:
            for p in poly_ephys:
                di = g.ephys_data()
                dj = p.ephys_data()
                RelationshipAttribute.create(
                        parent_object=di,
                        target_object=dj,
                        target_axis=1,
                        axis=1,
                        relationship_type='order',
                        description='Relationship describing that time is ordered in the same way',
                        attribute='order_time_' + di.name + "_" + dj.name)
                RelationshipAttribute.create(
                    parent_object=dj,
                    target_object=di,
                    target_axis=0,
                    axis=0,
                    relationship_type='order',
                    description='Relationship describing that times are  ordered in the same way',
                    attribute='order_time_' + dj.name + "_" + di.name)

    def relate_raw_data_electrode_ids(self, grid_ephys, poly_ephys):
        """
        Relate all electrode id axes of the grid and poly dataset via order and shared ascending encoding relationships

        4. all electrode axes and electrode_id scales of all grid datasets + shared_encoding relationships
        5. all electrode axes and electrode_id scales of all polytrode datasets + shared_encoding relationships

        :param grid_ephys: The list of grid ephys datasets
        :param poly_ephys: The list of poly ephys datasets

        :return:
        """
        logger.info("Relating raw data electrode ids")
        # relate all electrode axes and electrode_id scales of all grid datasets via order (and shared_encoding relationships)
        if len(grid_ephys) > 1:
            for i in range(len(grid_ephys)-1):
                for j in range(i+1, len(grid_ephys)):
                    di = grid_ephys[i].ephys_data()
                    dj = grid_ephys[j].ephys_data()
                    di_eid = grid_ephys[i]['electrode_id']
                    dj_eid = grid_ephys[j]['electrode_id']
                    # Relate the electrode_id of two datasets via order relationships
                    RelationshipAttribute.create(
                        parent_object=di_eid,
                        target_object=dj_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='order',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + di_eid.name + "_" + dj_eid.name)
                    RelationshipAttribute.create(
                        parent_object=dj_eid,
                        target_object=di_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='order',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + dj_eid.name + "_" + di_eid.name)
                    # Relate the electrode_id of two datasets via shared encoding relationships
                    RelationshipAttribute.create(
                        parent_object=di_eid,
                        target_object=dj_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='shared_encoding',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + di_eid.name + "_" + dj_eid.name)
                    RelationshipAttribute.create(
                        parent_object=dj_eid,
                        target_object=di_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='shared_encoding',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + dj_eid.name + "_" + di_eid.name)

        # relate all electrode axes and electrode_id scales of all polytrode datasets via order (and shared_encoding relationships)
        if len(poly_ephys) > 1:
            for i in range(len(poly_ephys)-1):
                for j in range(i+1, len(poly_ephys)):
                    di = poly_ephys[i].ephys_data()
                    dj = poly_ephys[j].ephys_data()
                    di_eid = poly_ephys[i]['electrode_id']
                    dj_eid = poly_ephys[j]['electrode_id']
                    # Relate the electrode_id of two datasets via order relationships
                    RelationshipAttribute.create(
                        parent_object=di_eid,
                        target_object=dj_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='order',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + di_eid.name + "_" + dj_eid.name)
                    RelationshipAttribute.create(
                        parent_object=dj_eid,
                        target_object=di_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='order',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + dj_eid.name + "_" + di_eid.name)
                    # Relate the electrode_id of two datasets via shared encoding relationships
                    RelationshipAttribute.create(
                        parent_object=di_eid,
                        target_object=dj_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='shared_encoding',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + di_eid.name + "_" + dj_eid.name)
                    RelationshipAttribute.create(
                        parent_object=dj_eid,
                        target_object=di_eid,
                        target_axis=0,
                        axis=0,
                        relationship_type='shared_encoding',
                        description='Relationship describing that electrodes are ordered in the same way',
                        attribute='order_eid_' + dj_eid.name + "_" + di_eid.name)

    def relate_layout_index_with_raw_and_processed_data(self, ephys_data, ephys_instrument):
        """
        For a particular instrument, relate the raw and processed data to the layout index of the given instrument

        1) indexes relationship to axis=0 of the ephys data (since we know that electrode id's are linearly ordered here)
        2) indexes relationship to the electrode-id dataset

        :param ephys_data: List of BrainDataEphys and BrainDataEphysProcessed objects
        :param ephys_instrument: The BrainDataStaticDescriptorInstrument object with the layout used to record the data
        """
        for ed in ephys_data:
            d1 = ephys_instrument['layout_index']
            d2 = ed.ephys_data()
            d3 = ed['electrode_id'] if 'electrode_id' in ed.keys() else ed['spatial_id']
            RelationshipAttribute.create(attribute='indexes_'+  d1.name + "_" + d2.name,
                                         parent_object=d1,
                                         target_object=d2,
                                         axis=None,
                                         target_axis=0,
                                         description='The location index describes a 0-based index into the electrodes of the ephys dataset',
                                         relationship_type='indexes')
            RelationshipAttribute.create(attribute='indexes_'+  d1.name + "_" + d3.name,
                                         parent_object=d1,
                                         target_object=d3,
                                         axis=None,
                                         target_axis=0,
                                         description='The location index describes a 0-based index into the electrodes of the ephys dataset',
                                         relationship_type='indexes')
            RelationshipAttribute.create(attribute='user_'+  d2.name + "_" + d1.name,
                                         parent_object=d2,
                                         target_object=d1,
                                         axis=None,
                                         target_axis=None,
                                         description='The traget location index describes a 0-based index into the electrodes of the ephys dataset',
                                         relationship_type='user')
            RelationshipAttribute.create(attribute='user_'+  d3.name + "_" + d1.name,
                                         parent_object=d3,
                                         target_object=d1,
                                         axis=None,
                                         target_axis=None,
                                         description='The traget location index describes a 0-based index into the electrodes of the ephys dataset',
                                         relationship_type='user')


    def relate_grid_locations_with_grid_data_via_imr(self, ephys_grid_instrument, grid_ephys, grid_ephys_processed):
        """
        Create the index map relationship between the channel locations of the grid with the raw and processed grid data

        :param ephys_grid_instrument: The BrainDataStaticDescriptorInstrument object with the data about the instrument
        :param grid_ephys: List of BrainDataEphys objects with the raw grid data
        :param grid_ephys_processed: List of BrainDataEphysProcessed objects with the processed grid data
        """
        logger.info("Relating grid locations via imr")
        if ephys_grid_instrument['layout_locations'] is not None and ephys_grid_instrument['layout_index'] is not None:
            ephys_grid_data = grid_ephys + grid_ephys_processed
            for ed in ephys_grid_data:
                RelationshipAttribute.create_index_map_relationship(
                    name="grid_locations_to " + ed.ephys_data().name,
                    map_object=ephys_grid_instrument['layout_index'],
                    source_object=ephys_grid_instrument['layout_locations'],
                    target_object=ed.ephys_data(),
                    source_axis=None,
                    target_axis=0,
                    map_indexing_axis=None,
                    map_stack_axis=None,
                    user_description=None, # Do not create a user relationship
                    user_properties=None,  # Do not create a user relationship
                    mapping_properties=None)
        else:
            logger.warning('ephys grid dataset does not appear to have a locations dataset for creating the index map relationship')

    def related_processed_and_raw_time_axes_by_shared_ascending_encoding(self,
                                                                         grid_ephys,
                                                                         poly_ephys,
                                                                         grid_ephys_processed,
                                                                         poly_ephys_processed):
        """
        Create the shared_ascending_encoding relationships between the raw and the processed time axes

        :param grid_ephys: List of BrainDataEphys object with the raw ephys grid data
        :param poly_ephys: List of BrainDataEphys object with the raw ephys polytrode data
        :param grid_ephys_processed: List of BrainDataEphysProcessed object with the processed ephys grid data
        :param poly_ephys_processed: List of BrainDataEphysProcessed object with the processed ephys polytrode data
        """
        logger.info("Relating processed and raw time access by shared ascending endcoding")
        ephys_raw = grid_ephys + poly_ephys
        ephys_processed = grid_ephys_processed + poly_ephys_processed
        for ep in ephys_processed:
            for er in ephys_raw:
                RelationshipAttribute.create(
                    parent_object=ep['time_axis'],
                    target_object=er['time_axis'],
                    target_axis=0,
                    axis=0,
                    relationship_type='shared_ascending_encoding',
                    description='Secondary relationship describing that the time axis between processed and raw data is related by shared ascending encoding, i.e., axes are aligned despite different sample rates. ',
                    attribute='shared_ascending_encoding_' + ep['time_axis'].name + '_' + er['time_axis'].name)
                RelationshipAttribute.create(
                    parent_object=er['time_axis'],
                    target_object=ep['time_axis'],
                    target_axis=0,
                    axis=0,
                    relationship_type='shared_ascending_encoding',
                    description='Secondary relationship describing that the time axis between processed and raw data is related by shared ascending encoding, i.e., axes are aligned despite different sample rates. ',
                    attribute='shared_ascending_encoding_' + er['time_axis'].name + '_' + ep['time_axis'].name)

    def relate_grid_location_and_index_via_order(self, ephys_grid_instrument):
        """
        Relate the location and index datasets for the ephys grid via corresponding order relationships

        :param ephys_grid_instrument: The BrainDataDescriptorsStaticInstrument object for the grid data
        :return:
        """
        logger.info("Relating grid location and index via order")
        loc = ephys_grid_instrument['layout_locations']
        ind = ephys_grid_instrument['layout_index']
        RelationshipAttribute.create(
            parent_object=loc,
            target_object=ind,
            target_axis=[0,1],
            axis=[0,1],
            relationship_type='order',
            description='The layout and index are stored in the same principle structure. ',
            attribute='order_' + loc.name + '_' + ind.name)
        RelationshipAttribute.create(
            parent_object=ind,
            target_object=loc,
            target_axis=[0,1],
            axis=[0,1],
            relationship_type='order',
            description='The layout and index are stored in the same principle structure. ',
            attribute='order_' + ind.name + '_' + loc.name)

    def relate_raw_data_with_static_metadata(selg,
                                             ephys_grid_instrument,
                                             ephys_poly_instrument,
                                             stimulus,
                                             surgical_notes,
                                             grid_ephys,
                                             poly_ephys):
        """
        Create user relationships between raw data and the corresponding stimulus, surgical notes, and instrument metadata

        :param ephys_grid_instrument: BrainDataStaticDescriptorsInstrument object for the ephys grid
        :param ephys_poly_instrument:  BrainDataStaticDescriptorsInstrument object for the ephys polytrode
        :param stimulus:   BrainDataStaticDescriptorsStimulus object for the ephys grid and polytrode data
        :param surgical_notes: BrainDataMetadataGroup with the surgical notes
        :param grid_ephys: List of BrainDataEphys and BrainDataCollection objects with the raw ephys Grid data
        :param poly_ephys: List of BrainDataEphys and BrainDataCollection objects with the raw ephys Polytrode data
        """
        logger.info("Relating raw data with static metadata")
        ephys_data = grid_ephys + poly_ephys
        for ed in ephys_data:
            ed_obj = ed.hdf_object
            i_obj = ephys_grid_instrument.hdf_object if ed in grid_ephys else ephys_poly_instrument.hdf_object
            s_obj = stimulus.hdf_object
            sn_obj = surgical_notes.hdf_object
            # Relate the EcoG data with the instrument
            RelationshipAttribute.create(
                parent_object=ed_obj,
                target_object=i_obj,
                relationship_type='user',
                description='Metadata about the instrument used for recording the ephys dataset',
                attribute='user_instrument_' + ed_obj.name + '_' + i_obj.name)
            # Relate the instrument and the grid
            RelationshipAttribute.create(
                parent_object=i_obj,
                target_object=ed_obj,
                relationship_type='user',
                description='ephys dataset recorded with this instrument desgin',
                attribute='user_recording_' + i_obj.name + '_' + ed_obj.name)
            # Relate the ephys data with the stimulus
            RelationshipAttribute.create(
                parent_object=ed_obj,
                target_object=s_obj,
                relationship_type='user',
                description='Metadata about the stimulus used for recording the ephys dataset',
                attribute='user_stimulus_' + ed_obj.name + '_' + s_obj.name)
            # Relate the stimulus with the ephys data
            RelationshipAttribute.create(
                parent_object=s_obj,
                target_object=ed_obj,
                relationship_type='user',
                description='ephys dataset recorded with the stimulus design',
                attribute='user_recording_' + s_obj.name + '_' + ed_obj.name)
            # Relate eqphys data with the surgical notes
            RelationshipAttribute.create(
                parent_object=ed_obj,
                target_object=sn_obj,
                relationship_type='user',
                description='Metadata about the surgical details for the ephys dataset',
                attribute='user_surgical_notes_' + ed_obj.name + '_' + sn_obj.name)
            # Relate the surgical notes with the ephys data
            RelationshipAttribute.create(
                parent_object=sn_obj,
                target_object=ed_obj,
                relationship_type='user',
                description='ephys dataset recorded with these surgical details',
                attribute='user_surgical_notes' + sn_obj.name + '_' + ed_obj.name)

    def related_processed_and_raw_by_user(self,
                                          raw_ephys,
                                          processed_ephys,
                                          processed_notes):
        """
        Create user relationship between the processed data and the raw data
        :param raw_ephys: List of BrainDataEphys object of all raw ephys grid data
        :param processed_ephys List of BrainDataEphysProcessed object of all processed ephys grid data
        :param processed_notes: List of strings describing how the processed data was derived from the raw data (or None)

        """
        logger.info("Relating processed and raw data by user relationships")
        for index, gr in enumerate(raw_ephys):
            for gp in processed_ephys:
                gr_d = gr.hdf_object
                gp_d = gp.hdf_object
                RelationshipAttribute.create(
                    parent_object=gp_d,
                    target_object=gr_d,
                    relationship_type='user',
                    description='Processed data derived from',
                    attribute='user_'+gr_d.name + '_' + gp_d.name,
                    properties=None if processed_notes is None else {'processing_notes': processed_notes[index]}
                )
                RelationshipAttribute.create(
                    parent_object=gr_d,
                    target_object=gp_d,
                    relationship_type='user',
                    description='Processed data derived from',
                    attribute='user_'+gp_d.name + '_' + gr_d.name,
                    properties=None if processed_notes is None else {'processing_notes': processed_notes[index]}
                )


    def relate_processed_and_raw_axes(self, raw_ephys, processed_ephys):
        """
        Create order and shared encoding relationships between the raw and processed data dimensions and axes

        :param raw_ephys: List of BrainDataEphys object of all raw ephys grid data
        :param processed_ephys List of BrainDataEphysProcessed object of all processed ephys grid data
        :return:
        """
        logger.info("Relate processed and raw axes")
        for gr in raw_ephys:
            for gp in processed_ephys:
                re_d = gr.ephys_data()
                re_t = gr['time_axis']
                re_eid = gr['electrode_id']
                pe_d = gp.ephys_data()
                pe_t = gp['time_axis']
                pe_eid = gp['spatial_id']
                # Relate ephys data by the electrode axis
                RelationshipAttribute.create(
                    parent_object=re_d,
                    target_object=pe_d,
                    target_axis=0,
                    axis=0,
                    description='Same electrode layout used',
                    relationship_type='order',
                    attribute='order_'+re_d.name + '_' + pe_d.name
                )
                RelationshipAttribute.create(
                    parent_object=pe_d,
                    target_object=re_d,
                    target_axis=0,
                    axis=0,
                    description='Same electrode layout used',
                    relationship_type='order',
                    attribute='order_'+pe_d.name + '_' + re_d.name
                )
                # Relate the electrode_id dimension scales via shared_encoding relationships
                RelationshipAttribute.create(
                    parent_object=re_eid,
                    target_object=pe_eid,
                    target_axis=0,
                    axis=0,
                    description='Same electrode layout used',
                    relationship_type='shared_encoding',
                    attribute='shared_encoding_'+re_eid.name + '_' + pe_eid.name
                )
                RelationshipAttribute.create(
                    parent_object=re_eid,
                    target_object=pe_eid,
                    target_axis=0,
                    axis=0,
                    description='Same electrode layout used',
                    relationship_type='order',
                    attribute='order_'+re_eid.name + '_' + pe_eid.name
                )
                RelationshipAttribute.create(
                    parent_object=pe_eid,
                    target_object=re_eid,
                    target_axis=0,
                    axis=0,
                    description='Same electrode layout used',
                    relationship_type='shared_encoding',
                    attribute='shared_encoding_'+pe_eid.name + '_' + re_eid.name
                )
                RelationshipAttribute.create(
                    parent_object=pe_eid,
                    target_object=re_eid,
                    target_axis=0,
                    axis=0,
                    description='Same electrode layout used',
                    relationship_type='order',
                    attribute='order_'+pe_eid.name + '_' + re_eid.name
                )
                # Relate the time axes of the datasets via shared_ascending_encoding relationships
                RelationshipAttribute.create(
                    parent_object=re_t,
                    target_object=pe_t,
                    target_axis=0,
                    axis=0,
                    description='Same timing but different sampling rates used',
                    relationship_type='shared_ascending_encoding',
                    attribute='shared_ascending_encoding_'+re_t.name + '_' + pe_t.name
                )
                RelationshipAttribute.create(
                    parent_object=pe_t,
                    target_object=re_t,
                    target_axis=0,
                    axis=0,
                    description='Same timing but different sampling rates used',
                    relationship_type='shared_ascending_encoding',
                    attribute='shared_ascending_encoding_'+pe_t.name + '_' + re_t.name
                )

    def relate_processed_axes(self, grid_ephys_processed, poly_ephys_processed):
        """
        Define the following relationships between the axes and dimensions of the processed data
        1. Relate grid and polytrode processed data by order along axes ([0,1], [0,1])
        2. Relate time axes of all processed data via order and shared ascending encoding relationship
        3. Relate spatial_id data of each of all the grid and polytrode processed data via order and shared encoding relationships

        :param grid_ephys_processed: List of BrainDataEphysProcessed object from the grid data
        :param poly_ephys_processed: List of BrainDataEphysProcessed objects from the polytrode data
        :return:
        """
        logger.info("Relating processed axes")
        # Relate grid and polytrode processed data by order along axes ([0,1], [0,1])
        for processed_set in (grid_ephys_processed, poly_ephys_processed):
            if len(processed_set) > 1:
                for i in range(len(processed_set)-1):
                    for j in range(i+1, len(processed_set)):
                        di = processed_set[i].ephys_data()
                        dj = processed_set[j].ephys_data()
                        RelationshipAttribute.create(
                            parent_object=di,
                            target_object=dj,
                            target_axis=[0,1],
                            axis=[0,1],
                            relationship_type='order',
                            description='Relationship describing that times and electrodes  are ordered in the same way',
                            attribute='order_time_eid_' + di.name + "_" + dj.name)
                        RelationshipAttribute.create(
                            parent_object=dj,
                            target_object=di,
                            target_axis=[0,1],
                            axis=[0,1],
                            relationship_type='order',
                            description='Relationship describing that times and electrodes  are ordered in the same way',
                            attribute='order_time_eid_' + dj.name + "_" + di.name)

        # Relate time axes of all processed data via order and shared ascending encoding relationship
        processed_ephys = grid_ephys_processed + poly_ephys_processed
        if len(processed_ephys) > 1:
            for i in range(len(processed_ephys)-1):
                for j in range(i+1, len(processed_ephys)):
                    di = processed_ephys[i]['time_axis']
                    dj = processed_ephys[j]['time_axis']
                    RelationshipAttribute.create(
                        parent_object=di,
                        target_object=dj,
                        target_axis=0,
                        axis=0,
                        relationship_type='order',
                        description='Relationship describing that times are ordered in the same way',
                        attribute='order_' + di.name + "_" + dj.name)
                    RelationshipAttribute.create(
                        parent_object=di,
                        target_object=dj,
                        target_axis=0,
                        axis=0,
                        relationship_type='shared_ascending_encoding',
                        description='Relationship describing that times have the same shared ascending encoding',
                        attribute='shared_ascending_encoding_' + di.name + "_" + dj.name)
                    RelationshipAttribute.create(
                        parent_object=dj,
                        target_object=di,
                        target_axis=0,
                        axis=0,
                        relationship_type='order',
                        description='Relationship describing that times are ordered in the same way',
                        attribute='order_' + dj.name + "_" + di.name)
                    RelationshipAttribute.create(
                        parent_object=dj,
                        target_object=di,
                        target_axis=0,
                        axis=0,
                        relationship_type='shared_ascending_encoding',
                        description='Relationship describing that times have the same shared ascending encoding',
                        attribute='shared_ascending_encoding_' + dj.name + "_" + di.name)

        # Relate spatial_id data of each of all the grid and polytrode processed data via order and
        # shared encoding relationships
        for processed_set in (grid_ephys_processed, poly_ephys_processed):
            if len(processed_set) > 1:
                for i in range(len(processed_set)-1):
                    for j in range(i+1, len(processed_set)):
                        di = processed_ephys[i]['spatial_id']
                        dj = processed_ephys[j]['spatial_id']
                        RelationshipAttribute.create(
                            parent_object=di,
                            target_object=dj,
                            target_axis=0,
                            axis=0,
                            relationship_type='order',
                            description='Relationship describing that electrodes are ordered in the same way',
                            attribute='order_' + di.name + "_" + dj.name)
                        RelationshipAttribute.create(
                            parent_object=di,
                            target_object=dj,
                            target_axis=0,
                            axis=0,
                            relationship_type='shared_encoding',
                            description='Relationship describing that electrodes ids have the same shared encoding',
                            attribute='shared_ascending_encoding_' + di.name + "_" + dj.name)
                        RelationshipAttribute.create(
                            parent_object=dj,
                            target_object=di,
                            target_axis=0,
                            axis=0,
                            relationship_type='order',
                            description='Relationship describing that electrodes are ordered in the same way',
                            attribute='order_' + dj.name + "_" + di.name)
                        RelationshipAttribute.create(
                            parent_object=dj,
                            target_object=di,
                            target_axis=0,
                            axis=0,
                            relationship_type='shared_encoding',
                            description='Relationship describing that electrodes ids have the same shared encoding',
                            attribute='shared_ascending_encoding_' + dj.name + "_" + di.name)


    def convert(self):
        """
        Convert the data to BrainFormat

        :return: The BrainDataFile object created (or None)
        """
        logger.info("Convert in progress...")
        outfile = self.create_output_file()

        # Get the base groups from the file
        internal_grp = None if outfile is None else outfile.data().internal()
        static_descriptors_grp = None if outfile is None else outfile.descriptors().static()

        # Write the ephys grid and polytrode instrument metadata
        ephys_grid_instrument, ephys_poly_instrument = self.write_instrument_metadata(outfile=outfile)

        # Write the stimulus metadata
        stimulus, stimulus_signal_obj, stimulus_trigger_obj, \
        stimulus_audio_obj, stimulus_trigger_mark_obj, \
        stimulus_frequency_obj, stimulus_amplitude_obj, \
        stimulus_trigger_mark_processed_obj, stimulus_frequency_processed_obj, \
        stimulus_amplitude_processed_obj    = \
            self.write_stimulus_metadata(outfile=outfile)

        # TODO relate the processed stimulus with the processed data

        # Write the surgical notes
        surgical_notes = self.write_surgical_notes(outfile=outfile)

        # Create collection group targets
        ephys_grid_collection, ephys_poly_collection = self.create_collection_objects(outfile=outfile)


        # Convert the raw ephys data and relate it to the time-aligned stimulus
        grid_ephys, poly_ephys = self.write_raw_data(
            outfile=outfile,
            ephys_grid_collection=ephys_grid_collection,
            ephys_poly_collection=ephys_poly_collection,
            ephys_grid_instrument=ephys_grid_instrument,
            ephys_poly_instrument=ephys_poly_instrument,
            stimulus_dimensions=[stimulus_frequency_obj.hdf_object,
                                 stimulus_amplitude_obj.hdf_object,
                                 stimulus_trigger_mark_obj.hdf_object])

        # Convert the processed ephys data and relate it to the time-aligned processed stimulus
        grid_ephys_processed, poly_ephys_processed = self.write_processed_data(
            outfile=outfile,
            ephys_grid_collection=ephys_grid_collection,
            ephys_poly_collection=ephys_poly_collection,
            ephys_grid_instrument=ephys_grid_instrument,
            ephys_poly_instrument=ephys_poly_instrument,
            stimulus_dimensions=[stimulus_frequency_processed_obj.hdf_object,
                                 stimulus_amplitude_processed_obj.hdf_object,
                                 stimulus_trigger_mark_processed_obj.hdf_object])

        ##########################################
        # Create the additional relationships    #
        ##########################################
        # Create order relationships between:
        #  1. all pairwise combinations of the time-axis dimensions scales of the raw grid and polytrode datasets
        #  2. all dimensions scales and the time axis of the oposing raw dataset
        #  3. all time axes of the raw datasets themself
        # Create shared_ascending_encoding relationship between:
        #  1. all pairwise combindations of the time only dimensions scale of the raw grid and polytrode datasets
        self.relate_raw_data_time_scales(grid_ephys=grid_ephys, poly_ephys=poly_ephys)

        #Relate all electrode id axes of the grid and poly dataset via order and shared ascending encoding relationships
        # 4. all electrode axes and electrode_id scales of all grid datasets + shared_encoding relationships
        # 5. all electrode axes and electrode_id scales of all polytrode datasets + shared_encoding relationships
        self.relate_raw_data_electrode_ids(grid_ephys=grid_ephys, poly_ephys=poly_ephys)

        # Relate all raw ephys datasets by order relationships along their dimensions
        self.relate_raw_data_blocks_by_order(grid_ephys=grid_ephys, poly_ephys=poly_ephys)

        # Create the index map relationship between the channel locations of the grid with the raw and processed grid data
        self.relate_grid_locations_with_grid_data_via_imr(
            ephys_grid_instrument=ephys_grid_instrument,
            grid_ephys=grid_ephys,
            grid_ephys_processed=grid_ephys_processed)

        # Create the shared_ascending_encoding relationships between the raw and the processed time axes
        self.related_processed_and_raw_time_axes_by_shared_ascending_encoding(grid_ephys=grid_ephys,
                                                                              poly_ephys=poly_ephys,
                                                                              grid_ephys_processed=grid_ephys_processed,
                                                                              poly_ephys_processed=poly_ephys_processed)

        # Create the order relationships between thegrid locations and layout datasets for the ephys grid instrument
        self.relate_grid_location_and_index_via_order(ephys_grid_instrument)

        # Create the indexes relationship between the layout index of the grid and raw and processed grid data (and user relationships in the oposite direction)
        self.relate_layout_index_with_raw_and_processed_data(ephys_data=(grid_ephys+grid_ephys_processed),
                                                             ephys_instrument=ephys_grid_instrument)

        # Create the indexes relationship between the layout index of the polytrode and raw and processed polytrode data (and user relationships in the oposite direction)
        self.relate_layout_index_with_raw_and_processed_data(ephys_data=(poly_ephys+poly_ephys_processed),
                                                             ephys_instrument=ephys_poly_instrument)

        # Create user relationships between raw data and the corresponding stimulus, surgical notes, and instrument metadata
        self.relate_raw_data_with_static_metadata(ephys_grid_instrument=ephys_grid_instrument,
                                                  ephys_poly_instrument=ephys_poly_instrument,
                                                  stimulus=stimulus,
                                                  surgical_notes=surgical_notes,
                                                  grid_ephys=(grid_ephys + [ephys_grid_collection,]),
                                                  poly_ephys=(poly_ephys + [ephys_poly_collection,]))

        # Create user relationship between the processed data and the raw data
        self.related_processed_and_raw_by_user(raw_ephys=grid_ephys,
                                               processed_ephys=grid_ephys_processed,
                                               processed_notes=self.processed_notes)
        self.related_processed_and_raw_by_user(raw_ephys=poly_ephys,
                                               processed_ephys=poly_ephys_processed,
                                               processed_notes=self.processed_notes)

        # Create order and shared encoding relationships between the raw and processed data dimensions and axes
        self.relate_processed_and_raw_axes(raw_ephys=grid_ephys,
                                           processed_ephys=grid_ephys_processed)
        self.relate_processed_and_raw_axes(raw_ephys=poly_ephys,
                                           processed_ephys=poly_ephys_processed)

        # Create order and shared ascending encoding relationships between the dimensions of the processed datasets.
        # Define the following relationships:
        # 1. Relate grid and polytrode processed data by order along axes ([0,1], [0,1])
        # 2. Relate time axes of all processed data via order and shared ascending encoding relationship
        # 3. Relate spatial_id data of each of all the grid and polytrode processed data via order and shared encoding relationships
        self.relate_processed_axes(grid_ephys_processed=grid_ephys_processed,
                                   poly_ephys_processed=poly_ephys_processed)

        # Create user relationship between the polytrode and the ephys grid describing how the two are related
        self.relate_grid_and_polytrode_instruments(ephys_grid_instrument=ephys_grid_instrument,
                                                   ephys_poly_instrument=ephys_poly_instrument)

        # Return the output file we generated
        return outfile


    @classmethod
    def parse_arguments(cls):
        """
        :return: Instance of ConvertRatData initalized from the arguments and dict of the arguments
        """
        parser = argparse.ArgumentParser(description="Convert Rat data to BrainFormat",
                                         epilog="\n LBNL all rights reserved.",
                                         formatter_class=RawDescriptionDefaultHelpArgParseFormatter,)
        parser.add_argument('--raw_dirs',
                            action='store',
                            required=False,
                            default=[],
                            help='list of path of directories with raw HTK recordings',
                            nargs='+')
        parser.add_argument('--processed_dirs',
                            action='store',
                            required=False,
                            default=[],
                            help='List of paths of directories with processed HTK recordings',
                            nargs='+')
        parser.add_argument('--processed_notes',
                            action='store',
                            required=False,
                            default=None,
                            help='list of strings with notes about how the processed data was derived from the raw data',
                            nargs='+')
        parser.add_argument('--bands_files',
                            action='store',
                            required=False,
                            default=[],
                            help='List of paths to files with the frequency bands from the processsing. ' +
                                 'Must have the same length as processed_dirs. Use "guess" to indicate that ' +
                                 'we should guess the frequency bands based on the filename. Use "None" to ' +
                                 'indicate that no bands should be used for the channel. Provide an explicit list' +
                                 'of values as a pyton literal string to set the bands explicitly.',
                            nargs='+')
        parser.add_argument('--stimulus_type',
                            action='store',
                            required=False,
                            default="undefined",
                            help='String describing the type of stimulus')
        parser.add_argument('--stimulus_signal',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the WAV file with the stimulus signal')
        parser.add_argument('--stimulus_amplitude',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the HTK file with the time-aligned stimulus amplitude')
        parser.add_argument('--stimulus_frequency',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the HTK file with the time-aligned stimulus frequency')
        parser.add_argument('--stimulus_trigger',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the WAV file with the stimulus trigger')
        parser.add_argument('--stimulus_audio',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the WAV file with the stimulus audio')
        parser.add_argument('--stimulus_trigger_mark',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the WAV file with the stimulus trigger marker')
        parser.add_argument('--stimulus_trigger_mark_processed',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the MAT file with the stimulus trigger marker for processed data')
        parser.add_argument('--stimulus_amplitude_processed',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the MAT file with the time-aligned stimulus amplitude for processed data')
        parser.add_argument('--stimulus_frequency_processed',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the MAT file with the time-aligned stimulus frequency for processed data')
        parser.add_argument('--surgical_notes_dir',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the directory with the surgical notes')
        parser.add_argument('--grid_geometry_image',
                            action='store',
                            required=False,
                            default=None,
                            help='Path to the file with the image of teh geometry of the chip')
        parser.add_argument('--grid_orientation',
                            action='store',
                            required=False,
                            default='R',
                            choices=('R', 'S'),
                            help='R or S depending describing the orientation of the chip')
        parser.add_argument('--grid_xspacing',
                            type=float,
                            action='store',
                            required=False,
                            default=1.0,
                            help='Floating value indicating the spacing in x between electrodes in the grid')
        parser.add_argument('--grid_yspacing',
                            type=float,
                            action='store',
                            required=False,
                            default=1.0,
                            help='Floating value indicating the spacing in y between electrodes in the grid')
        parser.add_argument('--polytrode_ncols',
                            type=int,
                            action='store',
                            required=False,
                            default=2,
                            choices=(2, 3),
                            help='Number of columns of the polytrode')
        parser.add_argument('--output',
                            action='store',
                            required=False,
                            default=None,
                            help='The output HDF5 file. Leave empty to perform dry run of data read only')
        curr_args = vars(parser.parse_args())
        return ConvertRatData(**curr_args)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.captureWarnings(True)
    crd = ConvertRatData.parse_arguments()
    crd.log_settings()
    outfile = crd.convert()
    if outfile:
        filestats = outfile.file_stats()
        logger.info('Number of datasets: ' + str(len(filestats['datasets'])))
        logger.info('Number of groups: ' + str(len(filestats['groups'])))
        logger.info('Number of managed objects: ' + str(len(filestats['managed'])))
        logger.info('Number of regular attributes: ' + str(len(filestats['attributes'])))
        logger.info('Number of relationship attributes: ' + str(len(filestats['relationships'])))


