#!/bin/bash
set -o verbose

python convert_rat_data.py \
           --raw_dirs '/Volumes/Data Drive/brain/R18_B12/RawHTK' \
           --stimulus_trigger_mark '/Volumes/Data Drive/brain/R18_B12/mrk11.htk' \
           --stimulus_type "Tone" \
           --stimulus_amplitude '/Volumes/Data Drive/brain/R18_B12/stimA.htk' \
           --stimulus_frequency '/Volumes/Data Drive/brain/R18_B12/stimF.htk' \
           --stimulus_signal '/Volumes/Data Drive/brain/Stimulus/stimulus_signal_03202013.wav' \
           --stimulus_audio '/Volumes/Data Drive/brain/R18_B12/aud11.htk' \
           --stimulus_trigger '/Volumes/Data Drive/brain/Stimulus/stimulus_trigger_03202013.wav' \
           --surgical_notes_dir '/Volumes/Data Drive/brain/SurgicalNotes/RAT18' \
           --grid_geometry_image '/Volumes/Data Drive/brain/Geometry/64ch_layout.png'\
           --grid_orientation R \
           --processed_dirs '/Volumes/Data Drive/brain/R18_B12/Wvlt_4to1200_54band_CAR1' '/Volumes/Data Drive/brain/R18_B12/Wvlt_70to170_1band' '/Volumes/Data Drive/brain/R18_B12/MUA' \
           --bands_files '/Volumes/Data Drive/brain/cfs.4_1200.54Wvl.mat' guess '[1200,]' \
           --processed_notes 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 54 different frequencies logarithmically spaced between 4 and 1200Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 1 frequency logarithmically spaced between 70 and 170Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' 'Preprocessed dataset where the raw voltage signal is decomposed using a Wavelet transformation to isolate the activity of 1 frequency logarithmically spaced between 70 and 170Hz. This dataset is also downsampled to 3200Hz. The Common Average Reference was calculated and subtracted from each channel. This is an important indication because it affects what type of analysis can be performed.' \
           --output '/Volumes/Data Drive/brain_R18_B12_Tone.h5'

