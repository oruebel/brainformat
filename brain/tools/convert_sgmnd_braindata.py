"""
Simple tool used to convert .mat files with segmented neural recordings data to HDF5.

Description of the contents of the .mat files:
----------------------------------------------

Baseline-related data:
^^^^^^^^^^^^^^^^^^^^^^

*meta/baslineMethod:* The method by which the baseline as generated. Shape= (1, numTokens) dtype=String

*meta/baselineTime:* Start-stop range in time in seconds the data was pulled from for base-line correction.
Shape = (1, numTokens), dtype=Struct of two ints. Store as two separate dimension scales for
baseline_start_time and baseline_stop_time and convert to ms

*meta/baselineBlock:* String indicating the block the baseline came from. Shape = (1, numTokens), dtype=String

*meta/baselineSTD:* Array of floats describing the standard deviation for the baseline of each channel.
 Shape = [Array of (2580 x 1) or arrays of (numElectrodes x 1] , dtype=Float. In Matlab these are stored
 as HDF5 region references that we need to resolve.

*meta/baselineMu:* Array of floats describing the mean for the baseline of each channel.
 Shape = [Array of (2580 x 1) or arrays of (numElectrodes x 1] , dtype=Float. In Matlab these are stored
 as HDF5 region references that we need to resolve.


*meta/baseline:* 2D Array of floats describing the mean and standard devication for the baseline.
Shape = (1, numTokens), dtype=Float. Possibly store mean as seperate arrays, one for the mean,
one for the standard deviation. This has been split into two arrays

    * meta/baselineSTD
    * meta/baselineMu

Token related data:
^^^^^^^^^^^^^^^^^^^

*meta/labs:* Integer indicating the ID of the token. Shape = (1, numTokens) , dtype=uint  (references wlist)

*meta/notes:* String indicating the name of the token. Shape = (1, numTokens), dtype=string

*meta/wlist:* List of available token strings. Shape = (1, numTokenTypes), dtype=string

*meta/OnsetTime:* Ignore. Specific to data structures used during data processing only

*meta/OffsetTime:* Ignore. Specific to data structures used during data processing only

*meta/AbsStart:* Actual time in ms float when the token started in the raw data. Shape = (1, numTokens), dtype=string.
Raw time is calculated by multiplying the integer index of the sample with the sampling rate. Need to convert
to nano seconds to match the raw data specification.

*meta/AbsStop:* Actual time in ms float when the token stopped in the raw data. Shape = (1, numTokens), dtype=string.
Raw time is calculated by multiplying the integer index of the sample with the sampling rate. In order to interpret
the times, we need to know which block the times came from. Need to convert to ns to match the raw data specification.

*meta/block:* The name of the block/file the token came from. This is required to interpret the start
and stop times. Shape = (1, numTokens), dtype=string.

*meta/evntTrial:* refers to an intermediary data structure between the raw data and the files you're working with.
You can disregard it.

Other data metadata:
^^^^^^^^^^^^^^^^^^^^

*fs* = Sampling rate in Hz (single integer)

*spatialID:* String dataset describing the ID of the electrode. Shape = {1, numElectroded} ,
dtype=string (This replaces the electrode_id dataset)

Raw Ephys data:
^^^^^^^^^^^^^^
*neuralY, audY, rawY etc.:* The floating point arrays with 3 dimensions (space x time x token)

General notes:
^^^^^^^^^^^^^^

- The metadata is always the same, but not everyone is going to have all the the metadata \
  (e.g, the baseline may not be there for all)
- We need to either change the unit for the time axis in the BrainFormat to ms or convert the AbsStart \
  and AbsStop from mili to nano-seconds

"""

from brain.dataformat.brainformat import *
from brain.readers.htkcollection import HTKCollection
import numpy as np
import h5py
import argparse
import logging

logger = logging.getLogger(__name__)


def read_mat_string_data(infile, dataset):
    """
    Helper function used to convert 1 x size string datasets to numpy

    :parm dataset: The uncoverted dataset with the HDF5 region references
    """
    dtrans = [infile[dataset[0, l]][:] for l in range(dataset.size)]
    strans = [''] * len(dtrans)
    for i, v in enumerate(dtrans):
        for j in v:
            strans[i] += str(unichr(j))
    return np.asarray(strans)


def convert_segmented_file(input_filename,
                           output_filename,
                           data_units,
                           anatomy_filename=None,):
    """
    Main function
    """
    logging.info('Using data units:' + str(data_units))
    # 1) Open the input data file
    logging.info("Reading input data: " + input_filename)
    infile = h5py.File(input_filename, 'r')

    # 2) Read the main data
    # 2.1) Determine the name of the main dataset
    main_dataname = None
    for dataname in infile.keys():
        if isinstance(infile[dataname], h5py.Dataset) and dataname.endswith('Y'):
            main_dataname = dataname
            break
    if main_dataname is None:
        logging.error('No matching dataname found ending with Y')
    # 2.2) Load and transpose the main data
    main_data = infile[main_dataname][:].transpose()

    # 3) Load the core metadata saved directly upon creation of the BrainFormat processed Ephys dataset
    metadata = infile['meta']
    metadata_keys = metadata.keys()

    # 3.1) Load the names of the tokens
    # the token names are stored as a strange array of object references to unsigned integers
    # we here need to extract and then translate the values back to strings
    token_data = metadata['notes'][:]
    token_names = read_mat_string_data(infile=infile,
                                       dataset=token_data)
    logging.info('Read token names:' + str(token_names))

    # 3.2) Load the ids of the tokens
    token_ids = metadata['labs'][0, :].astype('uint')
    logging.info('Read token ids:' + str(token_ids))

    # 3.3) Load the anatomy data if possible
    anatomy_names = None
    if anatomy_filename is not None:
        logging.info('Loading anatomy data')
        anatomy_dict = HTKCollection.read_anatomy(anatomy_filename)
        anatomy_names = HTKCollection.get_anatomy_map(anatomy_dict, main_data.shape[0])
    else:
        logging.info('No anatomy data specified')

    # 3.4) Determine the layout
    layout = HTKCollection.get_layout(256)
    logging.info('Generated layout data:' + str(layout))

    # 3.5) Determine the sampling rate
    sampling_rate = metadata['fs'][0][0]
    logging.info('Reading sampling rate fs=' + str(sampling_rate))

    # 3.6) Load the electrode identifier
    if 'spatialID' in metadata_keys:
        spatial_ids = read_mat_string_data(infile=infile,
                                           dataset=metadata['spatialID'])
        logging.info('Reading SpatialID=' + str(spatial_ids))
    else:
        spatial_ids = None
        logging.info('No spatial id found')

    # 4) Write the data to file
    logging.info("Writing data to HDF5: " + output_filename)
    outfile = BrainDataFile.create(output_filename)
    internal = outfile.data().internal()
    brain_dataset = BrainDataEphysProcessed.create(parent_object=internal,
                                                  ephys_data=main_data,
                                                  ephys_data_units=data_units,
                                                  layout=layout,
                                                  spatial_ids=spatial_ids,
                                                  sampling_rate=sampling_rate,
                                                  anatomy_names=anatomy_names,
                                                  token_names=token_names,
                                                  token_ids=token_ids)

    # 5) Add dimensions scales for the other additional metadata
    # 5.1) Save baseline method
    if 'baselineMethod' in metadata_keys:
        logging.info('Converting baselineMethod')
        # Load the baseline method data
        baseline_method_description = 'The method by which the baseline as generated'
        baseline_method_axis = 2
        baseline_method_unit = 'method name'
        baseline_method_dset = 'baseline_method'
        baseline_method_scale = read_mat_string_data(infile=infile,
                                                     dataset=metadata['baselineMethod'][:])
        # Save the dimension scale
        brain_dataset.add_dimension_scale(scale=baseline_method_scale,
                                          unit=baseline_method_unit,
                                          axis=baseline_method_axis,
                                          dataset_name=baseline_method_dset,
                                          description=baseline_method_description)
    else:
        logging.info('baselineMethod not found')

    # 5.2) Save the baseline start and stop scales
    if 'baselineTime' in metadata_keys:
        logging.info('Converting baselineTime')
        #  Load the baseline times
        baseline_time_data = metadata['baselineTime'][:]
        #  Load the baseline start time
        baseline_start_description = 'Start time for the data range used for the baseline correction. ' + \
                                     'See also baseline_block for information about the block that is referenced.'
        baseline_start_axis = 2
        baseline_start_unit = 'ms'
        baseline_start_dset = 'baseline_start_time'
        baseline_start_scale = baseline_time_data[0, :] / 1000.  # Convert seconds to ms
        #  Load the baseline stop time
        baseline_stop_description = 'Stop time for the data range used for the baseline correction. ' + \
                                    'See also baseline_block for information about the block that is referenced.'
        baseline_stop_axis = 2
        baseline_stop_unit = 'ms'
        baseline_stop_dset = 'baseline_stop_time'
        baseline_stop_scale = baseline_time_data[1, :] / 1000.  # Convert seconds to ms
        #  Save the baseline start and stop times
        brain_dataset.add_dimension_scale(scale=baseline_start_scale,
                                          unit=baseline_start_unit,
                                          axis=baseline_start_axis,
                                          dataset_name=baseline_start_dset,
                                          description=baseline_start_description)
        brain_dataset.add_dimension_scale(scale=baseline_stop_scale,
                                          unit=baseline_stop_unit,
                                          axis=baseline_stop_axis,
                                          dataset_name=baseline_stop_dset,
                                          description=baseline_stop_description)
    else:
        logging.info('baselineTime not found')

    # 5.3) Save the baseline block scale
    if 'baselineBlock' in metadata_keys:
        logging.info('Converting baselineBlock')
        baseline_block_description = 'Description of the block the baseline came from.'
        baseline_block_axis = 2
        baseline_block_unit = 'block name'
        baseline_block_dset = 'baseline_block_name'
        baseline_block_scale = read_mat_string_data(infile=infile,
                                                    dataset=metadata['baselineBlock'][:])
        brain_dataset.add_dimension_scale(scale=baseline_block_scale,
                                          unit=baseline_block_unit,
                                          axis=baseline_block_axis,
                                          dataset_name=baseline_block_dset,
                                          description=baseline_block_description)
    else:
        logging.info('baselineBlock not found')

    # 5.4) Save the baseline mean and standard deviation scales
    if 'baselineSTD' in metadata_keys:
        logging.info('Converting baselineSTD')
        baseline_std_mat = metadata['baselineSTD'][:]
        baseline_std_scale = np.asarray([infile[baseline_std_mat[0, l]][:]
                                         for l in range(baseline_std_mat.size)]).squeeze(axis=1)
        baseline_std_description = 'The standard deviation value for the baseline'
        baseline_std_axis = 2
        baseline_std_unit = 'undefined'
        baseline_std_dset = 'baseline_std'
        brain_dataset.add_dimension_scale(scale=baseline_std_scale,
                                          unit=baseline_std_unit,
                                          axis=baseline_std_axis,
                                          dataset_name=baseline_std_dset,
                                          description=baseline_std_description)
    else:
        logging.info('baselineSTD not found')

    # 5.5) Save the baseline standard deviation data
    if 'baselineMu' in metadata_keys:
        logging.info('Converting baselineMu')
        baseline_mean_mat = metadata['baselineMu'][:]
        baseline_mean_scale = np.asarray([infile[baseline_mean_mat[0, l]][:]
                                          for l in range(baseline_mean_mat.size)]).squeeze(axis=1)
        baseline_mean_description = 'The mean value for the baseline'
        baseline_mean_axis = 2
        baseline_mean_unit = 'undefined'
        baseline_mean_dset = 'baseline_mean'
        brain_dataset.add_dimension_scale(scale=baseline_mean_scale,
                                          unit=baseline_mean_unit,
                                          axis=baseline_mean_axis,
                                          dataset_name=baseline_mean_dset,
                                          description=baseline_mean_description)
    else:
        logging.info('baselineMu not found')

    # 5.6) Save the absolute start time
    if 'AbsStart' in metadata_keys:
        logging.info('Converting AbsStart')
        #  Load the token absolute start time
        token_abs_start_description = 'Actual time in ns when the token started in the raw data' + \
                                      'See also the block dataset for further interpretation.'
        token_abs_start_axis = 2
        token_abs_start_unit = 'ms'
        token_abs_start_dset = 'token_abs_start_time'
        token_abs_start_scale = metadata['AbsStart'][0, :]   # In ms
        brain_dataset.add_dimension_scale(scale=token_abs_start_scale,
                                          unit=token_abs_start_unit,
                                          axis=token_abs_start_axis,
                                          dataset_name=token_abs_start_dset,
                                          description=token_abs_start_description)
    else:
        logging.info('AbsStart not found')

    # 5.7) Save the absolute stop time
    if 'AbsStop' in metadata_keys:
        logging.info('Converting AbsStop')
        #  Load the token absolute start time
        token_abs_stop_description = 'Actual time in ns when the token started in the raw data.' + \
                                     'See also the block dataset for further interpretation.'
        token_abs_stop_axis = 2
        token_abs_stop_unit = 'ms'
        token_abs_stop_dset = 'token_abs_stop_time'
        token_abs_stop_scale = metadata['AbsStop'][0, :]  # In ms
        brain_dataset.add_dimension_scale(scale=token_abs_stop_scale,
                                          unit=token_abs_stop_unit,
                                          axis=token_abs_stop_axis,
                                          dataset_name=token_abs_stop_dset,
                                          description=token_abs_stop_description)
    else:
        logging.info('AbsStop not found')

    if 'block' in metadata_keys:
        logging.info('Converting block')
        # Load and convert the block name data. Here the stings are converted differently
        # because the last value is actually an integer ID rather than the integer unit code (go figure)
        block_name_data = metadata['block'][:]
        dtrans = [infile[block_name_data[0, l]][:] for l in range(block_name_data.size)]
        strans = [''] * len(dtrans)
        for i, v in enumerate(dtrans):
            for j in v[:-1]:
                strans[i] += str(unichr(j))
            strans[i] += str(v[-1][0])
        block_name_scale = np.asarray(strans)
        block_name_axis = 2
        block_name_unit = 'block name'
        block_name_dset = 'block'
        block_name_description = 'The name of the block/file the token came from. Required for proper ' + \
                                 'interpretation fo the block start and stop times.'
        brain_dataset.add_dimension_scale(scale=block_name_scale,
                                          unit=block_name_unit,
                                          axis=block_name_axis,
                                          dataset_name=block_name_dset,
                                          description=block_name_description)
    else:
        logging.info('block not found')

def parse_arguments():
    """
    Get a dict with the user arguments.
    """
    # Setup the parser and define the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',
                        help='The input .mat file to be converted',
                        required=True,
                        dest='input_filename')
    parser.add_argument('-o', '--output',
                        help='The output .h5 BrainFormat file to be generated',
                        required=True,
                        dest='output_filename')
    parser.add_argument('-a', '--anatomy',
                        help='The optional .mat file with the anatomy data',
                        required=False,
                        dest='anatomy_filename')
    parser.add_argument('-l', '--log',
                        help='Specify the logging level to be used',
                        default='WARNING',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        dest='log_level')
    parser.add_argument('-u', '--unit',
                        help='The units of the main datasets. (Volts (V))',
                        required=False,
                        default='Volts (V)',
                        dest='data_units')

    # Parse the arguments
    arguments = vars(parser.parse_args())

    # Setup logging
    log_level = arguments.pop('log_level')
    logging_level = 'DEBUG'
    if log_level == 'DEBUG':
        logging_level = logging.DEBUG
    elif log_level == 'INFO':
        logging_level = logging.INFO
    elif log_level == 'WARNING':
        logging_level = logging.WARNING
    elif log_level == 'ERROR':
        logging_level = logging.ERROR
    elif log_level == 'CRITICAL':
        logging_level = logging.CRITICAL
    logging.basicConfig(level=logging_level)
    logging.captureWarnings(True)

    # Return the argument dictionary
    return arguments


if __name__ == "__main__":

    np.set_printoptions(threshold=10)  # Make sure the command-line prints are brief
    argument_dict = parse_arguments()  # Parse the command-line parameters
    convert_segmented_file(**argument_dict)  # Convert the data

