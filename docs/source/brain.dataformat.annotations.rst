:mod:`annotations` Package
==========================

.. automodule:: brain.dataformat.annotations
    :members:
    :undoc-members:
    :show-inheritance:

.. rubric:: Modules

.. autosummary::

    brain.dataformat.annotations.selection
    brain.dataformat.annotations.annotation
    brain.dataformat.annotations.collection

.. rubric:: Annotation Classes

The basic concept for using annotations are as follows. We have `DataSelections` to describe
a particular subset of a given data object (e.g, h5py.Dataset, numpy array, or any other kind
of data that supports `.shape` and `h5py.Dataset` slicing). An `Annotation` consists of a
`type`, `description`, and `selection` describing a particular data subset. An `AnnotationCollection`
then describes collection annotations and is used to query and manage many annotations. Finally the
`AnnotationDataGroup` describes the interface for storing and retrieving  `AnnotationCollections`
from/to HDF5. The :mod:`brain.dataformat.annotation` module provides the following
classes for definition, management, interaction, and storage of data annotations, which implement
these concepts:

.. autosummary::

    brain.dataformat.annotations.selection.DataSelection
    brain.dataformat.annotations.annotation.Annotation
    brain.dataformat.annotations.collection.AnnotationCollection
    brain.dataformat.annotations.collection.AnnotationDataGroup


:mod:`selection` Module
------------------------

.. automodule:: brain.dataformat.annotations.selection
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members:

:mod:`annotation` Module
------------------------

.. automodule:: brain.dataformat.annotations.annotation
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members:

:mod:`collection` Module
------------------------

.. automodule:: brain.dataformat.annotations.collection
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members: