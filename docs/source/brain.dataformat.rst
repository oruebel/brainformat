:mod:`dataformat` Package
=========================

.. automodule:: brain.dataformat
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::
    :maxdepth: 6

    brain.dataformat.annotations


.. rubric:: Modules

.. autosummary::

    brain.dataformat.base
    brain.dataformat.brainformat
    brain.dataformat.annotation
    brain.dataformat.spec

.. rubric:: File Format Base Classes

.. autosummary::

    brain.dataformat.base.ManagedObject
    brain.dataformat.base.ManagedGroup
    brain.dataformat.base.ManagedDataset
    brain.dataformat.base.ManagedFile
    brain.dataformat.base.RelationshipAttribute

.. rubric:: BRAIN File Format Classes

.. autosummary::

    brain.dataformat.brainformat.BrainDataFile
    brain.dataformat.brainformat.BrainDataData
    brain.dataformat.brainformat.BrainDataInternalData
    brain.dataformat.brainformat.BrainDataExternalData
    brain.dataformat.brainformat.BrainDataDescriptors
    brain.dataformat.brainformat.BrainDataStaticDescriptors
    brain.dataformat.brainformat.BrainDataDynamicDescriptors
    brain.dataformat.brainformat.BrainDataEphys
    brain.dataformat.brainformat.BrainDataEphysProcessed

.. rubric:: File Format Specification Helper Classes

.. autosummary::

    brain.dataformat.spec.BaseSpec
    brain.dataformat.spec.FormatDocument
    brain.dataformat.spec.DatasetSpec
    brain.dataformat.spec.GroupSpec
    brain.dataformat.spec.FileSpec
    brain.dataformat.spec.ManagedSpec
    brain.dataformat.spec.AttributeSpec
    brain.dataformat.spec.DimensionSpec
    brain.dataformat.spec.RelationshipSpec
    brain.dataformat.spec.RelationshipTargetSpec
    brain.dataformat.spec.APIGenerator


:mod:`base` Module
------------------

.. automodule:: brain.dataformat.base
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members:

:mod:`braindata` Module
-----------------------

.. automodule:: brain.dataformat.brainformat
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members:


:mod:`spec` Module
-----------------------

.. automodule:: brain.dataformat.spec
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members: