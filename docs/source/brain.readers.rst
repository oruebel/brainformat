:mod:`readers` Package
======================

.. automodule:: brain.readers
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`htkcollection` Module
---------------------------

.. automodule:: brain.readers.htkcollection
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members:

:mod:`htkfile` Module
---------------------

.. automodule:: brain.readers.htkfile
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members:
