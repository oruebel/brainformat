:mod:`brain` Package
====================

.. automodule:: brain.__init__
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::
    :maxdepth: 6

    brain.dataformat
    brain.readers
    brain.tools


