:mod:`tools` Package
====================

.. automodule:: brain.tools
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`convert_braindata` Module
-------------------------------

.. automodule:: brain.tools.convert_braindata
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
    :special-members:

