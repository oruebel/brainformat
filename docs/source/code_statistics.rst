Source Code Analysis: Report
============================

**Last Update:** 'Thu Oct 31 1:22:32 2014'

**Command** `pylint --max-line-length=120 brain/` : 2365 statements analysed.  (Version: pylint 1.3.1)



Global evaluation
-----------------
Your code has been rated at **9.47/10**

External dependencies
---------------------
::

    h5py (brain.dataformat.base,brain.dataformat.brainformat,brain.dataformat.annotation)
    numpy (brain.readers.htkcollection,brain.readers.htkfile,brain.dataformat.brainformat,brain.dataformat.annotation)
    scipy
      \-io (brain.readers.htkcollection)


Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |2843   |49.60 |2843     |=          |
+----------+-------+------+---------+-----------+
|docstring |2193   |38.26 |2193     |=          |
+----------+-------+------+---------+-----------+
|comment   |224    |3.91  |224      |=          |
+----------+-------+------+---------+-----------+
|empty     |472    |8.23  |472      |=          |
+----------+-------+------+---------+-----------+


Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |10     |10         |=          |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|class    |23     |23         |=          |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|method   |200    |200        |=          |100.00      |5.50     |
+---------+-------+-----------+-----------+------------+---------+
|function |1      |1          |=          |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+


Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |22    |22       |=          |
+-------------------------+------+---------+-----------+
|percent duplicated lines |0.377 |0.377    |=          |
+-------------------------+------+---------+-----------+


Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |31     |31       |=          |
+-----------+-------+---------+-----------+
|refactor   |62     |62       |=          |
+-----------+-------+---------+-----------+
|warning    |22     |22       |=          |
+-----------+-------+---------+-----------+
|error      |2      |2        |=          |
+-----------+-------+---------+-----------+



% errors / warnings by module
-----------------------------

+------------------------------+-------+--------+---------+-----------+
|module                        |error  |warning |refactor |convention |
+==============================+=======+========+=========+===========+
|brain.readers.htkfile         |100.00 |4.55    |3.23     |22.58      |
+------------------------------+-------+--------+---------+-----------+
|brain.dataformat.base         |0.00   |54.55   |32.26    |45.16      |
+------------------------------+-------+--------+---------+-----------+
|brain.dataformat.annotation   |0.00   |22.73   |17.74    |6.45       |
+------------------------------+-------+--------+---------+-----------+
|brain.dataformat.brainformat  |0.00   |13.64   |33.87    |3.23       |
+------------------------------+-------+--------+---------+-----------+
|brain.tools.convert_braindata |0.00   |4.55    |9.68     |12.90      |
+------------------------------+-------+--------+---------+-----------+



Messages
--------

+-----------------------------+------------+
|message id                   |occurrences |
+=============================+============+
|invalid-name                 |26          |
+-----------------------------+------------+
|too-many-public-methods      |16          |
+-----------------------------+------------+
|too-many-branches            |16          |
+-----------------------------+------------+
|too-many-statements          |10          |
+-----------------------------+------------+
|too-many-locals              |7           |
+-----------------------------+------------+
|too-many-arguments           |7           |
+-----------------------------+------------+
|abstract-method              |6           |
+-----------------------------+------------+
|fixme                        |4           |
+-----------------------------+------------+
|too-many-lines               |3           |
+-----------------------------+------------+
|star-args                    |3           |
+-----------------------------+------------+
|arguments-differ             |3           |
+-----------------------------+------------+
|unused-variable              |2           |
+-----------------------------+------------+
|too-many-instance-attributes |2           |
+-----------------------------+------------+
|too-few-public-methods       |2           |
+-----------------------------+------------+
|maybe-no-member              |2           |
+-----------------------------+------------+
|line-too-long                |2           |
+-----------------------------+------------+
|bare-except                  |2           |
+-----------------------------+------------+
|unpacking-non-sequence       |1           |
+-----------------------------+------------+
|too-many-return-statements   |1           |
+-----------------------------+------------+
|duplicate-code               |1           |
+-----------------------------+------------+
|bad-builtin                  |1           |
+-----------------------------+------------+
