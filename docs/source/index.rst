.. Brain Format documentation master file, created by
   sphinx-quickstart on Tue Jul 22 16:09:25 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Brain Format's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 7

   modules
   format_spec
   code_statistics
   licence

ToDo List:
==========
.. todolist::


Please use the online issue tracker to view and report bugs, enhancements, and proposals.
The issue tracker is available at `https://bitbucket.org/oruebel/brainformat/issues <https://bitbucket.org/oruebel/brainformat/issues>`_.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


