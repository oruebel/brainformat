from setuptools import setup, find_packages
from os import path

current_file_path = path.abspath(path.dirname(__file__))

description_short = 'The LBNL BrainFormat library specifies and implements a novel file format for management ' +\
                    'and storage of neuro-science data. The library provides a number of core modules that can ' +\
                    'be used for implementation and specification of scientific application formats in general. ' +\
                    'Based on these components, the library implements the LBNL BRAIN file format.'

description_long = description_short + \
                   'Important advantages and features of the format and library include: \n' + \
                   'Easy-to-use: User-friendly design and object-oriented python file API \n' + \
                   'Formal Specification: All components of the format have a formal specification which is part ' + \
                   'of the library as well as the files (JSON) \n' + \
                   'Verifiable: The library supports verification of format compliance of complete files and ' + \
                   'components of files \n' + \
                   'Modular: Managed objects allow semantic components of the format to be specified as ' + \
                   'self-contained units \n' + \
                   'Extensible: New components can be easily added to the format while different components ' + \
                   'can be designed independently \n' + \
                   'Reusable: Existing components can be nested, we can extend existing components through ' + \
                   'inheritance, and the library provides a number of base building blocks. \n' + \
                   'Data Annotation: Reusable modules for annotating data subsets are available which support ' + \
                   'searching, filtering, and merging of annotations and organization of annotations into ' + \
                   'collections. \n' + \
                   'Supports self-contained as well as modular file storage: All data can be stored in a single ' + \
                   'HDF5 file or individual managed object containers can be stored in separate files that can be ' + \
                   'accessed via external links directly from the main HDF5 file. \n' + \
                   'Application-independent design concepts & application-oriented modules: The library provides ' + \
                   'a number of core modules that can be used to define arbitrary, application file formats based ' + \
                   'on the concept of managed objects. Based on the concept of managed objects the library then ' + \
                   'defines the  application-oriented BRAIN file format.\n' + \
                   'Portable, Scalable, and Self-describing: Build on top of HDF5 using best practices.\n' + \
                   'Detailed developer and user documentation\n' + \
                   'Open Source\n'

setup(
    name='BrainFormat',
    version='0.1.1a',
    description=description_short,
    long_description=description_long,
    # The project's main homepage.
    url='https://bitbucket.org/oruebel/brainformat',

    # Author details
    author='Oliver Ruebel',
    author_email='oruebel@lbl.gov',

    # Choose your license
    license='BSD with extensions. See licence.txt for details.',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
    ],

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=['docs', '*.md', '*.txt', 'modulefiles']),

    # List run-time dependencies here.  These will be installed by pip when your
    # project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/technical.html#install-requires-vs-requirements-files
    install_requires=['h5py>=2.1', 'numpy'],

    # List additional groups of dependencies here (e.g. development dependencies).
    # You can install these using the following syntax, for example:
    # $ pip install -e .[dev,test]
    extras_require = {
        'HTK': ['scipy'],
    },
)